using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class WebForm1 : System.Web.UI.Page
	{
		protected System.Data.SqlClient.SqlCommand sqlSelectCommand1;
		protected System.Data.SqlClient.SqlCommand sqlInsertCommand1;
		protected System.Data.SqlClient.SqlConnection sqlConnection1;
		protected System.Data.SqlClient.SqlDataAdapter sqlDataAdapter1;
		protected System.Data.Odbc.OdbcCommand odbcSelectCommand1;
		protected System.Data.Odbc.OdbcConnection odbcConnection1;
		protected System.Data.Odbc.OdbcDataAdapter odbcDataAdapter1;
		protected memoryAdmin.DataSet2 dataSet21;
		protected memoryAdmin.DataSet3 dataSet31;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			odbcDataAdapter1.Fill(dataSet21);
			DataGrid1.DataSource = this.dataSet21;
			DataGrid1.DataBind();

//			sqlDataAdapter1.Fill(dataSet31);
//			DataGrid2.DataSource = this.dataSet31;
//			DataGrid2.DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
			this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
			this.sqlInsertCommand1 = new System.Data.SqlClient.SqlCommand();
			this.sqlDataAdapter1 = new System.Data.SqlClient.SqlDataAdapter();
			this.dataSet31 = new memoryAdmin.DataSet3();
			this.odbcSelectCommand1 = new System.Data.Odbc.OdbcCommand();
			this.odbcConnection1 = new System.Data.Odbc.OdbcConnection();
			this.odbcDataAdapter1 = new System.Data.Odbc.OdbcDataAdapter();
			this.dataSet21 = new memoryAdmin.DataSet2();
			((System.ComponentModel.ISupportInitialize)(this.dataSet31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet21)).BeginInit();
			// 
			// sqlSelectCommand1
			// 
			this.sqlSelectCommand1.CommandText = "SELECT User_name, Password, Sec_ques, Answer, First_name, Last_name, Email, Phone" +
				", Fax, Address1, Address2, City, State, Zip, Check_pay, Tax_id, Tax_id_type, Tax" +
				"_class_id, Active, Submit_date, Update_date, Submit_Confirm, Payment FROM Affili" +
				"ate";
			this.sqlSelectCommand1.Connection = this.sqlConnection1;
			// 
			// sqlConnection1
			// 
			this.sqlConnection1.ConnectionString = "workstation id=DELL;packet size=4096;user id=sa;pwd=sc_sql_sa0103;data source=WEB" +
				"_SERVER_1;persist security info=False;initial catalog=MemoryUp";
			// 
			// sqlInsertCommand1
			// 
			this.sqlInsertCommand1.CommandText = @"INSERT INTO Affiliate(User_name, Password, Sec_ques, Answer, First_name, Last_name, Email, Phone, Fax, Address1, Address2, City, State, Zip, Check_pay, Tax_id, Tax_id_type, Tax_class_id, Active, Submit_date, Update_date, Submit_Confirm, Payment) VALUES (@User_name, @Password, @Sec_ques, @Answer, @First_name, @Last_name, @Email, @Phone, @Fax, @Address1, @Address2, @City, @State, @Zip, @Check_pay, @Tax_id, @Tax_id_type, @Tax_class_id, @Active, @Submit_date, @Update_date, @Submit_Confirm, @Payment); SELECT User_name, Password, Sec_ques, Answer, First_name, Last_name, Email, Phone, Fax, Address1, Address2, City, State, Zip, Check_pay, Tax_id, Tax_id_type, Tax_class_id, Active, Submit_date, Update_date, Submit_Confirm, Payment FROM Affiliate";
			this.sqlInsertCommand1.Connection = this.sqlConnection1;
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@User_name", System.Data.SqlDbType.VarChar, 15, "User_name"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Password", System.Data.SqlDbType.VarChar, 30, "Password"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Sec_ques", System.Data.SqlDbType.Int, 4, "Sec_ques"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Answer", System.Data.SqlDbType.VarChar, 50, "Answer"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@First_name", System.Data.SqlDbType.VarChar, 25, "First_name"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Last_name", System.Data.SqlDbType.VarChar, 25, "Last_name"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 25, "Email"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Phone", System.Data.SqlDbType.VarChar, 15, "Phone"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Fax", System.Data.SqlDbType.VarChar, 15, "Fax"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Address1", System.Data.SqlDbType.VarChar, 50, "Address1"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Address2", System.Data.SqlDbType.VarChar, 50, "Address2"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@City", System.Data.SqlDbType.VarChar, 25, "City"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@State", System.Data.SqlDbType.VarChar, 2, "State"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Zip", System.Data.SqlDbType.VarChar, 10, "Zip"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Check_pay", System.Data.SqlDbType.VarChar, 50, "Check_pay"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Tax_id", System.Data.SqlDbType.VarChar, 15, "Tax_id"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Tax_id_type", System.Data.SqlDbType.VarChar, 5, "Tax_id_type"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Tax_class_id", System.Data.SqlDbType.VarChar, 5, "Tax_class_id"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Active", System.Data.SqlDbType.Bit, 1, "Active"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Submit_date", System.Data.SqlDbType.DateTime, 8, "Submit_date"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Update_date", System.Data.SqlDbType.DateTime, 8, "Update_date"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Submit_Confirm", System.Data.SqlDbType.Bit, 1, "Submit_Confirm"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Payment", System.Data.SqlDbType.VarChar, 20, "Payment"));
			// 
			// sqlDataAdapter1
			// 
			this.sqlDataAdapter1.InsertCommand = this.sqlInsertCommand1;
			this.sqlDataAdapter1.SelectCommand = this.sqlSelectCommand1;
			this.sqlDataAdapter1.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																									  new System.Data.Common.DataTableMapping("Table", "Affiliate", new System.Data.Common.DataColumnMapping[] {
																																																				   new System.Data.Common.DataColumnMapping("User_name", "User_name"),
																																																				   new System.Data.Common.DataColumnMapping("Password", "Password"),
																																																				   new System.Data.Common.DataColumnMapping("Sec_ques", "Sec_ques"),
																																																				   new System.Data.Common.DataColumnMapping("Answer", "Answer"),
																																																				   new System.Data.Common.DataColumnMapping("First_name", "First_name"),
																																																				   new System.Data.Common.DataColumnMapping("Last_name", "Last_name"),
																																																				   new System.Data.Common.DataColumnMapping("Email", "Email"),
																																																				   new System.Data.Common.DataColumnMapping("Phone", "Phone"),
																																																				   new System.Data.Common.DataColumnMapping("Fax", "Fax"),
																																																				   new System.Data.Common.DataColumnMapping("Address1", "Address1"),
																																																				   new System.Data.Common.DataColumnMapping("Address2", "Address2"),
																																																				   new System.Data.Common.DataColumnMapping("City", "City"),
																																																				   new System.Data.Common.DataColumnMapping("State", "State"),
																																																				   new System.Data.Common.DataColumnMapping("Zip", "Zip"),
																																																				   new System.Data.Common.DataColumnMapping("Check_pay", "Check_pay"),
																																																				   new System.Data.Common.DataColumnMapping("Tax_id", "Tax_id"),
																																																				   new System.Data.Common.DataColumnMapping("Tax_id_type", "Tax_id_type"),
																																																				   new System.Data.Common.DataColumnMapping("Tax_class_id", "Tax_class_id"),
																																																				   new System.Data.Common.DataColumnMapping("Active", "Active"),
																																																				   new System.Data.Common.DataColumnMapping("Submit_date", "Submit_date"),
																																																				   new System.Data.Common.DataColumnMapping("Update_date", "Update_date"),
																																																				   new System.Data.Common.DataColumnMapping("Submit_Confirm", "Submit_Confirm"),
																																																				   new System.Data.Common.DataColumnMapping("Payment", "Payment")})});
			// 
			// dataSet31
			// 
			this.dataSet31.DataSetName = "DataSet3";
			this.dataSet31.Locale = new System.Globalization.CultureInfo("en-US");
			// 
			// odbcSelectCommand1
			// 
			this.odbcSelectCommand1.CommandText = "select \"products\".ID, \"products\".CODE, \"products\".PRICE from \"products\"";
			this.odbcSelectCommand1.Connection = this.odbcConnection1;
			// 
			// odbcConnection1
			// 
			this.odbcConnection1.ConnectionString = "MaxBufferSize=2048;FIL=dBase III;DSN=Miva_Products;PageTimeout=600;DefaultDir=C:\\" +
				";DBQ=C:\\;DriverId=21";
			// 
			// odbcDataAdapter1
			// 
			this.odbcDataAdapter1.SelectCommand = this.odbcSelectCommand1;
			// 
			// dataSet21
			// 
			this.dataSet21.DataSetName = "DataSet2";
			this.dataSet21.Locale = new System.Globalization.CultureInfo("en-US");
			((System.ComponentModel.ISupportInitialize)(this.dataSet31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSet21)).EndInit();

		}
		#endregion
	}
}
