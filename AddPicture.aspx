<%@ Page language="c#" Inherits="memoryAdmin.AddPicture" CodeFile="AddPicture.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AddPicture</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" method="post" encType="multipart/form-data" runat="server">
			<table id="onUpload" cellPadding="4" width="408" bgColor="#5500ee" border="0" runat="server">
				<tr>
					<td align="center">
						<table cellSpacing="0" cellPadding="5" width="400" bgColor="#e0e0e0" border="0">
							<tr>
								<td colSpan="3"><br>
									<font face="arial" size="2"><B>Picture&nbsp;Upload: (
											<asp:label id="lblType" runat="server"></asp:label>&nbsp;Model&nbsp;Id#</B></font>
									<asp:label id="lblSystemId" runat="server" Font-Bold="True" Font-Size="X-Small" Font-Names="Arial"></asp:label><font face="arial" size="2"><B>)</B></font></td>
							</tr>
							<tr>
								<td colSpan="3"><font face="arial" size="2"><B>1.</B> Click on Browe to choose 
										a&nbsp;picture from your disk. </font>
								</td>
							</tr>
							<tr>
								<td width="10%">&nbsp;</td>
								<td><font face="arial" size="2"><B>File:</B></font></td>
								<td><input id="fleAddPicture" type="file" runat="server">
								</td>
							</tr>
							<tr>
								<td colSpan="3">&nbsp;
									<asp:label id="lblNoFileName" runat="server" ForeColor="Red" Visible="False">Error: You must enter a file name</asp:label></td>
							</tr>
							<tr>
								<td colSpan="3"><font face="arial" size="2"><B>2.</B> After selecting a picture, click 
										on&nbsp;Upload to save on server. </font>
								</td>
							</tr>
							<tr>
								<td width="10%">&nbsp;</td>
								<td>&nbsp;</td>
								<td><asp:button id="btnSend" runat="server" Text="Upload" onclick="btnSend_Click"></asp:button>&nbsp;&nbsp; <input class="button" id="btnCancel" onclick="window.close('this')" type="button" value="Cancel">
								</td>
							</tr>
							<tr>
								<td colSpan="3">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table id="afterUpload" cellPadding="4" width="408" bgColor="#5500ee" border="0" runat="server">
				<tr>
					<td align="center">
						<table cellSpacing="0" cellPadding="5" width="400" bgColor="#e0e0e0" border="0">
							<tr>
								<td><asp:label id="lblAfterUpload" runat="server"></asp:label>&nbsp;<INPUT onclick="window.close();" type="button" value="Close Window">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
