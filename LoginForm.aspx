<%@ Page language="c#" Inherits="web.LoginForm" CodeFile="LoginForm.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>LoginForm</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body onload="LoginForm.UserName.focus();">
		<form method="post" runat="server" id="formsAuth">
		    <asp:Panel ID="pnlLogin" runat="server">
			<TABLE class="item" id="Table1" cellSpacing="2" cellPadding="5" width="750" border="0">
				<TR>
					<TD width="50">&nbsp;</TD>
					<TD width="150">&nbsp;</TD>
					<TD width="200">&nbsp;</TD>
					<TD width="350">&nbsp;</TD>
				</TR>
				<TR>
					<TD>&nbsp;</TD>
					<TD>&nbsp;</TD>
					<TD>&nbsp;</TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR>
					<TD></TD>
					<TD>User Name:</TD>
					<TD><asp:textbox id="UserName" runat="server" ValidationGroup="grpLogin"></asp:textbox></TD>
					<TD>
                        <asp:RequiredFieldValidator ID="reqUserName" runat="server" ErrorMessage="Please enter user name!" Display="Dynamic" ControlToValidate="UserName" ValidationGroup="grpLogin"></asp:RequiredFieldValidator></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD>Password:</TD>
					<TD><asp:textbox id="Password" runat="server" TextMode="Password" ValidationGroup="grpLogin"></asp:textbox></TD>
					<TD>
                        <asp:RequiredFieldValidator ID="reqPassword" runat="server" ErrorMessage="Please enter password!" ControlToValidate="Password" Display="Dynamic" ValidationGroup="grpLogin"></asp:RequiredFieldValidator></TD>
				</TR>
                <tr>
                    <td>
                    </td>
                    <td colspan="2">
                        <asp:CheckBox ID="chkRemenberPass" runat="server" Text="Remember Password" /></td>
                    <td>
                    </td>
                </tr>
				<TR>
					<TD></TD>
					<TD colSpan="2"><asp:label id="Message" runat="server" ForeColor="Red"></asp:label>&nbsp;</TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD><asp:button id="LoginButton" runat="server" Text="Login" onclick="LoginButtonClick" ValidationGroup="grpLogin"></asp:button></TD>
					<TD><INPUT type="reset" value="Cancel"></TD>
					<TD>
                        <asp:Button ID="btnChangePassword" runat="server" OnClick="btnChangePassword_Click"
                            Text="Change Password" /></TD>
				</TR>
			</TABLE>
			</asp:Panel>
            <asp:Panel ID="pnlChangePass" runat="server" Visible="false">
                <table class="item" cellSpacing="2" cellPadding="5" width="750" border="0">
                    <tr>
                        <TD width="50">&nbsp;</TD>
					<TD width="150">&nbsp;</TD>
					<TD width="200">&nbsp;</TD>
					<TD width="350">&nbsp;</TD>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            User Name:</td>
                        <td>
                            <asp:TextBox ID="txtPassChangeUserName" runat="server" ValidationGroup="grpPassChange"></asp:TextBox></td>
                        <td>
                            <asp:RequiredFieldValidator ID="reqPassChangeUserName" runat="server" ErrorMessage="Please enter user name!" ValidationGroup="grpPassChange" ControlToValidate="txtPassChangeUserName" Display="Dynamic"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            Old Password:</td>
                        <td>
                            <asp:TextBox ID="txtPassChangeOldPass" runat="server" TextMode="Password" ValidationGroup="grpPassChange"></asp:TextBox></td>
                        <td>
                            <asp:RequiredFieldValidator ID="reqPassChangeOldPass" runat="server" ErrorMessage="Please enter your original password!" ValidationGroup="grpPassChange" ControlToValidate="txtPassChangeOldPass" Display="Dynamic"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td colspan="3">
                            <asp:Label ID="lblPassChangeError" runat="server" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            New Password:</td>
                        <td>
                            <asp:TextBox ID="txtPassChangeNewPass" runat="server" TextMode="Password" ValidationGroup="grpPassChange"></asp:TextBox></td>
                        <td>
                            <asp:RequiredFieldValidator ID="reqPassChangeNewPass" runat="server" ErrorMessage="Please enter your new password!" ValidationGroup="grpPassChange" ControlToValidate="txtPassChangeNewPass" Display="Dynamic"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            Confirm Password:</td>
                        <td>
                            <asp:TextBox ID="txtPassChangeConfirmPass" runat="server" TextMode="Password" ValidationGroup="grpPassChange"></asp:TextBox></td>
                        <td>
                            <asp:RequiredFieldValidator ID="reqPassChangeConfirm" runat="server" ErrorMessage="Please enter the password as the same above!" ValidationGroup="grpPassChange" ControlToValidate="txtPassChangeConfirmPass" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cmpPassword" runat="server" ControlToCompare="txtPassChangeNewPass"
                                ControlToValidate="txtPassChangeConfirmPass" Display="Dynamic" ErrorMessage="Please enter the same password on both fields!"
                                ValidationGroup="grpPassChange"></asp:CompareValidator></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnPassChangeConfirm" runat="server" Text="Confirm" OnClick="btnPassChangeConfirm_Click" ValidationGroup="grpPassChange" /></td>
                        <td>
                            <asp:Button ID="btnPassChangeCancel" runat="server" Text="Cancel" OnClick="btnPassChangeCancel_Click" ValidationGroup="grpPassChange" /></td>
                        <td>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            &nbsp;
		</form>
	</body>
</HTML>
