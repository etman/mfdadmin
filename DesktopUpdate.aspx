<%@ Page language="c#" Inherits="memoryAdmin.DesktopUpdate" CodeFile="DesktopUpdate.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DesktopUpdate</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
		<script>
			function openNewModel()
			{
				var strReturn = new Array('', '', '');
				
				var winSetting = 'status:no;dialogWidth:500px;dialogHeight:220px;dialogHide:true;help:no;scroll:no';
				strReturn = window.showModalDialog('newModel.aspx?type=desktop', null, winSetting);
				if (strReturn != null)
				{
					document.getElementById('txtManufacture').value = strReturn[0].toString();
					document.getElementById('txtProductLine').value = strReturn[1].toString();
					document.getElementById('txtModel').value = strReturn[2].toString();
					document.getElementById('txtReturnCancel').value = '0';
				}
				else
				{
					document.getElementById('txtReturnCancel').value = '1';
				}
			}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="1" width="780" border="0">
				<TR>
					<TD class="title"><STRONG><asp:imagebutton id="imgHome" runat="server" style="width:90px; height:40px;" ImageUrl="/images/memoryselector/bd_fb.png"></asp:imagebutton>
                        Desktop Memory Administration Page</STRONG></TD>
				</TR>
				<TR>
					<TD><asp:panel id="selector" runat="server">
						<TABLE class="item" id="Table2" cellSpacing="0" cellPadding="3" width="100%" border="0">
							<TR>
								<TD style="HEIGHT: 24px" width="300" colSpan="2">
									<asp:Label id="Label11" runat="server" Width="280px">Select System...</asp:Label></TD>
								<TD style="HEIGHT: 24px">
                                    User:
                                    <asp:Label ID="lblUser" runat="server"></asp:Label></TD>
							</TR>
							<TR>
								<TD align="right">
									<asp:Label id="Label12" runat="server">Select Desktop Manufacturer: </asp:Label></TD>
								<TD width="300">
									<asp:DropDownList id="lstManufacture" runat="server" AutoPostBack="True" Width="300px"
                                        onselectedindexchanged="lstManufacture_SelectedIndexChanged"></asp:DropDownList>
									<asp:Label id="hdnManufacture" runat="server" Visible="False"></asp:Label></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD align="right">
									<asp:Label id="Label8" runat="server">Select Desktop Product Line: </asp:Label></TD>
								<TD width="300">
									<asp:DropDownList id="lstProductLine" runat="server" AutoPostBack="True" Width="300px"
                                        onselectedindexchanged="lstProductLine_SelectedIndexChanged"></asp:DropDownList>
									<asp:Label id="hdnProductLine" runat="server" Visible="False"></asp:Label></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD align="right">
									<asp:Label id="Label13" runat="server">Select Desktop Model: </asp:Label></TD>
								<TD width="300">
									<asp:DropDownList id="lstModel" runat="server" AutoPostBack="True" Width="300px"
                                        onselectedindexchanged="lstModel_SelectedIndexChanged"></asp:DropDownList>
									<asp:Label id="hdnModel" runat="server" Visible="False"></asp:Label></TD>
								<TD>
									<asp:Button id="btnAddModel" runat="server" Text="Add New" onclick="btnAddModel_Click"></asp:Button></TD>
							</TR>
						</TABLE>
					</asp:panel>
					</TD>
				</TR>
				<TR>
					<TD><asp:panel id="SystemInformation" runat="server" Visible="False">
							<TABLE class="item" cellSpacing="1" cellPadding="3" width="100%" border="0">
								<TR>
									<TD colSpan="5">
										<HR width="100%" SIZE="1">
										<asp:TextBox id="txtDesktopID" runat="server" Visible="False"></asp:TextBox>
										<asp:Label id="lblModelExist" runat="server" Visible="False" ForeColor="Red">This model already exist!</asp:Label>
										<asp:Label id="lblViewed" runat="server"></asp:Label> Source: 
                                        <asp:Label ID="lblSource" runat="server"></asp:Label></TD>
								</TR>
                                <tr>
                                    <td colspan="5">
                                        <asp:Button ID="btnUpdate2" runat="server" OnClick="btnUpdate2_Click" Text="Update" /></td>
                                </tr>
								<TR>
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>Manufacturer:&nbsp; </STRONG>
									</TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:TextBox id="txtManufacture" runat="server"></asp:TextBox>
										<asp:DropDownList id="lstCurrManuf" runat="server" AutoPostBack="True" Visible="False" Width="180px"
                                            onselectedindexchanged="lstCurrManuf_SelectedIndexChanged"></asp:DropDownList>
										<asp:Label id="lblCurrManuf" runat="server" Visible="False">Or</asp:Label>
										<asp:Button id="btnNewManuf" runat="server" Visible="False" Text="Add New" onclick="btnNewManuf_Click"></asp:Button>
										<asp:Label id="lblManufError" runat="server" Visible="False" ForeColor="Red">Please enter manufacturer!</asp:Label><FONT color="red"></FONT></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>Product Line:&nbsp; </STRONG>
									</TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:TextBox id="txtProductLine" runat="server"></asp:TextBox>
										<asp:DropDownList id="lstCurrLine" runat="server" Visible="False" Width="180px"></asp:DropDownList>
										<asp:Label id="lblCurrLine" runat="server" Visible="False">Or</asp:Label>
										<asp:Button id="btnNewLine" runat="server" Visible="False" Text="Add New" onclick="btnNewLine_Click"></asp:Button>
										<asp:Label id="lblProductLineError" runat="server" Visible="False" ForeColor="Red">Please enter product line!</asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>Model:&nbsp; </STRONG>
									</TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:TextBox id="txtModel" runat="server"></asp:TextBox>
										<asp:Label id="lblModelError" runat="server" Visible="False" ForeColor="Red">Please enter a model!</asp:Label>
										<asp:Label id="lblLastUpdate" runat="server"></asp:Label><br />
                                        Please note: "<span style="font-weight: bold; color: red">First word</span>" in
                                        the model is the most important in order to idendify this model. Please put unique
                                        model number on the first word. For example, M9969LL/A.</TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>Type:</STRONG>&nbsp;
									</TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:RadioButtonList id="radType" runat="server" AutoPostBack="True" RepeatColumns="4" CssClass="select"
											RepeatDirection="Horizontal" onselectedindexchanged="radType_SelectedIndexChanged">
                                            <asp:ListItem>EDO</asp:ListItem>
											<asp:ListItem Value="SD">SD</asp:ListItem>
											<asp:ListItem Value="SD SODIMM">SD SODIMM</asp:ListItem>
											<asp:ListItem Value="Rambus">Rambus</asp:ListItem>
											<asp:ListItem Value="DDR">DDR</asp:ListItem>
											<asp:ListItem Value="DDR SODIMM">DDR SODIMM</asp:ListItem>
											<asp:ListItem Value="DDR2">DDR2</asp:ListItem>
											<asp:ListItem Value="DDR2 SODIMM">DDR2 SODIMM</asp:ListItem>
                                            <asp:ListItem>DDR3</asp:ListItem>
                                            <asp:ListItem Value="DDR3 SODIMM"></asp:ListItem>
										    <asp:ListItem>DDR3 ECC SODIMMs</asp:ListItem>
                                            <asp:ListItem>DDR4</asp:ListItem>
                                            <asp:ListItem>DDR4 SODIMM</asp:ListItem>
                                            <asp:ListItem>DDR4 ECC SODIMMs</asp:ListItem>
                                            <asp:ListItem>DDR5</asp:ListItem>
                                            <asp:ListItem>DDR5 SODIMM</asp:ListItem>
                                            <asp:ListItem>DDR5 ECC SODIMMs</asp:ListItem>
										</asp:RadioButtonList>
										<asp:Label id="lblTypeError" runat="server" Visible="False" ForeColor="Red">Please select a memory type!</asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>Min. Speed:&nbsp; </STRONG>
									</TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:DropDownList id="radMinSpeed" CssClass="select" Runat="server">
											<asp:ListItem Value="0">0</asp:ListItem>
											<asp:ListItem Value="100">100</asp:ListItem>
											<asp:ListItem Value="133">133</asp:ListItem>
											<asp:ListItem Value="266">266</asp:ListItem>
											<asp:ListItem Value="333">333</asp:ListItem>
											<asp:ListItem Value="400">400</asp:ListItem>
											<asp:ListItem Value="433">433</asp:ListItem>
											<asp:ListItem Value="466">466</asp:ListItem>
											<asp:ListItem Value="500">500</asp:ListItem>
											<asp:ListItem Value="533">533</asp:ListItem>
											<asp:ListItem Value="550">550</asp:ListItem>
											<asp:ListItem Value="566">566</asp:ListItem>
											<asp:ListItem Value="800">800</asp:ListItem>
											<asp:ListItem Value="1066">1066</asp:ListItem>
                                            <asp:ListItem Value="1333">1333</asp:ListItem>
                                            <asp:ListItem Value="1600">1600</asp:ListItem>
                                            <asp:ListItem Value="1866">1866</asp:ListItem>
                                            <asp:ListItem Value="2133">2133</asp:ListItem>
                                            <asp:ListItem Value="2400">2400</asp:ListItem>
                                            <asp:ListItem Value="2666">2666</asp:ListItem>
                                            <asp:ListItem Value="2933">2933</asp:ListItem>
                                            <asp:ListItem Value="3200">3200</asp:ListItem>
                                            <asp:ListItem Value="4800">4800</asp:ListItem>
                                            <asp:ListItem Value="5200">5200</asp:ListItem>
                                            <asp:ListItem Value="5600">5600</asp:ListItem>
                                            <asp:ListItem Value="5600">6000</asp:ListItem>
                                            <asp:ListItem Value="5600">6400</asp:ListItem>
                                            <asp:ListItem Value="6800">6800</asp:ListItem>
                                            <asp:ListItem Value="7200">7200</asp:ListItem>
                                            <asp:ListItem Value="7800">7800</asp:ListItem>
                                            <asp:ListItem Value="8000">8000</asp:ListItem>
										</asp:DropDownList>
										<asp:Label id="lblMinSpeedError" runat="server" Visible="False" ForeColor="Red">Please select a min. memory speed!</asp:Label>
										<asp:TextBox id="txtMinSpeed" runat="server" Width="50px" Visible="False" ReadOnly="True"></asp:TextBox></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>Max. Speed:&nbsp; </STRONG>
									</TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:DropDownList id="radMaxSpeed" CssClass="select" Runat="server">
											<asp:ListItem Value="0">0</asp:ListItem>
											<asp:ListItem Value="100">100</asp:ListItem>
											<asp:ListItem Value="133">133</asp:ListItem>
											<asp:ListItem Value="266">266</asp:ListItem>
											<asp:ListItem Value="333">333</asp:ListItem>
											<asp:ListItem Value="400">400</asp:ListItem>
											<asp:ListItem Value="433">433</asp:ListItem>
											<asp:ListItem Value="466">466</asp:ListItem>
											<asp:ListItem Value="500">500</asp:ListItem>
											<asp:ListItem Value="533">533</asp:ListItem>
											<asp:ListItem Value="550">550</asp:ListItem>
											<asp:ListItem Value="566">566</asp:ListItem>
											<asp:ListItem Value="800">800</asp:ListItem>
											<asp:ListItem Value="1066">1066</asp:ListItem>
                                            <asp:ListItem Value="1333">1333</asp:ListItem>
                                            <asp:ListItem Value="1600">1600</asp:ListItem>
                                            <asp:ListItem Value="1866">1866</asp:ListItem>
                                            <asp:ListItem Value="2133">2133</asp:ListItem>
                                            <asp:ListItem Value="2400">2400</asp:ListItem>
                                            <asp:ListItem Value="2666">2666</asp:ListItem>
                                            <asp:ListItem Value="2933">2933</asp:ListItem>
                                            <asp:ListItem Value="3200">3200</asp:ListItem>
                                            <asp:ListItem Value="4800">4800</asp:ListItem>
                                            <asp:ListItem Value="5200">5200</asp:ListItem>
                                            <asp:ListItem Value="5600">5600</asp:ListItem>
                                            <asp:ListItem Value="6800">6800</asp:ListItem>
                                            <asp:ListItem Value="7200">7200</asp:ListItem>
                                            <asp:ListItem Value="7800">7800</asp:ListItem>
                                            <asp:ListItem Value="8000">8000</asp:ListItem>
										</asp:DropDownList>
										<asp:Label id="lblMaxSpeedError" runat="server" Visible="False" ForeColor="Red">Please select a max. memory speed!</asp:Label>
										<asp:TextBox id="txtMaxSpeed" runat="server" Width="50px" Visible="False" ReadOnly="True"></asp:TextBox></TD>
									<TD><FONT color="red"><FONT color="red">(Required)</FONT></FONT></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>Slot:&nbsp; </STRONG>
									</TD>
									<TD style="WIDTH: 488px; HEIGHT: 25px" colSpan="3">
										<asp:RadioButtonList id="radSlot" runat="server" CssClass="select" RepeatDirection="Horizontal">
											<asp:ListItem Value="1">1</asp:ListItem>
											<asp:ListItem Value="2">2</asp:ListItem>
											<asp:ListItem Value="3">3</asp:ListItem>
											<asp:ListItem Value="4">4</asp:ListItem>
											<asp:ListItem Value="5">5</asp:ListItem>
											<asp:ListItem Value="6">6</asp:ListItem>
											<asp:ListItem Value="8">8</asp:ListItem>
											<asp:ListItem Value="9">9</asp:ListItem>
											<asp:ListItem Value="10">10</asp:ListItem>
											<asp:ListItem Value="12">12</asp:ListItem>
											<asp:ListItem Value="16">16</asp:ListItem>
                                            <asp:ListItem>18</asp:ListItem>
                                            <asp:ListItem>24</asp:ListItem>
                                            <asp:ListItem Value="32">32</asp:ListItem>
                                            <asp:ListItem>48</asp:ListItem>
                                            <asp:ListItem Value="64">64</asp:ListItem>
                                            <asp:ListItem>128</asp:ListItem>
										</asp:RadioButtonList>
										<asp:Label id="lblSlotError" runat="server" Visible="False" ForeColor="Red">Please select number of slots!</asp:Label></TD>
									<TD style="HEIGHT: 25px"><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>Standard Size:&nbsp; </STRONG>
									</TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:DropDownList ID="drpStandard" runat="server" CssClass="select">
                                            <asp:ListItem Value="0">0</asp:ListItem>
											<asp:ListItem Value="32">32</asp:ListItem>
											<asp:ListItem Value="64">64</asp:ListItem>
											<asp:ListItem Value="128">128</asp:ListItem>
											<asp:ListItem Value="256">256</asp:ListItem>
											<asp:ListItem Value="512">512</asp:ListItem>
											<asp:ListItem Value="1024">1G</asp:ListItem>
											<asp:ListItem Value="2048">2G</asp:ListItem>
											<asp:ListItem Value="3072">3G</asp:ListItem>
											<asp:ListItem Value="4096">4G</asp:ListItem>
											<asp:ListItem Value="5120">5G</asp:ListItem>
											<asp:ListItem Value="6144">6G</asp:ListItem>
                                            <asp:ListItem Value="8192">8G</asp:ListItem>
                                            <asp:ListItem Value="12288">12G</asp:ListItem>
                                            <asp:ListItem Value="16384">16G</asp:ListItem>
                                            <asp:ListItem Value="24576">24G</asp:ListItem>
                                            <asp:ListItem Value="32768">32G</asp:ListItem>
                                            <asp:ListItem Value="49152">48G</asp:ListItem>
                                            <asp:ListItem Value="65536">64G</asp:ListItem>
                                            <asp:ListItem Value="131072">128G</asp:ListItem>
                                            <asp:ListItem Value="262144">256G</asp:ListItem>
                                            <asp:ListItem Value="524288">512G</asp:ListItem>
                                        </asp:DropDownList>
										<asp:Label id="lblStandardSizeError" runat="server" Visible="False" ForeColor="Red">Please select onboard standard size!</asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>Max. Total:&nbsp; </STRONG>
									</TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:TextBox id="txtMaxTotal" runat="server"></asp:TextBox>
										<asp:Label id="lblMaxTotalError" runat="server" Visible="False" ForeColor="Red">
                                            Please enter valid memory size!</asp:Label>
                                    <asp:DropDownList ID="drpMaxTotal" runat="server" CssClass="select" Width="50px">
                                            <asp:ListItem Value="0"></asp:ListItem>
											<asp:ListItem Value="MB">MB</asp:ListItem>
											<asp:ListItem Value="GB">GB</asp:ListItem>
											<asp:ListItem Value="TB">TB</asp:ListItem>
                                        </asp:DropDownList>
									</TD>
									<TD><FONT color="red">(Required)</FONT>(256, 512, 1024, 2048)</TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>Min. Per Slot:&nbsp; </STRONG>
									</TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:RadioButtonList id="radMinPerSlot" runat="server" CssClass="select" RepeatDirection="Horizontal">
											<asp:ListItem Value="32">32</asp:ListItem>
											<asp:ListItem Value="64">64</asp:ListItem>
											<asp:ListItem Value="128">128</asp:ListItem>
											<asp:ListItem Value="256">256</asp:ListItem>
											<asp:ListItem Value="512">512</asp:ListItem>
											<asp:ListItem Value="1024">1G</asp:ListItem>
											<asp:ListItem Value="2048">2G</asp:ListItem>
											<asp:ListItem Value="4096">4G</asp:ListItem>
											<asp:ListItem Value="8192">8G</asp:ListItem>
                                            <asp:ListItem Value="16384">16G</asp:ListItem>
                                            <asp:ListItem Value="32768">32G</asp:ListItem>
                                            <asp:ListItem Value="65536">64G</asp:ListItem>
                                            <asp:ListItem Value="131072">128G</asp:ListItem>
                                            <asp:ListItem Value="262144">256G</asp:ListItem>
                                            <asp:ListItem Value="524288">512G</asp:ListItem>
                                            <asp:ListItem Value="1048576">1T</asp:ListItem>
										</asp:RadioButtonList>
										<asp:Label id="lblMinPerSlotError" runat="server" Visible="False" ForeColor="Red">Please select min. memory size per slot!</asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>Max. Per Slot:&nbsp; </STRONG>
									</TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:RadioButtonList id="radMaxPerSlot" runat="server" CssClass="select" RepeatDirection="Horizontal">
											<asp:ListItem Value="32">32</asp:ListItem>
											<asp:ListItem Value="64">64</asp:ListItem>
											<asp:ListItem Value="128">128</asp:ListItem>
											<asp:ListItem Value="256">256</asp:ListItem>
											<asp:ListItem Value="512">512</asp:ListItem>
											<asp:ListItem Value="1024">1G</asp:ListItem>
											<asp:ListItem Value="2048">2G</asp:ListItem>
											<asp:ListItem Value="4096">4G</asp:ListItem>
											<asp:ListItem Value="8192">8G</asp:ListItem>
                                            <asp:ListItem Value="16384">16G</asp:ListItem>
                                            <asp:ListItem Value="32768">32G</asp:ListItem>
                                            <asp:ListItem Value="65536">64G</asp:ListItem>
                                            <asp:ListItem Value="131072">128G</asp:ListItem>
                                            <asp:ListItem Value="262144">256G</asp:ListItem>
                                            <asp:ListItem Value="524288">512G</asp:ListItem>
                                            <asp:ListItem Value="1048576">1T</asp:ListItem>
										</asp:RadioButtonList>
										<asp:Label id="lblMaxPerSlotError" runat="server" Visible="False" ForeColor="Red">Please select max. memory size per slot!</asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="#d3d3d3"><STRONG>Dual Channel:</STRONG></TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:CheckBox id="chkDualChannel" runat="server" Text="Yes" CssClass="select"></asp:CheckBox></TD>
									<TD></TD>
								</TR>
                                <tr>
                                    <td bgcolor="#d3d3d3"><STRONG>
                                        Tri. Channel:</STRONG></td>
                                    <td colspan="3">
                                        <asp:CheckBox ID="chkTriChannel" runat="server" Text="Yes" CssClass="select" /></td>
                                    <td>
                                    </td>
                                </tr>
								<TR>
									<TD style="WIDTH: 148px" bgColor="#d3d3d3"><STRONG>Run in Pair:</STRONG></TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:CheckBox id="chkRunInPair" runat="server" Text="Yes" CssClass="select"></asp:CheckBox></TD>
									<TD></TD>
								</TR>
                                <tr>
                                    <td bgcolor="#d3d3d3" style="width: 148px">
                                        <STRONG>Server:</STRONG></td>
                                    <td colspan="3" style="width: 488px">
                                        <asp:CheckBox ID="chkServer" runat="server" Text="Yes" /></td>
                                    <td>
                                    </td>
                                </tr>
								<TR>
									<TD style="WIDTH: 148px" bgColor="#d3d3d3"><STRONG>ECC:</STRONG>
									</TD>
									<TD style="WIDTH: 488px" colSpan="4" rowSpan="1">
										<asp:CheckBoxList id="chkECC" runat="server" CssClass="select" RepeatDirection="Horizontal" Width="618px">
											<asp:ListItem Value="Non" Selected="True">Non-ECC, Non-Registered</asp:ListItem>
											<asp:ListItem Value="ECC">ECC, Non-Registered</asp:ListItem>
											<asp:ListItem Value="Register">ECC, Registered</asp:ListItem>
											<asp:ListItem Value="Full Buffered">Fully Buffered</asp:ListItem>
                                            <asp:ListItem Value="Apple FB">Fully Buffered (Apple)</asp:ListItem>
										</asp:CheckBoxList></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="#d3d3d3"><STRONG>USB:</STRONG></TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:RadioButtonList id="radUsb" runat="server" CssClass="select" RepeatDirection="Horizontal">
											<asp:ListItem Value="None">No USB</asp:ListItem>
											<asp:ListItem Value="1.0">USB1.0</asp:ListItem>
											<asp:ListItem Value="2.0" Selected="True">USB2.0</asp:ListItem>
										    <asp:ListItem Value="3.0">USB3.0</asp:ListItem>
										    <asp:ListItem Value="TypeC">USB-C</asp:ListItem>
										</asp:RadioButtonList></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR style="display:none;">
									<TD style="WIDTH: 148px" bgColor="#d3d3d3"><STRONG>FireWire(IEEE1394):</STRONG></TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:RadioButtonList id="radFireWire" runat="server" CssClass="select" RepeatDirection="Horizontal">
											<asp:ListItem Value="None" Selected="True">No FireWire</asp:ListItem>
											<asp:ListItem Value="400">FireWire 400</asp:ListItem>
											<asp:ListItem Value="800">FireWire 800</asp:ListItem>
										</asp:RadioButtonList></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="#d3d3d3"><STRONG>Storage Option:</STRONG></TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:RadioButtonList id="radSerialATA" runat="server" CssClass="select" RepeatDirection="Horizontal">
											<asp:ListItem Value="None" Selected="True">None</asp:ListItem>
											<asp:ListItem Value="SATA">SATA</asp:ListItem>
											<asp:ListItem Value="NVMe">NVMe</asp:ListItem>
										</asp:RadioButtonList></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="#d3d3d3"><STRONG>Chip Set:</STRONG></TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:TextBox id="txtChipSet" runat="server"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="#d3d3d3"><STRONG>General filter:</STRONG></TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:TextBox id="txtFilter" runat="server"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<TR style="display:none;">
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>64 MB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:TextBox id="txt64Op1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="lightgrey"><STRONG>Option 2:</STRONG></TD>
									<TD style="WIDTH: 239px">
										<asp:TextBox id="txt64Op2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR style="display:none;">
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>128&nbsp;MB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:TextBox id="txt128Op1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="lightgrey"><STRONG>Option 2:</STRONG></TD>
									<TD style="WIDTH: 239px">
										<asp:TextBox id="txt128Op2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR style="display:none;">
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>256 MB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:TextBox id="txt256Op1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="lightgrey"><STRONG>Option 2:</STRONG></TD>
									<TD style="WIDTH: 239px">
										<asp:TextBox id="txt256Op2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR style="display:none;">
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>512 MB Chip Option 1:</STRONG>&nbsp;
									</TD>
									<TD>
										<asp:TextBox id="txt512Op1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="lightgrey"><STRONG>Option 2:</STRONG></TD>
									<TD style="WIDTH: 239px">
										<asp:TextBox id="txt512Op2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR style="display:none;">
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>1 GB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:TextBox id="txt1GOp1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="lightgrey"><STRONG>Option 2:</STRONG></TD>
									<TD style="WIDTH: 239px">
										<asp:TextBox id="txt1GOp2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR style="display:none;">
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>2&nbsp;GB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:TextBox id="txt2GOp1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="lightgrey"><STRONG>Option 2:</STRONG></TD>
									<TD style="WIDTH: 239px">
										<asp:TextBox id="txt2GOp2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR style="display:none;">
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>4&nbsp;GB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:TextBox id="txt4GOp1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="lightgrey"><STRONG>Option 2:</STRONG></TD>
									<TD style="WIDTH: 239px">
										<asp:TextBox id="txt4GOp2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR style="display:none;">
									<TD style="WIDTH: 148px" bgColor="#d3d3d3"><STRONG>8 GB Chip Option 1:</STRONG></TD>
									<TD>
										<asp:TextBox id="txt8GOp1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="#d3d3d3"><STRONG>Option 2:</STRONG></TD>
									<TD style="WIDTH: 239px">
										<asp:TextBox id="txt8GOp2" runat="server"></asp:TextBox></TD>
									<TD></TD>
								</TR >
                                <tr style="display:none;">
                                    <td bgcolor="#d3d3d3" style="width: 148px">
                                        <strong>16 GB Chip Option 1:</strong></td>
                                    <td>
                                        <asp:TextBox ID="txt16GOp1" runat="server"></asp:TextBox></td>
                                    <td bgcolor="#d3d3d3" nowrap="nowrap">
                                        <strong>Option 2:</strong></td>
                                    <td style="width: 239px">
                                        <asp:TextBox ID="txt16GOp2" runat="server"></asp:TextBox></td>
                                    <td>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td bgcolor="#d3d3d3" style="width: 148px; height: 22px">
                                        <strong>32 GB Chip Option 1:</strong></td>
                                    <td style="height: 22px">
                                        <asp:TextBox ID="txt32GOp1" runat="server" OnTextChanged="TextBox2_TextChanged"></asp:TextBox></td>
                                    <td bgcolor="#d3d3d3" nowrap="nowrap" style="height: 22px">
                                        <strong>Option 2:</strong></td>
                                    <td style="width: 239px; height: 22px">
                                        <asp:TextBox ID="txt32GOp2" runat="server"></asp:TextBox></td>
                                    <td style="height: 22px">
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td bgcolor="#d3d3d3" style="width: 148px; height: 22px">
                                        <strong>64 GB Chip Option 1:</strong></td>
                                    <td style="height: 22px">
                                        <asp:TextBox ID="txt64GOp1" runat="server"></asp:TextBox></td>
                                    <td bgcolor="#d3d3d3" nowrap="nowrap" style="height: 22px">
                                        <strong>Option 2:</strong></td>
                                    <td style="width: 239px; height: 22px">
                                        <asp:TextBox ID="txt64GOp2" runat="server"></asp:TextBox></td>
                                    <td style="height: 22px">
                                    </td>
                                </tr>
								<TR>
									<TD style="WIDTH: 148px" bgColor="#d3d3d3"><STRONG>Notes:</STRONG></TD>
									<TD style="WIDTH: 488px" colSpan="4">
										<asp:TextBox id="txtNotes" runat="server" Rows="6" TextMode="MultiLine" Columns="65"></asp:TextBox></TD>
									<TD></TD>
								</TR>
                                <tr>
                                    <td bgcolor="#d3d3d3" style="width: 148px">
                                        <strong>Installation Guide:</strong></td>
                                    <td colspan="4" style="width: 488px">
                                        <asp:TextBox ID="txtInstallGuide" runat="server" Columns="65" Rows="6" TextMode="MultiLine"></asp:TextBox></td>
                                    <td>
                                    </td>
                                </tr>
								<TR>
									<TD style="WIDTH: 148px" bgColor="#d3d3d3"><STRONG>Internal Comments:</STRONG></TD>
									<TD style="WIDTH: 488px" colSpan="4">
										<asp:TextBox id="txtComments" runat="server" Rows="6" TextMode="MultiLine" Columns="65"></asp:TextBox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px" bgColor="lightgrey"><STRONG>Picture:</STRONG>&nbsp;
									</TD>
									<TD style="WIDTH: 488px" colSpan="3">&nbsp;
										<asp:Button id="btnAddPicture" runat="server" Visible="False" Text="Add Picture" onclick="btnAddPicture_Click"></asp:Button>
										<asp:Image id="imgPicture" runat="server"></asp:Image>
										<asp:Button id="btnDeletePicture" runat="server" Text="Delete Picture" onclick="btnDeletePicture_Click"></asp:Button></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 148px"></TD>
									<TD style="WIDTH: 488px" colSpan="3">
										<asp:Button id="btnUpdate" runat="server" Text="Update" onclick="btnUpdate_Click"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:Button id="btnAddNew" runat="server" Visible="False" Text="Add" ForeColor="Navy" onclick="btnAddNew_Click"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:Button id="btnCancelUpdate" runat="server" Text="Cancel" onclick="btnCancelUpdate_Click"></asp:Button>
										<asp:Button id="btnCancelAdd" runat="server" Visible="False" Text="Cancel" ForeColor="Navy" onclick="btnCancelAdd_Click"></asp:Button>&nbsp;&nbsp;&nbsp;
										<asp:Button id="btnSaveAs" runat="server" Text="Save As" onclick="btnSaveAs_Click"></asp:Button>
										<DIV style="DISPLAY: none">
											<asp:TextBox id="txtReturnCancel" runat="server"></asp:TextBox></DIV>
									</TD>
									<TD>
										<asp:Button id="btnDelete" runat="server" Text="Delete" onclick="btnDelete_Click"></asp:Button></TD>
								</TR>
                                <tr>
                                    <td style="width: 148px">
                                    </td>
                                    <td colspan="3" style="width: 488px">
                                        Data Inserted by:
                                        <asp:Label ID="lblUserInsert" runat="server"></asp:Label><br />
                                        Data Updated by:
                                        <asp:Label ID="lblUserUpate" runat="server"></asp:Label></td>
                                    <td>
                                    </td>
                                </tr>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<tr>
					<td><asp:panel id="showSystemInfo" runat="server" Visible="False">
							<TABLE class="item" cellSpacing="1" cellPadding="3" width="100%" border="0">
								<TR>
									<TD colSpan="5">
										<HR width="100%" SIZE="1">
									</TD>
								</TR>
								<TR>
									<TD align="right" width="400"><STRONG>Manufacture:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblManufacture" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Product Line:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblProductLine" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Model:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblModel" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Type:&nbsp;</STRONG></TD>
									<TD colSpan="3">
										<asp:Label id="lblType" runat="server"></asp:Label>-RAM</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Min. Speed:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblMinSpeed" runat="server"></asp:Label>&nbsp;MHz</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Max. Speed:&nbsp;</STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblMaxSpeed" runat="server"></asp:Label>&nbsp;MHz</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Slot:&nbsp;</STRONG></TD>
									<TD colSpan="3">
										<asp:Label id="lblSlot" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Standard Removable:</STRONG>&nbsp;
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblRemovable" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Max Total:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblMaxTotal" runat="server"></asp:Label>&nbsp;MB</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Min. Per Slot:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblMinPerSlot" runat="server"></asp:Label>&nbsp;MB</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Max. Per Slot:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblMaxPerSlot" runat="server"></asp:Label>&nbsp;MB</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Dual Channel:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblDualChannel" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
                                <tr>
                                    <td align="right"><STRONG>
                                        Tri Channel:</STRONG>&nbsp;
                                    </td>
                                    <td colspan="3">
                                        <asp:Label ID="lblTriChannel" runat="server"></asp:Label></td>
                                    <td>
                                    </td>
                                </tr>
								<TR>
									<TD align="right"><STRONG>Run in Pair:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblRunInPair" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
                                <tr>
                                    <td align="right"><STRONG>
                                        Server:</STRONG>
                                    </td>
                                    <td colspan="3">
                                        <asp:Label ID="lblServer" runat="server"></asp:Label></td>
                                    <td>
                                    </td>
                                </tr>
								<TR>
									<TD align="right"><STRONG>ECC Register:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblECC" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>USB:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblUSB" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>FireWire (IEEE 1394):</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblFireWire" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Serial ATA:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblSerialATA" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Chipset:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblChipSet" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>General Filter:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblFilter" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>64 MB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:Label id="lbl64Op1" runat="server"></asp:Label></TD>
									<TD align="right"><STRONG>Option 2:&nbsp;</STRONG></TD>
									<TD>
										<asp:Label id="lbl64Op2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>128&nbsp;MB Chip Option 1:&nbsp;</STRONG></TD>
									<TD>
										<asp:Label id="lbl128Op1" runat="server"></asp:Label></TD>
									<TD align="right"><STRONG>Option 2:&nbsp;</STRONG></TD>
									<TD>
										<asp:Label id="lbl128Op2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>256&nbsp;MB Chip Option 1:</STRONG>&nbsp;
									</TD>
									<TD>
										<asp:Label id="lbl256Op1" runat="server"></asp:Label></TD>
									<TD align="right"><STRONG>Option 2:&nbsp;</STRONG></TD>
									<TD>
										<asp:Label id="lbl256Op2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>512&nbsp;MB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:Label id="lbl512Op1" runat="server"></asp:Label></TD>
									<TD align="right"><STRONG>Option 2:&nbsp;</STRONG></TD>
									<TD>
										<asp:Label id="lbl512Op2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>1&nbsp;GB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:Label id="lbl1GOp1" runat="server"></asp:Label></TD>
									<TD align="right"><STRONG>Option 2:&nbsp;</STRONG></TD>
									<TD>
										<asp:Label id="lbl1GOp2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>2&nbsp;GB Chip Option 1:</STRONG>&nbsp;
									</TD>
									<TD>
										<asp:Label id="lbl2GOp1" runat="server"></asp:Label></TD>
									<TD align="right"><STRONG>Option 2:&nbsp;</STRONG></TD>
									<TD>
										<asp:Label id="lbl2GOp2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>4&nbsp;GB Chip Option 1:</STRONG>&nbsp;
									</TD>
									<TD>
										<asp:Label id="lbl4GOp1" runat="server"></asp:Label></TD>
									<TD align="right"><STRONG>Option 2:&nbsp;</STRONG></TD>
									<TD>
										<asp:Label id="lbl4GOp2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>8 GB Chip Option 1:</STRONG>&nbsp;</TD>
									<TD>
										<asp:Label id="lbl8GOp1" runat="server"></asp:Label></TD>
									<TD align="right"><STRONG>Option 2:&nbsp;</STRONG></TD>
									<TD>
										<asp:Label id="lbl8GOp2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>16 GB Chip Option 1:</STRONG>&nbsp;</TD>
									<TD>
										<asp:Label id="lbl16GOp1" runat="server"></asp:Label></TD>
									<TD align="right"><STRONG>Option 2:&nbsp;</STRONG></TD>
									<TD>
										<asp:Label id="lbl16GOp2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>32 GB Chip Option 1:</STRONG>&nbsp;</TD>
									<TD>
										<asp:Label id="lbl32GOp1" runat="server"></asp:Label></TD>
									<TD align="right"><STRONG>Option 2:&nbsp;</STRONG></TD>
									<TD>
										<asp:Label id="lbl32GOp2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>64 GB Chip Option 1:</STRONG>&nbsp;</TD>
									<TD>
										<asp:Label id="lbl64GOp1" runat="server"></asp:Label></TD>
									<TD align="right"><STRONG>Option 2:&nbsp;</STRONG></TD>
									<TD>
										<asp:Label id="lbl64GOp2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Notes:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblNotes" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
                                <tr>
                                    <td align="right">
                                        <strong>Installation Guide:</strong></td>
                                    <td colspan="3">
                                        <asp:Label id="lblInstallGuide" runat="server"></asp:Label></td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
								<TR>
									<TD align="right"><STRONG>Internal Comments:&nbsp;</STRONG></TD>
									<TD colSpan="3">
										<asp:Label id="lblComments" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Picture:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Image id="imgShowPicture" runat="server"></asp:Image></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"></TD>
									<TD colSpan="3">
										<asp:Button id="btnShowUpdate" runat="server" Text="Update" onclick="btnShowUpdate_Click"></asp:Button></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</asp:panel></td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
