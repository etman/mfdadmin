using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;

namespace memoryAdmin
{
    public partial class ModelSearch : System.Web.UI.Page
    {
        private string strSqlConnString = ConfigurationSettings.AppSettings["cnstr"];
        private ArrayList aryToken = new ArrayList();
        protected void Page_Load(object sender, EventArgs e)
        {
            string strParameter = "";
            string strIdEncode = "";
            char[] chaSep = { ' ' };
            string strGUID = "";
            decimal decTopLaptopPct = 0;
            decimal decTopDesktopPct = 0;
            decimal decTopCameraPct = 0;
            decimal decTopPhonePct = 0;
            decimal decTopPDAPct = 0;

            if (Request.Params["search"] != null)
            {
                strParameter = Request.Params["search"].ToString().Replace("'", "").Replace("`", "").Replace("\"", "").Replace("\\", "").Replace("<", "").Replace(">", "").Trim();
            }

            if (strParameter != "")
            {
                Page.Title = strParameter + " Search Your Model Result";
                string[] strToken = strParameter.Split(chaSep);
                for (int i = 0; i < strToken.Length; i++)
                {

                    if (strToken[i].Trim() != "")
                    {
                        int intDashIndex = strToken[i].IndexOf("-");
                        if (intDashIndex > 0)
                        {
                            aryToken.Add(strToken[i].Replace("-", ""));
                            aryToken.Add(strToken[i].Replace("-", " "));
                        }

                        aryToken.Add(strToken[i]);
                    }
                }

                strGUID = InsertToken(aryToken);
                decTopLaptopPct = GetLaptopResult(aryToken.Count, strGUID);
                decTopDesktopPct = GetDesktopResult(aryToken.Count, strGUID);
                decTopCameraPct = GetCameraResult(aryToken.Count, strGUID);
                decTopPhonePct = GetCellPhoneResult(aryToken.Count, strGUID);
                decTopPDAPct = GetPDAResult(aryToken.Count, strGUID);

                // **********************************************************************************
                // **********************************************************************************
                // Search by SKU
                int resultCount = 0;
                if (strParameter.Length >= 4)
                {
                    if (strParameter.Substring(0, 2).ToUpper() == "M-")
                    {
                        Table tblSearch = new Table();

                        tblSearch = ShowCategory(strParameter, out resultCount, false, "");
                        if (resultCount > 0)
                            hldLowPrice.Controls.Add(tblSearch);
                    }
                }
                // End search by SKU
                // **********************************************************************************
                // **********************************************************************************

                if (decTopLaptopPct == 0 && decTopDesktopPct == 0 && decTopCameraPct == 0 &&
                    decTopPhonePct == 0 && decTopPDAPct == 0 && resultCount == 0)
                    litNoResult.Visible = true;
                else
                    litNoResult.Visible = false;

                if (decTopLaptopPct == 0)
                    pnlLaptop.Visible = false;
                if (decTopDesktopPct == 0)
                    pnlDesktop.Visible = false;
                if (decTopCameraPct == 0)
                    pnlCamera.Visible = false;
                if (decTopPhonePct == 0)
                    pnlCellphone.Visible = false;
                if (decTopPDAPct == 0)
                    pnlPDA.Visible = false;

                if (decTopLaptopPct == 1)
                {
                    if (decTopDesktopPct != 1)
                        pnlDesktop.Visible = false;
                    if (decTopCameraPct != 1)
                        pnlCamera.Visible = false;
                    if (decTopPhonePct != 1)
                        pnlCellphone.Visible = false;
                    if (decTopPDAPct != 1)
                        pnlPDA.Visible = false;
                }
                else if (decTopDesktopPct == 1)
                {
                    if (decTopLaptopPct != 1)
                        pnlLaptop.Visible = false;
                    if (decTopCameraPct != 1)
                        pnlCamera.Visible = false;
                    if (decTopPhonePct != 1)
                        pnlCellphone.Visible = false;
                    if (decTopPDAPct != 1)
                        pnlPDA.Visible = false;
                }
                else if (decTopCameraPct == 1)
                {
                    if (decTopLaptopPct != 1)
                        pnlLaptop.Visible = false;
                    if (decTopDesktopPct != 1)
                        pnlDesktop.Visible = false;
                    if (decTopPhonePct != 1)
                        pnlCellphone.Visible = false;
                    if (decTopPDAPct != 1)
                        pnlPDA.Visible = false;
                }
                else if (decTopPhonePct == 1)
                {
                    if (decTopLaptopPct != 1)
                        pnlLaptop.Visible = false;
                    if (decTopDesktopPct != 1)
                        pnlDesktop.Visible = false;
                    if (decTopCameraPct != 1)
                        pnlCamera.Visible = false;
                    if (decTopPDAPct != 1)
                        pnlPDA.Visible = false;
                }
                else if (decTopPDAPct == 1)
                {
                    if (decTopLaptopPct != 1)
                        pnlLaptop.Visible = false;
                    if (decTopDesktopPct != 1)
                        pnlDesktop.Visible = false;
                    if (decTopCameraPct != 1)
                        pnlCamera.Visible = false;
                    if (decTopPhonePct != 1)
                        pnlCellphone.Visible = false;
                }


            }
            else
                litNoResult.Visible = true;
        }
        private string InsertToken(ArrayList strTokens)
        {
            string strSql = "";
            string strGUID = System.Guid.NewGuid().ToString();
            string strIPAddress = Request.ServerVariables["REMOTE_ADDR"];

            for (int i = 0; i < strTokens.Count; i++)
            {
                SqlConnection mupConn = new SqlConnection(strSqlConnString);
                strSql = "INSERT INTO MemorySearchToken (";
                strSql += "Id,SearchGUID, IPAddress, Token, SearchDate) VALUES (";
                strSql += "@Id,@strGUID, @strIPAddress, @strTokens, '" + DateTime.Now + "') ";
                SqlCommand mupComm = new SqlCommand(strSql, mupConn);
                mupComm.Parameters.AddWithValue("@Id", Mup_utilities.GetMemoryScannerId());
                mupComm.Parameters.AddWithValue("@strGUID", strGUID);
                mupComm.Parameters.AddWithValue("@strIPAddress", strIPAddress);
                mupComm.Parameters.AddWithValue("@strTokens", strTokens[i]);
                mupConn.Open();
                mupComm.ExecuteNonQuery();
            }

            return strGUID;
        }
        private string GenSearchSql(string strTableName, string strTableKey, int intTokenCount, string strGUID)
        {
            string strSql = "";

            strSql = "SELECT TOP 60 A.MatchPct, M." + strTableKey + " AS TableKey, M.Manufacture + ' ' + M.Product_line + ' ' + M.Model AS ModelDescr FROM " + strTableName + " M ";
            strSql += "INNER JOIN (SELECT " + strTableKey + ", COUNT (*) * 1.0 / " + intTokenCount.ToString() + " AS MatchPct ";
            strSql += "FROM " + strTableName + " M ";
            strSql += "INNER JOIN MemorySearchToken T ON '' + M.manufacture + '' + M.Product_line + '' + M.model + '' LIKE '%' + Token + '%' ";
            strSql += "WHERE SearchGUID = '" + strGUID + "' ";
            strSql += "GROUP BY " + strTableKey + ") A ON A." + strTableKey + " = M." + strTableKey + " ";
            strSql += "ORDER BY MatchPct DESC ";

            return strSql;
        }
        private decimal GetLaptopResult(int tokenCount, string strGUID)
        {
            string strSql;
            SqlConnection memoryConn = new SqlConnection(strSqlConnString);
            strSql = GenSearchSql("LaptopMemory", "Laptop_id", tokenCount, strGUID);
            decimal MatchPct = 0;

            try
            {
                SqlDataAdapter memoryAdapter = new SqlDataAdapter(strSql, memoryConn);
                DS_SearchResult.SearchResultDataTable dtResult = new DS_SearchResult.SearchResultDataTable();

                memoryAdapter.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                {
                    if (dtResult.Rows[0]["MatchPct"] != DBNull.Value)
                        MatchPct = (decimal)dtResult.Rows[0]["MatchPct"];
                }
                grdLaptop.DataSource = dtResult;
                grdLaptop.DataBind();

                //memoryConn.Close();
                //memoryConn.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
                //if (memoryConn != null)
                //{
                //    memoryConn.Close();
                //    memoryConn.Dispose();
                //}
            }

            return MatchPct;
        }
        private decimal GetDesktopResult(int tokenCount, string strGUID)
        {
            string strSql;
            SqlConnection memoryConn = new SqlConnection(strSqlConnString);
            strSql = GenSearchSql("DesktopMemory", "Desktop_id", tokenCount, strGUID);
            decimal MatchPct = 0;

            try
            {
                SqlDataAdapter memoryAdapter = new SqlDataAdapter(strSql, memoryConn);
                DS_SearchResult.SearchResultDataTable dtResult = new DS_SearchResult.SearchResultDataTable();

                memoryAdapter.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                {
                    if (dtResult.Rows[0]["MatchPct"] != DBNull.Value)
                        MatchPct = (decimal)dtResult.Rows[0]["MatchPct"];
                }
                grdDesktop.DataSource = dtResult;
                grdDesktop.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return MatchPct;
        }
        private decimal GetCameraResult(int tokenCount, string strGUID)
        {
            string strSql;
            SqlConnection memoryConn = new SqlConnection(strSqlConnString);
            strSql = GenSearchSql("Digital_Camera", "Camera_id", tokenCount, strGUID);
            decimal MatchPct = 0;

            try
            {
                SqlDataAdapter memoryAdapter = new SqlDataAdapter(strSql, memoryConn);
                DS_SearchResult.SearchResultDataTable dtResult = new DS_SearchResult.SearchResultDataTable();

                memoryAdapter.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                {
                    if (dtResult.Rows[0]["MatchPct"] != DBNull.Value)
                        MatchPct = (decimal)dtResult.Rows[0]["MatchPct"];
                }
                grdDCamera.DataSource = dtResult;
                grdDCamera.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return MatchPct;
        }
        private decimal GetCellPhoneResult(int tokenCount, string strGUID)
        {
            string strSql;
            SqlConnection memoryConn = new SqlConnection(strSqlConnString);
            strSql = GenSearchSql("Phone_model", "Phone_id", tokenCount, strGUID);
            decimal MatchPct = 0;

            try
            {
                SqlDataAdapter memoryAdapter = new SqlDataAdapter(strSql, memoryConn);
                DS_SearchResult.SearchResultDataTable dtResult = new DS_SearchResult.SearchResultDataTable();

                memoryAdapter.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                {
                    if (dtResult.Rows[0]["MatchPct"] != DBNull.Value)
                        MatchPct = (decimal)dtResult.Rows[0]["MatchPct"];
                }
                grdCellPhone.DataSource = dtResult;
                grdCellPhone.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return MatchPct;
        }
        private decimal GetPDAResult(int tokenCount, string strGUID)
        {
            string strSql;
            SqlConnection memoryConn = new SqlConnection(strSqlConnString);
            strSql = GenSearchSql("PDA_model", "PDA_id", tokenCount, strGUID);
            decimal MatchPct = 0;

            try
            {
                SqlDataAdapter memoryAdapter = new SqlDataAdapter(strSql, memoryConn);
                DS_SearchResult.SearchResultDataTable dtResult = new DS_SearchResult.SearchResultDataTable();

                memoryAdapter.Fill(dtResult);
                if (dtResult.Rows.Count > 0)
                {
                    if (dtResult.Rows[0]["MatchPct"] != DBNull.Value)
                        MatchPct = (decimal)dtResult.Rows[0]["MatchPct"];
                }
                grdPDA.DataSource = dtResult;
                grdPDA.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return MatchPct;
        }
        protected void grdLaptop_DataBound(object sender, EventArgs e)
        {
            string strModelDescr = "";
            int intTokenLength = 0;
            int skipIndex = 0;
            for (int i = 0; i < grdLaptop.Rows.Count; i++)
            {
                strModelDescr = ((HyperLink)grdLaptop.Rows[i].FindControl("lnklaptopModel")).Text;
                ((HyperLink)grdLaptop.Rows[i].FindControl("lnklaptopModel")).NavigateUrl = "LaptopUpdate.aspx?modelid=" +
                    ((Label)grdLaptop.Rows[i].FindControl("lblModelId")).Text;
                for (int j = 0; j < aryToken.Count; j++)
                {
                    intTokenLength = aryToken[j].ToString().Length;
                    for (int k = 0; k < strModelDescr.Length - intTokenLength + 1; )
                    {
                        if (intTokenLength + k <= strModelDescr.Length)
                        {
                            skipIndex = 0;
                            skipIndex = strModelDescr.IndexOf("<strong>", k);
                            if (skipIndex == k)
                            {
                                skipIndex = strModelDescr.IndexOf("</strong>", k);
                                k = skipIndex + 9;
                            }
                            if (intTokenLength + k <= strModelDescr.Length)
                            {
                                if (strModelDescr.Substring(k, intTokenLength).ToUpper() == aryToken[j].ToString().ToUpper())
                                {
                                    strModelDescr = strModelDescr.Insert(k, "<strong><span style=\"background-color: #ccff99\">");
                                    strModelDescr = strModelDescr.Insert(k + intTokenLength + 48, "</span></strong>");
                                    skipIndex = strModelDescr.IndexOf("</strong>", k);
                                    k = skipIndex + 9;
                                }
                                else
                                    k++;
                            }
                            else
                                k++;
                        }
                        else
                            k++;
                    }
                }
                ((HyperLink)grdLaptop.Rows[i].FindControl("lnklaptopModel")).Text = strModelDescr;
            }
        }
        protected void grdDesktop_DataBound(object sender, EventArgs e)
        {
            string strModelDescr = "";
            int intTokenLength = 0;
            int skipIndex = 0;
            for (int i = 0; i < grdDesktop.Rows.Count; i++)
            {
                strModelDescr = ((HyperLink)grdDesktop.Rows[i].FindControl("lnklaptopModel")).Text;
                ((HyperLink)grdDesktop.Rows[i].FindControl("lnklaptopModel")).NavigateUrl = "DesktopUpdate.aspx?modelid=" +
                    ((Label)grdDesktop.Rows[i].FindControl("lblModelId")).Text;
                for (int j = 0; j < aryToken.Count; j++)
                {
                    intTokenLength = aryToken[j].ToString().Length;
                    for (int k = 0; k < strModelDescr.Length - intTokenLength + 1; )
                    {
                        if (intTokenLength + k <= strModelDescr.Length)
                        {
                            skipIndex = 0;
                            skipIndex = strModelDescr.IndexOf("<strong>", k);
                            if (skipIndex == k)
                            {
                                skipIndex = strModelDescr.IndexOf("</strong>", k);
                                k = skipIndex + 9;
                            }
                            if (intTokenLength + k <= strModelDescr.Length)
                            {
                                if (strModelDescr.Substring(k, intTokenLength).ToUpper() == aryToken[j].ToString().ToUpper())
                                {
                                    strModelDescr = strModelDescr.Insert(k, "<strong><span style=\"background-color: #ccff99\">");
                                    strModelDescr = strModelDescr.Insert(k + intTokenLength + 48, "</span></strong>");
                                    skipIndex = strModelDescr.IndexOf("</strong>", k);
                                    k = skipIndex + 9;
                                }
                                else
                                    k++;
                            }
                            else
                                k++;
                        }
                        else
                            k++;
                    }
                }
                ((HyperLink)grdDesktop.Rows[i].FindControl("lnklaptopModel")).Text = strModelDescr;
            }
        }
        protected void grdDCamera_DataBound(object sender, EventArgs e)
        {
            string strModelDescr = "";
            int intTokenLength = 0;
            int skipIndex = 0;
            for (int i = 0; i < grdDCamera.Rows.Count; i++)
            {
                strModelDescr = ((HyperLink)grdDCamera.Rows[i].FindControl("lnklaptopModel")).Text;
                ((HyperLink)grdDCamera.Rows[i].FindControl("lnklaptopModel")).NavigateUrl = "CameraUpdate.aspx?modelid=" +
                    ((Label)grdDCamera.Rows[i].FindControl("lblModelId")).Text;
                for (int j = 0; j < aryToken.Count; j++)
                {
                    intTokenLength = aryToken[j].ToString().Length;
                    for (int k = 0; k < strModelDescr.Length - intTokenLength + 1; )
                    {
                        if (intTokenLength + k <= strModelDescr.Length)
                        {
                            skipIndex = 0;
                            skipIndex = strModelDescr.IndexOf("<strong>", k);
                            if (skipIndex == k)
                            {
                                skipIndex = strModelDescr.IndexOf("</strong>", k);
                                k = skipIndex + 9;
                            }
                            if (intTokenLength + k <= strModelDescr.Length)
                            {
                                if (strModelDescr.Substring(k, intTokenLength).ToUpper() == aryToken[j].ToString().ToUpper())
                                {
                                    strModelDescr = strModelDescr.Insert(k, "<strong><span style=\"background-color: #ccff99\">");
                                    strModelDescr = strModelDescr.Insert(k + intTokenLength + 48, "</span></strong>");
                                    skipIndex = strModelDescr.IndexOf("</strong>", k);
                                    k = skipIndex + 9;
                                }
                                else
                                    k++;
                            }
                            else
                                k++;
                        }
                        else
                            k++;
                    }
                }
                ((HyperLink)grdDCamera.Rows[i].FindControl("lnklaptopModel")).Text = strModelDescr;
            }
        }
        protected void grdCellPhone_DataBound(object sender, EventArgs e)
        {
            string strModelDescr = "";
            int intTokenLength = 0;
            int skipIndex = 0;
            for (int i = 0; i < grdCellPhone.Rows.Count; i++)
            {
                strModelDescr = ((HyperLink)grdCellPhone.Rows[i].FindControl("lnklaptopModel")).Text;
                ((HyperLink)grdCellPhone.Rows[i].FindControl("lnklaptopModel")).NavigateUrl = "CellPhoneUpdate.aspx?modelid=" +
                    ((Label)grdCellPhone.Rows[i].FindControl("lblModelId")).Text;
                for (int j = 0; j < aryToken.Count; j++)
                {
                    intTokenLength = aryToken[j].ToString().Length;
                    for (int k = 0; k < strModelDescr.Length - intTokenLength + 1; )
                    {
                        if (intTokenLength + k <= strModelDescr.Length)
                        {
                            skipIndex = 0;
                            skipIndex = strModelDescr.IndexOf("<strong>", k);
                            if (skipIndex == k)
                            {
                                skipIndex = strModelDescr.IndexOf("</strong>", k);
                                k = skipIndex + 9;
                            }
                            if (intTokenLength + k <= strModelDescr.Length)
                            {
                                if (strModelDescr.Substring(k, intTokenLength).ToUpper() == aryToken[j].ToString().ToUpper())
                                {
                                    strModelDescr = strModelDescr.Insert(k, "<strong><span style=\"background-color: #ccff99\">");
                                    strModelDescr = strModelDescr.Insert(k + intTokenLength + 48, "</span></strong>");
                                    skipIndex = strModelDescr.IndexOf("</strong>", k);
                                    k = skipIndex + 9;
                                }
                                else
                                    k++;
                            }
                            else
                                k++;
                        }
                        else
                            k++;
                    }
                }
                ((HyperLink)grdCellPhone.Rows[i].FindControl("lnklaptopModel")).Text = strModelDescr;
            }
        }
        protected void grdPDA_DataBound(object sender, EventArgs e)
        {
            string strModelDescr = "";
            int intTokenLength = 0;
            int skipIndex = 0;
            for (int i = 0; i < grdPDA.Rows.Count; i++)
            {
                strModelDescr = ((HyperLink)grdPDA.Rows[i].FindControl("lnklaptopModel")).Text;
                ((HyperLink)grdPDA.Rows[i].FindControl("lnklaptopModel")).NavigateUrl = "PDASelector.aspx?modelid=" +
                    ((Label)grdPDA.Rows[i].FindControl("lblModelId")).Text;
                for (int j = 0; j < aryToken.Count; j++)
                {
                    intTokenLength = aryToken[j].ToString().Length;
                    for (int k = 0; k < strModelDescr.Length - intTokenLength + 1; )
                    {
                        if (intTokenLength + k <= strModelDescr.Length)
                        {
                            skipIndex = 0;
                            skipIndex = strModelDescr.IndexOf("<strong>", k);
                            if (skipIndex == k)
                            {
                                skipIndex = strModelDescr.IndexOf("</strong>", k);
                                k = skipIndex + 9;
                            }
                            if (intTokenLength + k <= strModelDescr.Length)
                            {
                                if (strModelDescr.Substring(k, intTokenLength).ToUpper() == aryToken[j].ToString().ToUpper())
                                {
                                    strModelDescr = strModelDescr.Insert(k, "<strong><span style=\"background-color: #ccff99\">");
                                    strModelDescr = strModelDescr.Insert(k + intTokenLength + 48, "</span></strong>");
                                    skipIndex = strModelDescr.IndexOf("</strong>", k);
                                    k = skipIndex + 9;
                                }
                                else
                                    k++;
                            }
                            else
                                k++;
                        }
                        else
                            k++;
                    }
                }
                ((HyperLink)grdPDA.Rows[i].FindControl("lnklaptopModel")).Text = strModelDescr;
            }
        }
        private Table ShowCategory(string strTableCode, out int intItemCount, bool isStandard, string strSpecial)
        {
            Table tblLowPrice = new Table();
            DataGrid grdLowPrice = new DataGrid();
            DataTable dtMemory = new DataTable();
            DataRow drMemory;
            ArrayList priceList =  Mup_utilities.GetStandardPriceList(strTableCode, isStandard, "");
            web.cls_MemoryDescr sampleMemory = web.MupUtilities.GetMemoryDescr(strTableCode);
            char[] charsToTrim = { '-' };

            // Create datatable for available memory module
            dtMemory.Columns.Add(new DataColumn("MemoryCode", Type.GetType("System.String")));      // drMemory[0] is module name
            dtMemory.Columns.Add(new DataColumn("MemoryName", Type.GetType("System.String")));           // drMemory[1] is speed
            dtMemory.Columns.Add(new DataColumn("MemoryDescr", Type.GetType("System.String")));      // drMemory[2] is size
            // dtMemory.Columns.Add(new DataColumn("ChipType", Type.GetType("System.String")));        // drMemory[3] is chip type
            // dtMemory.Columns.Add(new DataColumn("FindIt!", Type.GetType("System.String")));         // drMemory[4] is 
            // dtMemory.Columns.Add(new DataColumn("filter", Type.GetType("System.String")));          // drMemory[5] is filter
            // dtMemory.Columns.Add(new DataColumn("type", Type.GetType("System.String")));            // drMemory[3] is type
            dtMemory.Columns.Add(new DataColumn("sizeName", Type.GetType("System.String")));        // drMemory[4] is size name
            // dtMemory.Columns.Add(new DataColumn("typeName", Type.GetType("System.String")));        // drMemory[8] is type name
            // dtMemory.Columns.Add(new DataColumn("title", Type.GetType("System.String")));           // drMemory[9] is title
            // dtMemory.Columns.Add(new DataColumn("Model", Type.GetType("System.String")));           // drMemory[10] is Model
            // dtMemory.Columns.Add(new DataColumn("ModelURL", Type.GetType("System.String")));        // drMemory[11] is ModelURL
            // dtMemory.Columns.Add(new DataColumn("LowestPrice", Type.GetType("System.String")));     // drMemory[12] is LowestPrice
            // dtMemory.Columns.Add(new DataColumn("ConfigId", Type.GetType("System.String")));        // drMemory[5] is Config Module ID
            dtMemory.Columns.Add(new DataColumn("SKU", Type.GetType("System.String")));             // drMemory[6] is module SKU
            dtMemory.Columns.Add(new DataColumn("ProductId", Type.GetType("System.String")));       // drMemory[7] is ProductId
            dtMemory.Columns.Add(new DataColumn("Price", Type.GetType("System.Decimal")));          // drMemory[8] id Price
            dtMemory.Columns.Add(new DataColumn("VariantId", Type.GetType("System.String")));       // drMemory[9] is VariantId

            //dtMemory.Columns.Add(new DataColumn("MemoryCode", Type.GetType("System.String")));
            //dtMemory.Columns.Add(new DataColumn("MemoryName", Type.GetType("System.String")));
            //dtMemory.Columns.Add(new DataColumn("MemoryDescr", Type.GetType("System.String")));
            //dtMemory.Columns.Add(new DataColumn("Size", Type.GetType("System.Int32")));
            //dtMemory.Columns.Add(new DataColumn("SizeName", Type.GetType("System.String")));
            //dtMemory.Columns.Add(new DataColumn("LowestPrice", Type.GetType("System.String")));

            for (int i = 0; i < priceList.Count; i++)
            {
                drMemory = dtMemory.NewRow();
                drMemory["MemoryCode"] = strTableCode;
                if (isStandard)
                    drMemory["MemoryName"] = sampleMemory.MemoryDescr;
                else
                    drMemory["MemoryName"] = ((cls_StandardPriceList)priceList[i]).Name;
                if (sampleMemory.MemoryDetail.IndexOf("hz", 0) > 0)
                    drMemory["MemoryDescr"] = sampleMemory.MemoryDetail.Substring(0, sampleMemory.MemoryDetail.IndexOf("hz", 0) + 2);
                else
                    drMemory["MemoryDescr"] = sampleMemory.MemoryDetail;
                //drMemory["Size"] = Convert.ToInt32(((cls_PriceList)priceList[i]).ProductCode.Replace(strTableCode, ""));
                if (((cls_StandardPriceList)priceList[i]).Size < 1024)
                    drMemory["SizeName"] = Convert.ToString(((cls_StandardPriceList)priceList[i]).Size) + "MB";
                else
                    drMemory["SizeName"] = Convert.ToString(((cls_StandardPriceList)priceList[i]).Size / 1024) + "GB";
                drMemory["SKU"] = ((cls_StandardPriceList)priceList[i]).SKU;
                drMemory["ProductId"] = ((cls_StandardPriceList)priceList[i]).ProductId;
                drMemory["Price"] = ((cls_StandardPriceList)priceList[i]).Price;
                drMemory["VariantId"] = ((cls_StandardPriceList)priceList[i]).VariantId;

                dtMemory.Rows.Add(drMemory);
            }

            // Create dataview for available memory selector
            DataView dvMemory = new DataView(dtMemory);

            tblLowPrice.CssClass = "btext";
            tblLowPrice.CellSpacing = 0;
            tblLowPrice.CellPadding = 0;
            tblLowPrice.BorderWidth = Unit.Pixel(0);
            tblLowPrice.Width = Unit.Percentage(100.0);

            Table tblRange = new Table();
            tblRange.Width = Unit.Percentage(100.0);
            tblRange.CellSpacing = 0;
            tblRange.CellPadding = 0;
            tblRange.BorderWidth = Unit.Pixel(0);

            Table tblTitleTab = new Table();
            tblTitleTab.Width = Unit.Percentage(100.0);
            tblTitleTab.BorderWidth = Unit.Pixel(0);
            tblTitleTab.CellPadding = 0;
            tblTitleTab.CellSpacing = 0;
            tblTitleTab.CssClass = "btext";
            TableCell celTitleLeft = new TableCell();
            celTitleLeft.HorizontalAlign = HorizontalAlign.Right;
            TableCell celTitleRight = new TableCell();
            celTitleRight.Width = Unit.Pixel(46);
            Label lblSubtitle = new Label();
            //lblSubtitle.Font.Name = "Arial";
            //lblSubtitle.Font.Size = FontUnit.Point(11);
            lblSubtitle.Font.Bold = true;
            lblSubtitle.Text = sampleMemory.MemoryDescr + " Black Diamond Memory";
            System.Web.UI.WebControls.Image imgTitleLeft = new System.Web.UI.WebControls.Image();
            imgTitleLeft.ImageUrl = "/memory_finders/images/MemorySelector/k1.gif";
            System.Web.UI.WebControls.Image imgTitleRight = new System.Web.UI.WebControls.Image();
            imgTitleRight.ImageUrl = "/memory_finders/images/MemorySelector/k2.gif";
            TableCell celTitle = new TableCell();
            celTitle.Attributes.Add("background", "/memory_finders/images/MemorySelector/k3.gif");
            celTitle.Width = Unit.Pixel(275);
            celTitle.Wrap = false;
            celTitle.HorizontalAlign = HorizontalAlign.Right;
            celTitle.CssClass = "bigBText";
            TableRow rowTitle = new TableRow();
            celTitle.Controls.Add(lblSubtitle);
            celTitleLeft.Controls.Add(imgTitleLeft);
            celTitleRight.Controls.Add(imgTitleRight);
            rowTitle.Controls.Add(celTitleLeft);
            rowTitle.Controls.Add(celTitle);
            rowTitle.Controls.Add(celTitleRight);
            tblTitleTab.Controls.Add(rowTitle);
            tblTitleTab.HorizontalAlign = HorizontalAlign.Right;

            TableCell celFirst = new TableCell();
            TableRow rowFirst = new TableRow();
            celFirst.Controls.Add(tblTitleTab);
            rowFirst.Controls.Add(celFirst);
            tblRange.Controls.Add(rowFirst);    // Group title

            TableCell celSecond = new TableCell();
            celSecond.VerticalAlign = VerticalAlign.Top;
            TableRow rowSecond = new TableRow();
            rowSecond.Controls.Add(celSecond);
            celSecond.Controls.Add(tblLowPrice);
            tblRange.Controls.Add(rowSecond);

            TableCell celBoldFirstLeft = new TableCell();
            celBoldFirstLeft.Width = Unit.Pixel(15);
            System.Web.UI.WebControls.Image imgBoldFirstLeft = new System.Web.UI.WebControls.Image();
            imgBoldFirstLeft.ImageUrl = "/memory_finders/images/MemorySelector/a1.gif";
            celBoldFirstLeft.Controls.Add(imgBoldFirstLeft);
            TableCell celBoldFirstCenter = new TableCell();
            celBoldFirstCenter.Attributes.Add("background", "/memory_finders/images/MemorySelector/a2.gif");
            System.Web.UI.WebControls.Image imgBoldFirstBG = new System.Web.UI.WebControls.Image();
            imgBoldFirstBG.ImageUrl = "/memory_finders/images/MemorySelector/a2.gif";
            celBoldFirstCenter.Controls.Add(imgBoldFirstBG);
            TableCell celBoldFirstRight = new TableCell();
            celBoldFirstRight.Width = Unit.Pixel(15);
            System.Web.UI.WebControls.Image imgBoldFirstRight = new System.Web.UI.WebControls.Image();
            imgBoldFirstRight.ImageUrl = "/memory_finders/images/MemorySelector/a3.gif";
            celBoldFirstRight.Controls.Add(imgBoldFirstRight);
            TableRow rowBoldFirst = new TableRow();
            rowBoldFirst.Controls.Add(celBoldFirstLeft);
            rowBoldFirst.Controls.Add(celBoldFirstCenter);
            rowBoldFirst.Controls.Add(celBoldFirstRight);
            tblLowPrice.Controls.Add(rowBoldFirst);

            // Add picture here, between data grid and table
            Panel pnlModuleImage = new Panel();
            HyperLink lnkLargeImage = new HyperLink();
            System.Web.UI.WebControls.Image imgMemory = new System.Web.UI.WebControls.Image();
            HyperLink lnkSeeLarger = new HyperLink();


            if (File.Exists(System.IO.Path.Combine("C:\\Inetpub\\wwwroot\\Memory-Up\\MfdImages\\MemoryImages\\", strTableCode + "S.jpg")))
            {
                if (File.Exists(System.IO.Path.Combine("C:\\Inetpub\\wwwroot\\Memory-Up\\MfdImages\\MemoryImages\\", strTableCode.TrimEnd(charsToTrim) + ".jpg")))
                {
                    lnkLargeImage.Attributes.Add("OnClick", "return Show_Large_Memory('" + Server.HtmlEncode("/Memory_Finders/MemoryImage.aspx?img=" + strTableCode.TrimEnd(charsToTrim) + ".jpg") + "', " +
                        "'BlackDiamond', 'width=700,height=400,resizable=1,scrollbars=1,toolbar=0,status=1');");
                    lnkSeeLarger.Attributes.Add("OnClick", "return Show_Large_Memory('" + Server.HtmlEncode("/Memory_Finders/MemoryImage.aspx?img=" + strTableCode.TrimEnd(charsToTrim) + ".jpg") + "', " +
                        "'BlackDiamond', 'width=700,height=400,resizable=1,scrollbars=1,toolbar=0,status=1');");
                    lnkLargeImage.NavigateUrl = Server.HtmlEncode("/Memory_Finders/MemoryImage.aspx?img=" + strTableCode.TrimEnd(charsToTrim) + ".jpg");
                    lnkSeeLarger.NavigateUrl = Server.HtmlEncode("/Memory_Finders/MemoryImage.aspx?img=" + strTableCode.TrimEnd(charsToTrim) + ".jpg");
                    lnkSeeLarger.Text = "See Larger Image";
                }
                imgMemory.ImageUrl = "/MfdImages/MemoryImages/" + strTableCode + "S.jpg";
            }
            else if (File.Exists(System.IO.Path.Combine("C:\\Inetpub\\wwwroot\\Memory-Up\\MfdImages\\MemoryImages\\", strTableCode + "-S.jpg")))
            {
                if (File.Exists(System.IO.Path.Combine("C:\\Inetpub\\wwwroot\\Memory-Up\\MfdImages\\MemoryImages\\", strTableCode + ".jpg")))
                {
                    lnkLargeImage.Attributes.Add("OnClick", "return Show_Large_Memory('" + Server.HtmlEncode("/Memory_Finders/MemoryImage.aspx?img=" + strTableCode + ".jpg") + "', " +
                        "'BlackDiamond', 'width=700,height=400,resizable=1,scrollbars=1,toolbar=0,status=1');");
                    lnkSeeLarger.Attributes.Add("OnClick", "return Show_Large_Memory('" + Server.HtmlEncode("/Memory_Finders/MemoryImage.aspx?img=" + strTableCode + ".jpg") + "', " +
                        "'BlackDiamond', 'width=700,height=400,resizable=1,scrollbars=1,toolbar=0,status=1');");
                    lnkLargeImage.NavigateUrl = Server.HtmlEncode("/Memory_Finders/MemoryImage.aspx?img=" + strTableCode + ".jpg");
                    lnkSeeLarger.NavigateUrl = Server.HtmlEncode("/Memory_Finders/MemoryImage.aspx?img=" + strTableCode + ".jpg");
                    lnkSeeLarger.Text = "See Larger Image";
                }
                imgMemory.ImageUrl = "/MfdImages/MemoryImages/" + strTableCode + "-S.jpg";
            }
            else
                pnlModuleImage.Visible = false;

            lnkLargeImage.Target = "BlackDiamond";
            lnkSeeLarger.Target = "BlackDiamond";

            lnkLargeImage.Controls.Add(imgMemory);

            TableCell celPictureBorder = new TableCell();
            celPictureBorder.BackColor = Color.White;
            TableRow rowPictureBorder = new TableRow();
            Table tblPictureBorder = new Table();
            tblPictureBorder.BorderWidth = Unit.Pixel(0);
            tblPictureBorder.CellPadding = 0;
            tblPictureBorder.CellSpacing = 1;
            tblPictureBorder.BackColor = ColorTranslator.FromHtml("#C2C2C2");
            tblPictureBorder.CssClass = "text";

            System.Web.UI.WebControls.Image imgBlank = new System.Web.UI.WebControls.Image();
            imgBlank.ImageUrl = "/memory_finders/images/MemorySelector/dote_pix.gif";
            imgBlank.Width = Unit.Pixel(5);
            imgBlank.Height = Unit.Pixel(8);

            HtmlGenericControl newLine = new HtmlGenericControl();
            newLine.InnerHtml = "<br />";

            celPictureBorder.Controls.Add(lnkLargeImage);
            rowPictureBorder.Controls.Add(celPictureBorder);
            tblPictureBorder.Controls.Add(rowPictureBorder);
            pnlModuleImage.Controls.Add(tblPictureBorder);
            pnlModuleImage.Controls.Add(lnkSeeLarger);
            pnlModuleImage.Controls.Add(newLine);
            pnlModuleImage.Controls.Add(imgBlank);

            TableCell celPictureLeft = new TableCell();
            celPictureLeft.Attributes.Add("background", "/memory_finders/images/MemorySelector/a7.gif");
            celPictureLeft.Text = "&nbsp;";
            TableCell celPicture = new TableCell();
            celPicture.HorizontalAlign = HorizontalAlign.Right;
            TableCell celPictureRight = new TableCell();
            celPictureRight.Attributes.Add("background", "/memory_finders/images/MemorySelector/a8.gif");
            celPictureRight.Text = "&nbsp;";
            TableRow rowPicture = new TableRow();
            rowPicture.Controls.Add(celPictureLeft);
            rowPicture.Controls.Add(celPicture);
            rowPicture.Controls.Add(celPictureRight);
            tblLowPrice.Controls.Add(rowPicture);
            celPicture.Controls.Add(pnlModuleImage);
            // End add picture

            TableCell celLowPriceLeft = new TableCell();
            celLowPriceLeft.Attributes.Add("background", "/memory_finders/images/MemorySelector/a7.gif");
            celLowPriceLeft.Text = "&nbsp;";
            TableCell celLowPrice = new TableCell();
            celLowPrice.VerticalAlign = VerticalAlign.Top;
            TableCell celLowPriceRight = new TableCell();
            celLowPriceRight.Attributes.Add("background", "/memory_finders/images/MemorySelector/a8.gif");
            celLowPriceRight.Text = "&nbsp;";
            TableRow rowLowPrice = new TableRow();

            celLowPrice.Controls.Add(grdLowPrice);
            rowLowPrice.Controls.Add(celLowPriceLeft);
            rowLowPrice.Controls.Add(celLowPrice);
            rowLowPrice.Controls.Add(celLowPriceRight);
            tblLowPrice.Controls.Add(rowLowPrice);

            TableCell celBoldThirdLeft = new TableCell();
            celBoldThirdLeft.Width = Unit.Pixel(15);
            System.Web.UI.WebControls.Image imgBoldThirdLeft = new System.Web.UI.WebControls.Image();
            imgBoldThirdLeft.ImageUrl = "/memory_finders/images/MemorySelector/a4.gif";
            celBoldThirdLeft.Controls.Add(imgBoldThirdLeft);
            TableCell celBoldThirdCenter = new TableCell();
            celBoldThirdCenter.Attributes.Add("background", "/memory_finders/images/MemorySelector/a5.gif");
            System.Web.UI.WebControls.Image imgBoldThirdBG = new System.Web.UI.WebControls.Image();
            imgBoldThirdBG.ImageUrl = "/memory_finders/images/MemorySelector/a5.gif";
            celBoldThirdCenter.Controls.Add(imgBoldThirdBG);
            TableCell celBoldThirdRight = new TableCell();
            celBoldThirdRight.Width = Unit.Pixel(15);
            System.Web.UI.WebControls.Image imgBoldThirdRight = new System.Web.UI.WebControls.Image();
            imgBoldThirdRight.ImageUrl = "/memory_finders/images/MemorySelector/a6.gif";
            celBoldThirdRight.Controls.Add(imgBoldThirdRight);
            TableRow rowBoldThird = new TableRow();
            rowBoldThird.Controls.Add(celBoldThirdLeft);
            rowBoldThird.Controls.Add(celBoldThirdCenter);
            rowBoldThird.Controls.Add(celBoldThirdRight);
            tblLowPrice.Controls.Add(rowBoldThird);

            TableCell celButton = new TableCell();
            celButton.ColumnSpan = 3;
            celButton.Text = "&nbsp;";
            TableRow rowButton = new TableRow();
            rowButton.Controls.Add(celButton);
            tblRange.Controls.Add(rowButton);

            grdLowPrice.CssClass = "btext";
            grdLowPrice.BorderWidth = Unit.Pixel(0);
            grdLowPrice.CellSpacing = 1;
            grdLowPrice.CellPadding = 2;
            grdLowPrice.BackColor = Color.FromArgb(136, 153, 223);
            grdLowPrice.AutoGenerateColumns = false;
            grdLowPrice.Width = Unit.Percentage(100.0);
            grdLowPrice.HorizontalAlign = HorizontalAlign.Center;
            grdLowPrice.ID = "grdLowPrice";

            grdLowPrice.HeaderStyle.CssClass = "WhiteTitleText";
            grdLowPrice.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            grdLowPrice.AlternatingItemStyle.BackColor = Color.FromArgb(240, 240, 240);
            grdLowPrice.ItemStyle.BackColor = Color.FromArgb(255, 255, 255);

            TemplateColumn tmpName = new TemplateColumn();
            if (strTableCode.Length > 5)
            {
                if (isStandard)
                {
                    tmpName.HeaderTemplate = new CategoryNameTemplate(ListItemType.Header, "MemoryName", "SizeName", "MemoryDescr", 9);
                    tmpName.ItemTemplate = new CategoryNameTemplate(ListItemType.Item, "MemoryName", "SizeName", "MemoryDescr", 9);
                }
                else
                {
                    tmpName.HeaderTemplate = new CategoryNameTemplate(ListItemType.Header, "MemoryName", "", "MemoryDescr", 9);
                    tmpName.ItemTemplate = new CategoryNameTemplate(ListItemType.Item, "MemoryName", "", "MemoryDescr", 9);
                }
            }
            else
            {
                if (isStandard)
                {
                    tmpName.HeaderTemplate = new CategoryNameTemplate(ListItemType.Header, "MemoryName", "SizeName", "MemoryDescr", 8);
                    tmpName.ItemTemplate = new CategoryNameTemplate(ListItemType.Item, "MemoryName", "SizeName", "MemoryDescr", 8);
                }
                else
                {
                    tmpName.HeaderTemplate = new CategoryNameTemplate(ListItemType.Header, "MemoryName", "", "MemoryDescr", 8);
                    tmpName.ItemTemplate = new CategoryNameTemplate(ListItemType.Item, "MemoryName", "", "MemoryDescr", 8);
                }
            }
            tmpName.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            //tmpName.ItemStyle.Width = Unit.Pixel(220);
            grdLowPrice.Columns.Add(tmpName);

            TemplateColumn tmpSize = new TemplateColumn();
            tmpSize.HeaderTemplate = new CategoryItemDescrTemplate(ListItemType.Header, "Part Number", "SKU", false, 8);
            tmpSize.ItemTemplate = new CategoryItemDescrTemplate(ListItemType.Item, "Part Number", "SKU", false, 8);
            tmpSize.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            tmpSize.ItemStyle.Width = Unit.Pixel(100);
            //tmpSize.ItemStyle.Height = Unit.Pixel(40);
            grdLowPrice.Columns.Add(tmpSize);

            // Temparary remove for not crowdy
            //TemplateColumn tmpPartNumber = new TemplateColumn();
            //tmpPartNumber.HeaderTemplate = new CategoryItemDescrTemplate(ListItemType.Header, "Part Number", "SKU", false, 8);
            //tmpPartNumber.ItemTemplate = new CategoryItemDescrTemplate(ListItemType.Item, "Part Number", "SKU", false, 8);
            //tmpPartNumber.ItemStyle.CssClass = "list_left";
            //tmpPartNumber.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            //tmpPartNumber.ItemStyle.Width = Unit.Pixel(100);
            //grdLowPrice.Columns.Add(tmpPartNumber);

            TemplateColumn tmpPrice = new TemplateColumn();
            tmpPrice.HeaderTemplate = new CategoryItemDescrTemplate(ListItemType.Header, "Our Price", "Price", true, 9);
            tmpPrice.ItemTemplate = new CategoryItemDescrTemplate(ListItemType.Item, "Our Price", "Price", true, 9);
            tmpPrice.ItemStyle.CssClass = "SuperGText";
            tmpPrice.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;
            tmpPrice.ItemStyle.Width = Unit.Pixel(70);
            //tmpPrice.ItemStyle.Height = Unit.Pixel(40);
            grdLowPrice.Columns.Add(tmpPrice);

            TemplateColumn tmpAddToCart = new TemplateColumn();
            tmpAddToCart.HeaderTemplate = new CategoryAddToCartTemplate(ListItemType.Header, "ProductId", "ProductId", "VariantId");
            tmpAddToCart.ItemTemplate = new CategoryAddToCartTemplate(ListItemType.Item, "ProductId", "ProductId", "VariantId");
            //tmpAddToCart.ItemStyle.Height = Unit.Pixel(40);
            tmpAddToCart.ItemStyle.Width = Unit.Pixel(190);
            tmpAddToCart.ItemStyle.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Left;
            grdLowPrice.Columns.Add(tmpAddToCart);

            //boundcolumn example
            //DataColumn dcMemorySpeed = dtMemory.Columns["MemoryName"];
            //BoundColumn bunMemorySpeed = CreateBoundColumn(dcMemorySpeed);
            //bunMemorySpeed.HeaderText = "Memory Speed";
            //bunMemorySpeed.ItemStyle.Font.Size = FontUnit.Point(13);
            //bunMemorySpeed.ItemStyle.Font.Bold = true;
            //grdLowPrice.Columns.Add(bunMemorySpeed);

            grdLowPrice.DataSource = dvMemory;
            grdLowPrice.DataBind();

            intItemCount = grdLowPrice.Items.Count;

            return tblRange;
        }
    }
}