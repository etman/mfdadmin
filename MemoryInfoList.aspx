<%@ Page language="c#" Inherits="memoryAdmin.MemoryInfoList" CodeFile="MemoryInfoList.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>MemoryInfoList</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="1" width="780" border="0">
				<TR>
					<TD class="title"><a href="default.aspx"><IMG alt="Memory-Up.com" src="/images/memoryselector/bd_fb.png"
								border="0"></a>Laptop Hit Count</TD>
				</TR>
				<TR>
					<TD><asp:datagrid id="grdMemoryList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
							PageSize="25" GridLines="Vertical" CellPadding="3" BackColor="White" BorderWidth="1px" BorderStyle="None"
							BorderColor="#999999">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
							<AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
							<ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#000084"></HeaderStyle>
							<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
							<Columns>
								<asp:BoundColumn DataField="manufacture" SortExpression="manufacture" HeaderText="Manufacture"></asp:BoundColumn>
								<asp:BoundColumn DataField="Product_line" SortExpression="Product_line" HeaderText="Product Line"></asp:BoundColumn>
								<asp:BoundColumn DataField="model" SortExpression="Model" HeaderText="Model"></asp:BoundColumn>
								<asp:BoundColumn DataField="speed" SortExpression="Speed" HeaderText="Speed"></asp:BoundColumn>
								<asp:BoundColumn DataField="Filter" SortExpression="filter" HeaderText="Filter"></asp:BoundColumn>
								<asp:BoundColumn DataField="date_visit" SortExpression="date_visit" HeaderText="Last Visit"></asp:BoundColumn>
								<asp:BoundColumn DataField="Counter" SortExpression="Counter" HeaderText="Counter"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
