<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModelSearch.aspx.cs" Inherits="memoryAdmin.ModelSearch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:PlaceHolder ID="hldLowPrice" runat="server"></asp:PlaceHolder>
            <br />
            <asp:Panel ID="pnlLaptop" runat="server">
                <asp:GridView ID="grdLaptop" runat="server" AutoGenerateColumns="false" OnDataBound="grdLaptop_DataBound"
                    GridLines="none" Width="100%" CssClass="btext">
                    <HeaderStyle CssClass="bigBText" Font-Bold="true" />
                    <Columns>
                        <asp:TemplateField HeaderText="Laptop Search Result:" HeaderStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <img src="/memory_finders/images/MemorySelector/arrow2.gif" width="6" height="8" />
                                <asp:HyperLink ID="lnklaptopModel" runat="server" Text='<%# Bind("ModelDescr") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblModelId" runat="server" Text='<%# Bind("TableKey") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <br />
            <asp:Panel ID="pnlDesktop" runat="server">
                <asp:GridView ID="grdDesktop" runat="server" AutoGenerateColumns="false" GridLines="none"
                    Width="100%" OnDataBound="grdDesktop_DataBound" CssClass="btext">
                    <HeaderStyle CssClass="bigBText" Font-Bold="true" />
                    <Columns>
                        <asp:TemplateField HeaderText="Desktop / Server Search Result:" HeaderStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <img src="/memory_finders/images/MemorySelector/arrow2.gif" width="6" height="8" />
                                <asp:HyperLink ID="lnklaptopModel" runat="server" Text='<%# Bind("ModelDescr") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblModelId" runat="server" Text='<%# Bind("TableKey") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <br />
            <asp:Panel ID="pnlCamera" runat="server">
                <asp:GridView ID="grdDCamera" runat="server" AutoGenerateColumns="false" GridLines="none"
                    Width="100%" OnDataBound="grdDCamera_DataBound" CssClass="btext">
                    <HeaderStyle CssClass="bigBText" Font-Bold="true" />
                    <Columns>
                        <asp:TemplateField HeaderText="Digital Camera Search Result:" HeaderStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <img src="/memory_finders/images/MemorySelector/arrow2.gif" width="6" height="8" />
                                <asp:HyperLink ID="lnklaptopModel" runat="server" Text='<%# Bind("ModelDescr") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblModelId" runat="server" Text='<%# Bind("TableKey") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <br />
            <asp:Panel ID="pnlCellphone" runat="server">
                <asp:GridView ID="grdCellPhone" runat="server" AutoGenerateColumns="false" GridLines="none"
                    Width="100%" OnDataBound="grdCellPhone_DataBound" CssClass="btext">
                    <HeaderStyle CssClass="bigBText" Font-Bold="true" />
                    <Columns>
                        <asp:TemplateField HeaderText="Cell Phone Search Result:" HeaderStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <img src="/memory_finders/images/MemorySelector/arrow2.gif" width="6" height="8" />
                                <asp:HyperLink ID="lnklaptopModel" runat="server" Text='<%# Bind("ModelDescr") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblModelId" runat="server" Text='<%# Bind("TableKey") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <br />
            <asp:Panel ID="pnlPDA" runat="server">
                <asp:GridView ID="grdPDA" runat="server" AutoGenerateColumns="false" GridLines="none"
                    Width="100%" OnDataBound="grdPDA_DataBound" CssClass="btext">
                    <HeaderStyle CssClass="bigBText" Font-Bold="true" />
                    <Columns>
                        <asp:TemplateField HeaderText="PDA Search Result:" HeaderStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnklaptopModel" runat="server" Text='<%# Bind("ModelDescr") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblModelId" runat="server" Text='<%# Bind("TableKey") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <br />
            <asp:Literal ID="litNoResult" runat="server" Text="There is no result match your searching quiteria!"
                Visible="False"></asp:Literal>
        </div>
    </form>
</body>
</html>
