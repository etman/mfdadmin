namespace memoryAdmin
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for ucNewModel.
	/// </summary>
	public partial class ucNewModel : System.Web.UI.UserControl
	{

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		public void PreLoad(string strTableName)
		{
			txtTableName.Text = strTableName;
		}

		public void SetNewModelParameter(object sender, SubmitModelSearchEventArgs args)
		{
			clearAllField();

			if (args.Model == null)
			{
				SetNewModel();
			}
		}

		public SubmitModelSearchEventArgs GetSelectMode()
		{
			SubmitModelSearchEventArgs modelArgs = new SubmitModelSearchEventArgs();

			if (lstCurrManuf.Visible && lstCurrManuf.SelectedIndex > 0)
				modelArgs.Manufacturer = lstCurrManuf.SelectedValue;
			else if (txtManufacture.Visible && txtManufacture.Text.Trim() != "")
				modelArgs.Manufacturer = txtManufacture.Text.Trim();
			else
				modelArgs.Manufacturer = null;

			if (lstCurrLine.Visible && lstCurrLine.SelectedIndex > 0)
				modelArgs.ProductLine = lstCurrLine.SelectedValue;
			else if (txtProductLine.Visible && txtProductLine.Text.Trim() != "")
				modelArgs.ProductLine = txtProductLine.Text.Trim();
			else
				modelArgs.ProductLine = null;

			if (txtModel.Text.Trim() != "")
				modelArgs.Model = txtModel.Text.Trim();
			else
				modelArgs.Model = null;

			return modelArgs;
		}

		public void SetExistModel(SubmitModelSearchEventArgs args)
		{
			txtManufacture.Visible = true;
			txtProductLine.Visible = true;

			lstCurrManuf.Visible = false;
			lstCurrLine.Visible = false;
			lblOrAddManuf.Visible = false;
			lblOrAddLine.Visible = false;
			btnNewManuf.Visible = false;
			btnNewLine.Visible = false;
			
			errorInvisible();

			txtManufacture.Text = args.Manufacturer;
			txtProductLine.Text = args.ProductLine;
			txtModel.Text = args.Model;
		}

		public void SetNewModel()
		{
			txtManufacture.Visible = false;
			txtProductLine.Visible = false;

			lstCurrManuf.Visible = true;
			lstCurrLine.Visible = true;
			lblOrAddManuf.Visible = true;
			lblOrAddLine.Visible = true;
			btnNewManuf.Visible = true;
			btnNewLine.Visible = true;
			
			errorInvisible();

			lstCurrManuf.Items.Clear();
			lstCurrLine.Items.Clear();
			ListItem tempListItem = new ListItem("(Select Manufacturer)", "");
			lstCurrManuf.Items.Add(tempListItem);
			lstCurrLine.Items.Add(tempListItem);

			Mup_utilities.fillManufacture(lstCurrManuf, txtTableName.Text);
		}

		private void clearAllField()
		{
			txtManufacture.Text = "";
			txtProductLine.Text = "";
			txtModel.Text = "";
			lstCurrManuf.ClearSelection();
			lstCurrLine.ClearSelection();
		}

		public void errorInvisible()
		{
			lblManufError.Visible = false;
			lblProductLineError.Visible = false;
			lblModelError.Visible = false;
		}

		protected void lstCurrManuf_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem tempListItem;

			if (lstCurrManuf.SelectedItem.Text == "(Select Manufacturer)")
			{
				lstCurrLine.Items.Clear ();
				tempListItem = new ListItem("(Select Manufacturer)", "");
				lstCurrLine.Items.Add(tempListItem);
			}
			else
			{
				// Manufacturer was selected, remove the "(Select a Manufacturer") entry
				if (lstCurrManuf.Items[0].Value == "")
					lstCurrManuf.Items.RemoveAt(0);

				// Now populate the product line
				Mup_utilities.fillProductLine (lstCurrManuf.SelectedItem.Value, lstCurrLine, txtTableName.Text);
			}
		}

		protected void btnNewManuf_Click(object sender, System.EventArgs e)
		{
			lblOrAddManuf.Visible = false;
			lblOrAddLine.Visible = false;
			lstCurrManuf.Visible = false;
			lstCurrLine.Visible = false;
			btnNewManuf.Visible = false;
			btnNewLine.Visible = false;
			txtManufacture.Visible = true;
			txtProductLine.Visible = true;
		}

		protected void btnNewLine_Click(object sender, System.EventArgs e)
		{
			btnNewLine.Visible = false;
			txtProductLine.Visible = true;
			lblOrAddLine.Visible = false;
			lstCurrLine.Visible = false;
		}

		public bool ErrorCheck()
		{
			if (txtManufacture.Visible == true && txtManufacture.Text.Trim() == "")
			{
				lblManufError.Visible = true;
				return false;
			}
			if (lstCurrManuf.Visible == true && lstCurrManuf.SelectedIndex <= 0)
			{
				lblManufError.Visible = true;
				return false;
			}
			if (txtProductLine.Visible == true && txtProductLine.Text.Trim() == "")
			{
				lblProductLineError.Visible = true;
				return false;
			}
			if (lstCurrLine.Visible == true && lstCurrLine.SelectedIndex <= 0)
			{
				lblProductLineError.Visible = true;
				return false;
			}
			if (txtModel.Visible == true && txtModel.Text.Trim() == "")
			{
				lblModelError.Visible = true;
				return false;
			}
			else
				return true;
		}
	}
}
