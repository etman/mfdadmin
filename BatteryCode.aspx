<%@ Page language="c#" Inherits="memoryAdmin.BatteryCode" CodeFile="BatteryCode.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BatteryCode</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="780" border="0">
				<TR>
					<TD class="title" colspan="2"><IMG alt="Memory-Up.com" src="http://www.memory-up.com/merchant2/images/logo_big.gif"
							border="0">Battery Compatability</TD>
				</TR>
				<TR class="item">
					<TD colspan="2">Search:
						<asp:TextBox id="txtSearch" runat="server"></asp:TextBox>&nbsp;
						<asp:Button id="btnSubmit" runat="server" Text="Go" onclick="btnSubmit_Click"></asp:Button></TD>
				</TR>
				<TR class="item">
					<TD width="33%">
						<asp:Panel id="pnlOemCode" runat="server" Visible="False">
							Manufacturer Battery: 
<asp:Label id="lblMfCode" runat="server"></asp:Label><BR>
<asp:DataGrid id="grdCompatable" runat="server" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
								BackColor="White" CellPadding="3" AutoGenerateColumns="False">
								<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
								<ItemStyle ForeColor="#000066"></ItemStyle>
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699" Wrap="False"></HeaderStyle>
								<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
								<Columns>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
									<asp:ButtonColumn Text="Delete" CommandName="Delete"></asp:ButtonColumn>
									<asp:BoundColumn DataField="Mf_battery_code" SortExpression="Mf_battery_code" HeaderText="Mf Code"></asp:BoundColumn>
									<asp:BoundColumn DataField="Oem_battery_code" SortExpression="Oem_battery_code" HeaderText="OEM Code"></asp:BoundColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
							</asp:DataGrid><BR>
<asp:TextBox id="txtOemCode" runat="server"></asp:TextBox>
<asp:Button id="btnAddNew" runat="server" Text="Add New"></asp:Button>
						</asp:Panel></TD>
					<TD width="67%">
						<asp:DataGrid id="grdBatteryList" runat="server" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
							BackColor="White" CellPadding="3" AutoGenerateColumns="False">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle ForeColor="#000066"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699" Wrap="False"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
								<asp:BoundColumn DataField="Mf_battery_code" SortExpression="Mf_battery_code" HeaderText="Mf Code"></asp:BoundColumn>
								<asp:BoundColumn DataField="Oem_battery_code" SortExpression="Oem_battery_code" HeaderText="OEM Code"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:DataGrid></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
