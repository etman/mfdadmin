<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MemoryScannerHistory.aspx.cs" Inherits="MemoryScannerHistory" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <table style="width: 800px" class="item">
        <tr>
            <td style="width: 100px">
                Date From:</td>
            <td style="width: 120px">
                <asp:UpdatePanel ID="pnlDateFrom" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtDateFrom" runat="server" Columns="8" AutoPostBack="True" OnTextChanged="txtDateFrom_TextChanged"></asp:TextBox>
                        <asp:ImageButton ID="imgDateFrom" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                        <cc1:CalendarExtender ID="calDateFrom" runat="server" PopupButtonID="imgDateFrom"
                            TargetControlID="txtDateFrom">
                        </cc1:CalendarExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
                &nbsp;
            </td>
            <td>
                <asp:RangeValidator ID="rngDateFrom" runat="server" ErrorMessage="Please enter a valid date format (mm/dd/yyyy)."
                    ControlToValidate="txtDateFrom" MaximumValue="12/31/2099" MinimumValue="1/1/2000"
                    Type="Date" Display="Dynamic"></asp:RangeValidator></td>
        </tr>
        <tr>
            <td style="width: 100px">
                DateTo:</td>
            <td style="width: 120px">
                <asp:UpdatePanel ID="pnlDateTo" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtDateTo" runat="server" Columns="8" AutoPostBack="True" OnTextChanged="txtDateTo_TextChanged"></asp:TextBox>
                        <asp:ImageButton ID="imgDateTo" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                        <cc1:CalendarExtender ID="calDateTo" runat="server" PopupButtonID="imgDateTo" TargetControlID="txtDateTo">
                        </cc1:CalendarExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <asp:RangeValidator ID="rngDateTo" runat="server" ErrorMessage="Please enter a valid date format (mm/dd/yyyy)."
                    ControlToValidate="txtDateTo" MaximumValue="12/31/2099" MinimumValue="1/1/2000"
                    Type="Date" Display="Dynamic"></asp:RangeValidator></td>
        </tr>
        <tr>
            <td style="width: 100px">
                W/ Email Only:</td>
            <td style="width: 120px">
                <asp:CheckBox ID="chkEmailOnly" runat="server" /></td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 120px">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" /></td>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <div class="item">Top Model:</div>
    <asp:UpdatePanel ID="updTopModel" runat="server">
        <ContentTemplate>
            <asp:GridView ID="grdTopModel" runat="server" AutoGenerateColumns="false" Width="1200px" CssClass="item">
                <HeaderStyle BackColor="lightBlue" />
                <AlternatingRowStyle BackColor="lightgray" />
                <Columns>
                    <asp:BoundField HeaderText="Count" DataField="ModelCount" />
                    <asp:BoundField HeaderText="Scanned Manuf." DataField="Manufacturer" />
                    <asp:BoundField HeaderText="Scanned Model" DataField="Model" />
                    <asp:BoundField HeaderText="Chip Set" DataField="Chipset" />
                    <asp:BoundField HeaderText="Motherboard" DataField="Motherboard" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <br />
    <div class="item">Scan History:</div>
    <asp:UpdatePanel ID="updScannerHistory" runat="server">
        <ContentTemplate>
            <asp:GridView ID="grdScannerHistory" runat="server" AutoGenerateColumns="false" Width="1200px" CssClass="item" OnRowDataBound="grdScannerHistory_RowDataBound">
                <HeaderStyle BackColor="lightBlue" />
                <AlternatingRowStyle BackColor="lightgray" />
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <%# Container.DisplayIndex + 1 %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Date/Time" DataField="TimeStamp" />
                    <asp:TemplateField HeaderText="IP">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkIP" runat="server" Text='<%# Bind("RemoteHost") %>' 
                                NavigateUrl='<%# Bind("HyperLink") %>'  Target="_blank"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Scanned Manuf." DataField="Manufacturer" />
                    <asp:TemplateField HeaderText="Scanned Model">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkScannedModel" runat="server" Text='<%# Bind("Model") %>' 
                                NavigateUrl='<%# Bind("HyperLink") %>'  Target="_blank"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Chip Set">
                        <ItemTemplate>
                            <asp:Label ID="lblChipSet" runat="server" Text='<%# Bind("Chipset") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Motherboard">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkMotherboard" runat="server" Text='<%# Bind("Motherboard") %>' 
                                NavigateUrl='<%# Bind("HyperLink") %>' Target="_blank"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lblScanResult" runat="server" Text='<%# Bind("SystemResults") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Email" DataField="UserEmail" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
