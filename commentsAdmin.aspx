<%@ Page language="c#" Inherits="memoryAdmin.commentsAdmin" CodeFile="commentsAdmin.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>commentsAdmin</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE cellSpacing="0" cellPadding="5" width="780" border="0">
				<TR>
					<TD><asp:imagebutton id="imgHome" runat="server" ImageUrl="/images/memoryselector/bd_fb.png"></asp:imagebutton><FONT size="4"><STRONG>Comments 
								Administrator Page</STRONG></FONT></TD>
				</TR>
				<TR>
					<TD><asp:datagrid id="comments" runat="server" AutoGenerateColumns="False" BorderColor="#999999" BorderStyle="None"
							BorderWidth="1px" BackColor="White" CellPadding="3" GridLines="Vertical">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
							<AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
							<ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#000084"></HeaderStyle>
							<FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
							<Columns>
								<asp:BoundColumn DataField="comment_id" HeaderText="Id"></asp:BoundColumn>
								<asp:BoundColumn DataField="post_date" HeaderText="Date"></asp:BoundColumn>
								<asp:BoundColumn DataField="nick_name" HeaderText="Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="email" HeaderText="E-Mail"></asp:BoundColumn>
								<asp:BoundColumn DataField="subject" HeaderText="subject"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="comment">
									<ItemTemplate>
										<textarea id="txtComments" cols="30" rows="5" readonly><%# DataBinder.Eval(Container.DataItem, "comment") %></textarea>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:ButtonColumn Text="Delete" CommandName="Delete"></asp:ButtonColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
