<%@ Reference Page="~/newModel.aspx" %>
<%@ Register TagPrefix="uc1" TagName="ucAdminHeader" Src="ucAdminHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucModelSelector" Src="ucModelSelector.ascx" %>
<%@ Page language="c#" Inherits="memoryAdmin.CellPhoneUpdate" CodeFile="CellPhoneUpdate.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ucNewModel" Src="ucNewModel.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CellPhoneUpdate</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
		<script language="javascript" type="text/javascript">
			function CheckFields()
			{
				if (isNaN(document.all("txtInternal").value))
				{
					alert('Please enter a valid number for internal memory size!');
					return false;
				}
			}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="2"><uc1:ucadminheader id="ucHeader" runat="server"></uc1:ucadminheader></TD>
				</TR>
				<TR>
					<TD width="50" rowSpan="2">&nbsp;</TD>
					<TD><uc1:ucmodelselector id="ucModelSelect" runat="server"></uc1:ucmodelselector></TD>
				</TR>
				<TR>
					<TD><asp:panel id="pnlUpdate" Runat="server" Visible="False">
							<TABLE class="item" id="Table2" cellSpacing="1" cellPadding="3" width="780" border="0">
								<TR>
									<TD width="200"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD colSpan="3">
										<asp:textbox id="txtPhoneId" runat="server" Visible="False"></asp:textbox>
										<asp:label id="lblModelExist" runat="server" Visible="False" ForeColor="Red">This model already exist!!</asp:label>
										<asp:label id="lblUpdateError" runat="server" Visible="False" ForeColor="Red">There is an error when updating camera selector!</asp:label>
										<asp:Label id="lblViewed" runat="server"></asp:Label><BR>
										<asp:Label id="lblLastUpdate" runat="server"></asp:Label></TD>
								</TR>
                                <tr>
                                    <td colspan="3">
                                        <asp:Button ID="btnUpdate2" runat="server" OnClick="btnUpdate2_Click" Text="Update" /></td>
                                </tr>
								<TR>
									<TD colSpan="3">
										<uc1:ucnewmodel id="ucNewModelSelect" runat="server"></uc1:ucnewmodel></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Memory Card Type:</STRONG></TD>
									<TD>
										<asp:checkboxlist id="chkMemoryType" runat="server" CssClass="item" RepeatDirection="Horizontal" Width="537px"
											Height="60px" RepeatColumns="2"></asp:checkboxlist></TD>
									<TD><FONT color="red">(Requied)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Internal Memory Size:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtInternal" runat="server"></asp:TextBox></TD>
									<TD><FONT color="red"></FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Min. Memory Size:</STRONG></TD>
									<TD>
										<asp:radiobuttonlist id="radMinMemorySize" runat="server" CssClass="item" RepeatDirection="Horizontal">
											<asp:ListItem Value="32">32M</asp:ListItem>
											<asp:ListItem Value="64">64M</asp:ListItem>
											<asp:ListItem Value="128">128M</asp:ListItem>
											<asp:ListItem Value="256">256M</asp:ListItem>
											<asp:ListItem Value="512">512M</asp:ListItem>
											<asp:ListItem Value="1024">1G</asp:ListItem>
											<asp:ListItem Value="2048">2G</asp:ListItem>
											<asp:ListItem Value="4096">4G</asp:ListItem>
                                            <asp:ListItem Value="8192">8G</asp:ListItem>
                                            <asp:ListItem Value="16384">16G</asp:ListItem>
                                            <asp:ListItem Value="32768">32G</asp:ListItem>
                                            <asp:ListItem Value="65536">64G</asp:ListItem>
										</asp:radiobuttonlist>
										<asp:label id="lblMinMemoryError" runat="server" Visible="False" ForeColor="Red">Please adjust max. or min. memory size!</asp:label></TD>
									<TD><FONT color="red">(Requied)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Max. Memory Size:</STRONG></TD>
									<TD>
										<asp:radiobuttonlist id="radMaxMemorySize" runat="server" CssClass="item" RepeatDirection="Horizontal">
											<asp:ListItem Value="32">32M</asp:ListItem>
											<asp:ListItem Value="64">64M</asp:ListItem>
											<asp:ListItem Value="128">128M</asp:ListItem>
											<asp:ListItem Value="256">256M</asp:ListItem>
											<asp:ListItem Value="512">512M</asp:ListItem>
											<asp:ListItem Value="1024">1G</asp:ListItem>
											<asp:ListItem Value="2048">2G</asp:ListItem>
											<asp:ListItem Value="4096">4G</asp:ListItem>
                                            <asp:ListItem Value="8192">8G</asp:ListItem>
                                            <asp:ListItem Value="16384">16G</asp:ListItem>
                                            <asp:ListItem Value="32768">32G</asp:ListItem>
                                            <asp:ListItem Value="65536">64G</asp:ListItem>
										</asp:radiobuttonlist>
										<asp:label id="lblMaxMemoryError" runat="server" Visible="False" ForeColor="Red">Please select max. memory size!</asp:label></TD>
									<TD><FONT color="red">(Requied)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Battery:</STRONG></TD>
									<TD>
										<asp:dropdownlist id="lstBattery" runat="server">
											<asp:ListItem Value="(Select Battery)">(Select Battery)</asp:ListItem>
										</asp:dropdownlist>
										<asp:textbox id="txtBatteryDesc" runat="server" Width="352px" ReadOnly="True"></asp:textbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Manufacturer Battery Code:</STRONG></TD>
									<TD>
										<asp:textbox id="txtBatteryCode" runat="server" MaxLength="50"></asp:textbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Camera Mega Pixels:</STRONG></TD>
									<TD>
										<asp:textbox id="txtMegaPixels" runat="server" MaxLength="10"></asp:textbox>Mega 
										Pixels</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>LCD Screen:</STRONG></TD>
									<TD>
										<asp:textbox id="txtLcd" runat="server" Width="420px" MaxLength="50"></asp:textbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Operating System:</STRONG></TD>
									<TD>
										<asp:textbox id="txtOS" runat="server"></asp:textbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="#d3d3d3"><STRONG>Release Date:</STRONG></TD>
									<TD>Month
										<asp:TextBox id="txtReleaseMonth" runat="server" Columns="2"></asp:TextBox>&nbsp;&nbsp;&nbsp; 
										Year
										<asp:TextBox id="txtReleaseYear" runat="server" Columns="4"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Java Enhanced:</STRONG></TD>
									<TD>
										<asp:checkbox id="chkJava" runat="server" Text="Yes"></asp:checkbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Blue Tooth:</STRONG></TD>
									<TD>
										<asp:checkbox id="chkBlueTooth" runat="server" Text="Yes"></asp:checkbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="#d3d3d3"><STRONG>SDIO:</STRONG></TD>
									<TD>
										<asp:RadioButtonList id="radSDIO" runat="server" CssClass="item" RepeatDirection="Horizontal" RepeatLayout="Flow">
											<asp:ListItem Value="N" Selected="True">None</asp:ListItem>
											<asp:ListItem Value="S">SDIO</asp:ListItem>
											<asp:ListItem Value="W">SDIO Now</asp:ListItem>
										</asp:RadioButtonList></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Music File Supported:</STRONG></TD>
									<TD>&nbsp;Polyphonic
										<asp:textbox id="txtPolyphonic" runat="server" Columns="3"></asp:textbox>
										<asp:checkboxlist id="chkMusic" runat="server" CssClass="item" RepeatDirection="Horizontal" RepeatLayout="Flow">
											<asp:ListItem Value="MIDI">MIDI</asp:ListItem>
											<asp:ListItem Value="MP3">MP3</asp:ListItem>
											<asp:ListItem Value="AAC">AAC</asp:ListItem>
											<asp:ListItem Value="WMA">WMA</asp:ListItem>
											<asp:ListItem Value="RM">RM</asp:ListItem>
										</asp:checkboxlist></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Cellular Network:</STRONG></TD>
									<TD>
										<asp:checkboxlist id="chkNetwork" runat="server" CssClass="item" RepeatDirection="Horizontal" RepeatColumns="5">
											<asp:ListItem Value="GSM_850">GSM_850</asp:ListItem>
											<asp:ListItem Value="GSM_900">GSM_900</asp:ListItem>
											<asp:ListItem Value="GSM_1800">GSM_1800</asp:ListItem>
											<asp:ListItem Value="GSM_1900">GSM_1900</asp:ListItem>
											<asp:ListItem Value="TDMA">TDMA</asp:ListItem>
											<asp:ListItem Value="CDMA_800">CDMA_800</asp:ListItem>
											<asp:ListItem Value="CDMA_1900">CDMA_1900</asp:ListItem>
											<asp:ListItem Value="WCDMA">WCDMA</asp:ListItem>
											<asp:ListItem Value="CDMA2000">CDMA2000</asp:ListItem>
											<asp:ListItem Value="TDMA_Edge">TDMA_Edge</asp:ListItem>
										</asp:checkboxlist></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Notes:</STRONG></TD>
									<TD colSpan="2">
										<asp:textbox id="txtNotes" runat="server" Columns="65" TextMode="MultiLine" Rows="6"></asp:textbox></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Internal Comments:</STRONG></TD>
									<TD colSpan="2">
										<asp:textbox id="txtComments" runat="server" Columns="65" TextMode="MultiLine" Rows="6"></asp:textbox></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Picture:</STRONG></TD>
									<TD>
										<asp:button id="btnAddPicture" runat="server" Text="Add Picture" onclick="btnAddPicture_Click"></asp:button>
										<asp:image id="imgPicture" runat="server"></asp:image>
										<asp:button id="btnDeletePicture" runat="server" Text="Delete Picture" onclick="btnDeletePicture_Click"></asp:button></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD>&nbsp;</TD>
									<TD>&nbsp;</TD>
									<TD>&nbsp;</TD>
								</TR>
								<TR>
									<TD></TD>
									<TD>
										<asp:button id="btnUpdate" runat="server" Text="Update" onclick="btnUpdate_Click"></asp:button>
										<asp:button id="btnAdd" runat="server" ForeColor="Navy" Text="Add" onclick="btnAdd_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:button id="btnCancelUpdate" runat="server" Text="Cancel" onclick="btnCancelUpdate_Click"></asp:button>
										<asp:button id="btnCancelAdd" runat="server" ForeColor="Navy" Text="Cancel" onclick="btnCancelAdd_Click"></asp:button></TD>
									<TD>
										<asp:Button id="btnDelete" runat="server" Text="Delete" onclick="btnDelete_Click"></asp:Button></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="pnlInformation" runat="server" Visible="False">
							<TABLE class="item" id="Table3" cellSpacing="1" cellPadding="3" width="780" border="0">
								<TR>
									<TD width="200"><STRONG>Manufacturer:</STRONG></TD>
									<TD>
										<asp:Label id="lblManufacture" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Prodoct Line:</STRONG></TD>
									<TD>
										<asp:Label id="lblProductLine" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 13px"><STRONG>Model:</STRONG></TD>
									<TD style="HEIGHT: 13px">
										<asp:Label id="lblModel" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Memory Card Type:</STRONG></TD>
									<TD>
										<asp:Label id="lblMemoryType" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Internal Memory Size:</STRONG></TD>
									<TD>
										<asp:Label id="lblInterMemorySize" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Min. Memory Size:</STRONG></TD>
									<TD>
										<asp:Label id="lblMinMemorySize" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Max. Memory Size:</STRONG></TD>
									<TD>
										<asp:Label id="lblMaxMemorySize" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Battery:</STRONG></TD>
									<TD>
										<asp:Label id="lblBattery" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Manufacturer Battery Code:</STRONG></TD>
									<TD>
										<asp:Label id="lblBatteryCode" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Mega Pixels:</STRONG></TD>
									<TD>
										<asp:Label id="lblMegaPixels" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>LCD Screen:</STRONG></TD>
									<TD>
										<asp:Label id="lblLcd" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Operatiing System:</STRONG></TD>
									<TD>
										<asp:Label id="lblOS" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Release Date:</STRONG></TD>
									<TD>
										<asp:Label id="lblReleaseMonth" runat="server"></asp:Label>,
										<asp:Label id="lblReleaseYear" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Java Enhanced:</STRONG></TD>
									<TD>
										<asp:Label id="lblJava" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Blue Tooth:</STRONG></TD>
									<TD>
										<asp:Label id="lblBlueTooth" runat="server" Visible="False"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>SDIO:</STRONG></TD>
									<TD>
										<asp:Label id="lblSDIO" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Music File Supported:</STRONG></TD>
									<TD>
										<asp:Label id="lblMusic" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Cellular Network:</STRONG></TD>
									<TD>
										<asp:Label id="lblNetwork" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Notes:</STRONG></TD>
									<TD>
										<asp:Label id="lblNotes" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Internal Comments:</STRONG></TD>
									<TD>
										<asp:Label id="lblComments" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Picture:</STRONG></TD>
									<TD>
										<asp:Image id="imgShowPicture" runat="server"></asp:Image></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD>
										<asp:Button id="btnShowUpdate" runat="server" Text="Update" onclick="btnShowUpdate_Click"></asp:Button></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
