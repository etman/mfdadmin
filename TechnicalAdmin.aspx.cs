using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for TechnicalAdmin.
	/// </summary>
	public partial class TechnicalAdmin : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnReset;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            //if (Session["LoginName"] == null && Request.Cookies["LoginName"] == null)
            //{
            //    Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
            //}
            //else if (Request.Cookies["LoginName"].Value.ToUpper() != "MUP")
            //{
            //    Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
            //}

			
			System.Data.SqlClient.SqlDataReader drCase;
			System.Data.SqlClient.SqlDataReader drRMA;

			if (!Page.IsPostBack)
			{
				string caseNumber = Request.Params["case_number"];
				//string status;

				String sql;
				MupDbConnection dsConn = new MupDbConnection();

				sql = "Select * from CaseRequest where Case_number = "+ caseNumber;
				drCase = dsConn.GetReader(sql);

				if (drCase == null || !drCase.Read ())
				{
					dsConn.Dispose ();
					MupGenericException ex = new MupGenericException ("Case Display Error",
						"Register_Click ()", "MupRegistration.ascx.cs");
					throw ex;
				}

				lblCaseNumber.Text = caseNumber;
				lblRequestType.Text = drCase["Request_type"].ToString();
				lblOrderNumber.Text = drCase["Order_number"].ToString();
				lblInvoiceNumber.Text = drCase["Invoice_number"].ToString();
				lblItemPurchase.Text = drCase["Item"].ToString();
				lblModel.Text = drCase["System_model"].ToString();
				lblFirstName.Text = drCase["First_name"].ToString();
				lblLastName.Text = drCase["Last_name"].ToString();
				lblEmail.Text = drCase["Email"].ToString();
				lblPhone.Text = drCase["Phone"].ToString();
				lblAddress1.Text = drCase["Address1"].ToString();
				lblAddress2.Text = drCase["Address2"].ToString();
				lblCity.Text = drCase["City"].ToString();
				lblState.Text = drCase["State"].ToString();
				lblZip.Text = drCase["Zip"].ToString();
				lblCaseReceiveDate.Text = drCase["receive_date"].ToString();
				lblVerificationDate.Text = drCase["verify_date"].ToString();
				lblCompleteDate.Text = drCase["Complete_date"].ToString();

				txtProblem.Text = drCase["Problem"].ToString();
				txtAnswer.Text = drCase["Answer"].ToString();
				txtNotes.Text = drCase["Notes"].ToString();
				lblLastVisit.Text = drCase["Last_visit"].ToString();
				lblUpdateDate.Text = drCase["update_date"].ToString();
				if (drCase["Request_type"].ToString() == "Technical")
				{
					radRMA.Enabled = false;
					radItemReceive.Enabled = false;
					radApprove.Enabled = false;
					btnCancel.Enabled = false;
				}
				
				// Swich status step by step by disabled some status
				switch (drCase["status"].ToString().Substring(0, 1))
				{
					case "a": // Case Received
						radCaseReceive.Checked = true;
						radRMA.Enabled = false;
						radItemReceive.Enabled = false;
						radApprove.Enabled = false;
						radComplete.Enabled = false;
						btnEmailRMA.Enabled = false;
						radCancel.Enabled = false;
						break;
					case "b": // Case Verification
						radCaseReceive.Enabled = false;
						radVerification.Checked = true;
						radItemReceive.Enabled = false;
						radApprove.Enabled = false;
						radComplete.Enabled = false;
						btnEmailRMA.Enabled = false;
						radCancel.Enabled = false;
						if (drCase["Request_type"].ToString() == "Technical")
						{
							radComplete.Enabled = true;
						}
						break;
					case "c": // Processing
						radCaseReceive.Enabled = false;
						radVerification.Checked = true;
						radItemReceive.Enabled = false;
						radApprove.Enabled = false;
						radComplete.Enabled = false;
						btnEmailRMA.Enabled = false;
						radCancel.Enabled = false;
						if (drCase["Request_type"].ToString() == "Technical")
						{
							radComplete.Enabled = true;
						}
						break;
					case "d": // RMA Number
						radCaseReceive.Enabled = false;
						radVerification.Enabled = false;
						radRMA.Checked = true;
						radApprove.Enabled = false;
						radComplete.Enabled = false;
						btnEmailRMA.Enabled = true;
						radCancel.Enabled = false;
						break;
					case "e": // Item Received
						radCaseReceive.Enabled = false;
						radVerification.Enabled = false;
						radRMA.Enabled = false;
						radItemReceive.Checked = true;
						radComplete.Enabled = false;
						btnEmailRMA.Enabled = false;
						radCancel.Enabled = false;
						break;
					case "f": // Pending
						radCaseReceive.Enabled = false;
						radVerification.Enabled = false;
						radRMA.Enabled = false;
						radItemReceive.Checked = true;
						radComplete.Enabled = false;
						btnEmailRMA.Enabled = false;
						radCancel.Enabled = false;
						break;
					case "g": // Approve
						radCaseReceive.Enabled = false;
						radVerification.Enabled = false;
						radRMA.Enabled = false;
						radItemReceive.Enabled = false;
						radApprove.Checked = true;
						btnEmailRMA.Enabled = false;
						radCancel.Enabled = false;
						break;
					case "h": // Complete
						radCaseReceive.Enabled = false;
						radVerification.Enabled = false;
						radRMA.Enabled = false;
						radItemReceive.Enabled = false;
						radApprove.Enabled = false;
						radComplete.Checked = true;
						btnEmailRMA.Enabled = false;
						radCancel.Enabled = false;
						break;
					case "s": // Cancel
						radCaseReceive.Enabled = false;
						radVerification.Enabled = false;
						radRMA.Enabled = false;
						radItemReceive.Enabled = false;
						radApprove.Enabled = false;
						radComplete.Enabled = false;
						radCancel.Checked = true;
						break;
				}

				if (drCase["Request_type"].ToString() == "RMA")
				{
					MupDbConnection dsConnRMA = new MupDbConnection();
					

					sql = "Select * from RMA where case_number = " + caseNumber;
					drRMA = dsConnRMA.GetReader(sql);

					if (drRMA == null || !drRMA.Read ())
					{
						dsConnRMA.Dispose ();
						MupGenericException ex = new MupGenericException ("RMA Display Error",
							"Register_Click ()", "MupRegistration.ascx.cs");
						throw ex;
					}

					lblRMANumber.Text = drRMA["RMA_Number"].ToString();
					lblRMAType.Text = drRMA["RMA_type"].ToString();
					lblRMADate.Text = drRMA["Giveout_date"].ToString();
					lblItemReceiveDate.Text = drRMA["Item_receive_date"].ToString();
					lblApproveDate.Text = drRMA["Approve_date"].ToString();
					lblCancelDate.Text = drRMA["Cancel_date"].ToString();

					dsConnRMA.Dispose();
					dsConnRMA = null;
				}

				dsConn.Dispose();
				dsConn = null;

				// Display technial support follow up second try
				DataSet dsFollow2;
				MupDbConnection dsConnFollow2 = new MupDbConnection();
				sql = "Select Line_number, prob_desc, prob_date, prob_answer, publish, mod_problem from TechFollowUp where case_number = " + lblCaseNumber.Text + " order by Line_number";
				dsFollow2 = dsConnFollow2.CreateDataSet();
				dsConnFollow2.FillDataSet(sql, "followList", ref dsFollow2);
				dtlFollowUp.DataSource = dsFollow2;
				dtlFollowUp.DataKeyField = "Line_number";
				dtlFollowUp.DataBind();
			
				dsConnFollow2.Dispose();
				dsConnFollow2 = null;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.imgHome.Click += new System.Web.UI.ImageClickEventHandler(this.imgHome_Click);

		}
		#endregion

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			String sql;
            MupDbConnection dsConn = new MupDbConnection();
            MupDbConnection dsConnRMA = new MupDbConnection();
            MupDbConnection dsConnFollowUpdate = new MupDbConnection();

			try
			{
				sql = "Update CaseRequest set ";
				if (radCaseReceive.Checked)
					sql += "Status = 'a: Case Received', ";
				else if (radVerification.Checked && lblVerificationDate.Text == "")
				{
					sql += "verify_date = '" + DateTime.Now + "', ";
					sql += "Status = 'b: Case Verification', ";
				}
				else if (radRMA.Checked)
					sql += "Status = 'd: RMA Number', ";
				else if (radItemReceive.Checked)
					sql += "Status = 'e: Item Received', ";
				else if (radApprove.Checked)
					sql += "Status = 'g: Approve', ";
				else if (radComplete.Checked && lblCompleteDate.Text == "")
				{
					sql += "complete_date = '" + DateTime.Now + "', ";
					sql += "Status = 'h: Complete', ";
				}

				sql += "Answer = '" + txtAnswer.Text.Replace("'", "''") + "', ";
				sql += "Notes = '" + txtNotes.Text.Replace("'", "''") + "', ";
				sql += "Update_date = '" + DateTime.Now + "' ";
				sql += "Where Case_Number = '" + lblCaseNumber.Text + "' ";

				if (dsConn.ExecSql(sql) >= 0)
				{
					sql = "Update RMA set ";
					if (radCaseReceive.Checked)
						sql += "status = 'a: Case Received' ";
					else if (radVerification.Checked)
						sql += "Status = 'b: Case Verification' ";
					else if (radRMA.Checked)
						sql += "Status = 'd: RMA Number' ";
					else if (radItemReceive.Checked)
						sql += "Status = 'e: Item Received' ";
					else if (radApprove.Checked)
						sql += "Status = 'g: Approve' ";
					else if (radComplete.Checked)
						sql += "Status = 'h: Complete' ";
					else if (radCancel.Checked)
						sql += "Status = 's: Canceled' ";
					
					if (radRMA.Checked && lblRMADate.Text == "")
						sql += ", Giveout_Date = '" + DateTime.Now + "' ";
					else if (radItemReceive.Checked && lblItemReceiveDate.Text == "")
						sql += ", item_receive_Date = '" + DateTime.Now + "' ";
					else if (radApprove.Checked && lblApproveDate.Text == "")
						sql += ", Approve_Date = '" + DateTime.Now + "' ";
					else if (radCancel.Checked && lblUpdateDate.Text == "")
						sql += ", Cancel_date = '" + DateTime.Now + "' ";

					
					sql += "Where Case_number = '" + lblCaseNumber.Text + "' ";

					dsConnRMA.ExecSql(sql);
				}
                dsConnRMA.Dispose();
                dsConnRMA = null;

				dsConn.Dispose();
				dsConn = null;
				int lineNumber = 0;

				

				for (int i = 0; i < dtlFollowUp.Items.Count; i++)
				{
					if (((TextBox) dtlFollowUp.Items[i].FindControl("txtProblemAns")).Text != "")
					{
						lineNumber = i + 1;
						sql = "Update TechFollowUp set ";
						sql += "prob_answer = '" + ((TextBox) dtlFollowUp.Items[i].FindControl("txtProblemAns")).Text.Replace("'", "''") + "', ";
						sql += "Answer_date = '" + DateTime.Now + "', ";
						if (((CheckBox) dtlFollowUp.Items[i].FindControl("chkPublish")).Checked)
							sql += "publish = 1, ";
						else
							sql += "publish = 0, ";
						sql += "mod_problem = '" + ((TextBox) dtlFollowUp.Items[i].FindControl("txtModProblem")).Text.Replace("'", "''") + "' ";
						sql += "where case_number = '" + lblCaseNumber.Text + "' ";
						sql += "and Line_number = '" + lineNumber + "' ";

						dsConnFollowUpdate.ExecSql(sql);
					}
				}

				lineNumber++;
				if (txtPostAnswer.Text != "")
				{
					sql = "Insert into TechFollowUp (Case_number, Line_number, Prob_desc, Prob_answer, Answer_date, Mod_problem, publish, Follow_type, Updater) ";
					sql += "values ('" + lblCaseNumber.Text + "', '" + lineNumber + "', '', ";
					sql += "'" + txtPostAnswer.Text + "', '" + DateTime.Now + "', ";
					sql += "'', 0, 'Memoryup', 'mup')";

					dsConnFollowUpdate.ExecSql(sql);
				}

				dsConnFollowUpdate.Dispose();
				dsConnFollowUpdate = null;

				Response.Redirect("TechnicalAdmin.aspx?case_number=" + lblCaseNumber.Text);

			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
                if (dsConn != null)
                {
                    dsConn.Dispose();
                    dsConn = null;
                }
                if (dsConnRMA != null)
                {
                    dsConnRMA.Dispose();
                    dsConnRMA = null;
                }
                if (dsConnFollowUpdate != null)
                {
                    dsConnFollowUpdate.Dispose();
                    dsConnFollowUpdate = null;
                }
			}
		}

		private void imgHome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("default.aspx");
		}

		protected void btnEmailRMA_Click(object sender, System.EventArgs e)
		{
			Response.Write("<script language=\"javascript\">window.open('EMailRMA.aspx?case_number=" + lblCaseNumber.Text + "','Email','toolbar=no,center=yes,location=no,titlebar=no,resizable=no,status=no,scrollbars=no,menubar=no,width=600,height=400');</script>");
		}

		protected void btnList_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("CustomerSupportAdmin.aspx");
		}

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			String sql;
            MupDbConnection dsConn = new MupDbConnection();

			try
			{
				sql = "Update CaseRequest set ";
				sql += "Status = 's: Canceled', ";
				sql += "Update_date = '" + DateTime.Now + "' ";
				sql += "Where Case_Number = '" + lblCaseNumber.Text + "' ";
				dsConn.ExecSql(sql);

				sql = "Update RMA set ";
				sql += "Status = 's: Canceled', ";
				sql += "Cancel_date = '" + DateTime.Now + "' ";
				sql += "Where Case_Number = '" + lblCaseNumber.Text + "' ";
				dsConn.ExecSql(sql);

				dsConn.Dispose();
				dsConn = null;

				Response.Redirect("TechnicalAdmin.aspx?case_number=" + lblCaseNumber.Text);
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
                if (dsConn != null)
                {
                    dsConn.Dispose();
                    dsConn = null;
                }
			}
		}

		protected void Reset1_ServerClick(object sender, System.EventArgs e)
		{
		
		}
	}
}
