<%@ Reference Page="~/newModel.aspx" %>
<%@ Register TagPrefix="uc1" TagName="ucNewModel" Src="ucNewModel.ascx" %>
<%@ Page language="c#" Inherits="memoryAdmin.PDAUpdate" CodeFile="PDAUpdate.aspx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ucAdminHeader" Src="ucAdminHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucModelSelector" Src="ucModelSelector.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PDAUpdate</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
		<script language="javascript" type="text/javascript">
			function CheckFields()
			{
				if (isNaN(document.all("txtInternal").value))
				{
					alert('Please enter a valid number for internal memory size!');
					return false;
				}
			}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colspan="2">
						<uc1:ucAdminHeader id="ucHeader" runat="server"></uc1:ucAdminHeader></TD>
				</TR>
				<TR>
					<TD width="50" rowspan="2"></TD>
					<TD>
						<uc1:ucModelSelector id="ucModelSelect" runat="server"></uc1:ucModelSelector></TD>
				</TR>
				<TR>
					<TD>
						<asp:Panel id="pnlUpdate" runat="server" Visible="False">
							<TABLE class="item" id="Table2" cellSpacing="1" cellPadding="3" width="780" border="0">
								<TR>
									<TD width="200"></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD colSpan="3">
										<P>
											<asp:textbox id="txtPDAId" runat="server" Visible="False"></asp:textbox>
											<asp:label id="lblModelExist" runat="server" Visible="False" ForeColor="Red">This model already exist!!</asp:label>
											<asp:label id="lblUpdateError" runat="server" Visible="False" ForeColor="Red">There is an error when updating camera selector!</asp:label>
											<asp:Label id="lblViewed" runat="server"></asp:Label><BR>
											<asp:Label id="lblLastUpdate" runat="server"></asp:Label></P>
									</TD>
								</TR>
                                <tr>
                                    <td colspan="3">
                                        <asp:Button ID="btnUpdate2" runat="server" OnClick="btnUpdate2_Click" Text="Update" /></td>
                                </tr>
								<TR>
									<TD colSpan="3">
										<uc1:ucNewModel id="ucNewModelSelect" runat="server"></uc1:ucNewModel></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Memory Card Type:</STRONG></TD>
									<TD>
										<asp:checkboxlist id="chkMemoryType" runat="server" RepeatColumns="2" Height="60px" Width="537px"
											RepeatDirection="Horizontal" CssClass="item"></asp:checkboxlist></TD>
									<TD><FONT color="red">(Requied)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Internal Memory Size (RAM):</STRONG></TD>
									<TD>
										<asp:TextBox id="txtInternalRAM" runat="server"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Internal Memory Size (ROM):</STRONG></TD>
									<TD>
										<asp:TextBox id="txtInternalROM" runat="server"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Min. Memory Size:</STRONG></TD>
									<TD>
										<asp:radiobuttonlist id="radMinMemorySize" runat="server" RepeatDirection="Horizontal" CssClass="item">
											<asp:ListItem Value="32">32M</asp:ListItem>
											<asp:ListItem Value="64">64M</asp:ListItem>
											<asp:ListItem Value="128">128M</asp:ListItem>
											<asp:ListItem Value="256">256M</asp:ListItem>
											<asp:ListItem Value="512">512M</asp:ListItem>
											<asp:ListItem Value="1024">1G</asp:ListItem>
											<asp:ListItem Value="2048">2G</asp:ListItem>
											<asp:ListItem Value="4096">4G</asp:ListItem>
                                            <asp:ListItem Value="8192">8G</asp:ListItem>
                                            <asp:ListItem Value="16384">16G</asp:ListItem>
                                            <asp:ListItem Value="32768">32G</asp:ListItem>
                                            <asp:ListItem Value="65536">64G</asp:ListItem>
										</asp:radiobuttonlist>
										<asp:label id="lblMinMemoryError" runat="server" Visible="False" ForeColor="Red">Please adjust max. or min. memory size!</asp:label></TD>
									<TD><FONT color="red">(Requied)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Max. Memory Size:</STRONG></TD>
									<TD>
										<asp:radiobuttonlist id="radMaxMemorySize" runat="server" RepeatDirection="Horizontal" CssClass="item">
											<asp:ListItem Value="32">32M</asp:ListItem>
											<asp:ListItem Value="64">64M</asp:ListItem>
											<asp:ListItem Value="128">128M</asp:ListItem>
											<asp:ListItem Value="256">256M</asp:ListItem>
											<asp:ListItem Value="512">512M</asp:ListItem>
											<asp:ListItem Value="1024">1G</asp:ListItem>
											<asp:ListItem Value="2048">2G</asp:ListItem>
											<asp:ListItem Value="4096">4G</asp:ListItem>
                                            <asp:ListItem Value="8192">8G</asp:ListItem>
                                            <asp:ListItem Value="16384">16G</asp:ListItem>
                                            <asp:ListItem Value="32768">32G</asp:ListItem>
                                            <asp:ListItem Value="65536">64G</asp:ListItem>
										</asp:radiobuttonlist>
										<asp:label id="lblMaxMemoryError" runat="server" Visible="False" ForeColor="Red">Please select max. memory size!</asp:label></TD>
									<TD><FONT color="red">(Requied)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Battery:</STRONG></TD>
									<TD>
										<asp:dropdownlist id="lstBattery" runat="server">
											<asp:ListItem Value="(Select Battery)">(Select Battery)</asp:ListItem>
										</asp:dropdownlist>
										<asp:textbox id="txtBatteryDesc" runat="server" Width="352px" ReadOnly="True"></asp:textbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Manufacturer Battery Code:</STRONG></TD>
									<TD>
										<asp:textbox id="txtBatteryCode" runat="server" MaxLength="50"></asp:textbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Processor:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtProcessor" runat="server" MaxLength="100" Columns="50"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Operating System:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtOS" runat="server" MaxLength="100" Columns="50"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Display:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtDisplay" runat="server" MaxLength="100" Columns="50"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Graphics:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtGraphics" runat="server" MaxLength="100" Columns="50"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Wireless:</STRONG></TD>
									<TD>
										<asp:RadioButtonList id="radWireless" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
											<asp:ListItem Value="None" Selected="True">None</asp:ListItem>
											<asp:ListItem Value="802.11a">802.11a</asp:ListItem>
											<asp:ListItem Value="802.11b">802.11b</asp:ListItem>
											<asp:ListItem Value="802.11g">802.11g</asp:ListItem>
										</asp:RadioButtonList></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Blue Tooth:</STRONG></TD>
									<TD>
										<asp:checkbox id="chkBlueTooth" runat="server" Text="Yes"></asp:checkbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>SDIO:</STRONG></TD>
									<TD>
										<asp:RadioButtonList id="radSDIO" runat="server" RepeatDirection="Horizontal" CssClass="item" RepeatLayout="Flow">
											<asp:ListItem Value="N" Selected="True">None</asp:ListItem>
											<asp:ListItem Value="S">SDIO</asp:ListItem>
											<asp:ListItem Value="W">SDIO Now</asp:ListItem>
										</asp:RadioButtonList></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Notes:</STRONG></TD>
									<TD colSpan="2">
										<asp:textbox id="txtNotes" runat="server" Columns="65" Rows="6" TextMode="MultiLine"></asp:textbox></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Internal Comments:</STRONG></TD>
									<TD colSpan="2">
										<asp:textbox id="txtComments" runat="server" Columns="65" Rows="6" TextMode="MultiLine"></asp:textbox></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Picture:</STRONG></TD>
									<TD>
										<asp:button id="btnAddPicture" runat="server" Text="Add Picture" onclick="btnAddPicture_Click"></asp:button>
										<asp:image id="imgPicture" runat="server"></asp:image>
										<asp:button id="btnDeletePicture" runat="server" Text="Delete Picture" onclick="btnDeletePicture_Click"></asp:button></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD>
										<asp:button id="btnUpdate" runat="server" Text="Update" onclick="btnUpdate_Click"></asp:button>
										<asp:button id="btnAdd" runat="server" ForeColor="Navy" Text="Add" onclick="btnAdd_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:button id="btnCancelUpdate" runat="server" Text="Cancel" onclick="btnCancelUpdate_Click"></asp:button>
										<asp:button id="btnCancelAdd" runat="server" ForeColor="Navy" Text="Cancel" onclick="btnCancelAdd_Click"></asp:button></TD>
									<TD>
										<asp:Button id="btnDelete" runat="server" Text="Delete" onclick="btnDelete_Click"></asp:Button></TD>
								</TR>
							</TABLE>
						</asp:Panel>
						<asp:Panel id="pnlInformation" runat="server" Visible="False">
							<TABLE class="item" id="Table3" cellSpacing="1" cellPadding="3" width="100%" border="0">
								<TR>
									<TD width="200"><STRONG>Manufacturer:</STRONG></TD>
									<TD>
										<asp:Label id="lblManufacture" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Prodoct Line:</STRONG></TD>
									<TD>
										<asp:Label id="lblProductLine" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Model:</STRONG></TD>
									<TD>
										<asp:Label id="lblModel" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Memory Card Type:</STRONG></TD>
									<TD>
										<asp:Label id="lblMemoryType" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Internal Memory (RAM):</STRONG></TD>
									<TD>
										<asp:Label id="lblInterMemoryRAM" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Internal Memory (ROM):</STRONG></TD>
									<TD>
										<asp:Label id="lblInterMemoryROM" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Min. Memory Size:</STRONG></TD>
									<TD>
										<asp:Label id="lblMinMemorySize" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Max. Memory Size:</STRONG></TD>
									<TD>
										<asp:Label id="lblMaxMemorySize" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Battery:</STRONG></TD>
									<TD>
										<asp:Label id="lblBattery" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Manufacturer Battery Code:</STRONG></TD>
									<TD>
										<asp:Label id="lblBatteryCode" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Processor:</STRONG></TD>
									<TD>
										<asp:Label id="lblProcessor" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Operating System:</STRONG></TD>
									<TD>
										<asp:Label id="lblOS" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Display:</STRONG></TD>
									<TD>
										<asp:Label id="lblDisplay" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Graphics:</STRONG></TD>
									<TD>
										<asp:Label id="lblGraphics" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Wireless:</STRONG></TD>
									<TD>
										<asp:Label id="lblWireless" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Blue Tooth:</STRONG></TD>
									<TD>
										<asp:Label id="lblBlueTooth" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>SDIO:</STRONG></TD>
									<TD>
										<asp:Label id="lblSDIO" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Notes:</STRONG></TD>
									<TD>
										<asp:Label id="lblNotes" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Internal Comments:</STRONG></TD>
									<TD>
										<asp:Label id="lblComments" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD><STRONG>Picture:</STRONG></TD>
									<TD>
										<asp:Image id="imgShowPicture" runat="server"></asp:Image></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD>
										<asp:Button id="btnShowUpdate" runat="server" Text="Update" onclick="btnShowUpdate_Click"></asp:Button></TD>
								</TR>
							</TABLE>
						</asp:Panel></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
