<%@ Page language="c#" Inherits="memoryAdmin.TechnicalAdmin" CodeFile="TechnicalAdmin.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>TechnicalAdmin</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="technical" method="post" runat="server">
			<TABLE cellSpacing="0" cellPadding="5" width="780" border="0">
				<TR>
					<TD><asp:imagebutton id="imgHome" runat="server" ImageUrl="http://www.memory-up.com/merchant2/images/logo_big.gif"></asp:imagebutton></TD>
				</TR>
				<TR>
					<TD>
						<TABLE class="item_title" cellSpacing="1" cellPadding="5" width="100%" border="0">
							<TR>
								<TD align="right" width="390" colSpan="2">Case&nbsp;Number:</TD>
								<TD width="390" colSpan="2"><asp:label id="lblCaseNumber" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD align="right" colSpan="2">RMA Number:</TD>
								<TD colSpan="2"><asp:label id="lblRMANumber" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD colSpan="4"></TD>
							</TR>
							<TR>
								<TD noWrap align="right" width="190" bgColor="lightgrey">Request Type:</TD>
								<TD><asp:label id="lblRequestType" runat="server" Width="200" ForeColor="Red"></asp:label></TD>
								<TD noWrap align="right" width="150" bgColor="lightgrey">First Name:</TD>
								<TD><asp:label id="lblFirstName" runat="server" Width="240"></asp:label></TD>
							</TR>
							<TR>
								<TD noWrap align="right" bgColor="lightgrey">Order Number:</TD>
								<TD><asp:label id="lblOrderNumber" runat="server"></asp:label></TD>
								<TD noWrap align="right" bgColor="lightgrey">Last Name:</TD>
								<TD><asp:label id="lblLastName" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD noWrap align="right" bgColor="lightgrey">Invoice Number:</TD>
								<TD><asp:label id="lblInvoiceNumber" runat="server"></asp:label></TD>
								<TD noWrap align="right" bgColor="lightgrey">E-mail:</TD>
								<TD><asp:label id="lblEmail" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD noWrap align="right" bgColor="lightgrey">Exchange / Refund:
								</TD>
								<TD><asp:label id="lblRMAType" runat="server" ForeColor="Red"></asp:label></TD>
								<TD noWrap align="right" bgColor="lightgrey">Contact Phone:</TD>
								<TD><asp:label id="lblPhone" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD noWrap align="right" bgColor="lightgrey">Item Purchase:
								</TD>
								<TD><asp:label id="lblItemPurchase" runat="server"></asp:label></TD>
								<TD noWrap align="right" bgColor="lightgrey">Shipping Address:</TD>
								<TD><asp:label id="lblAddress1" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD noWrap align="right" bgColor="lightgrey">Desktop / Laptop Model:</TD>
								<TD vAlign="top" rowSpan="2"><asp:label id="lblModel" runat="server"></asp:label></TD>
								<TD bgColor="lightgrey"></TD>
								<TD><asp:label id="lblAddress2" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 26px" bgColor="#d3d3d3"></TD>
								<TD style="HEIGHT: 26px" noWrap align="right" bgColor="lightgrey">City:</TD>
								<TD style="HEIGHT: 26px"><asp:label id="lblCity" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD noWrap align="right" bgColor="#d3d3d3">Last Customer Visit Date:</TD>
								<TD noWrap><asp:label id="lblLastVisit" runat="server"></asp:label></TD>
								<TD noWrap align="right" bgColor="lightgrey">State:</TD>
								<TD><asp:label id="lblState" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD noWrap align="right" bgColor="#d3d3d3">Last Update:</TD>
								<TD noWrap><asp:label id="lblUpdateDate" runat="server"></asp:label></TD>
								<TD noWrap align="right" bgColor="lightgrey">Zip:</TD>
								<TD><asp:label id="lblZip" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD></TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD noWrap align="right"><asp:radiobutton id="radCaseReceive" runat="server" Text="Case Receive" GroupName="rdbstatus" TextAlign="Left"></asp:radiobutton></TD>
								<TD noWrap><asp:label id="lblCaseReceiveDate" runat="server"></asp:label></TD>
								<TD vAlign="top" align="left" colSpan="2" rowSpan="8">
									<P>Problem Description:</P>
									<P><asp:textbox id="txtProblem" runat="server" Width="390px" Height="195px" TextMode="MultiLine"
											ReadOnly="True"></asp:textbox></P>
								</TD>
							</TR>
							<TR>
								<TD noWrap align="right"><asp:radiobutton id="radVerification" runat="server" Text="Case Verification" GroupName="rdbstatus"
										TextAlign="Left"></asp:radiobutton></TD>
								<TD noWrap><asp:label id="lblVerificationDate" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD noWrap align="right"><asp:radiobutton id="radRMA" runat="server" Text="RMA Number" GroupName="rdbstatus" TextAlign="Left"></asp:radiobutton></TD>
								<TD noWrap><asp:label id="lblRMADate" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD noWrap align="right"><asp:radiobutton id="radItemReceive" runat="server" Text="Item Received" GroupName="rdbstatus" TextAlign="Left"></asp:radiobutton></TD>
								<TD noWrap><asp:label id="lblItemReceiveDate" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD noWrap align="right"><asp:radiobutton id="radApprove" runat="server" Text="Approve" GroupName="rdbStatus" TextAlign="Left"></asp:radiobutton></TD>
								<TD noWrap><asp:label id="lblApproveDate" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD noWrap align="right"><asp:radiobutton id="radComplete" runat="server" Text="Complete" GroupName="rdbStatus" TextAlign="Left"></asp:radiobutton></TD>
								<TD noWrap><asp:label id="lblCompleteDate" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD noWrap align="right"><asp:radiobutton id="radCancel" runat="server" Text="Cancel" GroupName="rdbStatus" TextAlign="Left"></asp:radiobutton></TD>
								<TD noWrap><asp:label id="lblCancelDate" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD noWrap align="right">&nbsp;</TD>
								<TD noWrap><asp:button id="btnUpdate" runat="server" Text="Update" onclick="btnUpdate_Click"></asp:button></TD>
							</TR>
							<TR>
								<TD colSpan="4">
									<HR width="100%" SIZE="1">
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD>
						<TABLE class="item" cellSpacing="0" cellPadding="5" width="100%" border="0">
							<TR>
								<TD>Answer for Customer</TD>
								<TD>Notes for Administrator</TD>
							</TR>
							<TR>
								<TD><asp:textbox id="txtAnswer" runat="server" TextMode="MultiLine" Rows="15" Columns="40"></asp:textbox></TD>
								<TD><asp:textbox id="txtNotes" runat="server" TextMode="MultiLine" Rows="15" Columns="40"></asp:textbox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<tr>
					<td align="center">
						<HR width="100%" SIZE="1">
					</td>
				</tr>
				<tr>
					<td align="center"><asp:datalist id="dtlFollowUp" runat="server" CellPadding="3" BackColor="White" BorderWidth="1px"
							BorderStyle="None" BorderColor="#CCCCCC" GridLines="Both">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<HeaderTemplate>
								<asp:Label id="lblLineNumberHeader" Runat="server">Line#</asp:Label><TD bgcolor="#006699">
									<asp:Label id="lblProblemDateHeader" ForeColor="White" Runat="server" Font-Bold="True">Date</asp:Label></TD>
								<TD bgcolor="#006699">
									<asp:Label id="lblProblemDescHeader" ForeColor="White" Runat="server" Font-Bold="True">Problem Desc</asp:Label></TD>
								<TD bgcolor="#006699">
									<asp:Label id="lblProblemAnsHeader" ForeColor="White" Runat="server" Font-Bold="True">Problem Answer</asp:Label></TD>
								<TD bgcolor="#006699">
									<asp:Label id="lblPublish" ForeColor="White" Runat="server" Font-Bold="True">Publish</asp:Label></TD>
								<TD bgcolor="#006699">
									<asp:Label id="lblModProblemHeader" ForeColor="White" Runat="server" Font-Bold="True">Mod Problem</asp:Label>
							</HeaderTemplate>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<ItemStyle CssClass="item_title"></ItemStyle>
							<ItemTemplate>
								<asp:Label id="lblLineNumber" Runat="server">
									<%# DataBinder.Eval(Container.DataItem, "Line_number") %>
								</asp:Label><TD class="item_title" />
								<asp:Label id="lblProblemDate" Runat="server">
									<%# DataBinder.Eval(Container.DataItem, "Prob_date") %>
								</asp:Label><TD class="item_title" />
								<asp:Label id="lblProblemDesc" Runat="server">
									<%# DataBinder.Eval(Container.DataItem, "Prob_desc") %>
								</asp:Label><TD class="item_title" />
								<asp:textbox id=txtProblemAns TextMode="MultiLine" Rows="5" columns="25" Runat="server" text='<%# DataBinder.Eval(Container.DataItem, "prob_answer") %>'>
								</asp:textbox><TD class="item_title" align="center" />
								<asp:CheckBox id=chkPublish Runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Publish") %>'>
								</asp:CheckBox><TD class="item_title" />
								<asp:textbox id=txtModProblem TextMode="MultiLine" Rows="5" columns="25" Runat="server" text='<%# DataBinder.Eval(Container, "DataItem.mod_problem") %>'>
								</asp:textbox>
							</ItemTemplate>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
						</asp:datalist></td>
				</tr>
				<tr>
					<td>
						<TABLE class="item_title" id="Table1" cellSpacing="1" cellPadding="3" width="100%" bgColor="#cccccc"
							border="0">
							<TR>
								<TD bgColor="#ffffff">Post a Support:</TD>
								<TD bgColor="#ffffff"><asp:textbox id="txtPostAnswer" runat="server" TextMode="MultiLine" ReadOnly="True" Rows="4"
										columns="60"></asp:textbox></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td align="center"><asp:button id="btnCancel" runat="server" Text="Cancel RMA" onclick="btnCancel_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<INPUT id="Reset1" type="reset" value="Reset" name="Reset1" runat="server" onserverclick="Reset1_ServerClick">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:button id="btnEmailRMA" runat="server" Text="Email RMA #" Enabled="False" onclick="btnEmailRMA_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:button id="btnList" runat="server" Text="Back to List" onclick="btnList_Click"></asp:button></td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
