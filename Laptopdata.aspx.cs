using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for Laptopdata.
	/// </summary>
    public partial class Laptopdata : System.Web.UI.Page
	{
		protected System.Data.SqlClient.SqlDataAdapter sqlDataAdapter1;
		protected System.Data.SqlClient.SqlConnection sqlConnection1;
		protected System.Data.DataSet dataSet1;
		protected System.Data.SqlClient.SqlCommand sqlSelectCommand1;
		protected System.Data.SqlClient.SqlCommand sqlInsertCommand1;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (!IsPostBack)
			{
				sqlConnection1.Open();
				sqlDataAdapter1.Fill(dataSet1);
				Cache["CustomerData"] = dataSet1;
				bindGrid();
				sqlConnection1.Close();
			}
		}

		private void bindGrid()
		{
			dataSet1 = (DataSet)Cache["CustomerData"];
			DataGrid1.DataBind();
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.sqlDataAdapter1 = new System.Data.SqlClient.SqlDataAdapter();
			this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
			this.dataSet1 = new System.Data.DataSet();
			this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
			this.sqlInsertCommand1 = new System.Data.SqlClient.SqlCommand();
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
			// 
			// sqlDataAdapter1
			// 
			this.sqlDataAdapter1.InsertCommand = this.sqlInsertCommand1;
			this.sqlDataAdapter1.SelectCommand = this.sqlSelectCommand1;
			this.sqlDataAdapter1.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																									  new System.Data.Common.DataTableMapping("Table", "LaptopMemory", new System.Data.Common.DataColumnMapping[] {
																																																					  new System.Data.Common.DataColumnMapping("laptop_id", "laptop_id"),
																																																					  new System.Data.Common.DataColumnMapping("manufacture", "manufacture"),
																																																					  new System.Data.Common.DataColumnMapping("Product_line", "Product_line"),
																																																					  new System.Data.Common.DataColumnMapping("model", "model"),
																																																					  new System.Data.Common.DataColumnMapping("slot", "slot"),
																																																					  new System.Data.Common.DataColumnMapping("type", "type"),
																																																					  new System.Data.Common.DataColumnMapping("speed", "speed"),
																																																					  new System.Data.Common.DataColumnMapping("max_total", "max_total"),
																																																					  new System.Data.Common.DataColumnMapping("standard_size", "standard_size"),
																																																					  new System.Data.Common.DataColumnMapping("std_removable", "std_removable"),
																																																					  new System.Data.Common.DataColumnMapping("min_size", "min_size"),
																																																					  new System.Data.Common.DataColumnMapping("max_size", "max_size"),
																																																					  new System.Data.Common.DataColumnMapping("MB64OP1", "MB64OP1"),
																																																					  new System.Data.Common.DataColumnMapping("MB64OP2", "MB64OP2"),
																																																					  new System.Data.Common.DataColumnMapping("MB128OP1", "MB128OP1"),
																																																					  new System.Data.Common.DataColumnMapping("MB128OP2", "MB128OP2"),
																																																					  new System.Data.Common.DataColumnMapping("MB256OP1", "MB256OP1"),
																																																					  new System.Data.Common.DataColumnMapping("MB256OP2", "MB256OP2"),
																																																					  new System.Data.Common.DataColumnMapping("MB512OP1", "MB512OP1"),
																																																					  new System.Data.Common.DataColumnMapping("MB512OP2", "MB512OP2"),
																																																					  new System.Data.Common.DataColumnMapping("GB1OP1", "GB1OP1"),
																																																					  new System.Data.Common.DataColumnMapping("GB1OP2", "GB1OP2")})});
			// 
			// sqlConnection1
			// 
			this.sqlConnection1.ConnectionString = "data source=DUAL-850;initial catalog=MemoryUp;password=sc_cbe_sql0103;persist sec" +
				"urity info=True;user id=sa;workstation id=DELL;packet size=4096";
			// 
			// dataSet1
			// 
			this.dataSet1.DataSetName = "NewDataSet";
			this.dataSet1.Locale = new System.Globalization.CultureInfo("en-US");
			// 
			// sqlSelectCommand1
			// 
			this.sqlSelectCommand1.CommandText = "SELECT laptop_id, manufacture, Product_line, model, slot, type, speed, max_total," +
				" standard_size, std_removable, min_size, max_size, MB64OP1, MB64OP2, MB128OP1, M" +
				"B128OP2, MB256OP1, MB256OP2, MB512OP1, MB512OP2, GB1OP1, GB1OP2 FROM LaptopMemor" +
				"y";
			this.sqlSelectCommand1.Connection = this.sqlConnection1;
			// 
			// sqlInsertCommand1
			// 
			this.sqlInsertCommand1.CommandText = @"INSERT INTO LaptopMemory(manufacture, Product_line, model, slot, type, speed, max_total, standard_size, std_removable, min_size, max_size, MB64OP1, MB64OP2, MB128OP1, MB128OP2, MB256OP1, MB256OP2, MB512OP1, MB512OP2, GB1OP1, GB1OP2) VALUES (@manufacture, @Product_line, @model, @slot, @type, @speed, @max_total, @standard_size, @std_removable, @min_size, @max_size, @MB64OP1, @MB64OP2, @MB128OP1, @MB128OP2, @MB256OP1, @MB256OP2, @MB512OP1, @MB512OP2, @GB1OP1, @GB1OP2); SELECT laptop_id, manufacture, Product_line, model, slot, type, speed, max_total, standard_size, std_removable, min_size, max_size, MB64OP1, MB64OP2, MB128OP1, MB128OP2, MB256OP1, MB256OP2, MB512OP1, MB512OP2, GB1OP1, GB1OP2 FROM LaptopMemory";
			this.sqlInsertCommand1.Connection = this.sqlConnection1;
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@manufacture", System.Data.SqlDbType.NVarChar, 50, "manufacture"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Product_line", System.Data.SqlDbType.NVarChar, 50, "Product_line"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@model", System.Data.SqlDbType.NVarChar, 255, "model"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@slot", System.Data.SqlDbType.Int, 4, "slot"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@type", System.Data.SqlDbType.NVarChar, 25, "type"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@speed", System.Data.SqlDbType.NVarChar, 25, "speed"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@max_total", System.Data.SqlDbType.Int, 4, "max_total"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@standard_size", System.Data.SqlDbType.Int, 4, "standard_size"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@std_removable", System.Data.SqlDbType.Bit, 1, "std_removable"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@min_size", System.Data.SqlDbType.Int, 4, "min_size"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@max_size", System.Data.SqlDbType.Int, 4, "max_size"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MB64OP1", System.Data.SqlDbType.VarChar, 10, "MB64OP1"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MB64OP2", System.Data.SqlDbType.VarChar, 10, "MB64OP2"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MB128OP1", System.Data.SqlDbType.VarChar, 10, "MB128OP1"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MB128OP2", System.Data.SqlDbType.VarChar, 10, "MB128OP2"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MB256OP1", System.Data.SqlDbType.VarChar, 10, "MB256OP1"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MB256OP2", System.Data.SqlDbType.VarChar, 10, "MB256OP2"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MB512OP1", System.Data.SqlDbType.VarChar, 10, "MB512OP1"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MB512OP2", System.Data.SqlDbType.VarChar, 10, "MB512OP2"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@GB1OP1", System.Data.SqlDbType.VarChar, 10, "GB1OP1"));
			this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@GB1OP2", System.Data.SqlDbType.VarChar, 10, "GB1OP2"));
			this.DataGrid1.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.changePage);
			this.DataGrid1.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.cancelChange);
			this.DataGrid1.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.editRow);
			this.DataGrid1.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.updateRow);
			this.DataGrid1.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.deleteRow);
			this.Load += new System.EventHandler(this.Page_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();

		}
		#endregion


		private void changePage(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			DataGrid1.CurrentPageIndex = e.NewPageIndex;
			bindGrid();
		}

		private void deleteRow(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{
				ErrorMessage.Text = "";
				SqlCommand delCommand = new SqlCommand();
				delCommand.Connection = sqlConnection1;
				delCommand.CommandText = "Delete From LaptopMemory where laptop_id = " + e.Item.Cells[2].Text;
				delCommand.CommandType = CommandType.Text;
				sqlConnection1.Open();
				delCommand.ExecuteNonQuery();
				sqlDataAdapter1.Fill(dataSet1);
				Cache["CustomerData"] = dataSet1;
				bindGrid();
				sqlConnection1.Close();
			}
			catch (Exception ex)
			{
				ErrorMessage.Text = ex.Message;
			}
		}

		private void editRow(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			DataGrid1.EditItemIndex = e.Item.ItemIndex;
			bindGrid();
		}

		private void cancelChange(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			DataGrid1.EditItemIndex = -1;
			bindGrid();
		}

		private void updateRow(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{
				ErrorMessage.Text = "";
				SqlCommand updCommand = new SqlCommand();
				updCommand.Connection = sqlConnection1;
				updCommand.CommandText = "Update LaptopMemory Set manufacture = '" +
					((TextBox)e.Item.Cells[3].Controls[0]).Text + "', product_Line = '" +
					((TextBox)e.Item.Cells[4].Controls[0]).Text + "', model = '" +
					((TextBox)e.Item.Cells[5].Controls[0]).Text + "', slot = " +
					((TextBox)e.Item.Cells[6].Controls[0]).Text + ", type = '" +
					((TextBox)e.Item.Cells[7].Controls[0]).Text + "', speed = '" +
					((TextBox)e.Item.Cells[8].Controls[0]).Text + "', max_total = " +
					((TextBox)e.Item.Cells[9].Controls[0]).Text + ", standard_size = " +
					((TextBox)e.Item.Cells[10].Controls[0]).Text + ", std_removable = '" +
					((TextBox)e.Item.Cells[11].Controls[0]).Text + "', min_size = " +
					((TextBox)e.Item.Cells[12].Controls[0]).Text + ", max_size = " +
					((TextBox)e.Item.Cells[13].Controls[0]).Text + ", MB64OP1 = '" +
					((TextBox)e.Item.Cells[14].Controls[0]).Text + "', MB64OP2 = '" +
					((TextBox)e.Item.Cells[15].Controls[0]).Text + "', MB128OP1 = '" +
					((TextBox)e.Item.Cells[16].Controls[0]).Text + "', MB128OP2 = '" +
					((TextBox)e.Item.Cells[17].Controls[0]).Text + "', MB256OP1 = '" +
					((TextBox)e.Item.Cells[18].Controls[0]).Text + "', MB256OP2 = '" +
					((TextBox)e.Item.Cells[19].Controls[0]).Text + "', MB512OP1 = '" +
					((TextBox)e.Item.Cells[20].Controls[0]).Text + "', MB512OP2 = '" +
					((TextBox)e.Item.Cells[21].Controls[0]).Text + "', GB1OP1 = '" +
					((TextBox)e.Item.Cells[22].Controls[0]).Text + "', GB1OP2 = '" +
					((TextBox)e.Item.Cells[23].Controls[0]).Text + "' Where laptop_id = " +
					((TextBox)e.Item.Cells[2].Controls[0]).Text;
				updCommand.CommandType = CommandType.Text;
				sqlConnection1.Open();
				updCommand.ExecuteNonQuery();
				sqlDataAdapter1.Fill(dataSet1);
				Cache["CustomerData"] = dataSet1;
				DataGrid1.EditItemIndex = -1;
				bindGrid();
				sqlConnection1.Close();
			}
			catch (Exception ex)
			{
				ErrorMessage.Text = ex.Message;
			}
		}
	}
}
