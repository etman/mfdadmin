using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for CameraUpdate.
	/// </summary>
	public partial class CameraUpdate : System.Web.UI.Page
	{
		private string strSqlConnString = ConfigurationSettings.AppSettings["cnstr"];
		private string strSqlConnStringProd = ConfigurationSettings.AppSettings["prodStr"];
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure to delete?')){}else{return false;}");
			btnCancelAdd.Attributes.Add("onclick", "if(confirm('Are you sure to cancel add new item?')){}else{return false;}");
			btnCancelUpdate.Attributes.Add("onclick", "if(confirm('Are you sure to cancel update item?')){}else{return false;}");
			lstBattery.Attributes.Add("onChange", "document.all('txtBatterydesc').value = document.all('lstBattery').value;");

			if (!Page.IsPostBack)
			{
                //if (Session["LoginName"] == null)
                //{
                //    this.Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
                //}

				ListItem tempListItem = new ListItem("(Select Manufacturer)", "");
				lstManufacture.Items.Add(tempListItem);
				lstProductLine.Items.Add(tempListItem);
				lstModel.Items.Add(tempListItem);

				fillManufacture(lstManufacture);
				Mup_utilities.fillFlashType(chkMemoryType);

                if (Request.Params["modelid"] != null)
                {
                    txtCameraId.Text = Request.Params["modelid"].ToString();
                    updateSystemInformation();
                }
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion


		private void imgHome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("default.aspx");
		}

		protected void lstManufacture_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem tempListItem;

			errorInvisible();

			if (lstManufacture.SelectedItem.Text == "(Select Manufacturer)")
			{
				lstProductLine.Items.Clear ();
				lstModel.Items.Clear ();
				tempListItem = new ListItem("(Select Manufacturer)", "");
				lstProductLine.Items.Add(tempListItem);
				lstModel.Items.Add (tempListItem);

				clearAllData();
				pnlUpdate.Visible = false;
			}
			else
			{
				hdnManufacture.Text = lstManufacture.SelectedItem.ToString();
				// Manufacturer was selected, remove the "(Select a Manufacturer") entry
				if (lstManufacture.Items[0].Value == "")
					lstManufacture.Items.RemoveAt(0);

				// Set the product line and model entries to "Select Product Line"
				lstModel.Items.Clear();
				tempListItem = new ListItem ("(Select Product Line)", "");
				lstModel.Items.Add(tempListItem);

				// Now populate the product line
				fillProductLine (lstManufacture.SelectedItem.Value, lstProductLine);
			}
		}

		protected void lstProductLine_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem tempListItem;

			errorInvisible();

			if (lstProductLine.SelectedItem.Text == "(Select Product Line)")
			{
				lstModel.Items.Clear ();
				tempListItem = new ListItem ("(Select Product Line)", "");
				lstModel.Items.Add(tempListItem);

				clearAllData();
				pnlUpdate.Visible = false;
			}
			else
			{
				hdnProductLine.Text = lstProductLine.SelectedItem.ToString();
				// Manufacture was selected, remove the "(Select a State") entry
				if (lstProductLine.Items[0].Value == "")
					lstProductLine.Items.RemoveAt(0);

				// Set the school name and class level entries to "Select a School System"
				tempListItem = new ListItem ("(Select Model)", "");

				// Now populate the school systems
				fillModel (lstManufacture.SelectedItem.Value, lstProductLine.SelectedItem.Value);
			}
		}

		protected void lstModel_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem tempListItem;

			errorInvisible();
            txtCameraId.Text = "";

			if (lstModel.SelectedItem.Text == "(Select Model)")
			{
				pnlInformation.Visible = false;
				tempListItem = new ListItem ("(Select Model)", "");

				clearAllData();
				pnlUpdate.Visible = false;
			}
			else
			{
				hdnModel.Text = lstModel.SelectedItem.ToString();
				updateSystemInformation();
			}
		}

		private void fillManufacture(DropDownList manufactureList)
		{
			SqlDataReader drManufacture;
            SqlConnection drConn = new SqlConnection(strSqlConnString);

			try
			{
				string strSql = "Select distinct manufacture from Digital_Camera order by manufacture";
				
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				drManufacture = drComm.ExecuteReader();
			
				manufactureList.DataSource = drManufacture;
				manufactureList.DataTextField = "Manufacture";
				manufactureList.DataValueField = "Manufacture";
				manufactureList.DataBind();

                drConn.Close();
				drConn.Dispose();
			}
			catch (MupException e)
			{
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
				Response.Write(e.MupMessage);
			}
		}

		private void fillProductLine (string strManufacture, DropDownList productLineList)
		{
			// SQL command for query manufacture list
			SqlDataReader drProductLine;
            SqlConnection drConn = new SqlConnection(strSqlConnString);

			try
			{
				string strSql = "Select distinct product_line from Digital_Camera ";
				strSql += "Where Manufacture = '" + strManufacture + "' ";
				strSql += "Or Manufacture = '(Select Manufacturer)' order by product_line";
				
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				drProductLine = drComm.ExecuteReader();
			
				productLineList.DataSource = drProductLine;
				productLineList.DataTextField = "Product_line";
				productLineList.DataValueField = "Product_line";
				productLineList.DataBind();

				drConn.Close();
				drConn.Dispose();
			}
			catch (MupException e)
			{
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
				Response.Write(e.MupMessage);
			}
		}

		private void fillModel(string strManufacture, string strProductLine)
		{
			// SQL command for query manufacture list
			SqlDataReader drModel;
            SqlConnection drConn = new SqlConnection(strSqlConnString);

			try
			{
				string strSql = "Select model from Digital_camera ";
				strSql += "Where Manufacture = '" + strManufacture + "' ";
				strSql += "And Product_Line = '" + strProductLine + "' ";
				strSql += "Or Manufacture = '(Select Manufacturer)' order by Model";
				
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				drModel = drComm.ExecuteReader();
			
				lstModel.DataSource = drModel;
				lstModel.DataTextField = "model";
				lstModel.DataValueField = "model";
				lstModel.DataBind();

				drConn.Close();
				drConn.Dispose();
			}
			catch(MupException e)
			{
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
				Response.Write(e.MupMessage);
			}
		}

		private void updateSystemInformation()
		{
			string strManufacture = hdnManufacture.Text;
			string strProductLine = hdnProductLine.Text;
			string strModel = hdnModel.Text;
			SqlDataReader drModel;

			txtManufacture.Visible = true;
			txtProductLine.Visible = true;
			pnlUpdate.Visible = true;
			pnlInformation.Visible = false;
			lstCurrManuf.Visible = false;
			lblOrAddManuf.Visible = false;
			btnNewManuf.Visible = false;
			lstCurrLine.Visible = false;
			lblOrAddLine.Visible = false;
			btnNewLine.Visible = false;
			btnUpdate.Visible = true;
			btnAdd.Visible = false;
			btnCancelUpdate.Visible = true;
			btnCancelAdd.Visible = false;
			btnDelete.Visible = true;

			showBatteryItems();

            SqlConnection drConn = new SqlConnection(strSqlConnString);

			try
			{
				string strSql = "Select * from Digital_camera LEFT OUTER join Camera_memory on Camera_memory.Camera_id = Digital_camera.Camera_id ";
                if (txtCameraId.Text != "")
                    strSql += "where Digital_camera.camera_id = '" + txtCameraId.Text + "' ";
                else
                {
                    strSql += "Where Manufacture = '" + strManufacture + "' ";
                    strSql += "And Product_Line = '" + strProductLine + "' ";
                    strSql += "And Model = '" + strModel + "' ";
                }
				
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				drModel = drComm.ExecuteReader();

				if (drModel.Read())
				{
                    clearAllData();

                    txtManufacture.Text = drModel["Manufacture"].ToString();
                    txtProductLine.Text = drModel["Product_Line"].ToString();
                    txtModel.Text = drModel["Model"].ToString();

					txtCameraId.Text = drModel["camera_id"].ToString();
					if (drModel["counter"] != null && drModel["counter"].ToString() != "")
						lblViewed.Text = "Viewed by customers: " + (int)drModel["counter"];
					else
						lblViewed.Text = "";
					if (drModel["UpdateDate"] != DBNull.Value)
						lblLastUpdate.Text = "Last Update: " + ((DateTime)drModel["UpdateDate"]).ToShortDateString();
					else
						lblLastUpdate.Text = "";
                    if (drModel["Min_Size"] != DBNull.Value)
                    {
                        for (int i = 0; i < radMinMemorySize.Items.Count; i++)
                        {
                            if (radMinMemorySize.Items[i].Value == drModel["Min_Size"].ToString())
                                radMinMemorySize.Items[i].Selected = true;
                        }
                    }
                    else
						radMinMemorySize.SelectedIndex = -1;
                    if (drModel["Max_size"] != DBNull.Value)
                    {
                        for (int i = 0; i < radMaxMemorySize.Items.Count; i++)
                        {
                            if (radMaxMemorySize.Items[i].Value == drModel["Max_size"].ToString())
                                radMaxMemorySize.Items[i].Selected = true;
                        }
                    }
                    else
                        radMaxMemorySize.SelectedIndex = -1;
					if (drModel["Battery"].ToString() != "")
					{
						for (int i = 0; i < lstBattery.Items.Count; i++)
						{
							if (lstBattery.Items[i].Text == drModel["Battery"].ToString())
							{
								lstBattery.SelectedIndex = i;
								txtBatteryDesc.Text = lstBattery.SelectedValue;
							}
						}
					}
					else
						lstBattery.SelectedIndex = 0;
					//txtBatteryDesc.Text = drModel["Battery_desc"].ToString();
					if (drModel["Mf_battery_code"].ToString() != "")
						txtBatteryCode.Text = drModel["Mf_Battery_code"].ToString();
					else
						txtBatteryCode.Text = "";
					if (drModel["Mega_Pixels"].ToString() != "")
						txtMegaPixels.Text = drModel["Mega_Pixels"].ToString();
					else
						txtMegaPixels.Text = "";
					if (drModel["Optical_zoom"].ToString() != "")
						txtOpticalZoom.Text = drModel["Optical_zoom"].ToString();
					else
						txtOpticalZoom.Text = "";
					if (drModel["Digital_zoom"].ToString() != "")
						txtDigitalZoom.Text = drModel["Digital_zoom"].ToString();
					else
						txtDigitalZoom.Text = "";
					if (drModel["LCD"].ToString() != "")
						txtLcd.Text = drModel["LCD"].ToString();
					else
						txtLcd.Text = "";
					if (drModel["Flash"].ToString() != "")
						chkFlash.Checked = Convert.ToBoolean(drModel["Flash"].ToString());
					else
						chkFlash.Checked = false;
					if (drModel["Usb"].ToString() != "")
						chkUsb.Checked = Convert.ToBoolean(drModel["Usb"].ToString());
					else
						chkUsb.Checked = false;
					if (drModel["Fire_wire"].ToString() != "")
						chkFireWire.Checked = Convert.ToBoolean(drModel["Fire_wire"].ToString());
					else
						chkFireWire.Checked = false;
					if (drModel["Notes"].ToString() != "")
						txtNotes.Text = drModel["Notes"].ToString();
					else
						txtNotes.Text = "";
					if (drModel["Comments"].ToString() != "")
						txtComments.Text = drModel["Comments"].ToString();
					else
						txtComments.Text = "";
					if (drModel["Picture"].ToString() == "")
					{
						btnAddPicture.Visible = true;
						btnDeletePicture.Visible = false;
						imgPicture.Visible = false;
					}
					else
					{
						btnAddPicture.Visible = false;
						btnDeletePicture.Visible = true;
						imgPicture.Visible = true;

						imgPicture.ImageUrl = System.IO.Path.Combine(Server.UrlPathEncode("/MfdImages/ModelImages"), drModel["picture"].ToString());
					}

					chkMemoryType.ClearSelection();
                    if (drModel["memory_code"] != DBNull.Value)
                    {
                        for (int i = 0; i < chkMemoryType.Items.Count; i++)
                        {
                            if (drModel["memory_code"].ToString() == chkMemoryType.Items[i].Value)
                                chkMemoryType.Items[i].Selected = true;
                        }
                        while (drModel.Read())
                        {
                            for (int i = 0; i < chkMemoryType.Items.Count; i++)
                            {
                                if (drModel["memory_code"].ToString() == chkMemoryType.Items[i].Value)
                                    chkMemoryType.Items[i].Selected = true;
                            }
                        }
                    }
				}
				else
					clearAllData();

                drConn.Close();
				drConn.Dispose();
			}
			catch(MupException e)
			{
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
				Response.Write(e.MupMessage);
			}
		}

		private void clearAllData()
		{
			txtManufacture.Text = "";
			txtProductLine.Text = "";
			txtModel.Text = "";
			chkMemoryType.ClearSelection();
			radMinMemorySize.ClearSelection();
			radMaxMemorySize.ClearSelection();
			lstBattery.ClearSelection();
			txtBatteryDesc.Text = "";
			txtBatteryCode.Text = "";
			txtMegaPixels.Text = "";
			txtDigitalZoom.Text = "";
			txtOpticalZoom.Text = "";
			txtLcd.Text = "";
			chkFlash.Checked = false;
			chkUsb.Checked = false;
			chkFireWire.Checked = false;
			txtNotes.Text = "";
			txtComments.Text = "";
		}

		protected void btnAddModel_Click(object sender, System.EventArgs e)
		{
			selector.Visible = false;
			pnlUpdate.Visible = true;
			pnlInformation.Visible = false;
			lblModelExist.Visible = false;

			clearAllData();
			errorInvisible();

			txtManufacture.Visible = false;
			txtProductLine.Visible = false;
			lblOrAddManuf.Visible = true;
			lblOrAddLine.Visible = true;
			lstCurrManuf.Visible = true;
			lstCurrLine.Visible = true;
			btnNewManuf.Visible = true;
			btnNewLine.Visible = true;

			lstCurrManuf.Items.Clear();
			lstCurrLine.Items.Clear();
			ListItem tempListItem = new ListItem("(Select Manufacturer)", "");
			lstCurrManuf.Items.Add(tempListItem);
			lstCurrLine.Items.Add(tempListItem);
			btnAddPicture.Visible = false;
			btnDeletePicture.Visible = false;
			imgPicture.Visible = false;
			btnUpdate.Visible = false;
			btnCancelUpdate.Visible = false;
			btnAdd.Visible = true;
			btnCancelAdd.Visible = true;
			btnDelete.Visible = false;

			fillManufacture(lstCurrManuf);
		}

		protected void btnNewManuf_Click(object sender, System.EventArgs e)
		{
			lblOrAddManuf.Visible = false;
			lblOrAddLine.Visible = false;
			lstCurrManuf.Visible = false;
			lstCurrLine.Visible = false;
			btnNewManuf.Visible = false;
			btnNewLine.Visible = false;
			txtManufacture.Visible = true;
			txtProductLine.Visible = true;
		}

		protected void lstCurrManuf_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem tempListItem;

			if (lstCurrManuf.SelectedItem.Text == "(Select Manufacturer)")
			{
				lstCurrLine.Items.Clear ();
				tempListItem = new ListItem("(Select Manufacturer)", "");
				lstCurrLine.Items.Add(tempListItem);

				clearAllData();
			}
			else
			{
				// Manufacturer was selected, remove the "(Select a Manufacturer") entry
				if (lstCurrManuf.Items[0].Value == "")
					lstCurrManuf.Items.RemoveAt(0);

				// Now populate the product line
				fillProductLine (lstCurrManuf.SelectedItem.Value, lstCurrLine);
			}
		}

		protected void btnNewLine_Click(object sender, System.EventArgs e)
		{
			btnNewLine.Visible = false;
			txtProductLine.Visible = true;
			lblOrAddLine.Visible = false;
			lstCurrLine.Visible = false;
		}

		protected void btnAddPicture_Click(object sender, System.EventArgs e)
		{
			Response.Write("<script language=\"javascript\">window.open('AddPicture.aspx?type=Digital_Camera&system_id=" + txtCameraId.Text + "','Picture','toolbar=no,center=yes,location=no,titlebar=no,resizable=no,status=no,scrollbars=no,menubar=no,width=500,height=400');</script>");
		}

		protected void btnDeletePicture_Click(object sender, System.EventArgs e)
		{
			String strSql;
            SqlConnection dsConn = new SqlConnection(strSqlConnString);

			try
			{
				strSql = "Update Digital_camera set picture = null where Camera_id = " + txtCameraId.Text;

				
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					btnAddPicture.Visible = true;
					btnDeletePicture.Visible = false;
					imgPicture.Visible = false;
				}

                dsConn.Close();
				dsConn.Dispose();
			}
			catch (MupException ex)
			{
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			if (saveUpdate() == false)
				return;
		}

		protected void btnCancelUpdate_Click(object sender, System.EventArgs e)
		{
			pnlInformation.Visible = true;
			pnlUpdate.Visible = false;
			showSystemInformation();
		}

		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			addNewCamera();
		}

		protected void btnCancelAdd_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("CameraUpdate.aspx");
		}

		private bool saveUpdate()
		{
            SqlConnection dsConn = new SqlConnection(strSqlConnString);

			// Validation check invisible at the beginning
			errorInvisible();

			//Check the required fields
			if (errorCheck() == false)
				return false;

			try
			{
				string strSql = "Update Digital_Camera set Manufacture = '" + txtManufacture.Text.Trim() + "', ";
				strSql += "Product_line = '" + txtProductLine.Text.Trim() + "', ";
				strSql += "Model = '" + txtModel.Text.Trim() + "', ";
				strSql += "Min_size = " + radMinMemorySize.SelectedValue + ", ";
				strSql += "Max_size = " + radMaxMemorySize.SelectedValue + ", ";
				if (lstBattery.SelectedIndex > 0)
					strSql += "Battery = '" + lstBattery.SelectedItem.Text + "', ";
				else
					strSql += "Battery = null, ";
				strSql += "Mf_battery_code = '" + txtBatteryCode.Text.Trim() + "', ";
				strSql += "Mega_pixels = '" + txtMegaPixels.Text.Trim() + "', ";
				strSql += "Optical_zoom = '" + txtOpticalZoom.Text.Trim() + "', ";
				strSql += "Digital_zoom = '" + txtDigitalZoom.Text.Trim() + "', ";
				strSql += "LCD = '" + txtLcd.Text.Trim().Replace("'", "''") + "', ";
				if (chkFlash.Checked)
					strSql += "Flash = 1, ";
				else
					strSql += "Flash = 0, ";
				if (chkUsb.Checked)
					strSql += "Usb = 1, ";
				else
					strSql += "Usb = 0, ";
				if (chkFireWire.Checked)
					strSql += "Fire_wire = 1, ";
				else
					strSql += "Fire_wire = 0, ";
				strSql += "Notes = '" + txtNotes.Text.Trim().Replace("'", "''") + "', ";
				strSql += "Comments = '" + txtComments.Text.Trim().Replace("'", "''") + "', ";
				strSql += "UpdateDate = '" + DateTime.Now + "' ";
				strSql += "Where Camera_id = " + txtCameraId.Text + " ";

				
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					saveMemoryCard();
				}
				else
				{
					lblUpdateError.Visible = true;
				}

                dsConn.Close();
				dsConn.Dispose();
				showSystemInformation();
				return true;
			}
			catch (MupException ex)
			{
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
				Response.Write (ex.MupMessage);
				return false;
			}
		}

		private void errorInvisible()
		{
			lblUpdateError.Visible = false;
			lblModelExist.Visible = false;
			lblManufError.Visible = false;
			lblProductLineError.Visible = false;
			lblModelError.Visible = false;
			lblMinMemoryError.Visible = false;
			lblMaxMemoryError.Visible = false;
		}

		private bool errorCheck()
		{
			if (txtManufacture.Visible == true && txtManufacture.Text.Trim() == "")
			{
				lblManufError.Visible = true;
				return false;
			}
			if (lstCurrManuf.Visible == true && lstCurrManuf.SelectedIndex <= 0)
			{
				lblManufError.Visible = true;
				return false;
			}
			if (txtProductLine.Visible == true && txtProductLine.Text.Trim() == "")
			{
				lblProductLineError.Visible = true;
				return false;
			}
			if (lstCurrLine.Visible == true && lstCurrLine.SelectedIndex <= 0)
			{
				lblProductLineError.Visible = true;
				return false;
			}
			if (txtModel.Visible == true && txtModel.Text.Trim() == "")
			{
				lblModelError.Visible = true;
				return false;
			}
			if (radMaxMemorySize.SelectedIndex == -1)
			{
				lblMaxMemoryError.Visible = true;
				return false;
			}
			if (int.Parse(radMinMemorySize.SelectedValue) > int.Parse(radMaxMemorySize.SelectedValue))
			{
				lblMinMemoryError.Visible = true;
				return false;
			}
			else
				return true;
		}

		private void showSystemInformation()
		{
            SqlConnection drConn = new SqlConnection(strSqlConnString);

			string strSql = "SELECT * FROM Digital_Camera LEFT OUTER JOIN Camera_memory On Digital_Camera.Camera_id = Camera_memory.Camera_id ";
			strSql += "INNER JOIN Memory_card On Memory_card.code = Camera_memory.Memory_Code ";
			strSql += "where Digital_Camera.Camera_id = " + txtCameraId.Text + " ";
			strSql += "Order by Digital_Camera.Camera_id";

			selector.Visible = true;
			pnlInformation.Visible = true;
			pnlUpdate.Visible = false;

			try
			{
				
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				SqlDataReader drModel = drComm.ExecuteReader();

				if (drModel.Read())
				{
					lblManufacture.Text = drModel["Manufacture"].ToString();
					lblProductLine.Text = drModel["Product_line"].ToString();
					lblModel.Text = drModel["Model"].ToString();
					lblMinMemorySize.Text = drModel["Min_size"].ToString() + "M";
					lblMaxMemorySize.Text = drModel["Max_size"].ToString() + "M";
					lblBattery.Text = drModel["Battery"].ToString();
					lblBatteryCode.Text = drModel["Mf_battery_code"].ToString();
					lblMegaPixels.Text = drModel["Mega_Pixels"].ToString() + " mega pixels";
					lblOpticalZoom.Text = drModel["Optical_zoom"].ToString() + " X";
					lblDigitalZoom.Text = drModel["Digital_zoom"].ToString() + " X";
					lblLcd.Text = drModel["LCD"].ToString();
					if (drModel["Flash"].ToString() != "")
					{
						if ((bool)drModel["Flash"] == true)
							lblFlash.Text = "Build in";
						else
							lblFlash.Text = "Not build in";
					}
					else
						lblFlash.Text = "Not build in";
					if (drModel["Usb"].ToString() != "")
					{
						if ((bool)drModel["Usb"] == true)
							lblUsb.Text = "Supported";
						else
							lblUsb.Text = "Not Supported";
					}
					else
						lblUsb.Text = "Not Supported";
					if (drModel["Fire_wire"].ToString() != "")
					{
						if ((bool)drModel["Fire_wire"] == true)
							lblFireWire.Text = "Supported";
						else
							lblFireWire.Text = "Not Supported";
					}
					else
						lblFireWire.Text = "Not Supported";
					lblNotes.Text = drModel["Notes"].ToString();
					lblComments.Text = drModel["Comments"].ToString();
					if (drModel["Picture"].ToString() == "")
						imgShowPicture.Visible = false;
					else
					{
						imgShowPicture.Visible = true;
						imgShowPicture.ImageUrl = System.IO.Path.Combine(Server.UrlPathEncode("/MfdImages/ModelImages"), drModel["picture"].ToString());
					}
					lblMemoryType.Text = drModel["Description"].ToString();
					while (drModel.Read())
					{
						lblMemoryType.Text += ", " + drModel["Description"].ToString();
					}
				}

				drConn.Close();
				drConn.Dispose();
			}
			catch (MupException ex)
			{
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}

		protected void btnShowUpdate_Click(object sender, System.EventArgs e)
		{
			updateSystemInformation();
		}

		private void saveMemoryCard()
		{
            SqlConnection dsConn = new SqlConnection(strSqlConnString);
			int intData;

			try
			{
				string strSql = "Delete from Camera_memory where camera_id = " + txtCameraId.Text + " ";

				
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					for (int i = 0; i < chkMemoryType.Items.Count; i++)
					{
						if (chkMemoryType.Items[i].Selected)
						{
							strSql = "Insert into Camera_memory (Camera_id, Memory_code) values (";
							strSql += txtCameraId.Text + ", '" + chkMemoryType.Items[i].Value + "') ";

							dsComm.CommandText = strSql;
							intData = dsComm.ExecuteNonQuery();
						}
					}
				}

                dsConn.Close();
				dsConn.Dispose();
			}
			catch (MupException ex)
			{
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}

		private void addNewCamera()
		{
			string currManufacture = "";
			string currProductLine = "";
            SqlConnection drChConn = new SqlConnection(strSqlConnString);
            SqlConnection dsConn = new SqlConnection(strSqlConnString);
            SqlConnection drConn = new SqlConnection(strSqlConnString);

			errorInvisible();
			if (errorCheck() == false)
				return;

			if (txtManufacture.Visible == true && txtManufacture.Text.Trim() != "")
				currManufacture = txtManufacture.Text.Trim();
			else if (lstCurrManuf.Visible == true && lstCurrManuf.SelectedIndex > 0)
				currManufacture = lstCurrManuf.SelectedValue;
			if (txtProductLine.Visible == true && txtProductLine.Text.Trim() != "")
				currProductLine = txtProductLine.Text.Trim();
			else if (lstCurrLine.Visible == true && lstCurrLine.SelectedIndex > 0)
				currProductLine = lstCurrLine.SelectedValue;

			hdnManufacture.Text = currManufacture;
			hdnProductLine.Text = currProductLine;
			hdnModel.Text = txtModel.Text.Trim();

			try
			{
				string strSql = "Select * from Digital_Camera Where Manufacture = '" + currManufacture + "' ";
				strSql += "And Product_Line = '" + currProductLine + "' ";
				strSql += "And Model = '" + txtModel.Text.Trim() + "' ";

				
				drChConn.Open();
				SqlCommand drChComm = new SqlCommand(strSql, drChConn);
				SqlDataReader drCheck = drChComm.ExecuteReader();

                if (drCheck.Read())
                {
                    updateSystemInformation();
                    lblModelExist.Visible = true;

                    drChConn.Close();
                    drChConn.Dispose();
                    return;
                }
                else
                {
                    drChConn.Close();
                    drChConn.Dispose();
                }
					

				strSql = "Insert Into Digital_Camera (Manufacture, Product_line, Model, Min_size, Max_size, Battery, Mf_battery_code, Mega_Pixels, Optical_zoom, Digital_zoom, LCD, Flash, Usb, Fire_wire, Notes, Comments, UpdateDate) Values (";
				strSql += "'" + currManufacture + "', ";
				strSql += "'" + currProductLine + "', ";
				strSql += "'" + txtModel.Text.Trim() + "', ";
				strSql += radMinMemorySize.SelectedValue + ", ";
				strSql += radMaxMemorySize.SelectedValue + ", ";
				if (lstBattery.SelectedIndex > 0)
					strSql += "'" + lstBattery.SelectedItem.Text + "', ";
				else
					strSql += "null, ";
				strSql += "'" + txtBatteryCode.Text + "', ";
				strSql += "'" + txtMegaPixels.Text + "', ";
				strSql += "'" + txtOpticalZoom.Text + "', ";
				strSql += "'" + txtDigitalZoom.Text + "', ";
				strSql += "'" + txtLcd.Text.Replace("'", "''") + "', ";
				if (chkFlash.Checked)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkUsb.Checked)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkFireWire.Checked)
					strSql += "1, ";
				else
					strSql += "0, ";
				strSql += "'" + txtNotes.Text + "', ";
				strSql += "'" + txtComments.Text + "', ";
				strSql += "'" + DateTime.Now + "') ";

				
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					strSql = "Select Camera_id from Digital_Camera Where Manufacture = '" + currManufacture + "' ";
					strSql += "And Product_Line = '" + currProductLine + "' ";
					strSql += "And Model = '" + txtModel.Text.Trim() + "' ";

					
					drConn.Open();
					SqlCommand drComm = new SqlCommand(strSql, drConn);
					SqlDataReader drModel = drComm.ExecuteReader();

					if (drModel.Read())
					{
						txtCameraId.Text = drModel["Camera_id"].ToString();
						saveMemoryCard();
					}

					drConn.Close();
					drConn.Dispose();
				}

                dsConn.Close();
				dsConn.Dispose();

				showSystemInformation();
			}
			catch (MupException ex)
			{
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
                if (drChConn != null)
                {
                    drChConn.Close();
                    drChConn.Dispose();
                }
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}

		protected void btnDelete_Click(object sender, System.EventArgs e)
		{
            SqlConnection dsConn = new SqlConnection(strSqlConnString);

			try
			{
				string strSql = "Delete from Digital_Camera where camera_id = " + txtCameraId.Text + " ";

				
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					Mup_utilities.DeleteMemroyCard("Camera_memory", "Camera", txtCameraId.Text);

					clearAllData();
					selector.Visible = true;
					pnlUpdate.Visible = false;

					lstManufacture.Items.Clear();
					lstProductLine.Items.Clear();
					lstModel.Items.Clear();

					ListItem tempListItem = new ListItem("(Select Manufacturer)", "");
					lstManufacture.Items.Add(tempListItem);
					lstProductLine.Items.Add(tempListItem);
					lstModel.Items.Add(tempListItem);

					fillManufacture(lstManufacture);
				}

                dsConn.Close();
				dsConn.Dispose();
			}
			catch (MupException ex)
			{
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}

		private void showBatteryItems()
		{
            SqlConnection drConn = new SqlConnection(strSqlConnStringProd);

			try
			{
				string strSql;
				int tempIndex;
				string tempCode;
				int i = 1;

				strSql = "Select * from Product inner join ProductCategory ON pk_product = fk_product ";
				strSql += "Inner Join Category On pk_category = fk_category ";
				strSql += "Where Category.Name = 'Digital Camera Batteries' ";

				
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				SqlDataReader drModel = drComm.ExecuteReader();

				lstBattery.Items.Clear();
				ListItem tempListItem = new ListItem("(Select Battery)", "");
				lstBattery.Items.Add(tempListItem);
				
				while (drModel.Read())
				{
					tempIndex = drModel["Link"].ToString().IndexOf("Product_Code");
					tempCode = drModel["Link"].ToString().Substring(tempIndex + 13).Trim();
					lstBattery.Items.Add(tempCode);
					lstBattery.Items[i].Value = drModel["Name"].ToString().Trim();
					i++;
				}

				drConn.Close();
				drConn.Dispose();
			}
			catch (MupException ex)
			{
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}

		private void showMemroyType()
		{
            SqlConnection drConn = new SqlConnection(strSqlConnString);

			try
			{
				string strSql;
				int i = 0;

				strSql = "Select * from Memory_card ";
				strSql += "Where Active = 1 ";
				strSql += "Order by Description ";

				
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				SqlDataReader drModel = drComm.ExecuteReader();

				chkMemoryType.Items.Clear();
				while (drModel.Read())
				{
					chkMemoryType.Items.Add(drModel["description"].ToString());
					chkMemoryType.Items[i].Value = drModel["code"].ToString();
					i++;
				}

                drConn.Close();
				drConn.Dispose();
			}
			catch (MupException ex)
			{
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}
        protected void btnUpdate2_Click(object sender, EventArgs e)
        {
            if (saveUpdate() == false)
                return;
        }
}
}
