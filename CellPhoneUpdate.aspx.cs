using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for CellPhoneUpdate.
	/// </summary>
	public partial class CellPhoneUpdate : System.Web.UI.Page
	{
		private string strSqlConnString = ConfigurationSettings.AppSettings["cnstr"];

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			try
			{
				if (ucHeader != null)
				{
					ucHeader.PreLoad();
					ucHeader.SetPageTitle("Cellular Phone Selector");
				}
				if (ucModelSelect != null)
				{
					ucModelSelect.PreLoad("Phone_model");
					ucModelSelect.SubmitModel += new ucModelSelectorEventHandler(ucNewModelSelect.SetNewModelParameter);
				}
				if (ucNewModelSelect != null)
				{
					ucNewModelSelect.PreLoad("Phone_model");
				}
				ucModelSelect.SubmitModel += new ucModelSelectorEventHandler(this.SetModelParameter);
			}
			catch (Exception ex)
			{
				Trace.Write(ex.Message);
			}

			if (!Page.IsPostBack)
			{
				btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure to delete?')){}else{return false;}");
				btnCancelAdd.Attributes.Add("onclick", "if(confirm('Are you sure to cancel add new item?')){}else{return false;}");
				btnCancelUpdate.Attributes.Add("onclick", "if(confirm('Are you sure to cancel update item?')){}else{return false;}");
				lstBattery.Attributes.Add("onChange", "document.all('txtBatterydesc').value = document.all('lstBattery').value;");
				btnAdd.Attributes.Add("onClick", "return CheckFields();");
				btnUpdate.Attributes.Add("onClick", "return CheckFields();");

				Mup_utilities.fillFlashType(chkMemoryType);


                if (Request.Params["modelid"] != null)
                {
                    txtPhoneId.Text = Request.Params["modelid"].ToString();
                    updateSystemInformation();
                }
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		public void SetModelParameter(object sender, SubmitModelSearchEventArgs args)
		{
			ClearAllData();
			pnlUpdate.Visible = true;
			pnlInformation.Visible = false;
			lblModelExist.Visible = false;

			if (args.Model == null)
			{
				errorInvisible();
				ucModelSelect.Visible = false;

				btnAddPicture.Visible = false;
				btnDeletePicture.Visible = false;
				btnDelete.Visible = false;
				btnUpdate.Visible = false;
				btnCancelUpdate.Visible = false;
				imgPicture.Visible = false;

				btnAdd.Visible = true;
				btnCancelAdd.Visible = true;
			}
			else
			{
				errorInvisible();
				ucModelSelect.Visible = true;

				btnAddPicture.Visible = true;
				btnDeletePicture.Visible = true;
				btnDelete.Visible = true;
				btnUpdate.Visible = true;
				btnCancelUpdate.Visible = true;

				btnAdd.Visible = false;
				btnCancelAdd.Visible = false;

				updateSystemInformation();
			}
		}

		private void ClearAllData()
		{
			chkMemoryType.ClearSelection();
			txtInternal.Text = "";
			radMinMemorySize.ClearSelection();
			radMaxMemorySize.ClearSelection();
			lstBattery.ClearSelection();
			txtBatteryDesc.Text = "";
			txtBatteryCode.Text = "";
			txtMegaPixels.Text = "";
			txtLcd.Text = "";
			txtOS.Text = "";
			txtReleaseMonth.Text = "";
			txtReleaseYear.Text = "";
			chkJava.Checked = false;
			chkBlueTooth.Checked = false;
			radSDIO.SelectedIndex = 0;
			txtPolyphonic.Text = "";
			chkMusic.ClearSelection();
			chkNetwork.ClearSelection();
			txtNotes.Text = "";
			txtComments.Text = "";
		}

		private void errorInvisible()
		{
			ucNewModelSelect.errorInvisible();
			lblUpdateError.Visible = false;
			lblModelExist.Visible = false;
			lblMinMemoryError.Visible = false;
			lblMaxMemoryError.Visible = false;
		}

		private void updateSystemInformation()
		{
			SubmitModelSearchEventArgs modelUpdate;
			SqlDataReader drModel;

			pnlUpdate.Visible = true;
			pnlInformation.Visible = false;
			btnAdd.Visible = false;
			btnCancelAdd.Visible = false;
			btnUpdate.Visible = true;
			btnCancelUpdate.Visible = true;
			btnDelete.Visible = true;

			modelUpdate = ucModelSelect.GetUpdateModel();
			ucNewModelSelect.SetExistModel(modelUpdate);
			Mup_utilities.ShowBatteryItems(lstBattery);

            SqlConnection drConn = new SqlConnection(strSqlConnString);

			try
			{
				string strSql = "Select * from Phone_model LEFT OUTER join Phone_memory on Phone_memory.Phone_id = Phone_model.Phone_id ";
                if (txtPhoneId.Text != "")
                    strSql += "where Phone_model.phone_id = '" + txtPhoneId.Text + "' ";
                else
                {
                    strSql += "Where Manufacture = '" + modelUpdate.Manufacturer + "' ";
                    strSql += "And Product_Line = '" + modelUpdate.ProductLine + "' ";
                    strSql += "And Model = '" + modelUpdate.Model + "' ";
                }
				
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				drModel = drComm.ExecuteReader();

				if (drModel.Read())
				{
					txtPhoneId.Text = drModel["phone_id"].ToString();
					if (drModel["counter"] != null && drModel["counter"].ToString() != "")
						lblViewed.Text = "Viewed by customers: " + (int)drModel["counter"];
					else
						lblViewed.Text = "";
					if (drModel["UpdateDate"] != DBNull.Value)
						lblLastUpdate.Text = "Last Update: " + ((DateTime)drModel["UpdateDate"]).ToShortDateString();
					else
						lblLastUpdate.Text = "";
					if (drModel["Internal_size"].ToString() != "")
						txtInternal.Text  = drModel["Internal_size"].ToString();
					else
						txtInternal.Text = "";
                    if (drModel["Min_Size"] != DBNull.Value)
                    {
                        for (int i = 0; i < radMinMemorySize.Items.Count; i++)
                        {
                            if (radMinMemorySize.Items[i].Value == drModel["Min_Size"].ToString())
                                radMinMemorySize.Items[i].Selected = true;
                        }
                    }
                    else
                        radMinMemorySize.SelectedIndex = -1;
                    if (drModel["Max_size"] != DBNull.Value)
                    {
                        for (int i = 0; i < radMaxMemorySize.Items.Count; i++)
                        {
                            if (radMaxMemorySize.Items[i].Value == drModel["Max_size"].ToString())
                                radMaxMemorySize.Items[i].Selected = true;
                        }
                    }
                    else
                        radMaxMemorySize.SelectedIndex = -1;
					if (drModel["Battery"].ToString() != "")
					{
						for (int i = 0; i < lstBattery.Items.Count; i++)
						{
							if (lstBattery.Items[i].Text == drModel["Battery"].ToString())
							{
								lstBattery.SelectedIndex = i;
								txtBatteryDesc.Text = lstBattery.SelectedValue;
							}
						}
					}
					else
						lstBattery.SelectedIndex = 0;
					//txtBatteryDesc.Text = drModel["Battery_desc"].ToString();
					if (drModel["Mf_battery_code"].ToString() != "")
						txtBatteryCode.Text = drModel["Mf_Battery_code"].ToString();
					else
						txtBatteryCode.Text = "";
					if (drModel["Mega_Pixels"].ToString() != "")
						txtMegaPixels.Text = drModel["Mega_Pixels"].ToString();
					else
						txtMegaPixels.Text = "";
					
					if (drModel["LCD"].ToString() != "")
						txtLcd.Text = drModel["LCD"].ToString();
					else
						txtLcd.Text = "";
					if (drModel["OS"].ToString() != "")
						txtOS.Text = drModel["OS"].ToString();
					else
						txtOS.Text = "";
					if (drModel["Release_date"].ToString() != "")
					{
						txtReleaseMonth.Text = ((DateTime)drModel["Release_date"]).Month.ToString();
						txtReleaseYear.Text = ((DateTime)drModel["Release_date"]).Year.ToString();
					}
					else
					{
						txtReleaseMonth.Text = "";
						txtReleaseYear.Text = "";
					}
					if (drModel["Java"].ToString() != "")
						chkJava.Checked = Convert.ToBoolean(drModel["Java"].ToString());
					else
						chkJava.Checked = false;
					if (drModel["Blue_tooth"].ToString() != "")
						chkBlueTooth.Checked = Convert.ToBoolean(drModel["Blue_tooth"].ToString());
					else
						chkBlueTooth.Checked = false;
					if (drModel["SDIO"].ToString() != "")
						radSDIO.SelectedValue = drModel["SDIO"].ToString();
					else
						radSDIO.SelectedValue = "N";
					if (drModel["Polyphonic"].ToString() != "")
						txtPolyphonic.Text = drModel["Polyphonic"].ToString();
					else
						txtPolyphonic.Text = "";
					if (drModel["MIDI"].ToString() != "")
						chkMusic.Items.FindByValue("MIDI").Selected = Convert.ToBoolean(drModel["MIDI"].ToString());
					else
						chkMusic.Items.FindByValue("MIDI").Selected = false;
					if (drModel["MP3"].ToString() != "")
						chkMusic.Items.FindByValue("MP3").Selected = Convert.ToBoolean(drModel["MP3"].ToString());
					else
						chkMusic.Items.FindByValue("MP3").Selected = false;
					if (drModel["AAC"].ToString() != "")
						chkMusic.Items.FindByValue("AAC").Selected = Convert.ToBoolean(drModel["AAC"].ToString());
					else
						chkMusic.Items.FindByValue("AAC").Selected = false;
					if (drModel["WMA"].ToString() != "")
						chkMusic.Items.FindByValue("WMA").Selected = Convert.ToBoolean(drModel["WMA"].ToString());
					else
						chkMusic.Items.FindByValue("WMA").Selected = false;
					if (drModel["RM"].ToString() != "")
						chkMusic.Items.FindByValue("RM").Selected = Convert.ToBoolean(drModel["RM"].ToString());
					else
						chkMusic.Items.FindByValue("RM").Selected = false;
					if (drModel["GSM_850"].ToString() != "")
						chkNetwork.Items.FindByValue("GSM_850").Selected = Convert.ToBoolean(drModel["GSM_850"].ToString());
					else
						chkNetwork.Items.FindByValue("GSM_850").Selected = false;
					if (drModel["GSM_900"].ToString() != "")
						chkNetwork.Items.FindByValue("GSM_900").Selected = Convert.ToBoolean(drModel["GSM_900"].ToString());
					else
						chkNetwork.Items.FindByValue("GSM_900").Selected = false;
					if (drModel["GSM_1800"].ToString() != "")
						chkNetwork.Items.FindByValue("GSM_1800").Selected = Convert.ToBoolean(drModel["GSM_1800"].ToString());
					else
						chkNetwork.Items.FindByValue("GSM_1800").Selected = false;
					if (drModel["GSM_1900"].ToString() != "")
						chkNetwork.Items.FindByValue("GSM_1900").Selected = Convert.ToBoolean(drModel["GSM_1900"].ToString());
					else
						chkNetwork.Items.FindByValue("GSM_1900").Selected = false;
					if (drModel["TDMA"].ToString() != "")
						chkNetwork.Items.FindByValue("TDMA").Selected = Convert.ToBoolean(drModel["TDMA"].ToString());
					else
						chkNetwork.Items.FindByValue("TDMA").Selected = false;
					if (drModel["CDMA_800"].ToString() != "")
						chkNetwork.Items.FindByValue("CDMA_800").Selected = Convert.ToBoolean(drModel["CDMA_800"].ToString());
					else
						chkNetwork.Items.FindByValue("CDMA_800").Selected = false;
					if (drModel["CDMA_1900"].ToString() != "")
						chkNetwork.Items.FindByValue("CDMA_1900").Selected = Convert.ToBoolean(drModel["CDMA_1900"].ToString());
					else
						chkNetwork.Items.FindByValue("CDMA_1900").Selected = false;
					if (drModel["WCDMA"].ToString() != "")
						chkNetwork.Items.FindByValue("WCDMA").Selected = Convert.ToBoolean(drModel["WCDMA"].ToString());
					else
						chkNetwork.Items.FindByValue("WCDMA").Selected = false;
					if (drModel["CDMA2000"].ToString() != "")
						chkNetwork.Items.FindByValue("CDMA2000").Selected = Convert.ToBoolean(drModel["CDMA2000"].ToString());
					else
						chkNetwork.Items.FindByValue("CDMA2000").Selected = false;
					if (drModel["TDMA_Edge"].ToString() != "")
						chkNetwork.Items.FindByValue("TDMA_Edge").Selected = Convert.ToBoolean(drModel["TDMA_Edge"].ToString());
					else
						chkNetwork.Items.FindByValue("TDMA_Edge").Selected = false;
					if (drModel["Notes"].ToString() != "")
						txtNotes.Text = drModel["Notes"].ToString();
					else
						txtNotes.Text = "";
					if (drModel["Comments"].ToString() != "")
						txtComments.Text = drModel["Comments"].ToString();
					else
						txtComments.Text = "";
					if (drModel["Picture"].ToString() == "")
					{
						btnAddPicture.Visible = true;
						btnDeletePicture.Visible = false;
						imgPicture.Visible = false;
					}
					else
					{
						btnAddPicture.Visible = false;
						btnDeletePicture.Visible = true;
						imgPicture.Visible = true;

						imgPicture.ImageUrl = System.IO.Path.Combine(Server.UrlPathEncode("/MfdImages/ModelImages"), drModel["picture"].ToString());
					}

					chkMemoryType.ClearSelection();
                    if (drModel["memory_code"] != DBNull.Value)
                    {
                        for (int i = 0; i < chkMemoryType.Items.Count; i++)
                        {
                            if (drModel["memory_code"].ToString() == chkMemoryType.Items[i].Value)
                                chkMemoryType.Items[i].Selected = true;
                        }
                        while (drModel.Read())
                        {
                            for (int i = 0; i < chkMemoryType.Items.Count; i++)
                            {
                                if (drModel["memory_code"].ToString() == chkMemoryType.Items[i].Value)
                                    chkMemoryType.Items[i].Selected = true;
                            }
                        }
                    }
				}
				else
					ClearAllData();

				drConn.Close();
				drConn.Dispose();
			}
			catch(MupException e)
			{
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
				Response.Write(e.MupMessage);
			}
		}

		private bool saveUpdate()
		{
			SubmitModelSearchEventArgs modelSave;

			// Validation check invisible at the beginning
			errorInvisible();

			//Check the required fields
			if (errorCheck() == false)
				return false;

			modelSave = ucNewModelSelect.GetSelectMode();
            SqlConnection dsConn = new SqlConnection(strSqlConnString);

			try
			{
				string strSql = "Update Phone_model set Manufacture = '" + modelSave.Manufacturer + "', ";
				strSql += "Product_line = '" + modelSave.ProductLine + "', ";
				strSql += "Model = '" + modelSave.Model + "', ";
				if (txtInternal.Text.Trim() != "")
					strSql += "Internal_size = " + txtInternal.Text.Trim() + ", ";
				else
					strSql += "Internal_size = null, ";
				strSql += "Min_size = " + radMinMemorySize.SelectedValue + ", ";
				strSql += "Max_size = " + radMaxMemorySize.SelectedValue + ", ";
				if (lstBattery.SelectedIndex > 0)
					strSql += "Battery = '" + lstBattery.SelectedItem.Text + "', ";
				else
					strSql += "Battery = null, ";
				strSql += "Mf_battery_code = '" + txtBatteryCode.Text.Trim() + "', ";
				strSql += "Mega_pixels = '" + txtMegaPixels.Text.Trim() + "', ";
				strSql += "LCD = '" + txtLcd.Text.Trim().Replace("'", "''") + "', ";
				strSql += "OS = '" + txtOS.Text.Trim().Replace("'", "''") + "', ";
				if (txtReleaseMonth.Text.Trim() != "" && txtReleaseYear.Text.Trim() != "")
					strSql += "Release_date = '" + txtReleaseMonth.Text.Trim() + "/01/" + txtReleaseYear.Text + "', ";
				else
					strSql += "Release_date = null, ";
				if (chkJava.Checked)
					strSql += "Java = 1, ";
				else
					strSql += "Java = 0, ";
				if (chkBlueTooth.Checked)
					strSql += "Blue_tooth = 1, ";
				else
					strSql += "Blue_tooth = 0, ";
				strSql += "SDIO = '" + radSDIO.SelectedValue + "', ";
				strSql += "Polyphonic = '" + txtPolyphonic.Text.Trim().Replace("'", "''") + "', ";
				if (chkMusic.Items.FindByValue("MIDI").Selected)
					strSql += "MIDI = 1, ";
				else
					strSql += "MIDI = 0, ";
				if (chkMusic.Items.FindByValue("MP3").Selected)
					strSql += "MP3 = 1, ";
				else
					strSql += "MP3 = 0, ";
				if (chkMusic.Items.FindByValue("AAC").Selected)
					strSql += "AAC = 1, ";
				else
					strSql += "AAC = 0, ";
				if (chkMusic.Items.FindByValue("WMA").Selected)
					strSql += "WMA = 1, ";
				else
					strSql += "WMA = 0, ";
				if (chkMusic.Items.FindByValue("RM").Selected)
					strSql += "RM = 1, ";
				else
					strSql += "RM = 0, ";
				if (chkNetwork.Items.FindByValue("GSM_850").Selected)
					strSql += "GSM_850 = 1, ";
				else
					strSql += "GSM_850 = 0, ";
				if (chkNetwork.Items.FindByValue("GSM_900").Selected)
					strSql += "GSM_900 = 1, ";
				else
					strSql += "GSM_900 = 0, ";
				if (chkNetwork.Items.FindByValue("GSM_1800").Selected)
					strSql += "GSM_1800 = 1, ";
				else
					strSql += "GSM_1800 = 0, ";
				if (chkNetwork.Items.FindByValue("GSM_1900").Selected)
					strSql += "GSM_1900 = 1, ";
				else
					strSql += "GSM_1900 = 0, ";
				if (chkNetwork.Items.FindByValue("TDMA").Selected)
					strSql += "TDMA = 1, ";
				else
					strSql += "TDMA = 0, ";
				if (chkNetwork.Items.FindByValue("CDMA_800").Selected)
					strSql += "CDMA_800 = 1, ";
				else
					strSql += "CDMA_800 = 0, ";
				if (chkNetwork.Items.FindByValue("CDMA_1900").Selected)
					strSql += "CDMA_1900 = 1, ";
				else
					strSql += "CDMA_1900 = 0, ";
				if (chkNetwork.Items.FindByValue("WCDMA").Selected)
					strSql += "WCDMA = 1, ";
				else
					strSql += "WCDMA = 0, ";
				if (chkNetwork.Items.FindByValue("CDMA2000").Selected)
					strSql += "CDMA2000 = 1, ";
				else
					strSql += "CDMA2000 = 0, ";
				if (chkNetwork.Items.FindByValue("TDMA_Edge").Selected)
					strSql += "TDMA_Edge = 1, ";
				else
					strSql += "TDMA_Edge = 0, ";
				strSql += "Notes = '" + txtNotes.Text.Trim().Replace("'", "''") + "', ";
				strSql += "Comments = '" + txtComments.Text.Trim().Replace("'", "''") + "', ";
				strSql += "UpdateDate = '" + DateTime.Now + "' ";
				strSql += "Where Phone_id = " + txtPhoneId.Text + " ";

				
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					Mup_utilities.saveMemoryCard("Phone", txtPhoneId.Text, chkMemoryType);
				}
				else
				{
					lblUpdateError.Visible = true;
				}

                dsConn.Close();
				dsConn.Dispose();
				showSystemInformation();
				return true;
			}
			catch (MupException ex)
			{
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
				Response.Write (ex.MupMessage);
				return false;
			}
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			if (saveUpdate() == false)
				return;
		}

		protected void btnCancelUpdate_Click(object sender, System.EventArgs e)
		{
			showSystemInformation();
		}

		private void showSystemInformation()
		{
			string strSql = "SELECT * FROM Phone_model LEFT OUTER JOIN Phone_memory On Phone_model.Phone_id = Phone_memory.Phone_id ";
			strSql += "INNER JOIN Memory_card On Memory_card.code = Phone_memory.Memory_Code ";
			strSql += "where Phone_model.Phone_id = " + txtPhoneId.Text + " ";
			strSql += "Order by Phone_model.Phone_id";

			ucModelSelect.Visible = true;
			pnlInformation.Visible = true;
			pnlUpdate.Visible = false;

            SqlConnection drConn = new SqlConnection(strSqlConnString);

			try
			{
				
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				SqlDataReader drModel = drComm.ExecuteReader();

				if (drModel.Read())
				{
					lblManufacture.Text = drModel["Manufacture"].ToString();
					lblProductLine.Text = drModel["Product_line"].ToString();
					lblModel.Text = drModel["Model"].ToString();
					lblInterMemorySize.Text = drModel["Internal_size"].ToString() + "M";
					lblMinMemorySize.Text = drModel["Min_size"].ToString() + "M";
					lblMaxMemorySize.Text = drModel["Max_size"].ToString() + "M";
					lblBattery.Text = drModel["Battery"].ToString();
					lblBatteryCode.Text = drModel["Mf_battery_code"].ToString();
					lblMegaPixels.Text = drModel["Mega_Pixels"].ToString() + " mega pixels";
					lblLcd.Text = drModel["LCD"].ToString();
					lblOS.Text = drModel["OS"].ToString();
					if (drModel["Release_date"].ToString() != "")
					{
						lblReleaseMonth.Text = ((DateTime)drModel["Release_date"]).Month.ToString();
						lblReleaseYear.Text = ((DateTime)drModel["Release_date"]).Year.ToString();
					}
					if (drModel["Java"].ToString() != "")
					{
						if ((bool)drModel["Java"] == true)
							lblJava.Text = "Supported";
						else
							lblJava.Text = "Not Supported";
					}
					else
						lblJava.Text = "Not Supported";
					if (drModel["Blue_tooth"].ToString() != "")
					{
						if ((bool)drModel["Blue_tooth"] == true)
							lblBlueTooth.Text = "Supported";
						else
							lblBlueTooth.Text = "Not Supported";
					}
					else
						lblBlueTooth.Text = "Not Supported";
					if (drModel["SDIO"].ToString() == "S")
						lblSDIO.Text = "SDIO Supported";
					else if (drModel["SDIO"].ToString() == "W")
						lblSDIO.Text = "SDIO Now Supported";
					else
						lblSDIO.Text = "Not Supported";
					lblMusic.Text = "MIDI " + drModel["MIDI"].ToString() + ", ";
					if (drModel["MP3"].ToString() != "")
					{
						if ((bool)drModel["MP3"] == true)
							lblMusic.Text += "MP3, ";
					}
					if (drModel["AAC"].ToString() != "")
					{
						if ((bool)drModel["AAC"] == true)
							lblMusic.Text += "AAC, ";
					}
					if (drModel["WMA"].ToString() != "")
					{
						if ((bool)drModel["WMA"] == true)
							lblMusic.Text += "WMA, ";
					}
					if (drModel["RM"].ToString() != "")
					{
						if ((bool)drModel["RM"] == true)
							lblMusic.Text += "RM, ";
					}
					lblNetwork.Text = "";
					if (drModel["GSM_850"].ToString() != "")
					{
						if ((bool)drModel["GSM_850"] == true)
							lblNetwork.Text += "GSM_850, ";
					}
					if (drModel["GSM_900"].ToString() != "")
					{
						if ((bool)drModel["GSM_900"] == true)
							lblNetwork.Text += "GSM_900, ";
					}
					if (drModel["GSM_1800"].ToString() != "")
					{
						if ((bool)drModel["GSM_1800"] == true)
							lblNetwork.Text += "GSM_1800, ";
					}
					if (drModel["GSM_1900"].ToString() != "")
					{
						if ((bool)drModel["GSM_1900"] == true)
							lblNetwork.Text += "GSM_1900, ";
					}
					if (drModel["TDMA"].ToString() != "")
					{
						if ((bool)drModel["TDMA"] == true)
							lblNetwork.Text += "TDMA, ";
					}
					if (drModel["CDMA_800"].ToString() != "")
					{
						if ((bool)drModel["CDMA_800"] == true)
							lblNetwork.Text += "CDMA_800, ";
					}
					if (drModel["CDMA_1900"].ToString() != "")
					{
						if ((bool)drModel["CDMA_1900"] == true)
							lblNetwork.Text += "CDMA_1900, ";
					}
					if (drModel["WCDMA"].ToString() != "")
					{
						if ((bool)drModel["WCDMA"] == true)
							lblNetwork.Text += "WCDMA, ";
					}
					if (drModel["CDMA2000"].ToString() != "")
					{
						if ((bool)drModel["CDMA2000"] == true)
							lblNetwork.Text += "CDMA2000, ";
					}
					if (drModel["TDMA_Edge"].ToString() != "")
					{
						if ((bool)drModel["TDMA_Edge"] == true)
							lblNetwork.Text += "TDMA_Edge, ";
					}
					lblNotes.Text = drModel["Notes"].ToString();
					lblComments.Text = drModel["Comments"].ToString();
					if (drModel["Picture"].ToString() == "")
						imgShowPicture.Visible = false;
					else
					{
						imgShowPicture.Visible = true;
						imgShowPicture.ImageUrl = System.IO.Path.Combine(Server.UrlPathEncode("/MfdImages/ModelImages"), drModel["picture"].ToString());
					}
					lblMemoryType.Text = drModel["Description"].ToString();
					while (drModel.Read())
					{
						lblMemoryType.Text += ", " + drModel["Description"].ToString();
					}
				}

				drConn.Close();
				drConn.Dispose();
			}
			catch (MupException ex)
			{
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}

		private bool errorCheck()
		{
			if (!ucNewModelSelect.ErrorCheck())
				return false;
			if (radMinMemorySize.SelectedIndex == -1)
			{
				lblMinMemoryError.Visible = true;
				return false;
			}
			if (radMaxMemorySize.SelectedIndex == -1)
			{
				lblMaxMemoryError.Visible = true;
				return false;
			}
			if (int.Parse(radMinMemorySize.SelectedValue) > int.Parse(radMaxMemorySize.SelectedValue))
			{
				lblMinMemoryError.Visible = true;
				return false;
			}
			else
				return true;
		}

		private void addNewPhone()
		{
			SubmitModelSearchEventArgs newModel;

			errorInvisible();
			if (errorCheck() == false)
				return;

			newModel = ucNewModelSelect.GetSelectMode();

            SqlConnection drChConn = new SqlConnection(strSqlConnString);
            SqlConnection dsConn = new SqlConnection(strSqlConnString);
            SqlConnection drConn = new SqlConnection(strSqlConnString);

			try
			{
                string strSql = "Select * from Phone_model ";
                if (txtPhoneId.Text != "")
                    strSql += "where Phone_id = '" + txtPhoneId.Text + "' ";
                else
                {
                    strSql += "Where Manufacture = '" + newModel.Manufacturer + "' ";
                    strSql += "And Product_Line = '" + newModel.ProductLine + "' ";
                    strSql += "And Model = '" + newModel.Model + "' ";
                }

				
				drChConn.Open();
				SqlCommand drChComm = new SqlCommand(strSql, drChConn);
				SqlDataReader drCheck = drChComm.ExecuteReader();

				if (drCheck.Read())
				{
					updateSystemInformation();
					lblModelExist.Visible = true;
					drChConn.Close();
					drChConn.Dispose();
					return;
				}
                else
                {
                    drChConn.Close();
                    drChConn.Dispose();
                }

				strSql = "Insert Into Phone_model (Manufacture, Product_line, Model, Internal_size, Min_size, Max_size, ";
				strSql += "Battery, Mf_battery_code, Mega_Pixels, LCD, OS, Release_date, Java, Blue_tooth, SDIO, Polyphonic, MIDI, MP3, AAC, WMA, RM, ";
				strSql += "GSM_850, GSM_900, GSM_1800, GSM_1900, TDMA, CDMA_800, CDMA_1900, WCDMA, CDMA2000, TDMA_Edge, ";
				strSql += "Notes, Comments, UpdateDate) Values (";
				strSql += "'" + newModel.Manufacturer + "', ";
				strSql += "'" + newModel.ProductLine + "', ";
				strSql += "'" + newModel.Model + "', ";
				if (txtInternal.Text.Trim() != "")
					strSql += txtInternal.Text.Trim() + ", ";
				else
					strSql += "null, ";
				strSql += radMinMemorySize.SelectedValue + ", ";
				strSql += radMaxMemorySize.SelectedValue + ", ";
				if (lstBattery.SelectedIndex > 0)
					strSql += "'" + lstBattery.SelectedItem.Text + "', ";
				else
					strSql += "null, ";
				strSql += "'" + txtBatteryCode.Text + "', ";
				strSql += "'" + txtMegaPixels.Text + "', ";
				strSql += "'" + txtLcd.Text.Replace("'", "''") + "', ";
				strSql += "'" + txtOS.Text.Replace("'", "''") + "', ";
				if (txtReleaseMonth.Text.Trim() != "" && txtReleaseYear.Text.Trim() != "")
					strSql += "'" + txtReleaseMonth.Text.Trim() + "/01/" + txtReleaseYear.Text.Trim() + "', ";
				else
					strSql += "null, ";
				if (chkJava.Checked)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkBlueTooth.Checked)
					strSql += "1, ";
				else
					strSql += "0, ";
				strSql += "'" + radSDIO.SelectedValue + "', ";
				strSql += "'" + txtPolyphonic.Text + "', ";
				if (chkMusic.Items.FindByValue("MIDI").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkMusic.Items.FindByValue("MP3").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkMusic.Items.FindByValue("AAC").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkMusic.Items.FindByValue("WMA").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkMusic.Items.FindByValue("RM").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkNetwork.Items.FindByValue("GSM_850").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkNetwork.Items.FindByValue("GSM_900").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkNetwork.Items.FindByValue("GSM_1800").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkNetwork.Items.FindByValue("GSM_1900").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkNetwork.Items.FindByValue("TDMA").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkNetwork.Items.FindByValue("CDMA_800").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkNetwork.Items.FindByValue("CDMA_1900").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkNetwork.Items.FindByValue("WCDMA").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkNetwork.Items.FindByValue("CDMA2000").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				if (chkNetwork.Items.FindByValue("TDMA_Edge").Selected)
					strSql += "1, ";
				else
					strSql += "0, ";
				strSql += "'" + txtNotes.Text + "', ";
				strSql += "'" + txtComments.Text + "', ";
				strSql += "'" + DateTime.Now + "') ";

				
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					strSql = "Select Phone_id from Phone_Model Where Manufacture = '" + newModel.Manufacturer + "' ";
					strSql += "And Product_Line = '" + newModel.ProductLine + "' ";
					strSql += "And Model = '" + newModel.Model + "' ";

					
					drConn.Open();
					SqlCommand drComm = new SqlCommand(strSql, drConn);
					SqlDataReader drModel = drComm.ExecuteReader();

					if (drModel.Read())
					{
						txtPhoneId.Text = drModel["Phone_id"].ToString();
						Mup_utilities.saveMemoryCard("Phone", txtPhoneId.Text, chkMemoryType);
					}

                    drConn.Close();
                    drConn.Dispose();
				}

                dsConn.Close();
                dsConn.Dispose();

				ucModelSelect.SetNewModel(newModel, "Phone_Model");
				showSystemInformation();
			}
			catch (MupException ex)
			{
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
                if (drChConn != null)
                {
                    drChConn.Close();
                    drChConn.Dispose();
                }
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}

		protected void btnShowUpdate_Click(object sender, System.EventArgs e)
		{
			updateSystemInformation();
		}

		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			addNewPhone();
		}

		protected void btnCancelAdd_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("CellphoneUpdate.aspx");
		}

		protected void btnAddPicture_Click(object sender, System.EventArgs e)
		{
			Response.Write("<script language=\"javascript\">window.open('AddPicture.aspx?type=Phone_model&system_id=" + txtPhoneId.Text + "','Picture','toolbar=no,center=yes,location=no,titlebar=no,resizable=no,status=no,scrollbars=no,menubar=no,width=500,height=400');</script>");
		}

		protected void btnDeletePicture_Click(object sender, System.EventArgs e)
		{
			String strSql;

            SqlConnection dsConn = new SqlConnection(strSqlConnString);

			try
			{
				strSql = "Update Phone_model set picture = null where Phone_id = " + txtPhoneId.Text;

				
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					btnAddPicture.Visible = true;
					btnDeletePicture.Visible = false;
					imgPicture.Visible = false;
				}

                dsConn.Close();
				dsConn.Dispose();
			}
			catch (MupException ex)
			{
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}

		protected void btnDelete_Click(object sender, System.EventArgs e)
		{
            SqlConnection dsConn = new SqlConnection(strSqlConnString);

			try
			{
				string strSql = "Delete from Phone_model where Phone_id = " + txtPhoneId.Text + " ";

				
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					Mup_utilities.DeleteMemroyCard("Phone_Memory", "Phone", txtPhoneId.Text);

					ClearAllData();
					ucModelSelect.Visible = true;
					pnlUpdate.Visible = false;

					ucModelSelect.InitialSelector();
				}

                dsConn.Close();
				dsConn.Dispose();
			}
			catch (MupException ex)
			{
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}
        protected void btnUpdate2_Click(object sender, EventArgs e)
        {
            if (saveUpdate() == false)
                return;
        }
}
}
