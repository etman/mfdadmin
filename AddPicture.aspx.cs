using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for AddPicture.
	/// </summary>
	public partial class AddPicture : System.Web.UI.Page
	{
		protected string systemId;

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (!Page.IsPostBack)
			{
				afterUpload.Visible = false;

				systemId = Request.Params["system_id"];
				Session.Add("tableId", Request.Params["type"]);
				lblSystemId.Text = systemId;
				if (Session["tableId"].ToString() == "Desktop" || Session["tableId"].ToString() == "Laptop")
				{
					lblType.Text = Session["tableId"].ToString();
					Session["tableId"] = Session["tableId"] + "Memory";
				}
				else if (Session["tableId"].ToString() == "Digital_Camera")
				{
					lblType.Text = "Camera";
				}
				else if (Session["tableId"].ToString() == "Phone_model")
				{
					lblType.Text = "Phone";
				}
				else if (Session["tableId"].ToString() == "PDA_model")
				{
					lblType.Text = "PDA";
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnSend_Click(object sender, System.EventArgs e)
		{
			string sql;

			if (fleAddPicture.Value == "") 
			{
				lblNoFileName.Visible = true;
				return;
			}

			if (fleAddPicture.PostedFile != null)
			{
                string filepath = System.IO.Path.Combine(Server.MapPath("/MfdImages/ModelImages"), System.IO.Path.GetFileName(fleAddPicture.Value));

				try 
				{
					fleAddPicture.PostedFile.SaveAs(filepath);

					MupDbConnection dsConn = new MupDbConnection();
					sql = "Update " + Session["tableId"] + " set picture = '" + System.IO.Path.GetFileName(fleAddPicture.Value) + "' ";
					sql = sql + "where " + lblType.Text + "_id = " + lblSystemId.Text;

					if (dsConn.ExecSql(sql) >= 0)
					{
						onUpload.Visible = false;
						afterUpload.Visible = true;
						lblAfterUpload.Text = lblType.Text + " model id#" + lblSystemId.Text + " upload success! ";
					}

					dsConn.Dispose();
					dsConn = null;
				}
				catch (Exception exc) 
				{
					lblNoFileName.Text = "Error saving file <b>" + filepath + "</b><br>"+ exc.ToString();
					lblNoFileName.Visible = true;
				}
			}

		}
	}
}
