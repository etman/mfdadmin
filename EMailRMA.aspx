<%@ Page language="c#" Inherits="memoryAdmin.EMailRMA" CodeFile="EMailRMA.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>EMailRMA</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="tblEmail" cellSpacing="0" cellPadding="3" width="650" border="0" runat="server">
				<TR>
					<TD><asp:label id="Label1" runat="server">To:</asp:label></TD>
					<TD><asp:textbox id="txtTo" runat="server"></asp:textbox></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label2" runat="server">CC:</asp:label></TD>
					<TD><asp:textbox id="txtCC" runat="server"></asp:textbox></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label3" runat="server">BCC:</asp:label></TD>
					<TD><asp:textbox id="txtBCC" runat="server"></asp:textbox></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label6" runat="server">Tem:plate</asp:label></TD>
					<TD><asp:DropDownList id="lstTemplate" runat="server"></asp:DropDownList></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label4" runat="server">Subject:</asp:label></TD>
					<TD><asp:textbox id="txtSubject" runat="server"></asp:textbox></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label5" runat="server">Body:</asp:label></TD>
					<TD><asp:textbox id="txtBody" runat="server" Rows="10" Columns="60" TextMode="MultiLine"></asp:textbox></TD>
				</TR>
				<TR>
					<TD colSpan="2"><asp:button id="btnSend" runat="server" Text="Send" onclick="btnSend_Click"></asp:button>&nbsp;&nbsp;&nbsp;<INPUT id="btnReset" type="reset" value="Reset" name="Reset1" runat="server">&nbsp;&nbsp;&nbsp;
						<INPUT id="btnCloseWOSend" type="button" value="Close" onclick="window.close();"></TD>
				</TR>
			</TABLE>
			<TABLE id="tblAfterEmail" cellSpacing="0" cellPadding="5" width="650" border="0" runat="server">
				<TR>
					<TD></TD>
				</TR>
				<TR>
					<TD>
						<asp:label id="lblSent" runat="server">RMA number sent to customer.</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<INPUT id="btnClose" onclick="window.close();" type="button" value="Close"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
