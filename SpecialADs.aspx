<%@ Page language="c#" Inherits="memoryAdmin.SpecialADs" CodeFile="SpecialADs.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>SpecialADs</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="780" border="0">
				<TR>
					<TD class="title"><A href="default.aspx"><IMG alt="Memory-Up.com" src="/images/memoryselector/bd_fb.png"
								border="0"></A>Special ADs for Memory Selector</TD>
				</TR>
				<TR>
					<TD>
						<TABLE class="item" id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD colSpan="2"><asp:label id="Label11" runat="server" Width="280px">Select System...</asp:label></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD>
									<P align="right"><asp:label id="Label1" runat="server">Select Page Type:</asp:label></P>
								</TD>
								<TD><asp:radiobuttonlist id="radPageType" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" onselectedindexchanged="radPageType_SelectedIndexChanged">
										<asp:ListItem Value="DesktopMemory">Desktop</asp:ListItem>
										<asp:ListItem Value="LaptopMemory">Laptop</asp:ListItem>
										<asp:ListItem Value="Digital_Camera">Camera</asp:ListItem>
									</asp:radiobuttonlist></TD>
								<TD><asp:label id="hdnPageType" runat="server" Visible="False"></asp:label></TD>
							</TR>
							<TR>
								<TD>
									<P align="right"><asp:label id="Label12" runat="server">Select Desktop Manufacturer: </asp:label></P>
								</TD>
								<TD><asp:dropdownlist id="lstManufacture" runat="server" AutoPostBack="True" onselectedindexchanged="lstManufacture_SelectedIndexChanged"></asp:dropdownlist><asp:label id="hdnManufacture" runat="server" Visible="False"></asp:label></TD>
								<TD>
									<asp:Button id="btnListAll" runat="server" Text="List All" onclick="btnListAll_Click"></asp:Button>
									<asp:Label id="hdnAllFlag" runat="server" Visible="False">0</asp:Label></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 17px">
									<P align="right"><asp:label id="Label8" runat="server">Select Desktop Product Line: </asp:label></P>
								</TD>
								<TD style="HEIGHT: 17px"><asp:dropdownlist id="lstProductLine" runat="server" AutoPostBack="True" onselectedindexchanged="lstProductLine_SelectedIndexChanged"></asp:dropdownlist><asp:label id="hdnProductLine" runat="server" Visible="False"></asp:label></TD>
								<TD style="HEIGHT: 17px"></TD>
							</TR>
							<TR>
								<TD>
									<P align="right"><asp:label id="Label13" runat="server">Select Desktop Model: </asp:label></P>
								</TD>
								<TD><asp:dropdownlist id="lstModel" runat="server" AutoPostBack="True" onselectedindexchanged="lstModel_SelectedIndexChanged"></asp:dropdownlist><asp:label id="hdnModel" runat="server" Visible="False"></asp:label></TD>
								<TD><asp:button id="btnAddGlobal" runat="server" Text="Global" onclick="btnAddGlobal_Click"></asp:button>&nbsp;&nbsp;&nbsp;</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD><asp:panel id="pnlUpdate" runat="server" Visible="False">
							<TABLE class="item" id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD colSpan="3">
										<HR width="100%" SIZE="1">
									</TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Page Type and Model ID:</STRONG></TD>
									<TD>
										<asp:Label id="lblPageType" runat="server"></asp:Label>
										<asp:DropDownList id="drpPageType" runat="server">
											<asp:ListItem Value="All">All</asp:ListItem>
											<asp:ListItem Value="DesktopMemory">Desktop Memory</asp:ListItem>
											<asp:ListItem Value="LaptopMemory">Laptop Memory</asp:ListItem>
											<asp:ListItem Value="Digital_Camera">Digital Camera</asp:ListItem>
										</asp:DropDownList>, with ID:
										<asp:Label id="lblModelId" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" bgColor="lightgrey"><STRONG>Model:</STRONG></TD>
									<TD style="HEIGHT: 20px">
										<asp:Label id="lblManufacturer" runat="server"></asp:Label>,
										<asp:Label id="lblProductLine" runat="server"></asp:Label>,
										<asp:Label id="lblModel" runat="server"></asp:Label></TD>
									<TD style="HEIGHT: 20px"></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Memory Speed:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtSpeed" runat="server" MaxLength="10"></asp:TextBox>
										<asp:Label id="hdnSpeed" runat="server"></asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Memory Size:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtSize" runat="server" MaxLength="10"></asp:TextBox>
										<asp:Label id="hdnSize" runat="server"></asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Memory Specification:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtSpecification" runat="server" MaxLength="50"></asp:TextBox>
										<asp:Label id="hdnSpecification" runat="server"></asp:Label></TD>
									<TD>(Option)</TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 22px" bgColor="#d3d3d3"><STRONG>Announcement</STRONG></TD>
									<TD style="HEIGHT: 22px">
										<asp:CheckBox id="chkAnnounce" runat="server" Text="Announcement"></asp:CheckBox>
										<asp:Label id="hdnAnnounce" runat="server"></asp:Label></TD>
									<TD style="HEIGHT: 22px"></TD>
								</TR>
								<TR>
									<TD bgColor="#d3d3d3"><STRONG><FONT face="Arial, Helvetica" size="-1">Thumbnail Image&nbsp;</FONT>:</STRONG></TD>
									<TD colSpan="2">
										<asp:TextBox id="txtImage" runat="server" Width="500px" TextMode="MultiLine" Rows="10"></asp:TextBox>
										<asp:Label id="hdnImage" runat="server">Label</asp:Label></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Product Header:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtHeader" runat="server" Width="500px" MaxLength="2000" TextMode="MultiLine"
											Rows="10"></asp:TextBox>
										<asp:Label id="hdnHeader" runat="server"></asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Product URL:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtURL" runat="server" Width="500px" MaxLength="500"></asp:TextBox>
										<asp:Label id="hdnURL" runat="server"></asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD>
										<asp:Button id="btnAddNew" runat="server" Text="Add New" onclick="btnAddNew_Click"></asp:Button>&nbsp;&nbsp;&nbsp;
										<asp:Button id="btnEdit" runat="server" Text="Update" onclick="btnEdit_Click"></asp:Button></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD colSpan="3">
										<HR width="100%" SIZE="1">
										<asp:DataGrid id="grdSpecialAds" runat="server" Width="100%" Visible="False" AutoGenerateColumns="False"
											CellPadding="3" BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC">
											<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
											<ItemStyle Font-Size="10pt" ForeColor="Navy"></ItemStyle>
											<HeaderStyle Font-Size="10pt" Font-Bold="True" HorizontalAlign="Center" ForeColor="Navy" BackColor="LightGray"></HeaderStyle>
											<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
											<Columns>
												<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
												<asp:ButtonColumn Text="Delete" CommandName="Delete"></asp:ButtonColumn>
												<asp:TemplateColumn>
													<HeaderTemplate>
														Image
													</HeaderTemplate>
													<ItemTemplate>
														<img src='<%# DataBinder.Eval(Container.DataItem, "Image") %>'><!-- <%# DataBinder.Eval(Container.DataItem, "Image") %> -->
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="Table_name" SortExpression="Table_name" HeaderText="Memory Type"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="Fk_model_id" SortExpression="Fk_model_id" HeaderText="Model ID"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="Memory_speed" SortExpression="Memory_speed" HeaderText="Memory Speed"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="Memory_size" SortExpression="Memory_size" HeaderText="Memory Size"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="Special" SortExpression="Special" HeaderText="Special"></asp:BoundColumn>
												<asp:BoundColumn DataField="g_manufacture" SortExpression="g_manufacture" HeaderText="Manufacturer"></asp:BoundColumn>
												<asp:BoundColumn DataField="g_product_line" SortExpression="g_product_line" HeaderText="Product Line"></asp:BoundColumn>
												<asp:BoundColumn DataField="g_model" SortExpression="g_model" HeaderText="Model"></asp:BoundColumn>
												<asp:HyperLinkColumn DataNavigateUrlField="Url" DataTextField="Header" SortExpression="Url" HeaderText="Header"></asp:HyperLinkColumn>
												<asp:BoundColumn Visible="False" DataField="Header" SortExpression="Header" HeaderText="Header"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="Url" SortExpression="Url" HeaderText="URL"></asp:BoundColumn>
												<asp:BoundColumn DataField="Announce" SortExpression="Announce" HeaderText="Announce"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="Image" SortExpression="Image" HeaderText="Image"></asp:BoundColumn>
											</Columns>
											<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
										</asp:DataGrid></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
