<%@ Page language="c#" Inherits="memoryAdmin.TireSelector" CodeFile="WheelSelector.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Tire Selector</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="780" border="0">
				<TR>
					<TD class="title"><A href="default.aspx"><IMG alt="Memory-Up.com" src="http://www.memory-up.com/merchant2/images/logo_big.gif"
								border="0"></A>Wheel&nbsp;Selector</TD>
				</TR>
				<TR>
					<TD>
						<TABLE class="item" id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD>Please select the wheel...</TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 15px">
									<P align="right">Select Make..</P>
								</TD>
								<TD style="HEIGHT: 15px"><asp:dropdownlist id="lstMake" runat="server" AutoPostBack="True"></asp:dropdownlist><asp:label id="hdnMake" runat="server"></asp:label></TD>
								<TD style="HEIGHT: 15px"></TD>
							</TR>
							<TR>
								<TD>
									<P align="right">Select Model...</P>
								</TD>
								<TD><asp:dropdownlist id="lstModel" runat="server" AutoPostBack="True"></asp:dropdownlist><asp:label id="hdnModel" runat="server"></asp:label></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD>
									<P align="right">Select Model Year...</P>
								</TD>
								<TD><asp:dropdownlist id="lstModelYear" runat="server"></asp:dropdownlist><asp:label id="hdnModelYear" runat="server"></asp:label>Visit
									<asp:label id="lblCounter" runat="server"></asp:label>&nbsp;times by customer</TD>
								<TD>
									<asp:button id="btnAddModel" runat="server" Text="Add New"></asp:button></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD>
						<TABLE class="item" id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
							<TR>
								<TD colSpan="3">
									<HR width="100%" SIZE="1">
								</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 21px" bgColor="lightgrey">Make:</TD>
								<TD style="HEIGHT: 21px"><asp:textbox id="TextBox2" runat="server"></asp:textbox><asp:dropdownlist id="DropDownList2" runat="server"></asp:dropdownlist><asp:label id="lblOrAddMake" runat="server">Or</asp:label><asp:button id="btnNewMake" runat="server" Text="Add New"></asp:button></TD>
								<TD style="HEIGHT: 21px"><FONT color="red"><FONT color="red">(Required)</FONT></FONT></TD>
							</TR>
							<TR>
								<TD bgColor="lightgrey">Model:</TD>
								<TD><asp:textbox id="TextBox3" runat="server"></asp:textbox>
									<asp:DropDownList id="DropDownList4" runat="server"></asp:DropDownList>
									<asp:Label id="Label1" runat="server">Or</asp:Label>
									<asp:Button id="Button1" runat="server" Text="Button"></asp:Button></TD>
								<TD>(<FONT color="red">(Required)</FONT></TD>
							</TR>
							<TR>
								<TD bgColor="#d3d3d3">Model Year:</TD>
								<TD>
									<asp:DropDownList id="DropDownList1" runat="server"></asp:DropDownList>To
									<asp:DropDownList id="DropDownList3" runat="server"></asp:DropDownList></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD bgColor="lightgrey">Bolt Circle;</TD>
								<TD><asp:dropdownlist id="lstBolt" runat="server">
										<asp:ListItem Value="3">3</asp:ListItem>
										<asp:ListItem Value="4">4</asp:ListItem>
										<asp:ListItem Value="5">5</asp:ListItem>
										<asp:ListItem Value="6">6</asp:ListItem>
									</asp:dropdownlist>&nbsp;X
									<asp:textbox id="TextBox6" runat="server" Width="58px"></asp:textbox>&nbsp;MM</TD>
								<TD></TD>
							</TR>
							<TR>
								<TD bgColor="lightgrey">Offset:</TD>
								<TD><asp:textbox id="TextBox7" runat="server"></asp:textbox></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD bgColor="lightgrey">Original Factory Size:</TD>
								<TD><asp:dropdownlist id="lstFactorySize" runat="server">
										<asp:ListItem Value="13">13</asp:ListItem>
										<asp:ListItem Value="14">14</asp:ListItem>
										<asp:ListItem Value="15">15</asp:ListItem>
										<asp:ListItem Value="16">16</asp:ListItem>
										<asp:ListItem Value="17">17</asp:ListItem>
										<asp:ListItem Value="18">18</asp:ListItem>
										<asp:ListItem Value="19">19</asp:ListItem>
										<asp:ListItem Value="20">20</asp:ListItem>
										<asp:ListItem Value="21">21</asp:ListItem>
										<asp:ListItem Value="22">22</asp:ListItem>
										<asp:ListItem Value="23">23</asp:ListItem>
										<asp:ListItem Value="24">24</asp:ListItem>
										<asp:ListItem Value="25">25</asp:ListItem>
										<asp:ListItem Value="26">26</asp:ListItem>
										<asp:ListItem Value="27">27</asp:ListItem>
									</asp:dropdownlist></TD>
								<TD><FONT color="red">(Required)</FONT></TD>
							</TR>
							<TR>
								<TD bgColor="lightgrey">Optional Min, Size:</TD>
								<TD><asp:dropdownlist id="lstMinSize" runat="server">
										<asp:ListItem Value="13">13</asp:ListItem>
										<asp:ListItem Value="14">14</asp:ListItem>
										<asp:ListItem Value="15">15</asp:ListItem>
										<asp:ListItem Value="16">16</asp:ListItem>
										<asp:ListItem Value="17">17</asp:ListItem>
										<asp:ListItem Value="18">18</asp:ListItem>
										<asp:ListItem Value="19">19</asp:ListItem>
										<asp:ListItem Value="20">20</asp:ListItem>
										<asp:ListItem Value="21">21</asp:ListItem>
										<asp:ListItem Value="22">22</asp:ListItem>
										<asp:ListItem Value="23">23</asp:ListItem>
										<asp:ListItem Value="24">24</asp:ListItem>
										<asp:ListItem Value="25">25</asp:ListItem>
										<asp:ListItem Value="26">26</asp:ListItem>
										<asp:ListItem Value="27">27</asp:ListItem>
									</asp:dropdownlist></TD>
								<TD><FONT color="red">(Required)</FONT></TD>
							</TR>
							<TR>
								<TD bgColor="lightgrey">Optional Max. Size:</TD>
								<TD><asp:dropdownlist id="lstMaxSize" runat="server">
										<asp:ListItem Value="13">13</asp:ListItem>
										<asp:ListItem Value="14">14</asp:ListItem>
										<asp:ListItem Value="15">15</asp:ListItem>
										<asp:ListItem Value="16">16</asp:ListItem>
										<asp:ListItem Value="17">17</asp:ListItem>
										<asp:ListItem Value="18">18</asp:ListItem>
										<asp:ListItem Value="19">19</asp:ListItem>
										<asp:ListItem Value="20">20</asp:ListItem>
										<asp:ListItem Value="21">21</asp:ListItem>
										<asp:ListItem Value="22">22</asp:ListItem>
										<asp:ListItem Value="23">23</asp:ListItem>
										<asp:ListItem Value="24">24</asp:ListItem>
										<asp:ListItem Value="25">25</asp:ListItem>
										<asp:ListItem Value="26">26</asp:ListItem>
										<asp:ListItem Value="27">27</asp:ListItem>
									</asp:dropdownlist></TD>
								<TD><FONT color="red">(Required)</FONT></TD>
							</TR>
							<TR>
								<TD bgColor="lightgrey">Notes:</TD>
								<TD><asp:textbox id="TextBox4" runat="server" TextMode="MultiLine"></asp:textbox></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD bgColor="lightgrey">Internal Comments:</TD>
								<TD><asp:textbox id="TextBox5" runat="server" TextMode="MultiLine"></asp:textbox></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD bgColor="lightgrey">Picture:</TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD bgColor="lightgrey"></TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD bgColor="lightgrey"></TD>
								<TD></TD>
								<TD></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
