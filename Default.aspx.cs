using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for _default.
	/// </summary>
	public partial class _default : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            if (Session["LoginName"] == null)
            {
                Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
            }
			lblDesktopUpdate.Text = Mup_utilities.GetLatestUpdate("DesktopMemory").ToShortDateString();
			lblLaptopUpdate.Text = Mup_utilities.GetLatestUpdate("LaptopMemory").ToShortDateString();
			lblCameraUpdate.Text = Mup_utilities.GetLatestUpdate("Digital_Camera").ToShortDateString();
			lblPhoneUpdate.Text = Mup_utilities.GetLatestUpdate("Phone_model").ToShortDateString();
			lblPDAUpdate.Text = Mup_utilities.GetLatestUpdate("PDA_model").ToShortDateString();

            //if (!Page.IsPostBack)
            //{
            //    txtSearchQuery.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnSearch.UniqueID + "').click();return false;}} else {return true}; ");
            //}

            lblUserName.Text = Session["LoginName"].ToString();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.imgHome.Click += new System.Web.UI.ImageClickEventHandler(this.imgHome_Click);

		}
		#endregion

        protected void imgHome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("default.aspx");
		}

        protected void btnSearch_Click1(object sender, EventArgs e)
        {
            Response.Redirect("ModelSearch.aspx?search=" + txtSearchQuery.Text.Trim().Replace(" ", "+"));
        }
}
}
