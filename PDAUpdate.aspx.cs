using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for PDAUpdate.
	/// </summary>
	public partial class PDAUpdate : System.Web.UI.Page
	{
		private string strSqlConnString = ConfigurationSettings.AppSettings["cnstr"];
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			try
			{
				if (ucHeader != null)
				{
					ucHeader.PreLoad();
					ucHeader.SetPageTitle("PDA Selector");
				}
				if (ucModelSelect != null)
				{
					ucModelSelect.PreLoad("PDA_Model");
					ucModelSelect.SubmitModel += new ucModelSelectorEventHandler(ucNewModelSelect.SetNewModelParameter);
				}
				if (ucNewModelSelect != null)
				{
					ucNewModelSelect.PreLoad("PDA_model");
				}
				ucModelSelect.SubmitModel += new ucModelSelectorEventHandler(this.SetModelParameter);
			}
			catch (Exception ex)
			{
				Trace.Write(ex.Message);
			}

			if (!Page.IsPostBack)
			{
				btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure to delete?')){}else{return false;}");
				btnCancelAdd.Attributes.Add("onclick", "if(confirm('Are you sure to cancel add new item?')){}else{return false;}");
				btnCancelUpdate.Attributes.Add("onclick", "if(confirm('Are you sure to cancel update item?')){}else{return false;}");
				lstBattery.Attributes.Add("onChange", "document.all('txtBatterydesc').value = document.all('lstBattery').value;");
				btnAdd.Attributes.Add("onClick", "return CheckFields();");
				btnUpdate.Attributes.Add("onClick", "return CheckFields();");

                //if (Session["LoginName"] == null)
                //{
                //    Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
                //}

				Mup_utilities.fillFlashType(chkMemoryType);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		public void SetModelParameter(object sender, SubmitModelSearchEventArgs args)
		{
			ClearAllData();
			pnlUpdate.Visible = true;
			pnlInformation.Visible = false;
			lblModelExist.Visible = false;

			if (args.Model == null)
			{
				errorInvisible();
				ucModelSelect.Visible = false;

				btnAddPicture.Visible = false;
				btnDeletePicture.Visible = false;
				btnDelete.Visible = false;
				btnUpdate.Visible = false;
				btnCancelUpdate.Visible = false;
				imgPicture.Visible = false;

				btnAdd.Visible = true;
				btnCancelAdd.Visible = true;
			}
			else
			{
				errorInvisible();
				ucModelSelect.Visible = true;

				btnAddPicture.Visible = true;
				btnDeletePicture.Visible = true;
				btnDelete.Visible = true;
				btnUpdate.Visible = true;
				btnCancelUpdate.Visible = true;

				btnAdd.Visible = false;
				btnCancelAdd.Visible = false;

				updateSystemInformation();
			}
		}

		private void ClearAllData()
		{
			chkMemoryType.ClearSelection();
			txtInternalRAM.Text = "";
			txtInternalROM.Text = "";
			radMinMemorySize.ClearSelection();
			radMaxMemorySize.ClearSelection();
			lstBattery.ClearSelection();
			txtBatteryDesc.Text = "";
			txtBatteryCode.Text = "";
			txtProcessor.Text = "";
			txtOS.Text = "";
			txtDisplay.Text = "";
			txtGraphics.Text = "";
			radWireless.SelectedIndex = 0;
			chkBlueTooth.Checked = false;
			radSDIO.SelectedIndex = 0;
			txtNotes.Text = "";
			txtComments.Text = "";
		}

		private void errorInvisible()
		{
			ucNewModelSelect.errorInvisible();
			lblUpdateError.Visible = false;
			lblModelExist.Visible = false;
			lblMinMemoryError.Visible = false;
			lblMaxMemoryError.Visible = false;
		}

		private void updateSystemInformation()
		{
			SubmitModelSearchEventArgs modelUpdate;
			SqlDataReader drModel;

			pnlUpdate.Visible = true;
			pnlInformation.Visible = false;
			btnAdd.Visible = false;
			btnCancelAdd.Visible = false;
			btnUpdate.Visible = true;
			btnCancelUpdate.Visible = true;
			btnDelete.Visible = true;

			modelUpdate = ucModelSelect.GetUpdateModel();
			ucNewModelSelect.SetExistModel(modelUpdate);
			Mup_utilities.ShowBatteryItems(lstBattery);

            SqlConnection drConn = new SqlConnection(strSqlConnString);

			try
			{
				string strSql = "Select * from PDA_model LEFT OUTER join PDA_memory on PDA_memory.PDA_id = PDA_model.PDA_id ";
				strSql += "Where Manufacture = '" + modelUpdate.Manufacturer + "' ";
				strSql += "And Product_Line = '" + modelUpdate.ProductLine + "' ";
				strSql += "And Model = '" + modelUpdate.Model + "' ";
				
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				drModel = drComm.ExecuteReader();

				if (drModel.Read())
				{
					txtPDAId.Text = drModel["PDA_id"].ToString();
					if (drModel["counter"] != null && drModel["counter"].ToString() != "")
						lblViewed.Text = "Viewed by customers: " + (int)drModel["counter"];
					else
						lblViewed.Text = "";
					if (drModel["UpdateDate"] != DBNull.Value)
						lblLastUpdate.Text = "Last Update: " + ((DateTime)drModel["UpdateDate"]).ToShortDateString();
					else
						lblLastUpdate.Text = "";
					if (drModel["Internal_RAM"].ToString() != "")
						txtInternalRAM.Text  = drModel["Internal_RAM"].ToString();
					else
						txtInternalRAM.Text = "";
					if (drModel["Internal_ROM"].ToString() != "")
						txtInternalROM.Text  = drModel["Internal_ROM"].ToString();
					else
						txtInternalROM.Text = "";
                    if (drModel["Min_Size"] != DBNull.Value)
                    {
                        for (int i = 0; i < radMinMemorySize.Items.Count; i++)
                        {
                            if (radMinMemorySize.Items[i].Value == drModel["Min_Size"].ToString())
                                radMinMemorySize.Items[i].Selected = true;
                        }
                    }
                    else
                        radMinMemorySize.SelectedIndex = -1;
                    if (drModel["Max_size"] != DBNull.Value)
                    {
                        for (int i = 0; i < radMaxMemorySize.Items.Count; i++)
                        {
                            if (radMaxMemorySize.Items[i].Value == drModel["Max_size"].ToString())
                                radMaxMemorySize.Items[i].Selected = true;
                        }
                    }
                    else
                        radMaxMemorySize.SelectedIndex = -1;
					if (drModel["Battery"].ToString() != "")
					{
						for (int i = 0; i < lstBattery.Items.Count; i++)
						{
							if (lstBattery.Items[i].Text == drModel["Battery"].ToString())
							{
								lstBattery.SelectedIndex = i;
								txtBatteryDesc.Text = lstBattery.SelectedValue;
							}
						}
					}
					else
						lstBattery.SelectedIndex = 0;
					//txtBatteryDesc.Text = drModel["Battery_desc"].ToString();
					if (drModel["Mf_battery_code"].ToString() != "")
						txtBatteryCode.Text = drModel["Mf_Battery_code"].ToString();
					else
						txtBatteryCode.Text = "";
					if (drModel["Processor"].ToString() != "")
						txtProcessor.Text = drModel["Processor"].ToString();
					else
						txtProcessor.Text = "";
					if (drModel["LCD"].ToString() != "")
						txtDisplay.Text = drModel["LCD"].ToString();
					else
						txtDisplay.Text = "";
					if (drModel["OS"].ToString() != "")
						txtOS.Text = drModel["OS"].ToString();
					else
						txtOS.Text = "";
					if (drModel["Graphics"].ToString() != "")
						txtGraphics.Text = drModel["Graphics"].ToString();
					else
						txtGraphics.Text = "";
					if (drModel["Wireless"].ToString() != "")
						radWireless.SelectedValue = drModel["Wireless"].ToString();
					else
						radWireless.SelectedIndex = 0;
					if (drModel["Blue_tooth"].ToString() != "")
						chkBlueTooth.Checked = Convert.ToBoolean(drModel["Blue_tooth"].ToString());
					else
						chkBlueTooth.Checked = false;
					if (drModel["SDIO"].ToString() != "")
						radSDIO.SelectedValue = drModel["SDIO"].ToString();
					else
						radSDIO.SelectedValue = "N";
					if (drModel["Notes"].ToString() != "")
						txtNotes.Text = drModel["Notes"].ToString();
					else
						txtNotes.Text = "";
					if (drModel["Comments"].ToString() != "")
						txtComments.Text = drModel["Comments"].ToString();
					else
						txtComments.Text = "";
					if (drModel["Picture"].ToString() == "")
					{
						btnAddPicture.Visible = true;
						btnDeletePicture.Visible = false;
						imgPicture.Visible = false;
					}
					else
					{
						btnAddPicture.Visible = false;
						btnDeletePicture.Visible = true;
						imgPicture.Visible = true;

						imgPicture.ImageUrl = System.IO.Path.Combine(Server.UrlPathEncode("/MfdImages/ModelImages"), drModel["picture"].ToString());
					}

					chkMemoryType.ClearSelection();
					for (int i = 0; i < chkMemoryType.Items.Count; i++)
					{
						if (drModel["memory_code"].ToString() == chkMemoryType.Items[i].Value)
							chkMemoryType.Items[i].Selected = true;
					}
					while (drModel.Read())
					{
						for (int i = 0; i < chkMemoryType.Items.Count; i++)
						{
							if (drModel["memory_code"].ToString() == chkMemoryType.Items[i].Value)
								chkMemoryType.Items[i].Selected = true;
						}
					}
				}
				else
					ClearAllData();

				drConn.Close();
				drConn.Dispose();
			}
			catch(MupException e)
			{
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
				Response.Write(e.MupMessage);
			}
		}

		private bool errorCheck()
		{
			if (!ucNewModelSelect.ErrorCheck())
				return false;
			if (radMinMemorySize.SelectedIndex == -1)
			{
				lblMinMemoryError.Visible = true;
				return false;
			}
			if (radMaxMemorySize.SelectedIndex == -1)
			{
				lblMaxMemoryError.Visible = true;
				return false;
			}
			if (int.Parse(radMinMemorySize.SelectedValue) > int.Parse(radMaxMemorySize.SelectedValue))
			{
				lblMinMemoryError.Visible = true;
				return false;
			}
			else
				return true;
		}

		protected void btnAdd_Click(object sender, System.EventArgs e)
		{
			addNewPDA();
		}

		private void addNewPDA()
		{
			SubmitModelSearchEventArgs newModel;

			errorInvisible();
			if (errorCheck() == false)
				return;

			newModel = ucNewModelSelect.GetSelectMode();

            SqlConnection drChConn = new SqlConnection(strSqlConnString);
            SqlConnection drConn = new SqlConnection(strSqlConnString);
            SqlConnection dsConn = new SqlConnection(strSqlConnString);

			try
			{
				string strSql = "Select * from PDA_model Where Manufacture = '" + newModel.Manufacturer + "' ";
				strSql += "And Product_Line = '" + newModel.ProductLine + "' ";
				strSql += "And Model = '" + newModel.Model + "' ";

				
				drChConn.Open();
				SqlCommand drChComm = new SqlCommand(strSql, drChConn);
				SqlDataReader drCheck = drChComm.ExecuteReader();

				if (drCheck.Read())
				{
					updateSystemInformation();
					lblModelExist.Visible = true;
					drChConn.Close();
					drChConn.Dispose();
					return;
				}
                else
                {
                    drChConn.Close();
                    drChConn.Dispose();
                }

				strSql = "Insert Into PDA_model (Manufacture, Product_line, Model, Internal_RAM, Internal_ROM, Min_size, Max_size, ";
				strSql += "Battery, Mf_battery_code, Processor, OS, LCD, Graphics, Wireless, Blue_tooth, SDIO, ";
				strSql += "Notes, Comments, UpdateDate) Values (";
				strSql += "'" + newModel.Manufacturer + "', ";
				strSql += "'" + newModel.ProductLine + "', ";
				strSql += "'" + newModel.Model + "', ";
				if (txtInternalRAM.Text.Trim() != "")
					strSql += txtInternalRAM.Text.Trim() + ", ";
				else
					strSql += "null, ";
				if (txtInternalROM.Text.Trim() != "")
					strSql += txtInternalROM.Text.Trim() + ", ";
				else
					strSql += "null, ";
				strSql += radMinMemorySize.SelectedValue + ", ";
				strSql += radMaxMemorySize.SelectedValue + ", ";
				if (lstBattery.SelectedIndex > 0)
					strSql += "'" + lstBattery.SelectedItem.Text + "', ";
				else
					strSql += "null, ";
				strSql += "'" + txtBatteryCode.Text + "', ";
				strSql += "'" + txtProcessor.Text.Trim().Replace("'", "''") + "', ";
				strSql += "'" + txtOS.Text.Trim().Replace("'", "''") + "', ";
				strSql += "'" + txtDisplay.Text.Trim().Replace("'", "''") + "', ";
				strSql += "'" + txtGraphics.Text.Trim().Replace("'", "''") + "', ";
				strSql += "'" + radWireless.SelectedValue + "', ";
				if (chkBlueTooth.Checked)
					strSql += "1, ";
				else
					strSql += "0, ";
				strSql += "'" + radSDIO.SelectedValue + "', ";
				strSql += "'" + txtNotes.Text + "', ";
				strSql += "'" + txtComments.Text + "', ";
				strSql += "'" + DateTime.Now + "') ";

				
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					strSql = "Select PDA_id from PDA_Model Where Manufacture = '" + newModel.Manufacturer + "' ";
					strSql += "And Product_Line = '" + newModel.ProductLine + "' ";
					strSql += "And Model = '" + newModel.Model + "' ";

					
					drConn.Open();
					SqlCommand drComm = new SqlCommand(strSql, drConn);
					SqlDataReader drModel = drComm.ExecuteReader();

					if (drModel.Read())
					{
						txtPDAId.Text = drModel["PDA_id"].ToString();
						Mup_utilities.saveMemoryCard("PDA", txtPDAId.Text, chkMemoryType);
					}

					drConn.Close();
					drConn.Dispose();
				}

                dsConn.Close();
				dsConn.Dispose();

				ucModelSelect.SetNewModel(newModel, "PDA_Model");
				showSystemInformation();
			}
			catch (MupException ex)
			{
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
                if (drChConn != null)
                {
                    drChConn.Close();
                    drChConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}

		private void showSystemInformation()
		{
			string strSql = "SELECT * FROM PDA_model LEFT OUTER JOIN PDA_memory On PDA_model.PDA_id = PDA_memory.PDA_id ";
			strSql += "INNER JOIN Memory_card On Memory_card.code = PDA_memory.Memory_Code ";
			strSql += "where PDA_model.PDA_id = " + txtPDAId.Text + " ";
			strSql += "Order by PDA_model.PDA_id";

			ucModelSelect.Visible = true;
			pnlInformation.Visible = true;
			pnlUpdate.Visible = false;

            SqlConnection drConn = new SqlConnection(strSqlConnString);

			try
			{
				
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				SqlDataReader drModel = drComm.ExecuteReader();

				if (drModel.Read())
				{
					lblManufacture.Text = drModel["Manufacture"].ToString();
					lblProductLine.Text = drModel["Product_line"].ToString();
					lblModel.Text = drModel["Model"].ToString();
					lblInterMemoryRAM.Text = drModel["Internal_RAM"].ToString() + "M";
					lblInterMemoryROM.Text = drModel["Internal_ROM"].ToString() + "M";
					lblMinMemorySize.Text = drModel["Min_size"].ToString() + "M";
					lblMaxMemorySize.Text = drModel["Max_size"].ToString() + "M";
					lblBattery.Text = drModel["Battery"].ToString();
					lblBatteryCode.Text = drModel["Mf_battery_code"].ToString();
					lblProcessor.Text = drModel["Processor"].ToString();
					lblOS.Text = drModel["OS"].ToString();
					lblDisplay.Text = drModel["LCD"].ToString();
					lblGraphics.Text = drModel["Graphics"].ToString();
					lblWireless.Text = drModel["Wireless"].ToString();
					if (drModel["Blue_tooth"].ToString() != "")
					{
						if ((bool)drModel["Blue_tooth"] == true)
							lblBlueTooth.Text = "Supported";
						else
							lblBlueTooth.Text = "Not Supported";
					}
					else
						lblBlueTooth.Text = "Not Supported";
					if (drModel["SDIO"].ToString() == "S")
						lblSDIO.Text = "SDIO Supported";
					else if (drModel["SDIO"].ToString() == "W")
						lblSDIO.Text = "SDIO Now Supported";
					else
						lblSDIO.Text = "Not Supported";
					lblNotes.Text = drModel["Notes"].ToString();
					lblComments.Text = drModel["Comments"].ToString();
					if (drModel["Picture"].ToString() == "")
						imgShowPicture.Visible = false;
					else
					{
						imgShowPicture.Visible = true;
						imgShowPicture.ImageUrl = System.IO.Path.Combine(Server.UrlPathEncode("/MfdImages/ModelImages"), drModel["picture"].ToString());
					}
					lblMemoryType.Text = drModel["Description"].ToString();
					while (drModel.Read())
					{
						lblMemoryType.Text += ", " + drModel["Description"].ToString();
					}
				}

				drConn.Close();
				drConn.Dispose();
			}
			catch (MupException ex)
			{
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			if (saveUpdate() == false)
				return;
		}

		private bool saveUpdate()
		{
			SubmitModelSearchEventArgs modelSave;

			// Validation check invisible at the beginning
			errorInvisible();

			//Check the required fields
			if (errorCheck() == false)
				return false;

			modelSave = ucNewModelSelect.GetSelectMode();

            SqlConnection dsConn = new SqlConnection(strSqlConnString);

			try
			{
				string strSql = "Update PDA_model set Manufacture = '" + modelSave.Manufacturer + "', ";
				strSql += "Product_line = '" + modelSave.ProductLine + "', ";
				strSql += "Model = '" + modelSave.Model + "', ";
				if (txtInternalRAM.Text.Trim() != "")
					strSql += "Internal_RAM = " + txtInternalRAM.Text.Trim() + ", ";
				else
					strSql += "Internal_RAM = null, ";
				if (txtInternalROM.Text.Trim() != "")
					strSql += "Internal_ROM = " + txtInternalROM.Text.Trim() + ", ";
				else
					strSql += "Internal_ROM = null, ";
				strSql += "Min_size = " + radMinMemorySize.SelectedValue + ", ";
				strSql += "Max_size = " + radMaxMemorySize.SelectedValue + ", ";
				if (lstBattery.SelectedIndex > 0)
					strSql += "Battery = '" + lstBattery.SelectedItem.Text + "', ";
				else
					strSql += "Battery = null, ";
				strSql += "Mf_battery_code = '" + txtBatteryCode.Text.Trim() + "', ";
				strSql += "Processor = '" + txtProcessor.Text.Trim() + "', ";
				strSql += "LCD = '" + txtDisplay.Text.Trim().Replace("'", "''") + "', ";
				strSql += "OS = '" + txtOS.Text.Trim().Replace("'", "''") + "', ";
				strSql += "Graphics = '" + txtGraphics.Text.Trim().Replace("'", "''") + "', ";
				strSql += "Wireless = '" + radWireless.SelectedValue + "', ";
				if (chkBlueTooth.Checked)
					strSql += "Blue_tooth = 1, ";
				else
					strSql += "Blue_tooth = 0, ";
				strSql += "SDIO = '" + radSDIO.SelectedValue + "', ";
				strSql += "Notes = '" + txtNotes.Text.Trim().Replace("'", "''") + "', ";
				strSql += "Comments = '" + txtComments.Text.Trim().Replace("'", "''") + "', ";
				strSql += "UpdateDate = '" + DateTime.Now + "' ";
				strSql += "Where PDA_id = " + txtPDAId.Text + " ";

				
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					Mup_utilities.saveMemoryCard("PDA", txtPDAId.Text, chkMemoryType);
				}
				else
				{
					lblUpdateError.Visible = true;
				}

                dsConn.Close();
				dsConn.Dispose();
				showSystemInformation();
				return true;
			}
			catch (MupException ex)
			{
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
				Response.Write (ex.MupMessage);
				return false;
			}
		}

		protected void btnShowUpdate_Click(object sender, System.EventArgs e)
		{
			updateSystemInformation();
		}

		protected void btnCancelUpdate_Click(object sender, System.EventArgs e)
		{
			showSystemInformation();
		}

		protected void btnCancelAdd_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("PDAUpdate.aspx");
		}

		protected void btnDelete_Click(object sender, System.EventArgs e)
		{
            SqlConnection dsConn = new SqlConnection(strSqlConnString);

			try
			{
				string strSql = "Delete from PDA_model where PDA_id = " + txtPDAId.Text + " ";

				
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					Mup_utilities.DeleteMemroyCard("PDA_Memory", "PDA", txtPDAId.Text);

					ClearAllData();
					ucModelSelect.Visible = true;
					pnlUpdate.Visible = false;

					ucModelSelect.InitialSelector();
				}

                dsConn.Close();
				dsConn.Dispose();
			}
			catch (MupException ex)
			{
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}

		protected void btnAddPicture_Click(object sender, System.EventArgs e)
		{
			Response.Write("<script language=\"javascript\">window.open('AddPicture.aspx?type=PDA_model&system_id=" + txtPDAId.Text + "','Picture','toolbar=no,center=yes,location=no,titlebar=no,resizable=no,status=no,scrollbars=no,menubar=no,width=500,height=400');</script>");
		}

		protected void btnDeletePicture_Click(object sender, System.EventArgs e)
		{
			String strSql;
            SqlConnection dsConn = new SqlConnection(strSqlConnString);

			try
			{
				strSql = "Update PDA_model set picture = null where PDA_id = " + txtPDAId.Text;

				
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					btnAddPicture.Visible = true;
					btnDeletePicture.Visible = false;
					imgPicture.Visible = false;
				}

                dsConn.Close();
				dsConn.Dispose();
			}
			catch (MupException ex)
			{
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
				Response.Write (ex.MupMessage);
			}
		}
        protected void btnUpdate2_Click(object sender, EventArgs e)
        {
            if (saveUpdate() == false)
                return;
        }
}
}
