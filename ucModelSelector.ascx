<%@ Reference Page="~/newModel.aspx" %>
<%@ Control Language="c#" Inherits="memoryAdmin.ucModelSelector" CodeFile="ucModelSelector.ascx.cs" %>
<TABLE id="Table1" class="item" cellSpacing="0" cellPadding="3" width="780" border="0">
	<TR>
		<TD><asp:label id="lblGuide" runat="server"></asp:label></TD>
		<TD></TD>
		<TD><asp:textbox id="txtTableName" runat="server" Visible="False"></asp:textbox></TD>
	</TR>
	<TR>
		<TD align="right">Select the manufacturer:</TD>
		<TD><asp:dropdownlist id="lstManufacture" runat="server" AutoPostBack="True" onselectedindexchanged="lstManufacture_SelectedIndexChanged"></asp:dropdownlist></TD>
		<TD><asp:label id="hdnManufacture" runat="server" Visible="False"></asp:label></TD>
		<TD></TD>
	</TR>
	<TR>
		<TD align="right">Select the product line:</TD>
		<TD><asp:dropdownlist id="lstProductLine" runat="server" AutoPostBack="True" onselectedindexchanged="lstProductLine_SelectedIndexChanged"></asp:dropdownlist></TD>
		<TD><asp:label id="hdnProductLine" runat="server" Visible="False"></asp:label></TD>
		<TD></TD>
	</TR>
	<TR>
		<TD align="right">Select the model:</TD>
		<TD><asp:dropdownlist id="lstModel" runat="server" AutoPostBack="True" onselectedindexchanged="lstModel_SelectedIndexChanged"></asp:dropdownlist></TD>
		<TD><asp:label id="hdnModel" runat="server" Visible="False"></asp:label></TD>
		<TD><asp:button id="btnAddModel" runat="server" Text="Add New" onclick="btnAddModel_Click"></asp:button></TD>
	</TR>
	<TR>
		<TD colSpan="4">
			<HR width="100%" SIZE="1">
		</TD>
	</TR>
</TABLE>
