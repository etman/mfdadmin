<%@ Page language="c#" Inherits="memoryAdmin._default" CodeFile="default.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>default</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" runat="server">
			<TABLE cellSpacing="0" cellPadding="5" width="780" border="0">
				<TR>
					<TD><asp:imagebutton id="imgHome" runat="server" ImageUrl="/images/memoryselector/bd_fb.png" OnClick="imgHome_Click"></asp:imagebutton></TD>
				</TR>
				<TR>
					<TD class="item_title"><asp:hyperlink id="hplLaptopMemorySelector" runat="server" NavigateUrl="LaptopUpdate.aspx">Laptop Memory Selector Administrator</asp:hyperlink>&nbsp;(Last 
						Update:
						<asp:Label id="lblLaptopUpdate" runat="server"></asp:Label>)</TD>
				</TR>
				<TR>
					<TD class="item_title"><asp:hyperlink id="hplDesktopMemorySelector" runat="server" NavigateUrl="DesktopUpdate.aspx">Desktop Memory Selector Administrator</asp:hyperlink>&nbsp;(Last 
						Update:
						<asp:Label id="lblDesktopUpdate" runat="server"></asp:Label>)</TD>
				</TR>
				<TR>
					<TD class="item_title">
						<asp:HyperLink id="hplCameraSelector" runat="server" NavigateUrl="CameraUpdate.aspx">Digital Camera Selector Administrator</asp:HyperLink>&nbsp;(Last 
						Update:
						<asp:Label id="lblCameraUpdate" runat="server"></asp:Label>)</TD>
				</TR>
				<TR>
					<TD class="item_title">
						<asp:HyperLink id="lnkPhoneSelector" runat="server" NavigateUrl="CellPhoneUpdate.aspx">Cellular Phone Selector Administrator</asp:HyperLink>&nbsp;(Last 
						Update:
						<asp:Label id="lblPhoneUpdate" runat="server"></asp:Label>)</TD>
				</TR>
				<TR>
					<TD class="item_title">
						<asp:HyperLink id="HyperLink3" runat="server" NavigateUrl="PDAUpdate.aspx">PDA Selector Administrator</asp:HyperLink>&nbsp;(Last 
						Update:
						<asp:Label id="lblPDAUpdate" runat="server"></asp:Label>)</TD>
				</TR>
				<TR>
					<TD class="item_title">
						<asp:HyperLink id="hplSpecialADs" runat="server" NavigateUrl="specialADs.aspx">Memory Selector Special Promotion</asp:HyperLink></TD>
				</TR>
				<TR>
					<TD class="item_title">
						<asp:HyperLink id="lnkLaptopCount" runat="server" NavigateUrl="MemoryInfoList.aspx?Type=laptop">Laptop Memory Selector Hit Count</asp:HyperLink></TD>
				</TR>
				<TR>
					<TD class="item_title">
						<asp:HyperLink id="lnkDesktopCount" runat="server" NavigateUrl="DesktopInfo.aspx">Desktop Memory Selector Hit Count</asp:HyperLink></TD>
				</TR>
				<TR>
					<TD class="item_title"><asp:hyperlink id="hplTechnical" runat="server" NavigateUrl="CustomerSupportAdmin.aspx">Technical Support & RMA Administrator</asp:hyperlink></TD>
				</TR>
				<TR>
					<TD class="item_title"><asp:hyperlink id="hplComments" runat="server" NavigateUrl="commentsAdmin.aspx">Comments Administrator</asp:hyperlink></TD>
				</TR>
				<TR>
					<TD class="item_title">
						<asp:HyperLink id="HyperLink1" runat="server" NavigateUrl="AffiliateList.aspx">Affiliate Order</asp:HyperLink></TD>
				</TR>
				<TR>
					<TD class="item_title">
						<asp:HyperLink id="HyperLink2" runat="server" NavigateUrl="MemoryPrice.aspx">Memory Lowest Price</asp:HyperLink></TD>
				</TR>
                <tr>
                    <td class="item_title">
                        <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="MemoryScannerHistory.aspx">Memory Profiler Usage</asp:HyperLink></td>
                </tr>
                <tr>
                    <td>
                        Login:
                        <asp:Label ID="lblUserName" runat="server"></asp:Label></td>
                </tr>
                 <tr>
                    <td>
                        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                        
                        <asp:TextBox ID="txtSearchQuery" runat="server"></asp:TextBox>
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click1" Text="Search" />
                        </asp:Panel>
                        
                        </td>
                </tr>
			</TABLE>
		</form>
		
	</body>
</HTML>
