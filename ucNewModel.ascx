<%@ Control Language="c#" Inherits="memoryAdmin.ucNewModel" CodeFile="ucNewModel.ascx.cs" %>
<TABLE class="item" id="Table1" cellSpacing="1" cellPadding="3" width="780" border="0">
	<TR>
		<TD width="170" bgColor="lightgrey"><STRONG>Manufacturer:</STRONG></TD>
		<TD><asp:textbox id="txtManufacture" runat="server" MaxLength="50"></asp:textbox><asp:dropdownlist id="lstCurrManuf" runat="server" AutoPostBack="True" onselectedindexchanged="lstCurrManuf_SelectedIndexChanged"></asp:dropdownlist><asp:label id="lblOrAddManuf" runat="server">Or</asp:label><asp:button id="btnNewManuf" runat="server" Text="Add New" onclick="btnNewManuf_Click"></asp:button><asp:label id="lblManufError" runat="server" Visible="False" ForeColor="Red">Please enter manufacturer!</asp:label></TD>
		<TD><FONT color="red">(Requied)</FONT></TD>
	</TR>
	<TR>
		<TD bgColor="lightgrey"><STRONG>Product Line</STRONG></TD>
		<TD><asp:textbox id="txtProductLine" runat="server" MaxLength="50"></asp:textbox><asp:dropdownlist id="lstCurrLine" runat="server"></asp:dropdownlist><asp:label id="lblOrAddLine" runat="server">Or</asp:label><asp:button id="btnNewLine" runat="server" Text="Add New" onclick="btnNewLine_Click"></asp:button><asp:label id="lblProductLineError" runat="server" Visible="False" ForeColor="Red">Please enter product line!</asp:label></TD>
		<TD><FONT color="red">(Requied)</FONT></TD>
	</TR>
	<TR>
		<TD bgColor="lightgrey"><STRONG>Model:</STRONG></TD>
		<TD><asp:textbox id="txtModel" runat="server" MaxLength="50"></asp:textbox><asp:label id="lblModelError" runat="server" Visible="False" ForeColor="Red">Please enter a model!</asp:label><asp:textbox id="txtTableName" runat="server" Visible="False"></asp:textbox><br />
            Please note: "<span style="font-weight: bold; color: red">First word</span>" in
            the model is the most important in order to idendify this model. Please put unique
            model number on the first word. For example, M9969LL/A.</TD>
		<TD><FONT color="red">(Requied)</FONT></TD>
	</TR>
</TABLE>
