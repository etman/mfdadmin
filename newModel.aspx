<%@ Page language="c#" Inherits="memoryAdmin.newModel" CodeFile="newModel.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>newModel</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryStyle.css" type="text/css" rel="stylesheet">
		<base target="_self">
		<script lang="javascript">
			function submitModel()
			{
				var strManufacturer;
				var strProdLine;
				var strModel;
				
				if (document.all('txtManufacturer'))
				{
					strManufacturer = document.all('txtManufacturer').value;
				}
				else
				{
					strManufacturer = document.all('drpManufacturer').value;
				}
				
				if (document.all('txtProdLine'))
				{
					strProdLine = document.all('txtProdLine').value;
				}
				else
				{
					strProdLine = document.all('drpProdLine').value;
				}
				
				strModel = document.all('txtModel').value;
				
				var aryModel = new Array(strManufacturer, strProdLine, strModel);
				window.returnValue = aryModel;
			}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE height="97" cellSpacing="0" cellPadding="0" width="375" border="0" align="center">
				<TR class="standard">
					<TD width="7" background="images/MemorySelector/tblThin_ul.gif" height="25"></TD>
					<TD width="361" background="images/MemorySelector/tblThin_tc.gif" height="25"></TD>
					<TD width="7" background="images/MemorySelector/tblThin_ur.gif" height="25"></TD>
				</TR>
				<TR>
					<TD width="7" background="images/MemorySelector/tblThin_left.gif" height="100%"><IMG src="images/MemorySelector/spacer.gif" width="7"></TD>
					<TD vAlign="top" width="361" bgColor="#fefce5">
						<TABLE id="Table1" cellSpacing="1" cellPadding="3" width="100%" border="0">
							<TR class="standard">
								<TD>
									<asp:TextBox id="txtMemoryType" runat="server" Visible="False"></asp:TextBox></TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR class="standard">
								<TD>Manufacturer:</TD>
								<TD><asp:dropdownlist id="drpManufacturer" runat="server" CssClass="select" AutoPostBack="True" onselectedindexchanged="drpManufacturer_SelectedIndexChanged"></asp:dropdownlist><asp:textbox id="txtManufacturer" runat="server" CssClass="select" Visible="False"></asp:textbox></TD>
								<TD noWrap><asp:panel id="pnlNewManuf" runat="server">Or 
<asp:Button id="btnNewManufacturer" runat="server" CssClass="select" Text="Add New" onclick="btnNewManufacturer_Click"></asp:Button></asp:panel></TD>
							</TR>
							<TR class="standard">
								<TD>Product Line:</TD>
								<TD><asp:dropdownlist id="drpProdLine" runat="server" CssClass="select"></asp:dropdownlist><asp:textbox id="txtProdLine" runat="server" CssClass="select" Visible="False"></asp:textbox></TD>
								<TD noWrap><asp:panel id="pnlNewProdLine" runat="server">Or 
<asp:button id="btnNewProdLine" runat="server" CssClass="select" Text="Add New" onclick="btnNewProdLine_Click"></asp:button></asp:panel></TD>
							</TR>
							<TR class="standard">
								<TD>Model:</TD>
								<TD><asp:textbox id="txtModel" runat="server" CssClass="select"></asp:textbox></TD>
								<TD></TD>
							</TR>
							<TR class="standard">
								<TD></TD>
								<TD><asp:button id="btnSubmit" runat="server" Text="OK" CssClass="select" onclick="btnSubmit_Click"></asp:button>&nbsp;&nbsp;&nbsp;
									<asp:Button id="btnCancel" runat="server" Text="Cancel" CssClass="select"></asp:Button></TD>
								<TD></TD>
							</TR>
						</TABLE>
					<TD width="7" background="images/MemorySelector/tblThin_right.gif" height="100%"><IMG src="images/MemorySelector/spacer.gif" width="7"></TD>
				</TR>
				<TR>
					<TD width="7" background="images/MemorySelector/tblThin_ll.gif" height="7"></TD>
					<TD width="361" background="images/MemorySelector/tblThin_bc.gif" height="7"></TD>
					<TD width="7" background="images/MemorySelector/tblThin_lr.gif" height="7"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
