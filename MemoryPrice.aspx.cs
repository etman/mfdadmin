using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Configuration;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for MemoryPrice.
	/// </summary>
	public partial class MemoryPrice : System.Web.UI.Page
	{
		private string strSqlConnString = ConfigurationSettings.AppSettings["cnstr"];
		private dsMemoryPrice priceDataSet = new dsMemoryPrice();
		private string strOdbcConnString = ConfigurationSettings.AppSettings["odbcStr"];
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			ucHeader.PreLoad();
			ucHeader.SetPageTitle("Memory Up Lowest Price");
			

			if (!Page.IsPostBack)
			{
				chkSelectAll.Attributes.Add("onClick", "checkAllPartCode();");
				txtGrdCount.Attributes.Add("type", "hidden");
				GetDefualtValue();
				ShowMemoryList();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void ShowMemoryList()
		{
			try
			{
				SqlConnection priceConn = new SqlConnection(strSqlConnString);
				SqlCommand priceComm = new SqlCommand();
				SqlDataAdapter priceAdapter = new SqlDataAdapter();

				priceAdapter.SelectCommand = priceComm;
				priceAdapter.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																								new System.Data.Common.DataTableMapping("Table", "MemoryPrice", new System.Data.Common.DataColumnMapping[] {
																																																				  new System.Data.Common.DataColumnMapping("pk_Memory_price", "pk_Memory_price"),
																																																				  new System.Data.Common.DataColumnMapping("Part_code", "Part_code"),
																																																				  new System.Data.Common.DataColumnMapping("Lowest_price", "Lowest_price"),
																																																				  new System.Data.Common.DataColumnMapping("Comments", "Comments")})});

				
				priceComm.Connection = priceConn;

				priceComm.CommandText = "Select * ";
				priceComm.CommandText += "From MemoryPrice ";
				priceComm.CommandText += "Order by Part_code ";

				priceComm.Connection = priceConn;

				priceAdapter.Fill(priceDataSet, "MemoryPrice");

				grdMemoryPrice.DataSource = priceDataSet.MemoryPrice.DefaultView;
				grdMemoryPrice.DataBind();

				txtGrdCount.Text = grdMemoryPrice.Items.Count.ToString();
			}
			catch(Exception ex)
			{
				Response.Write(ex.Message);
			}
		}

		private void GetDefualtValue()
		{
			SqlDataReader drPartCode;
			string tempCodeSpeed;
			string tempCodeSize;
			string tempCodeFlash;
			string tempSizeFlash;
			int intCodeSpeed;
			int intCodeSize;
			int intFlashSize;
			int tempTopSpeed = 400;
			int tempLowSpeed = 100;
			int tempTopSize = 1024;
			int tempLowSize = 128;
			int tempFlashTopSize = 2048;
			int tempFlashLowSize = 32;
			char[] strDash = {'-'};
			string[] strCodeSplid;

			try
			{
				string strSql = "Select Part_code from MemoryPrice ";
				SqlConnection drConn = new SqlConnection(strSqlConnString);
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				drPartCode = drComm.ExecuteReader();

				while (drPartCode.Read())
				{
					intCodeSpeed = 0;
					intCodeSize = 0;
					intFlashSize = 0;

					strCodeSplid = ((string)drPartCode["Part_code"]).Split(strDash);
					if (strCodeSplid[1].Length == 2)
					{
						tempCodeFlash = strCodeSplid[1];
						tempSizeFlash = strCodeSplid[2];
						while (intFlashSize == 0)
						{
							try
							{
								intFlashSize = Convert.ToInt16(tempSizeFlash);
							}
							catch
							{
								tempSizeFlash = tempSizeFlash.Remove(tempSizeFlash.Length - 1, 1);
							}
						}
						if (intFlashSize > tempFlashTopSize)
							tempFlashTopSize = intFlashSize;
						if (intFlashSize < tempFlashLowSize)
							tempFlashLowSize = intFlashSize;
					}
					else
					{
						tempCodeSpeed = strCodeSplid[1];
						while (intCodeSpeed == 0)
						{
							try
							{
								intCodeSpeed = Convert.ToInt16(tempCodeSpeed);
							}
							catch
							{
								tempCodeSpeed = tempCodeSpeed.Remove(tempCodeSpeed.Length - 1, 1);
							}
						}
						tempCodeSize = strCodeSplid[2];
						while (intCodeSize == 0)
						{
							try
							{
								intCodeSize = Convert.ToInt32(tempCodeSize);
							}
							catch
							{
								tempCodeSize = tempCodeSize.Remove(tempCodeSize.Length, 1);
							}
						}
						if (intCodeSpeed > tempTopSpeed)
							tempTopSpeed = intCodeSpeed;
						if (intCodeSpeed < tempLowSpeed)
							tempLowSpeed = intCodeSpeed;
						if (intCodeSize > tempTopSize)
							tempTopSize = intCodeSize;
						if (intCodeSize < tempLowSize)
							tempLowSize = intCodeSize;
					}
				}

				txtLowSize.Text = tempLowSize.ToString();
				txtTopSize.Text = tempTopSize.ToString();
				txtLowSpeed.Text = tempLowSpeed.ToString();
				txtTopSpeed.Text = tempTopSpeed.ToString();
				txtFlashMin.Text = tempFlashLowSize.ToString();
				txtFlashMax.Text = tempFlashTopSize.ToString();

				drPartCode.Close();
				drConn.Dispose();
			}
			catch(Exception ex)
			{
				Response.Write(ex.Message);
			}
		}

		protected void btnDelete_Click(object sender, System.EventArgs e)
		{
			string sql;

			try
			{
				for (int i = 0; i < grdMemoryPrice.Items.Count; i++)
				{
					if (((CheckBox)grdMemoryPrice.Items[i].FindControl("chkDelete")).Checked == true)
					{
						sql = "Delete From MemoryPrice ";
						sql += "Where Part_code = '" + ((Label)grdMemoryPrice.Items[i].FindControl("lblPartCode")).Text + "' ";

						SqlConnection dsConn = new SqlConnection(strSqlConnString);
						SqlCommand dsComm = new SqlCommand();
						dsConn.Open();
						dsComm.Connection = dsConn;
						dsComm.CommandText = sql;

						dsComm.ExecuteNonQuery();

						dsConn.Dispose();
					}
				}
				ShowMemoryList();
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}

		private ArrayList showCodeList()
		{
			ArrayList lstCode = new ArrayList();
			ArrayList lstFlashCode = Mup_utilities.ListFlashCode();

			int[] arySpeed = {66, 100, 133, 266, 333, 400, 433, 466, 500, 533, 550, 566, 667, 800, 1066};

			try
			{
				int lowSize = Convert.ToInt32(txtLowSize.Text.Trim());
				int topSize = Convert.ToInt32(txtTopSize.Text.Trim());
				int lowSpeed = Convert.ToInt32(txtLowSpeed.Text.Trim());
				int topSpeed = Convert.ToInt32(txtTopSpeed.Text.Trim());
				int lowFlash = Convert.ToInt32(txtFlashMin.Text.Trim());
				int topFlash = Convert.ToInt32(txtFlashMax.Text.Trim());

				for (int i = 0; i < arySpeed.Length;i++)
				{
					if (arySpeed[i] >= lowSpeed && arySpeed[i] <= topSpeed)
					{
						for (int j = lowSize; j <= topSize; j = j * 2)
						{
							lstCode.Add("M-" + arySpeed[i].ToString() + "-" + j.ToString());
							lstCode.Add("M-" + arySpeed[i].ToString() + "N-" + j.ToString());
							lstCode.Add("M-" + arySpeed[i].ToString() + "M-" + j.ToString());
							lstCode.Add("M-" + arySpeed[i].ToString() + "D-" + j.ToString());
							lstCode.Add("M-" + arySpeed[i].ToString() + "DN-" + j.ToString());
							lstCode.Add("M-" + arySpeed[i].ToString() + "DE-" + j.ToString());
							lstCode.Add("M-" + arySpeed[i].ToString() + "DER-" + j.ToString());
							lstCode.Add("M-" + arySpeed[i].ToString() + "DM-" + j.ToString());
							lstCode.Add("M-" + arySpeed[i].ToString() + "E-" + j.ToString());
							lstCode.Add("M-" + arySpeed[i].ToString() + "ER-" + j.ToString());
							lstCode.Add("M-" + arySpeed[i].ToString() + "NE-" + j.ToString());
						}
					}
				}

				for (int k = 0; k < lstFlashCode.Count; k++)
				{
					for (int m = lowFlash; m <= topFlash; m = m * 2)
					{
						lstCode.Add("M-" + lstFlashCode[k].ToString() + "-" + m.ToString());
					}
				}
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}

			return lstCode;
		}

		private double GetLowestPrice(string tempCode)
		{
			string strSql;
			double lowPrice = 0;
			Object objLowPrice;
			string strLowPrice;

			try
			{
				strSql = "Select TOP 1 \"products\".PRICE ";
				strSql += "From \"products\" ";
				strSql += "Where \"products\".CODE Like '" + tempCode + "%' ";
				strSql += "And \"products\".ACTIVE = -1 ";
				strSql += "Order by \"products\".PRICE ";

				OdbcConnection dsConn = new OdbcConnection(strOdbcConnString);
				OdbcCommand dsComm = new OdbcCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				objLowPrice = dsComm.ExecuteScalar();

				try
				{
					strLowPrice = objLowPrice.ToString();
					lowPrice = Convert.ToDouble(strLowPrice);
				}
				catch
				{
					lowPrice = 0;
				}

				dsConn.Dispose();
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}

			return lowPrice;
		}

		protected void btnLatest_Click(object sender, System.EventArgs e)
		{
			ArrayList lstCode = new ArrayList();
			double lowPrice = 0;
			double exiPrice = 0;
			bool codeExist = false;
			string strCode;
			string strComments = "";

			lstCode = showCodeList();

			for (int i = 0; i < lstCode.Count; i++)
			{
				lowPrice = GetLowestPrice((string)lstCode[i]);
				
				if (lowPrice > 0)
				{
					codeExist = false;
					exiPrice = 0;
					strComments = "";
					for (int j = 0; j < grdMemoryPrice.Items.Count; j++)
					{
						if (((CheckBox)grdMemoryPrice.Items[j].FindControl("chkUpdate")).Checked &&
							((Label)grdMemoryPrice.Items[j].FindControl("lblPartCode")).Text == (string)lstCode[i])
						{
							((TextBox)grdMemoryPrice.Items[j].FindControl("txtLowPrice")).Text = lowPrice.ToString();
							exiPrice = lowPrice;
							strComments = ((TextBox)grdMemoryPrice.Items[j].FindControl("txtComments")).Text;
							codeExist = true;
						}
						else if (((Label)grdMemoryPrice.Items[j].FindControl("lblPartCode")).Text == (string)lstCode[i])
						{
							strCode = (string)lstCode[i];
							exiPrice = Convert.ToDouble(((TextBox)grdMemoryPrice.Items[j].FindControl("txtLowPrice")).Text);
							strComments = ((TextBox)grdMemoryPrice.Items[j].FindControl("txtComments")).Text;
							codeExist = true;
						}
					}

					if (codeExist == false)
					{
						DataRow newRow = priceDataSet.MemoryPrice.NewRow();
						newRow["Part_code"] = (string)lstCode[i];
						newRow["Lowest_price"] = lowPrice;
						newRow["Comments"] = strComments;
						priceDataSet.MemoryPrice.Rows.Add(newRow);
					}
					else
					{
						DataRow newRow = priceDataSet.MemoryPrice.NewRow();
						newRow["Part_code"] = (string)lstCode[i];
						newRow["Lowest_price"] = exiPrice;
						newRow["Comments"] = strComments;
						priceDataSet.MemoryPrice.Rows.Add(newRow);
					}
				}
			}

			grdMemoryPrice.DataSource = priceDataSet;
			grdMemoryPrice.DataBind();
		}

		protected void btnSave_Click(object sender, System.EventArgs e)
		{
			DeleteMemoryPrice();
			for (int i = 0; i < grdMemoryPrice.Items.Count; i++)
			{
				SaveMemoryPrice(((Label)grdMemoryPrice.Items[i].FindControl("lblPartCode")).Text,
					((TextBox)grdMemoryPrice.Items[i].FindControl("txtComments")).Text,
					Convert.ToDouble(((TextBox)grdMemoryPrice.Items[i].FindControl("txtLowPrice")).Text));
			}
		}

		private void SaveMemoryPrice(string strCode, string strComment, double dblPrice)
		{
			string sql;

			try
			{
				sql = "Insert into MemoryPrice ";
				sql += "(Part_code, Lowest_price, comments) values (";
				sql += "'" + strCode + "', ";
				sql += "'" + dblPrice + "', ";
				sql += "'" + strComment + "') ";

				SqlConnection dsConn = new SqlConnection(strSqlConnString);
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = sql;
				dsComm.ExecuteNonQuery();

				dsConn.Dispose();
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}

		private void DeleteMemoryPrice()
		{
			string sql;

			try
			{
				sql = "Delete From MemoryPrice ";

				SqlConnection dsConn = new SqlConnection(strSqlConnString);
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = sql;
				dsComm.ExecuteNonQuery();

				dsConn.Dispose();
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}
	}
}
