<%@ Page language="c#" Inherits="memoryAdmin.CustomerSupportAdmin" CodeFile="CustomerSupportAdmin.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomerSupportAdmin</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="5" width="100%" border="0">
				<tr>
					<td><asp:imagebutton id="imgHome" runat="server" ImageUrl="/images/memoryselector/bd_fb.png"></asp:imagebutton><b>Technical 
							Support and RMA Administration Page.</b>
					</td>
				</tr>
				<tr>
					<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana">Search Order #:
						<asp:textbox id="txtOrderNo" runat="server"></asp:textbox>&nbsp;Case #:
						<asp:textbox id="txtCaseNo" runat="server"></asp:textbox>&nbsp;RMA #:
						<asp:textbox id="txtRmaNo" runat="server"></asp:textbox></td>
				</tr>
				<TR>
					<TD style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana">Search Last Name:
						<asp:textbox id="txtLastName" runat="server"></asp:textbox>First Name:
						<asp:textbox id="txtFirstName" runat="server"></asp:textbox><asp:button id="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click"></asp:button></TD>
				</TR>
				<tr>
					<td><asp:panel id="pnlNew" Runat="server">
							<asp:datalist id="newList" Runat="server" CellPadding="3" GridLines="Both" BorderColor="#990033">
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#cc3366" Font-Size="8pt" Font-Names="verdana"
									HorizontalAlign="Center" Wrap="False"></HeaderStyle>
								<HeaderTemplate>
									<asp:label ID="Label13" Runat="server">Case Number
										</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#cc3366" />
									<asp:label ID="Label14" Runat="server">RMA Number
										</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#cc3366" />
									<asp:label ID="Label15" Runat="server">Invoice Number
										</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#cc3366" />
									<asp:label ID="Label16" Runat="server">Order Number</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#cc3366" />
									<asp:label ID="Label17" Runat="server">Last Name</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#cc3366" />
									<asp:label ID="Label18" Runat="server">First Name</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#cc3366" />
									<asp:label ID="Label20" Runat="server">Status</asp:label>
								</HeaderTemplate>
								<ItemStyle Font-Size="8pt" Font-Names="verdana" ForeColor="#224488" HorizontalAlign="Center"
									Wrap="False" BackColor="#ffcccc"></ItemStyle>
								<ItemTemplate>
									<!-- technicalAdmin.aspx?case_number=<%# DataBinder.Eval(Container.DataItem, "Case_number") %> --><A href="technicalAdmin.aspx?case_number=<%# DataBinder.Eval(Container.DataItem, "Case_number") %>"><%# DataBinder.Eval(Container.DataItem, "Case_number") %></A>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" align="center" bgcolor="#ffcccc" />
									<asp:hyperlink id="Hyperlink2" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "RMA_number") %>
									</asp:hyperlink>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" align="center" bgcolor="#ffcccc" />
									<asp:Label id="Label22" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "Invoice_number") %>
									</asp:Label>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" align="center" bgcolor="#ffcccc" />
									<asp:Label id="Label23" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "Order_number") %>
									</asp:Label>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" bgcolor="#ffcccc" />
									<asp:label id="Label24" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "Last_Name") %>
									</asp:label>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" bgcolor="#ffcccc" />
									<asp:Label id="Label25" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "First_name") %>
									</asp:Label>
									
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" bgcolor="#ffcccc" nowrap />
									<asp:Label id="Label21" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "status") %>
									</asp:Label>
								</ItemTemplate>
							</asp:datalist>
						</asp:panel></td>
				</tr>
				<tr>
					<td><asp:panel id="runingSupport" Runat="server">
							<asp:datalist id="supportStatus" Runat="server" CellPadding="3" GridLines="Both" BorderColor="#666699">
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#330066" Font-Size="8pt" Font-Names="verdana"
									HorizontalAlign="Center" Wrap="False"></HeaderStyle>
								<HeaderTemplate>
									<asp:label ID="caseNumberHeader" Runat="server">Case Number
										</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#330066" />
									<asp:label ID="RMANumberHeader" Runat="server">RMA Number
										</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#330066" />
									<asp:label ID="invoiceNumberHeader" Runat="server">Invoice Number
										</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#330066" />
									<asp:label ID="orderNumberHeader" Runat="server">Order Number</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#330066" />
									<asp:label ID="lastNameHeader" Runat="server">Last Name</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#330066" />
									<asp:label ID="firstNameHeader" Runat="server">First Name</asp:label>
									
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#330066" />
									<asp:label ID="statusHeader" Runat="server">Status</asp:label>
								</HeaderTemplate>
								<ItemStyle Font-Size="8pt" Font-Names="verdana" ForeColor="#224488" HorizontalAlign="Center"
									Wrap="False"></ItemStyle>
								<ItemTemplate>
									<!-- technicalAdmin.aspx?case_number=<%# DataBinder.Eval(Container.DataItem, "Case_number") %> --><A href="technicalAdmin.aspx?case_number=<%# DataBinder.Eval(Container.DataItem, "Case_number") %>"><%# DataBinder.Eval(Container.DataItem, "Case_number") %></A>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" align="center" />
									<asp:hyperlink id="RMANumber" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "RMA_number") %>
									</asp:hyperlink>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" align="center" />
									<asp:Label id="InvoiceNumber" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "Invoice_number") %>
									</asp:Label>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" align="center" />
									<asp:Label id="orderNumber" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "Order_number") %>
									</asp:Label>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" />
									<asp:label id="lastName" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "Last_Name") %>
									</asp:label>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" />
									<asp:Label id="firstName" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "First_name") %>
									</asp:Label>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" nowrap />
									
									<asp:Label id="status" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "status") %>
									</asp:Label>
								</ItemTemplate>
							</asp:datalist>
						</asp:panel></td>
				</tr>
				<tr>
					<td><asp:button id="btnShowComplete" Text="Show Complete" Runat="server" onclick="btnShowComplete_Click"></asp:button></td>
				</tr>
				<tr>
					<td><asp:panel id="completeSupport" Runat="server" Visible="False">
							<asp:DataList id="completeList" Runat="server" CellPadding="3" GridLines="Both" BorderColor="#333300">
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#666600" Font-Size="8pt" Font-Names="verdana"
									HorizontalAlign="Center" Wrap="False"></HeaderStyle>
								<HeaderTemplate>
									<asp:label ID="Label1" Runat="server">Case Number
										</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#666600" />
									<asp:label ID="Label2" Runat="server">RMA Number
										</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#666600" />
									<asp:label ID="Label3" Runat="server">Invoice Number
										</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#666600" />
									<asp:label ID="Label4" Runat="server">Order Number</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#666600" />
									<asp:label ID="Label5" Runat="server">Last Name</asp:label>
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#666600" />
									<asp:label ID="Label6" Runat="server">First Name</asp:label>
									
									<td style="FONT-WEIGHT: Bold; FONT-SIZE: 8pt; COLOR: #ffffff; FONT-FAMILY: verdana"
										align="center" bgcolor="#666600" />
									<asp:label ID="Label7" Runat="server">Status</asp:label>
								</HeaderTemplate>
								<ItemStyle Font-Size="8pt" Font-Names="verdana" ForeColor="#000000" HorizontalAlign="Center"
									Wrap="False"></ItemStyle>
								<ItemTemplate>
									<!-- technicalAdmin.aspx?case_number=<%# DataBinder.Eval(Container.DataItem, "Case_number") %> -->
									<a href="technicalAdmin.aspx?case_number=<%# DataBinder.Eval(Container.DataItem, "Case_number") %>">
										<%# DataBinder.Eval(Container.DataItem, "Case_number") %>
									</a>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" align="center" />
									<asp:hyperlink ID="Hyperlink1" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "RMA_number") %>
									</asp:hyperlink>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" align="center" />
									<asp:Label ID="Label8" Runat="server"></asp:Label>
									<%# DataBinder.Eval(Container.DataItem, "Invoice_number") %>
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" align="center" />
									<asp:Label id="Label9" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "Order_number") %>
									</asp:Label><td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" align="center" />
									<asp:label id="Label10" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "Last_Name") %>
									</asp:label><td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" align="center" />
									<asp:Label ID="Label11" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "First_name") %>
									</asp:Label>
									
									<td style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: verdana" align="center" />
									<asp:Label ID="Label12" Runat="server">
										<%# DataBinder.Eval(Container.DataItem, "status") %>
									</asp:Label>
								</ItemTemplate>
							</asp:DataList>
						</asp:panel></td>
				</tr>
				<tr>
					<td><asp:button id="btnShowRuningProblem" Text="Show Listing" Runat="server" Visible="False" onclick="btnShowRuningProblem_Click"></asp:button></td>
				</tr>
				<tr>
					<td></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
