using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using web;
using MyClassRef;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for AffiliateEdit.
	/// </summary>
	public partial class AffiliateEdit : System.Web.UI.Page
	{

		private string strSqlConnString = ConfigurationSettings.AppSettings["cnstr"];
		private dsAffiliateOrder affDataSet = new dsAffiliateOrder();
		private SqlDataAdapter affAdapter = new SqlDataAdapter();
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (Request.Params["user_name"] == "")
			{
				Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
			}

			showAffiliateOrder(Request.Params["user_name"]);
			affAdapter.Fill(affDataSet, "Affiliate_order");
			if (!Page.IsPostBack)
			{
                MupUtilities.showStatList(drpStates);
                MupUtilities.showIdentifyType(drpIdentifyType);
                MupUtilities.showTaxClassification(drpTaxClass);
                showAffiliateInfo(Request.Params["user_name"]);
				grdAffiliate.DataSource = affDataSet.Affiliate_order.DefaultView;
				grdAffiliate.DataBind();

				//				for (int i = 0; i < grdAffiliate.Items.Count; i++)
				//				{
				//					DropDownList drpTempStatus = (DropDownList)grdAffiliate.Items[i].FindControl("drpStatus");
				//					SonicCube.Mup.AffiliateStatusList(drpTempStatus);
				//				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.grdAffiliate.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdAffiliate_CancelCommand);
			this.grdAffiliate.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdAffiliate_EditCommand);
			this.grdAffiliate.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdAffiliate_UpdateCommand);
			this.grdAffiliate.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdAffiliate_DeleteCommand);
			this.grdAffiliate.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdAffiliate_ItemDataBound);

		}
		#endregion

		private void showAffiliateInfo(string strUserName)
		{
			SqlDataReader drUserName;

			try
			{
				string strSql = "Select * from Affiliate ";
				strSql += "where User_name = '" + strUserName + "' ";
				SqlConnection drConn = new SqlConnection(strSqlConnString);
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				drUserName = drComm.ExecuteReader();

				if (drUserName.Read())
				{
					lblUserName.Text = drUserName["User_name"].ToString();
					txtPassword.Text = MyClassRef.clsEncrypt.DecryptText(drUserName["Password"].ToString());
					txtFirstName.Text = drUserName["First_name"].ToString();
					txtLastName.Text = drUserName["Last_name"].ToString();
					txtEmail.Text = drUserName["Email"].ToString();
					txtPhone.Text = drUserName["Phone"].ToString();
					txtFax.Text = drUserName["Fax"].ToString();
					txtAddress1.Text = drUserName["Address1"].ToString();
					txtAddress2.Text = drUserName["Address2"].ToString();
					txtCity.Text = drUserName["City"].ToString();
					drpStates.SelectedValue = drUserName["State"].ToString();
					txtZip.Text = drUserName["Zip"].ToString();
					txtPayable.Text = drUserName["Check_pay"].ToString();
					txtIdentify.Text = drUserName["Tax_id"].ToString();
					drpIdentifyType.SelectedValue = drUserName["Tax_id_type"].ToString();
					drpTaxClass.SelectedValue = drUserName["Tax_class_id"].ToString();
					drpUserPayment.SelectedValue = drUserName["Payment"].ToString();
					drpPaymentType.SelectedValue = drUserName["Payment"].ToString();
					lblRegisterDate.Text = ((DateTime)drUserName["Submit_date"]).ToShortDateString();
			}

				drUserName.Close();
				drConn.Dispose();
			}
			catch (Exception ex)
			{
				Response.Write(ex.Message);
			}
		}

		private void showAffiliateOrder(string strUserName)
		{
			
			SqlConnection affConn = new SqlConnection(strSqlConnString);
			SqlCommand affComm = new SqlCommand();
			

			affAdapter.SelectCommand = affComm;
			affAdapter.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																							new System.Data.Common.DataTableMapping("Table", "Affilate_order", new System.Data.Common.DataColumnMapping[] {
																																																			  new System.Data.Common.DataColumnMapping("Order_no", "Order_no"),
																																																			  new System.Data.Common.DataColumnMapping("Order_value", "Order_value"),
																																																			  new System.Data.Common.DataColumnMapping("A_Order_date", "A_Order_date"),
																																																			  new System.Data.Common.DataColumnMapping("Rebate_per", "Rebate_per"),
																																																			  new System.Data.Common.DataColumnMapping("Rebate_amount", "Rebate_amount"),
																																																			  new System.Data.Common.DataColumnMapping("Update_date", "Update_date"),
																																																			  new System.Data.Common.DataColumnMapping("Payment_type", "Payment_type"),
																																																			  new System.Data.Common.DataColumnMapping("Payment_date", "Payment_date"),
																																																			  new System.Data.Common.DataColumnMapping("Ref_no", "Ref_no"),
																																																			  new System.Data.Common.DataColumnMapping("Description", "Description")})});

				
			affComm.Connection = affConn;

			affComm.CommandText = "Select Order_no, Order_value, Rebate_per, Rebate_amount, Convert(varchar(10), Order_date, 101) As A_Order_date, Update_date, Description, Payment_type, Payment_date, Ref_no ";
			affComm.CommandText += "From Affiliate_order ";
			affComm.CommandText += "INNER JOIN Affiliate_status ON Affiliate_order.status = Affiliate_status.pk_id ";
			affComm.CommandText += "Where fk_User_name = '" + strUserName + "' ";
			affComm.CommandText += "And Order_date > '" + DateTime.Today.AddYears(-1) + "' ";
			affComm.CommandText += "And status <> '80' ";
			affComm.CommandText += "Order by Update_date desc ";

			affComm.Connection = affConn;
		}

		private void grdAffiliate_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if (e.Item.Cells[8].Text == "Deleted")
			{
				Response.Write("<script language=\"javascript\">alert(\"This order has been deleted!!\");</script>");
				return;
			}

			grdAffiliate.DataSource = affDataSet.Affiliate_order.DefaultView;
			grdAffiliate.DataKeyField = "Order_no";
			grdAffiliate.EditItemIndex = e.Item.ItemIndex;
			grdAffiliate.DataBind();
		}

		private void grdAffiliate_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			grdAffiliate.DataSource = affDataSet.Affiliate_order.DefaultView;
			grdAffiliate.DataKeyField = "Order_no";
			grdAffiliate.EditItemIndex = -1;
			grdAffiliate.DataBind();
		}

		private void grdAffiliate_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			string sql;

			if (e.Item.Cells[8].Text == "Deleted")
			{
				Response.Write("<script language=\"javascript\">alert(\"This order has been deleted!!\");</script>");
				return;
			}

			try
			{
				sql = "Update Affiliate_order SET status = '80', ";
				sql += "Update_date = '" + DateTime.Now + "' ";
				sql += "Where Order_no = '" + e.Item.Cells[2].Text + "' ";

				SqlConnection dsConn = new SqlConnection(strSqlConnString);
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = sql;

				dsComm.ExecuteNonQuery();

				dsAffiliateOrder.Affiliate_orderDataTable affTable = affDataSet.Affiliate_order;
				dsAffiliateOrder.Affiliate_orderRow rowUpdate = (dsAffiliateOrder.Affiliate_orderRow)affTable.Rows[e.Item.ItemIndex];

				rowUpdate.Description = "Deleted";

				grdAffiliate.DataSource = affDataSet.Affiliate_order.DefaultView;
				grdAffiliate.EditItemIndex = -1;
				grdAffiliate.DataBind();
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}

		private void grdAffiliate_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			double dblOrderValue;
			string strPercent;
			string strAmount;
			double dblPercent;
			string strStatus;
			TextBox txtRebatePer;
			TextBox txtRebateAmount;
			string sql;
			DateTime currentTime = DateTime.Now;
			string strPayment;
			string strPayDate = "";
			string strRef;

			try
			{
				txtRebatePer = (TextBox)e.Item.FindControl("txtEditRebatePer");
				txtRebateAmount = (TextBox)e.Item.FindControl("txtEditRebateAmount");

				dblOrderValue = Convert.ToDouble(e.Item.Cells[4].Text.TrimStart('$'));
				strPercent = txtRebatePer.Text.TrimEnd('%');
				strAmount = txtRebateAmount.Text.TrimStart('$');
				dblPercent =  Convert.ToDouble(strPercent) / 100.0;
				strStatus = ((DropDownList)e.Item.FindControl("drpStatusEdit")).SelectedValue;
				strPayment = ((DropDownList)e.Item.FindControl("drpPaymentEdit")).SelectedValue;
				if (((TextBox)e.Item.FindControl("txtCompleteDate")).Text.Trim() != "")
					strPayDate = ((TextBox)e.Item.FindControl("txtCompleteDate")).Text.Trim();
				strRef = ((TextBox)e.Item.FindControl("txtRefNo")).Text.Trim();
				if (txtRebatePer.Text.Trim() != "")
				{
					strAmount = Convert.ToString(dblOrderValue * dblPercent);
				}

				sql = "Update Affiliate_order SET Rebate_per = " + dblPercent + ", ";
				sql += "Rebate_amount = " + strAmount + ", status = " + strStatus + ", ";
				sql += "Update_date = '" + currentTime + "', ";
				sql += "Payment_type = '" + strPayment + "', ";
				if (strPayDate != "")
					sql += "Payment_date = '" + strPayDate + "', ";
				else
					sql += "Payment_date = null, ";
				sql += "Ref_no = '" + strRef + "' ";
				sql += "Where Order_no = '" + e.Item.Cells[2].Text + "' ";

				SqlConnection dsConn = new SqlConnection(strSqlConnString);
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = sql;

				dsComm.ExecuteNonQuery();

				dsAffiliateOrder.Affiliate_orderDataTable affTable = affDataSet.Affiliate_order;
				dsAffiliateOrder.Affiliate_orderRow rowUpdate = (dsAffiliateOrder.Affiliate_orderRow)affTable.Rows[e.Item.ItemIndex];

				rowUpdate.Rebate_per = dblPercent;
				rowUpdate.Rebate_amount = Convert.ToDouble(strAmount);
				rowUpdate.Update_date = currentTime;
				rowUpdate.Description = ((DropDownList)e.Item.FindControl("drpStatusEdit")).SelectedItem.Text;
				rowUpdate.Payment_type = strPayment;
				if (strPayDate != "")
					rowUpdate.Payment_date = DateTime.Parse(strPayDate);
				else
					rowUpdate.Payment_date = DateTime.Parse("1/1/1900");
				rowUpdate.Ref_no = strRef;

				grdAffiliate.DataSource = affDataSet.Affiliate_order.DefaultView;
				grdAffiliate.EditItemIndex = -1;
				grdAffiliate.DataBind();
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}

		protected void btnAddNew_Click(object sender, System.EventArgs e)
		{
			if (txtRebatePer.Text.Trim() != "")
			{
				txtRebateAmn.Text = Convert.ToString(Convert.ToDouble(txtOrderValue.Text.Trim()) * Convert.ToDouble(txtRebatePer.Text.Trim()) / 100.0);
			}

			try
			{
				string strSql = "Insert Into Affiliate_order (fk_User_name, Order_no, Order_value, Order_date, Rebate_per, Rebate_amount, Update_date, status, Payment_type) Values (";
				strSql += "'" + Request.Params["user_name"] + "', ";
				strSql += "'" + txtOrderNo.Text.Trim() + "', ";
				strSql += "'" + txtOrderValue.Text.Trim() + "', ";
				strSql += "'" + txtOrderDate.Text.Trim() + "', ";
				strSql += "'" + Convert.ToDouble(txtRebatePer.Text.Trim()) / 100.0 + "', ";
				strSql += "'" + txtRebateAmn.Text.Trim() + "', ";
				strSql += "'" + DateTime.Now + "', ";
				strSql += "'10', ";
				strSql += "'" + drpPaymentType.SelectedValue + "') ";

				SqlConnection dsConn = new SqlConnection(strSqlConnString);
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					txtOrderNo.Text = "";
					txtOrderValue.Text = "";
					txtOrderDate.Text = "";
					txtRebatePer.Text = "";
					txtRebateAmn.Text = "";
				}
				dsConn.Dispose();

				Response.Redirect("AffiliateEdit.aspx?user_name=" + Request.Params["user_name"]);
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			try
			{
				string strSql = "Update Affiliate set ";
				strSql += "Password = '" + MyClassRef.clsEncrypt.EncryptText(txtPassword.Text.Trim()) + "', ";
				strSql += "First_name = '" + txtFirstName.Text.Trim().Replace("'", "''") + "', ";
				strSql += "Last_name = '" + txtLastName.Text.Trim().Replace("'", "''") + "', ";
				strSql += "Email = '" + txtEmail.Text.Trim().Replace("'", "''") + "', ";
				strSql += "Phone = '" + txtPhone.Text.Trim().Replace("'", "''") + "', ";
				strSql += "Fax = '" + txtFax.Text.Trim().Replace("'", "''") + "', ";
				strSql += "Address1 = '" + txtAddress1.Text.Trim().Replace("'", "''") + "', ";
				strSql += "Address2 = '" + txtAddress2.Text.Trim().Replace("'", "''") + "', ";
				strSql += "City = '" + txtCity.Text.Trim().Replace("'", "''") + "', ";
				strSql += "State = '" + drpStates.SelectedItem.Value + "', ";
				strSql += "Zip = '" + txtZip.Text.Trim().Replace("'", "''") + "', ";
				strSql += "Check_pay = '" + txtPayable.Text.Trim().Replace("'", "''") + "', ";
				string tempString = txtIdentify.Text.Trim().Substring(0, 4);
				if (txtIdentify.Text.Trim().Substring(0, 4) != "****")
					strSql += "Tax_id = '" + txtIdentify.Text.Trim().Replace("'", "''") + "', ";
				strSql += "Tax_id_type = '" + drpIdentifyType.SelectedItem.Value + "', ";
				strSql += "Tax_class_id = '" + drpTaxClass.SelectedItem.Value + "', ";
				strSql += "Update_date = '" + DateTime.Now + "', ";
				strSql += "Payment = '" + drpUserPayment.SelectedValue + "' ";
				strSql += "Where user_name = '" + Request.Params["User_Name"].ToString() + "' ";

				SqlConnection dsConn = new SqlConnection(strSqlConnString);
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					// Insert into database success
					Response.Redirect("AffiliateEdit.aspx?user_name=" + Request.Params["user_name"]);
				}
				else
				{
					// Insert into database failed
				}
			}
			catch (Exception ex)
			{
				Response.Write(ex.Message);
			}
		}

		public SqlDataReader showStatusList()
		{
			SqlDataReader drStatus;

			try
			{
				string strSql = "Select * from Affiliate_status ";

				SqlConnection drConn = new SqlConnection(strSqlConnString);
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				drStatus = drComm.ExecuteReader();
				
				return drStatus;

//				drStatus.Close();
//				drConn.Dispose();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private void grdAffiliate_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.EditItem)
			{
				DropDownList drpStatusTemp = (DropDownList)e.Item.FindControl("drpStatusEdit");
				drpStatusTemp.SelectedIndex = drpStatusTemp.Items.IndexOf(drpStatusTemp.Items.FindByText(((Label)e.Item.FindControl("lblStatusEdit")).Text));

				if (((Label)e.Item.FindControl("lblPaymentEdit")).Text.Trim() != "")
				{
					DropDownList drpPaymentTemp = (DropDownList)e.Item.FindControl("drpPaymentEdit");
					drpPaymentTemp.SelectedValue = ((Label)e.Item.FindControl("lblPaymentEdit")).Text;
				}
			}
		}
	}
}
