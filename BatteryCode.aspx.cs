using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for BatteryCode.
	/// </summary>
	public partial class BatteryCode : System.Web.UI.Page
	{

		protected memoryAdmin.dsBatteryComp memoryDataSet1;
		protected System.Data.SqlClient.SqlDataAdapter memoryAdapter;
		protected System.Data.SqlClient.SqlConnection memoryConn;
		protected System.Data.SqlClient.SqlCommand memorySelectCommand;
		private string strSqlConnString = ConfigurationSettings.AppSettings["cnstr"];
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		private void loadOemCode(string strMfCode)
		{
			try
			{
				memoryConn = new System.Data.SqlClient.SqlConnection();
				memoryAdapter = new System.Data.SqlClient.SqlDataAdapter();
				memorySelectCommand = new System.Data.SqlClient.SqlCommand();
				memoryDataSet1 = new memoryAdmin.dsBatteryComp();

				memoryAdapter.SelectCommand = memorySelectCommand;
				this.memoryAdapter.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																										new System.Data.Common.DataTableMapping("Table", "SpecialADs", new System.Data.Common.DataColumnMapping[] {
																																																					  new System.Data.Common.DataColumnMapping("Mf_battery_code", "Mf_battery_code"),
																																																					  new System.Data.Common.DataColumnMapping("Oem_battery_code", "Oem_battery_code")})});
				memoryConn.ConnectionString = strSqlConnString;
				memorySelectCommand.CommandText = "Select * from Battery_comp where mf_battery_code = '" + strMfCode + "' ";
				memorySelectCommand.Connection = memoryConn;
				memoryAdapter.Fill(memoryDataSet1, "Battery_comp");
				grdCompatable.DataSource = memoryDataSet1.Battery_comp.DefaultView;
				grdCompatable.DataBind();
				grdCompatable.Visible = true;
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}

		protected void btnSubmit_Click(object sender, System.EventArgs e)
		{
		
		}
	}
}
