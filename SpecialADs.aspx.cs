using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for SpecialADs.
	/// </summary>
	public partial class SpecialADs : System.Web.UI.Page
	{

		private string strSqlConnString = ConfigurationSettings.AppSettings["cnstr"];
		protected string strPageType;
		protected System.Data.SqlClient.SqlDataAdapter memoryAdapter;
		protected System.Data.SqlClient.SqlConnection memoryConn;
		protected System.Data.SqlClient.SqlCommand memorySelectCommand;
		protected memoryAdmin.dsSpecialADs memoryDataSet1;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (!Page.IsPostBack)
			{
                //if (Session["LoginName"] == null)
                //{
                //    Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
                //}
				ListItem li;

				// The other's shouldn't contain data yet
				li = new ListItem("(Select Page Type)","");
				lstManufacture.Items.Add (li);
				lstProductLine.Items.Add (li);
				lstModel.Items.Add (li);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.grdSpecialAds.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdSpecialAds_EditCommand);
			this.grdSpecialAds.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdSpecialAds_DeleteCommand);

		}
		#endregion

		protected void radPageType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			strPageType = radPageType.SelectedItem.Value;

			hdnPageType.Text = strPageType;

			ListItem li;

			// The other's shouldn't contain data yet
			lstProductLine.Items.Clear();
			lstModel.Items.Clear();
			li = new ListItem("(Select Manufacturer)","");
			lstManufacture.Items.Add (li);
			lstProductLine.Items.Add (li);
			lstModel.Items.Add (li);

			clearAllData();
			fillManufacture(lstManufacture, strPageType);
		}

		private void fillManufacture(DropDownList manufactureList, string tableName)
		{
			SqlDataReader drManufacture;

			try
			{
				string strSql = "Select distinct manufacture from " + tableName + " order by manufacture";
				SqlConnection drConn = new SqlConnection(strSqlConnString);
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				drManufacture = drComm.ExecuteReader();
			
				manufactureList.DataSource = drManufacture;
				manufactureList.DataTextField = "Manufacture";
				manufactureList.DataValueField = "Manufacture";
				manufactureList.DataBind();

				drManufacture.Close();
				drConn.Dispose();
			}
			catch (MupException e)
			{
				Response.Write(e.MupMessage);
			}
		}

		protected void lstManufacture_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem tempListItem;

//			errorInvisible();

			if (lstManufacture.SelectedItem.Text == "(Select Manufacturer)")
			{
				lstProductLine.Items.Clear ();
				lstModel.Items.Clear ();
				tempListItem = new ListItem("(Select Manufacturer)", "");
				lstProductLine.Items.Add(tempListItem);
				lstModel.Items.Add (tempListItem);

				pnlUpdate.Visible = false;
			}
			else
			{
				hdnManufacture.Text = lstManufacture.SelectedItem.ToString();
				// Manufacturer was selected, remove the "(Select Manufacturer") entry
				if (lstManufacture.Items[0].Value == "")
					lstManufacture.Items.RemoveAt(0);

				// Set the product line and model entries to "Select Product Line"
				lstModel.Items.Clear();
				tempListItem = new ListItem ("(Select Product Line)", "");
				lstModel.Items.Add(tempListItem);

				// Now populate the product line
				fillProductLine (lstManufacture.SelectedItem.Value, lstProductLine, hdnPageType.Text);
			}

			clearAllData();
		}

		private void fillProductLine (string strManufacture, DropDownList productLineList, string tableName)
		{
			// SQL command for query manufacture list
			SqlDataReader drProductLine;

			try
			{
				string strSql = "Select distinct product_line from " + tableName + " ";
				strSql += "Where Manufacture = '" + strManufacture + "' ";
				strSql += "Or Manufacture = '(Select Manufacturer)' order by product_line";
				SqlConnection drConn = new SqlConnection(strSqlConnString);
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				drProductLine = drComm.ExecuteReader();
			
				productLineList.DataSource = drProductLine;
				productLineList.DataTextField = "Product_line";
				productLineList.DataValueField = "Product_line";
				productLineList.DataBind();

				drProductLine.Close();
				drConn.Dispose();
			}
			catch (MupException e)
			{
				Response.Write(e.MupMessage);
			}
		}

		private void clearAllData()
		{
			if (radPageType.SelectedIndex != -1)
			{
				lblPageType.Text = radPageType.SelectedItem.Value;
				drpPageType.Visible = false;
			}
			lblModelId.Text = "";
			lblManufacturer.Text = "";
			lblProductLine.Text = "";
			lblModel.Text = "";
			txtSpeed.Text = "";
			txtSize.Text = "";
			txtSpecification.Text = "";
			chkAnnounce.Checked = false;
			txtImage.Text = "";
			txtHeader.Text = "";
			txtURL.Text = "";
			grdSpecialAds.Visible = false;

			hdnSpeed.Text = "";
			hdnSize.Text = "";
			hdnAnnounce.Text = "";
			hdnSpecification.Text = "";
			hdnImage.Text = "";
			hdnHeader.Text = "";
			hdnURL.Text = "";
		}

		protected void lstProductLine_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem tempListItem;

//			errorInvisible();

			if (lstProductLine.SelectedItem.Text == "(Select Product Line)")
			{
				lstModel.Items.Clear ();
				tempListItem = new ListItem ("(Select Product Line)", "");
				lstModel.Items.Add(tempListItem);

				pnlUpdate.Visible = false;
			}
			else
			{
				hdnProductLine.Text = lstProductLine.SelectedItem.ToString();
				// Manufacture was selected, remove the "(Select a State") entry
				if (lstProductLine.Items[0].Value == "")
					lstProductLine.Items.RemoveAt(0);

				// Set the school name and class level entries to "Select a School System"
				tempListItem = new ListItem ("(Select Model)", "");

				// Now populate the school systems
				fillModel (lstManufacture.SelectedItem.Value, lstProductLine.SelectedItem.Value, hdnPageType.Text);
			}

			clearAllData();
		}

		private void fillModel(string strManufacture, string strProductLine, string tableName)
		{
			// SQL command for query manufacture list
			SqlDataReader drModel;

			try
			{
				string strSql = "Select model from " + tableName + " ";
				strSql += "Where Manufacture = '" + strManufacture + "' ";
				strSql += "And Product_Line = '" + strProductLine + "' ";
				strSql += "Or Manufacture = '(Select Manufacturer)' order by Model";
				SqlConnection drConn = new SqlConnection(strSqlConnString);
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				drModel = drComm.ExecuteReader();
			
				lstModel.DataSource = drModel;
				lstModel.DataTextField = "model";
				lstModel.DataValueField = "model";
				lstModel.DataBind();

				drModel.Close();
				drConn.Dispose();
			}
			catch(MupException e)
			{
				Response.Write(e.MupMessage);
			}
		}

		protected void lstModel_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem tempListItem;

//			errorInvisible();

			if (lstModel.SelectedItem.Text == "(Select Model)")
			{
				tempListItem = new ListItem ("(Select Model)", "");

				clearAllData();
				pnlUpdate.Visible = false;
			}
			else
			{
				hdnModel.Text = lstModel.SelectedItem.ToString();
				updateSystemInformation();
				btnAddNew.Visible = true;
			}
		}

		private void updateSystemInformation()
		{
			string strManufacture = hdnManufacture.Text;
			string strProductLine = hdnProductLine.Text;
			string strModel = hdnModel.Text;
			SqlDataReader drModel;

			pnlUpdate.Visible = true;
			btnEdit.Visible = false;
			hdnSpeed.Visible = false;
			hdnSize.Visible = false;
			hdnAnnounce.Visible = false;
			hdnSpecification.Visible = false;
			hdnImage.Visible = false;
			hdnHeader.Visible = false;
			hdnURL.Visible = false;
			drpPageType.Visible = false;
			lblPageType.Visible = true;

			try
			{
				string strSql = "Select * from " + hdnPageType.Text + " ";
				strSql += "Where Manufacture = '" + strManufacture + "' ";
				strSql += "And Product_Line = '" + strProductLine + "' ";
				strSql += "And Model = '" + strModel + "' ";
				SqlConnection drConn = new SqlConnection(strSqlConnString);
				drConn.Open();
				SqlCommand drComm = new SqlCommand(strSql, drConn);
				drModel = drComm.ExecuteReader();

				if (drModel.Read())
				{
					lblPageType.Text = radPageType.SelectedItem.Value;
					if (hdnPageType.Text == "DesktopMemory")
						lblModelId.Text = drModel["Desktop_id"].ToString();
					else if (hdnPageType.Text == "LaptopMemory")
						lblModelId.Text = drModel["Laptop_id"].ToString();
					else if (hdnPageType.Text == "Digital_Camera")
						lblModelId.Text = drModel["Camera_id"].ToString();
					lblManufacturer.Text = drModel["manufacture"].ToString();
					lblProductLine.Text = drModel["Product_line"].ToString();
					lblModel.Text = drModel["Model"].ToString();

					createSpecialADs();
				}
				else
					clearAllData();

				drModel.Close();
				drConn.Dispose();
			}
			catch(MupException e)
			{
				Response.Write(e.MupMessage);
			}
		}

		protected void btnAddNew_Click(object sender, System.EventArgs e)
		{
			addNewAds();
			createSpecialADs();
		}

		private void createSpecialADs()
		{
			hdnAllFlag.Text = "0";

			try
			{
				memoryConn = new System.Data.SqlClient.SqlConnection();
				memoryAdapter = new System.Data.SqlClient.SqlDataAdapter();
				memorySelectCommand = new System.Data.SqlClient.SqlCommand();
				memoryDataSet1 = new memoryAdmin.dsSpecialADs();

				memoryAdapter.SelectCommand = memorySelectCommand;
				this.memoryAdapter.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																										new System.Data.Common.DataTableMapping("Table", "SpecialADs", new System.Data.Common.DataColumnMapping[] {
																																																					  new System.Data.Common.DataColumnMapping("Table_name", "Table_name"),
																																																					  new System.Data.Common.DataColumnMapping("Fk_model_id", "Fk_model_id"),
																																																					  new System.Data.Common.DataColumnMapping("Memory_speed", "Memory_speed"),
																																																					  new System.Data.Common.DataColumnMapping("Memory_size", "Memory_size"),
																																																					  new System.Data.Common.DataColumnMapping("Special", "Special"),
																																																					  new System.Data.Common.DataColumnMapping("g_manufacture", "g_manufacture"),
																																																					  new System.Data.Common.DataColumnMapping("g_product_line", "g_product_line"),
																																																					  new System.Data.Common.DataColumnMapping("g_model", "g_model"),
																																																					  new System.Data.Common.DataColumnMapping("Announce", "Announce"),
																																																					  new System.Data.Common.DataColumnMapping("Header", "Header"),
																																																					  new System.Data.Common.DataColumnMapping("Url", "Url"),
																																																					  new System.Data.Common.DataColumnMapping("Image", "Image")})});
				memoryConn.ConnectionString = strSqlConnString;
				memorySelectCommand.CommandText = "Select ISNULL(DesktopMemory.Manufacture, '') + ISNULL(LaptopMemory.Manufacture, '') + ISNULL(Digital_Camera.Manufacture, '') AS g_manufacture, ";
				memorySelectCommand.CommandText += "ISNULL(DesktopMemory.Product_line, '') + ISNULL(LaptopMemory.Product_line, '') + ISNULL(Digital_Camera.Product_line, '') AS g_product_line, ";
				memorySelectCommand.CommandText += "ISNULL(DesktopMemory.Model, '') + ISNULL(LaptopMemory.model, '') + ISNULL(Digital_Camera.Model, '') AS g_model, ";
				memorySelectCommand.CommandText += "Fk_model_id, Table_name, Header, Url, Image, Announce, Memory_speed, Memory_size, special ";
				memorySelectCommand.CommandText += "from SpecialADs ";
				memorySelectCommand.CommandText += "LEFT OUTER JOIN DesktopMemory ON SpecialADs.Fk_model_id = DesktopMemory.Desktop_id AND SpecialADs.Table_name = 'DesktopMemory' ";
				memorySelectCommand.CommandText += "LEFT OUTER JOIN LaptopMemory ON SpecialADs.Fk_model_id = LaptopMemory.laptop_id AND SpecialADs.Table_name = 'LaptopMemory' ";
				memorySelectCommand.CommandText += "LEFT OUTER JOIN Digital_Camera ON SpecialADs.Fk_model_id = Digital_Camera.Camera_id AND SpecialADs.Table_name = 'Digital_Camera' ";
				if (lblModelId.Text == "0")
					memorySelectCommand.CommandText += "where fk_model_id = '" + lblModelId.Text + "' ";
				else
					memorySelectCommand.CommandText += "where Table_name = '" + hdnPageType.Text + "' and fk_model_id = '" + lblModelId.Text + "' ";
				memorySelectCommand.CommandText += "Order by Table_name, fk_Model_id, Memory_Speed, memory_Size ";
				memorySelectCommand.Connection = memoryConn;
				memoryAdapter.Fill(memoryDataSet1, "SpecialADs");
				grdSpecialAds.DataSource = memoryDataSet1.SpecialADs.DefaultView;
				grdSpecialAds.DataBind();
				grdSpecialAds.Visible = true;
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}

		protected void btnAddGlobal_Click(object sender, System.EventArgs e)
		{
			ListItem li;

			pnlUpdate.Visible = true;
			btnAddNew.Visible = true;
			clearAllData();
			hdnPageType.Text = "All";
			lblPageType.Text = "All";
			drpPageType.SelectedIndex = 0;
			lblModelId.Text = "0";
			radPageType.SelectedIndex = -1;
			createSpecialADs();

			btnEdit.Visible = false;
			hdnSpeed.Visible = false;
			hdnSize.Visible = false;
			hdnSpecification.Visible = false;
			hdnAnnounce.Visible = false;
			hdnImage.Visible = false;
			hdnHeader.Visible = false;
			hdnURL.Visible = false;
			drpPageType.Visible = true;

			lstManufacture.Items.Clear();
			lstProductLine.Items.Clear();
			lstModel.Items.Clear();
			li = new ListItem("(Select Page Type)","");
			lstManufacture.Items.Add (li);
			lstProductLine.Items.Add (li);
			lstModel.Items.Add (li);

			btnEdit.Visible = false;
		}

		private void grdSpecialAds_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			string sql;

			try
			{
				sql = "Delete from SpecialADs where Header = '" + e.Item.Cells[12].Text.Trim().Replace("'", "''") + "' ";
				sql += "And table_name = '" + e.Item.Cells[3].Text + "' and fk_model_id = " + e.Item.Cells[4].Text + " ";

				SqlConnection dsConn = new SqlConnection(strSqlConnString);
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = sql;

				dsComm.ExecuteNonQuery();
				createSpecialADs();
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}

		private void grdSpecialAds_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			btnEdit.Visible = true;
			btnAddNew.Visible = false;

			lblPageType.Text = e.Item.Cells[3].Text.Trim();
			hdnPageType.Text = e.Item.Cells[3].Text.Trim();
			drpPageType.SelectedValue = e.Item.Cells[3].Text.Trim();
			lblModelId.Text = e.Item.Cells[4].Text.Trim();
			if (e.Item.Cells[4].Text.Trim() == "0")
			{
				lblPageType.Visible = false;
				drpPageType.Visible = true;
			}
			else
			{
				lblPageType.Visible = true;
				drpPageType.Visible = false;
			}
			if (e.Item.Cells[5].Text.Trim() != "&nbsp;")
			{
				txtSpeed.Text = e.Item.Cells[5].Text.Trim();
				hdnSpeed.Text = e.Item.Cells[5].Text.Trim();
			}
			else
			{
				txtSpeed.Text = "";
				hdnSpeed.Text = "";
			}
			if (e.Item.Cells[6].Text.Trim() != "&nbsp;")
			{
				txtSize.Text = e.Item.Cells[6].Text.Trim();
				hdnSize.Text = e.Item.Cells[6].Text.Trim();
			}
			else
			{
				txtSize.Text = "";
				hdnSize.Text = "";
			}
			if (e.Item.Cells[7].Text.Trim() != "&nbsp;")
			{
				txtSpecification.Text = e.Item.Cells[7].Text.Trim();
				hdnSpecification.Text = e.Item.Cells[7].Text.Trim();
			}
			else
			{
				txtSpecification.Text = "";
				hdnSpecification.Text = "";
			}
			if (e.Item.Cells[8].Text.Trim() != "&nbsp;")
				lblManufacturer.Text = e.Item.Cells[8].Text.Trim();
			else
				lblManufacturer.Text = "";
			if (e.Item.Cells[9].Text.Trim() != "&nbsp;")
				lblProductLine.Text = e.Item.Cells[9].Text.Trim();
			else
				lblProductLine.Text = "";
			if (e.Item.Cells[10].Text.Trim() != "&nbsp;")
				lblModel.Text = e.Item.Cells[10].Text.Trim();
			else
				lblModel.Text = "";

			if (e.Item.Cells[14].Text == "True")
			{
				chkAnnounce.Checked = true;
				hdnAnnounce.Text = "True";
			}
			else
			{
				chkAnnounce.Checked = false;
				hdnAnnounce.Text = "False";
			}
			txtImage.Text = e.Item.Cells[15].Text.Trim();
			hdnImage.Text = e.Item.Cells[15].Text.Trim();
			txtHeader.Text = e.Item.Cells[12].Text.Trim();
			hdnHeader.Text = e.Item.Cells[12].Text.Trim();
			if (e.Item.Cells[13].Text.Trim() != "&nbsp;")
			{
				txtURL.Text = e.Item.Cells[13].Text.Trim();
				hdnURL.Text = e.Item.Cells[13].Text.Trim();
			}
			else
			{
				txtURL.Text = "";
				hdnURL.Text = "";
			}

//			try
//			{
//				string strSql = "Select * from specialADs ";
//				if (hdnPageType.Text == "DesktopMemory")
//					strSql += "Inner Join " + hdnPageType.Text + " On Desktop_id = fk_model_id ";
//				else if (hdnPageType.Text == "LaptopMemory")
//					strSql += "Inner Join " + hdnPageType.Text + " On Laptop_id = fk_model_id ";
//				else if (hdnPageType.Text == "Digital_Camera")
//					strSql += "Inner Join " + hdnPageType.Text + " On Camera_id = fk_model_id ";
//				strSql += "where Header = '" + hdnHeader.Text.Replace("'", "''") + "' ";
//				strSql += "And table_name = '" + hdnPageType.Text + "' and fk_model_id = " + lblModelId.Text + " ";
//				SqlConnection drConn = new SqlConnection(strSqlConnString);
//				drConn.Open();
//				SqlCommand drComm = new SqlCommand(strSql, drConn);
//				drModel = drComm.ExecuteReader();
//
//				if (drModel.Read())
//				{
//					if (hdnPageType.Text != "All")
//					{
//						lblManufacturer.Text = drModel["manufacture"].ToString().Trim();
//						lblProductLine.Text = drModel["Product_line"].ToString().Trim();
//						lblModel.Text = drModel["model"].ToString().Trim();
//					}
//					txtImage.Text = drModel["Image"].ToString().Trim();
//					hdnImage.Text = drModel["Image"].ToString().Trim();
//				}
//			}
//			catch (MupException ex)
//			{
//				Response.Write (ex.MupMessage);
//			}
		}

		protected void btnEdit_Click(object sender, System.EventArgs e)
		{
			string sql;

			btnEdit.Visible = false;
			btnAddNew.Visible = true;

			try
			{
				sql = "Delete from SpecialADs where ";
				if (hdnSpeed.Text == "" && hdnSize.Text == "")
					sql += "memory_speed is null And memory_size is null ";
				else
					sql += "memory_speed = " + hdnSpeed.Text + " And memory_size = " + hdnSize.Text + " ";
				sql += "And Header = '" + hdnHeader.Text.Replace("'", "''") + "' ";
				sql += "And table_name = '" + hdnPageType.Text + "' and fk_model_id = " + lblModelId.Text + " ";

				SqlConnection dsConn = new SqlConnection(strSqlConnString);
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = sql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					addNewAds();
					if (hdnAllFlag.Text == "1")
						showAllADs();
					else
						createSpecialADs();
				}
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}

		private void addNewAds()
		{
			try
			{
				string strSql = "Insert Into SpecialADs (Table_name, Fk_model_id, Memory_speed, Memory_size, Special, Announce, Image, Header, Url) Values (";
				if (drpPageType.Visible == true)
					strSql += "'" + drpPageType.SelectedValue + "', ";
				else
					strSql += "'" + hdnPageType.Text + "', ";
				strSql += lblModelId.Text + ", ";
				if (chkAnnounce.Checked)
				{
					strSql += txtSpeed.Text + ", ";
					strSql += txtSize.Text + ", ";
					strSql += "'" + txtSpecification.Text.Replace("'", "''") + "', ";
					strSql += "1, ";
				}
				else
				{
					strSql += txtSpeed.Text + ", ";
					strSql += txtSize.Text + ", ";
					strSql += "'" + txtSpecification.Text.Replace("'", "''") + "', ";
					strSql += "0, ";
				}

				strSql += "'" + txtImage.Text.Replace("'", "''") + "', ";
				strSql += "'" + txtHeader.Text.Replace("'", "''") + "', ";
				strSql += "'" + txtURL.Text + "') ";

				SqlConnection dsConn = new SqlConnection(strSqlConnString);
				SqlCommand dsComm = new SqlCommand();
				dsConn.Open();
				dsComm.Connection = dsConn;
				dsComm.CommandText = strSql;

				if (dsComm.ExecuteNonQuery() >= 0)
				{
					txtSpeed.Text = "";
					txtSize.Text = "";
					txtSpecification.Text = "";
					chkAnnounce.Checked = false;
					txtImage.Text = "";
					txtHeader.Text = "";
					txtURL.Text = "";
				}
				dsConn.Dispose();
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}

		protected void btnListAll_Click(object sender, System.EventArgs e)
		{
			showAllADs();
		}

		private void showAllADs()
		{
			hdnAllFlag.Text = "1";

			try
			{
				memoryConn = new System.Data.SqlClient.SqlConnection();
				memoryAdapter = new System.Data.SqlClient.SqlDataAdapter();
				memorySelectCommand = new System.Data.SqlClient.SqlCommand();
				memoryDataSet1 = new memoryAdmin.dsSpecialADs();

				memoryAdapter.SelectCommand = memorySelectCommand;
				this.memoryAdapter.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																										new System.Data.Common.DataTableMapping("Table", "SpecialADs", new System.Data.Common.DataColumnMapping[] {
																																																					  new System.Data.Common.DataColumnMapping("Table_name", "Table_name"),
																																																					  new System.Data.Common.DataColumnMapping("Fk_model_id", "Fk_model_id"),
																																																					  new System.Data.Common.DataColumnMapping("Memory_speed", "Memory_speed"),
																																																					  new System.Data.Common.DataColumnMapping("Memory_size", "Memory_size"),
																																																					  new System.Data.Common.DataColumnMapping("Special", "Special"),
																																																					  new System.Data.Common.DataColumnMapping("g_manufacture", "g_manufacture"),
																																																					  new System.Data.Common.DataColumnMapping("g_product_line", "g_product_line"),
																																																					  new System.Data.Common.DataColumnMapping("g_model", "g_model"),
																																																					  new System.Data.Common.DataColumnMapping("Announce", "Announce"),
																																																					  new System.Data.Common.DataColumnMapping("Header", "Header"),
																																																					  new System.Data.Common.DataColumnMapping("Url", "Url"),
																																																					  new System.Data.Common.DataColumnMapping("Image", "Image")})});
				memoryConn.ConnectionString = strSqlConnString;
				memorySelectCommand.CommandText = "Select ISNULL(DesktopMemory.Manufacture, '') + ISNULL(LaptopMemory.Manufacture, '') + ISNULL(Digital_Camera.Manufacture, '') AS g_manufacture, ";
				memorySelectCommand.CommandText += "ISNULL(DesktopMemory.Product_line, '') + ISNULL(LaptopMemory.Product_line, '') + ISNULL(Digital_Camera.Product_line, '') AS g_product_line, ";
				memorySelectCommand.CommandText += "ISNULL(DesktopMemory.Model, '') + ISNULL(LaptopMemory.model, '') + ISNULL(Digital_Camera.Model, '') AS g_model, ";
				memorySelectCommand.CommandText += "Fk_model_id, Table_name, Header, Url, Image, Announce, Memory_speed, Memory_size, special ";
				memorySelectCommand.CommandText += "from SpecialADs ";
				memorySelectCommand.CommandText += "LEFT OUTER JOIN DesktopMemory ON SpecialADs.Fk_model_id = DesktopMemory.Desktop_id AND SpecialADs.Table_name = 'DesktopMemory' ";
				memorySelectCommand.CommandText += "LEFT OUTER JOIN LaptopMemory ON SpecialADs.Fk_model_id = LaptopMemory.laptop_id AND SpecialADs.Table_name = 'LaptopMemory' ";
				memorySelectCommand.CommandText += "LEFT OUTER JOIN Digital_Camera ON SpecialADs.Fk_model_id = Digital_Camera.Camera_id AND SpecialADs.Table_name = 'Digital_Camera' ";
				memorySelectCommand.CommandText += "Order by Table_name, fk_Model_id, Memory_Speed, memory_Size ";
				memorySelectCommand.Connection = memoryConn;
				memoryAdapter.Fill(memoryDataSet1, "SpecialADs");
				grdSpecialAds.DataSource = memoryDataSet1.SpecialADs.DefaultView;
				grdSpecialAds.DataBind();

				pnlUpdate.Visible = true;
				btnEdit.Visible = false;
				hdnSpeed.Visible = false;
				hdnSize.Visible = false;
				hdnAnnounce.Visible = false;
				hdnSpecification.Visible = false;
				hdnImage.Visible = false;
				hdnHeader.Visible = false;
				hdnURL.Visible = false;
				grdSpecialAds.Visible = true;
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}
	}
}
