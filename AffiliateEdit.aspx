<%@ Page language="c#" Inherits="memoryAdmin.AffiliateEdit" CodeFile="AffiliateEdit.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AffiliateEdit</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE class="item" id="Table1" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px"
				cellSpacing="0" cellPadding="1" width="780" border="0">
				<tr>
					<td colSpan="4"><A href="default.aspx"><IMG alt="Memory-Up.com" src="http://www.memory-up.com/merchant2/images/logo_big.gif"
								border="0"></A><b>Affiliate Program User</b>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td noWrap>User Name:</td>
					<td><asp:label id="lblUserName" runat="server">Label</asp:label></td>
					<td noWrap></td>
					<td></td>
				</tr>
				<TR>
					<TD noWrap>Password:</TD>
					<TD><asp:textbox id="txtPassword" runat="server"></asp:textbox></TD>
					<TD noWrap>Check Payable To:</TD>
					<TD><asp:textbox id="txtPayable" runat="server"></asp:textbox></TD>
				</TR>
				<tr>
					<td noWrap>Firat Name:</td>
					<td><asp:textbox id="txtFirstName" runat="server"></asp:textbox></td>
					<td noWrap>Taxpayer Identification Number:</td>
					<td><asp:textbox id="txtIdentify" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td noWrap>Last Name:</td>
					<td><asp:textbox id="txtLastName" runat="server"></asp:textbox></td>
					<td noWrap>Taxpayer Identification Type:</td>
					<td><asp:dropdownlist id="drpIdentifyType" runat="server"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap>E-Mail:</td>
					<td><asp:textbox id="txtEmail" runat="server"></asp:textbox></td>
					<td noWrap>Tax Classification:</td>
					<td><asp:dropdownlist id="drpTaxClass" runat="server"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap>Phone:</td>
					<td><asp:textbox id="txtPhone" runat="server"></asp:textbox></td>
					<td>Payment Type:</td>
					<td><asp:dropdownlist id="drpUserPayment" runat="server">
							<asp:ListItem Value="order">Applied the commission directly to the order</asp:ListItem>
							<asp:ListItem Value="check">Receive the commission via check</asp:ListItem>
							<asp:ListItem Value="credit">Exchange the commission to same value store credit</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr>
					<td noWrap>Fax:</td>
					<td><asp:textbox id="txtFax" runat="server"></asp:textbox></td>
					<td>Register Date:</td>
					<td>
						<asp:Label id="lblRegisterDate" runat="server"></asp:Label></td>
				</tr>
				<tr>
					<td noWrap>Address1:</td>
					<td><asp:textbox id="txtAddress1" runat="server" Width="300px"></asp:textbox></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td noWrap>Address2:</td>
					<td><asp:textbox id="txtAddress2" runat="server" Width="300px"></asp:textbox></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td noWrap>City:</td>
					<td><asp:textbox id="txtCity" runat="server"></asp:textbox>&nbsp;</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td noWrap>State:</td>
					<td noWrap><asp:dropdownlist id="drpStates" runat="server"></asp:dropdownlist>&nbsp;Zip:
						<asp:textbox id="txtZip" runat="server" Width="89px"></asp:textbox></td>
					<td><asp:button id="btnUpdate" runat="server" Text="Update" onclick="btnUpdate_Click"></asp:button></td>
					<td></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;
					</td>
					<td></td>
					<td></td>
				</tr>
				<TR>
					<TD colSpan="4">
						<TABLE class="item" id="Table3" cellSpacing="1" cellPadding="0" width="100%" bgColor="silver"
							border="0">
							<tr>
								<td colSpan="4">&nbsp;<STRONG>Add New Order</STRONG></td>
							</tr>
							<TR>
								<TD bgColor="white">Order Number:</TD>
								<TD bgColor="white"><asp:textbox id="txtOrderNo" runat="server"></asp:textbox></TD>
								<TD bgColor="white">Rebate Percent:</TD>
								<TD bgColor="white"><asp:textbox id="txtRebatePer" runat="server"></asp:textbox>%</TD>
							</TR>
							<TR>
								<TD bgColor="white">Order Date:</TD>
								<TD bgColor="white"><asp:textbox id="txtOrderDate" runat="server"></asp:textbox></TD>
								<TD bgColor="white">Rebate Amount:</TD>
								<TD bgColor="white"><asp:textbox id="txtRebateAmn" runat="server"></asp:textbox></TD>
							</TR>
							<TR>
								<TD bgColor="white">Order Value:</TD>
								<TD bgColor="white"><asp:textbox id="txtOrderValue" runat="server"></asp:textbox></TD>
								<td bgColor="white">Payment Type</td>
								<td bgColor="white"><asp:dropdownlist id="drpPaymentType" runat="server">
										<asp:ListItem Value="order">Applied the commission directly to the order</asp:ListItem>
										<asp:ListItem Value="check">Receive the commission via check</asp:ListItem>
										<asp:ListItem Value="credit">Exchange the commission to same value store credit</asp:ListItem>
									</asp:dropdownlist></td>
							</TR>
							<TR>
								<TD bgColor="white">&nbsp;</TD>
								<TD bgColor="white"><asp:button id="btnAddNew" runat="server" Text="Add New" onclick="btnAddNew_Click"></asp:button></TD>
								<td bgColor="white"></td>
								<td bgColor="white"></td>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colSpan="4"><asp:datagrid id="grdAffiliate" runat="server" Width="100%" CellPadding="3" BackColor="White"
							BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC" AutoGenerateColumns="False">
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<ItemStyle Font-Size="10pt" ForeColor="Navy"></ItemStyle>
							<HeaderStyle Font-Size="10pt" Font-Bold="True" HorizontalAlign="Center" ForeColor="Navy" BackColor="LightGray"></HeaderStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
								<asp:ButtonColumn Text="Delete" CommandName="Delete"></asp:ButtonColumn>
								<asp:BoundColumn DataField="Order_no" SortExpression="Order_no" ReadOnly="True" HeaderText="Order Number"></asp:BoundColumn>
								<asp:BoundColumn DataField="A_Order_date" SortExpression="A_Order_date" ReadOnly="True" HeaderText="Order Date"></asp:BoundColumn>
								<asp:BoundColumn DataField="Order_value" SortExpression="Order_value" ReadOnly="True" HeaderText="Order Value"
									DataFormatString="{0:C}"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Rebate Percent">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Rebate_per", "{0:P}") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox ID="txtEditRebatePer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Rebate_per", "{0:P}") %>' Columns="3">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Rebate Amount">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Rebate_amount", "{0:C}") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox ID="txtEditRebateAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Rebate_amount", "{0:F2}") %>' Columns="7">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="Update_date" SortExpression="Update_date" ReadOnly="True" HeaderText="Update Date"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Status">
									<ItemTemplate>
										<asp:Label id="lblLineStatus" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Description") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblStatusEdit" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Description") %>' Visible="False">
										</asp:Label>
										<asp:DropDownList ID="drpStatusEdit" Runat="server" DataSource="<%# showStatusList() %>" DataTextField="Description" DataValueField="pk_id">
										</asp:DropDownList>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Pay Type">
									<ItemTemplate>
										<asp:Label ID="lblPaymentType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Payment_type") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label ID="lblPaymentEdit" Runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Payment_type") %>' Visible="False">
										</asp:Label>
										<asp:DropDownList ID="drpPaymentEdit" Runat="server">
											<asp:ListItem Value="order">Order</asp:ListItem>
											<asp:ListItem Value="check">Check</asp:ListItem>
											<asp:ListItem Value="credit">Credit</asp:ListItem>
										</asp:DropDownList>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Payment Date">
									<ItemTemplate>
										<asp:Label ID="lblCompleteDate" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Payment_date", "{0:d}") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox ID="txtCompleteDate" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Payment_date", "{0:d}") %>' Columns="7">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Ref. Number">
									<ItemTemplate>
										<asp:Label ID="lblRefNumber" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ref_no") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox ID="txtRefNo" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ref_no") %>' Columns="3">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
