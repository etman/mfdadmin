using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace memoryAdmin
{
    /// <summary>
    /// Summary description for LaptopUpdate.
    /// </summary>
    public partial class LaptopUpdate : System.Web.UI.Page
    {
        private SqlConnection m_dbConnection = null;
        private SqlDataAdapter m_dbAdapter = null;
        private SqlCommand m_dbCommand = null;

        private int laptopID;
        private DataSet dsSystem;

        private const string strSqlConnectionString =
            "Network Library=DBMSSOCN;" +
            "Data Source=192.168.0.6,1433;" +
            "Initial Catalog=CBE;" +
            "User ID=cbe;" +
            "Password=sc_cbe_sql0103";

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            if (!Page.IsPostBack)
            {
                btnSaveAs.Attributes.Add("onclick", "openNewModel();");

                if (Session["LoginName"] == null)
                {
                    Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
                }
                ListItem li;

                // The other's shouldn't contain data yet
                li = new ListItem("(Select Manufacturer)", "");
                lstManufacture.Items.Add(li);
                lstProductLine.Items.Add(li);
                lstModel.Items.Add(li);

                fillManufacture(lstManufacture);

                drpMaxTotal.SelectedValue = "GB";
                if (Request.Params["modelid"] != null)
                {
                    txtLaptopID.Text = Request.Params["modelid"].ToString();
                    updateSystemInformation();
                }
            }
        }

        private void fillManufacture(System.Web.UI.WebControls.DropDownList manufactureList)
        {
            System.Data.SqlClient.SqlDataReader drManufacture;
            MupDbConnection drConn = new MupDbConnection();

            try
            {
                drManufacture = drConn.GetReader("Select distinct manufacture from laptopMemory order by manufacture");

                manufactureList.DataSource = drManufacture;
                manufactureList.DataTextField = "Manufacture";
                manufactureList.DataValueField = "Manufacture";
                manufactureList.DataBind();

                drConn.Dispose();
            }
            catch (MupException e)
            {
                if (drConn != null)
                {
                    drConn.Dispose();
                }
                Response.Write(e.MupMessage);
            }
        }

        private void fillProductLine(string strManufacture, System.Web.UI.WebControls.DropDownList productLineList)
        {
            // SQL command for query manufacture list
            System.Data.SqlClient.SqlDataReader drProductLine;
            MupDbConnection drConn = new MupDbConnection();

            try
            {

                drProductLine = drConn.GetReader("Select distinct Product_line from laptopMemory " +
                    "where Manufacture = '" + strManufacture +
                    "' or Manufacture = '(Select Manufacturer)' order by Product_line");

                productLineList.DataSource = drProductLine;
                productLineList.DataTextField = "Product_line";
                productLineList.DataValueField = "Product_line";
                productLineList.DataBind();

                drConn.Dispose();
            }
            catch (MupException e)
            {
                if (drConn != null)
                {
                    drConn.Dispose();
                }
                Response.Write(e.MupMessage);
            }
        }

        private void fillModel(string strManufacture, string strProductLine)
        {
            // SQL command for query manufacture list
            System.Data.SqlClient.SqlDataReader drModel;
            MupDbConnection drConn = new MupDbConnection();

            try
            {
                drModel = drConn.GetReader(String.Format(@"Select model from laptopMemory
                    where ((Manufacture = '{0}' and product_line = '{1}') or Manufacture = '(Select Manufacturer)')
                    order by model", strManufacture, strProductLine));

                lstModel.DataSource = drModel;
                lstModel.DataTextField = "model";
                lstModel.DataValueField = "model";
                lstModel.DataBind();

                drModel.Close();
                drConn.Dispose();
            }
            catch (MupException e)
            {
                if (drConn != null)
                {
                    drConn.Dispose();
                }
                Response.Write(e.MupMessage);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imgHome.Click += new System.Web.UI.ImageClickEventHandler(this.imgHome_Click);

        }
        #endregion

        protected void lstManufacture_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ListItem li;

            if (lstManufacture.SelectedItem.Text == "(Select Manufacturer)")
            {
                lstProductLine.Items.Clear();
                lstModel.Items.Clear();
                li = new ListItem("(Select Manufacturer)", "");
                lstProductLine.Items.Add(li);
                lstModel.Items.Add(li);
            }
            else
            {
                hdnManufacture.Text = lstManufacture.SelectedItem.ToString();
                // Manufacture was selected, remove the "(Select a State") entry
                if (lstManufacture.Items[0].Value == "")
                    lstManufacture.Items.RemoveAt(0);

                // Set the school name and class level entries to "Select a School System"
                lstModel.Items.Clear();
                li = new ListItem("(Select Product Line)", "");
                lstModel.Items.Add(li);

                // Now populate the school systems
                fillProductLine(lstManufacture.SelectedItem.Value, lstProductLine);
            }
        }

        protected void lstProductLine_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ListItem li;

            if (lstManufacture.SelectedItem.Text == "(Select Product Line)")
            {
                lstModel.Items.Clear();
                li = new ListItem("(Select Product Line)", "");
                lstProductLine.Items.Add(li);
            }
            else
            {
                hdnProductLine.Text = lstProductLine.SelectedItem.ToString();
                // Manufacture was selected, remove the "(Select a State") entry
                if (lstProductLine.Items[0].Value == "")
                    lstProductLine.Items.RemoveAt(0);

                // Set the school name and class level entries to "Select a School System"
                li = new ListItem("(Select Model)", "");

                // Now populate the school systems
                fillModel(lstManufacture.SelectedItem.Value, lstProductLine.SelectedItem.Value);
            }
        }

        protected void lstModel_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ListItem li;

            showSystemInfo.Visible = false;
            btnUpdate.Visible = true;
            btnAddNew.Visible = false;
            btnCancelUpdate.Visible = true;
            btnCancelAdd.Visible = false;
            btnSaveAs.Visible = true;
            btnDelete.Visible = true;
            lstCurrManuf.Visible = false;
            lstCurrLine.Visible = false;
            lblCurrManuf.Visible = false;
            lblCurrLine.Visible = false;
            btnNewManuf.Visible = false;
            btnNewLine.Visible = false;
            txtManufacture.Visible = true;
            txtProductLine.Visible = true;
            txtLaptopID.Text = "";

            if (lstManufacture.SelectedItem.Text == "(Select Model)")
            {
                SystemInformation.Visible = false;
                li = new ListItem("(Select Model)", "");
            }
            else
            {
                hdnModel.Text = lstModel.SelectedItem.ToString();
                updateSystemInformation();
            }
        }

        private void updateSystemInformation()
        {
            string strManufacture = hdnManufacture.Text;
            string strProductLine = hdnProductLine.Text;
            string strModel = hdnModel.Text;

            // Validation check invisible
            lblManufError.Visible = false;
            lblProductLineError.Visible = false;
            lblModelError.Visible = false;
            lblMaxTotalError.Visible = false;
            string maxTotalSize = drpMaxTotal.SelectedValue.ToString();
            SystemInformation.Visible = true;

            String sql = "Select * from laptopMemory ";
            if (txtLaptopID.Text != "")
                sql += "where laptop_id = '" + txtLaptopID.Text + "' ";
            else
                sql += "where Manufacture = '" + strManufacture + "' and product_line = '" + strProductLine +
                    "' and model = '" + strModel + "' ";

            MupDbConnection dsConn = new MupDbConnection();

            try
            {
                dsSystem = dsConn.CreateDataSet();
                dsConn.FillDataSet(sql, "laptop", ref dsSystem);
                DataTable laptopTable = dsSystem.Tables["laptop"];

                if (laptopTable != null && laptopTable.Rows.Count > 0)
                {
                    ClearFields();
                    DataRow row0 = laptopTable.Rows[0];

                    lblSource.Text = row0["Source"].ToString();
                    txtManufacture.Text = row0["Manufacture"].ToString();
                    txtProductLine.Text = row0["product_line"].ToString();
                    txtModel.Text = row0["model"].ToString();
                    laptopID = (int)row0["laptop_id"];
                    if (row0["counter"] != null && row0["counter"].ToString() != "")
                        lblViewed.Text = "Viewed by customers: " + (int)row0["counter"];
                    else
                        lblViewed.Text = "";
                    if (row0["UpdateDate"] != DBNull.Value)
                        lblLastUpdate.Text = "Last Update: " + ((DateTime)row0["UpdateDate"]).ToShortDateString();
                    else
                        lblLastUpdate.Text = "";

                    if (row0["type"] != DBNull.Value)
                    {
                        for (int i = 0; i < radType.Items.Count; i++)
                        {
                            if (radType.Items[i].Value == row0["type"].ToString())
                                radType.Items[i].Selected = true;
                        }
                        changeSpeedType();
                    }
                    else
                        radType.SelectedIndex = -1;

                    txtMinSpeed.Text = row0["speed"].ToString();
                    txtMaxSpeed.Text = row0["Max_speed"].ToString();

                    if (row0["speed"] != DBNull.Value)
                    {
                        for (int i = 0; i < radMinSpeed.Items.Count; i++)
                        {
                            if (radMinSpeed.Items[i].Value == row0["speed"].ToString())
                                radMinSpeed.Items[i].Selected = true;
                        }
                    }
                    else
                        radMinSpeed.SelectedIndex = -1;

                    if (row0["Max_speed"] != DBNull.Value)
                    {
                        for (int i = 0; i < radMaxSpeed.Items.Count; i++)
                        {
                            if (radMaxSpeed.Items[i].Value == row0["Max_speed"].ToString())
                                radMaxSpeed.Items[i].Selected = true;
                        }
                    }
                    else
                        radMaxSpeed.SelectedIndex = -1;

                    if (row0["slot"] != DBNull.Value)
                    {
                        for (int i = 0; i < radSlot.Items.Count; i++)
                        {
                            if (radSlot.Items[i].Value == row0["slot"].ToString())
                                radSlot.Items[i].Selected = true;
                        }
                    }
                    else
                        radSlot.SelectedIndex = -1;

                    if (row0["standard_size"] != DBNull.Value)
                    {
                        for (int i = 0; i < drpStandard.Items.Count; i++)
                        {
                            if (drpStandard.Items[i].Value == row0["standard_size"].ToString())
                                drpStandard.Items[i].Selected = true;
                        }
                    }

                    if (row0["std_removable"] != DBNull.Value)
                    {
                        if (row0["std_removable"].Equals(true))
                            chkRemovable.Checked = true;
                        else
                            chkRemovable.Checked = false;
                    }
                    else
                        chkRemovable.Checked = false;

                    txtMaxTotal.Text = row0["max_total"].ToString();
                    if (maxTotalSize == "TB")
                        txtMaxTotal.Text = (int.Parse(txtMaxTotal.Text) / 1024 / 1024).ToString();
                    else if (maxTotalSize == "GB")
                        txtMaxTotal.Text = (int.Parse(txtMaxTotal.Text) / 1024).ToString();
                    if (row0["min_size"] != DBNull.Value)
                    {
                        for (int i = 0; i < radMinPerSlot.Items.Count; i++)
                        {
                            if (radMinPerSlot.Items[i].Value == row0["min_size"].ToString())
                                radMinPerSlot.Items[i].Selected = true;
                        }
                    }
                    else
                        radMinPerSlot.SelectedIndex = -1;

                    if (row0["max_size"] != DBNull.Value)
                    {
                        for (int i = 0; i < radMaxPerSlot.Items.Count; i++)
                        {
                            if (radMaxPerSlot.Items[i].Value == row0["max_size"].ToString())
                                radMaxPerSlot.Items[i].Selected = true;
                        }
                    }
                    else
                        radMaxPerSlot.SelectedIndex = -1;

                    // Dual Channel
                    if (row0["Dual_ch"].ToString() != "")
                    {
                        if ((bool)row0["Dual_ch"] == true)
                            chkDualChannel.Checked = true;
                        else
                            chkDualChannel.Checked = false;
                    }
                    else
                        chkDualChannel.Checked = false;

                    // Tri Channel
                    if (row0["Tri_ch"].ToString() != "")
                    {
                        if ((bool)row0["Tri_ch"] == true)
                            chkTriChannel.Checked = true;
                        else
                            chkTriChannel.Checked = false;
                    }
                    else
                        chkTriChannel.Checked = false;

                    // Run in Pair
                    if (row0["Run_in_pair"].ToString() != "")
                    {
                        if ((bool)row0["Run_in_pair"] == true)
                            chkRunInPair.Checked = true;
                        else
                            chkRunInPair.Checked = false;
                    }
                    else
                        chkRunInPair.Checked = false;

                    // USB, SerialATA, FireWire
                    if (row0["USB"] != DBNull.Value)
                    {
                        for (int i = 0; i < radUsb.Items.Count; i++)
                        {
                            if (radUsb.Items[i].Value == row0["USB"].ToString())
                                radUsb.Items[i].Selected = true;
                        }
                    }
                    else
                        radUsb.SelectedIndex = 0;

                    if (row0["FireWire"] != DBNull.Value)
                    {
                        for (int i = 0; i < radFireWire.Items.Count; i++)
                        {
                            if (radFireWire.Items[i].Value == row0["FireWire"].ToString())
                                radFireWire.Items[i].Selected = true;
                        }
                    }
                    else
                        radFireWire.SelectedIndex = 0;

                    if (!row0["ChipSet"].Equals(null))
                        txtChipSet.Text = row0["ChipSet"].ToString();
                    if (!row0["Filter"].Equals(null))
                        txtFilter.Text = row0["Filter"].ToString();
                    if (!row0["notes"].Equals(null))
                        txtNotes.Text = row0["notes"].ToString();
                    if (!row0["comments"].Equals(null))
                        txtComments.Text = row0["comments"].ToString();
                    if (!row0["MB64Op1"].ToString().Equals(null))
                        txt64Op1.Text = row0["MB64Op1"].ToString();
                    if (!row0["MB64Op2"].ToString().Equals(null))
                        txt64Op2.Text = row0["MB64Op2"].ToString();
                    if (!row0["MB128Op1"].ToString().Equals(null))
                        txt128Op1.Text = row0["MB128Op1"].ToString();
                    if (!row0["MB128Op2"].ToString().Equals(null))
                        txt128Op2.Text = row0["MB128Op2"].ToString();
                    if (!row0["MB256Op1"].ToString().Equals(null))
                        txt256Op1.Text = row0["MB256Op1"].ToString();
                    if (!row0["MB256Op2"].ToString().Equals(null))
                        txt256Op2.Text = row0["MB256Op2"].ToString();
                    if (!row0["MB512Op1"].ToString().Equals(null))
                        txt512Op1.Text = row0["MB512Op1"].ToString();
                    if (!row0["MB512Op2"].ToString().Equals(null))
                        txt512Op2.Text = row0["MB512Op2"].ToString();
                    if (!row0["GB1Op1"].ToString().Equals(null))
                        txt1GOp1.Text = row0["GB1Op1"].ToString();
                    if (!row0["GB1Op2"].ToString().Equals(null))
                        txt1GOp2.Text = row0["GB1Op2"].ToString();
                    if (!row0["GB2Op1"].ToString().Equals(null))
                        txt2GOp1.Text = row0["GB2Op1"].ToString();
                    if (!row0["GB2Op2"].ToString().Equals(null))
                        txt2GOp2.Text = row0["GB2Op2"].ToString();
                    if (!row0["GB4Op1"].ToString().Equals(null))
                        txt4GOp1.Text = row0["GB4Op1"].ToString();
                    if (!row0["GB4Op2"].ToString().Equals(null))
                        txt4GOp2.Text = row0["GB4Op2"].ToString();
                    if (!row0["GB8Op1"].ToString().Equals(null))
                        txt8GOp1.Text = row0["GB8Op1"].ToString();
                    if (!row0["GB8Op2"].ToString().Equals(null))
                        txt8GOp2.Text = row0["GB8Op2"].ToString();
                    if (row0["picture"].ToString().Equals(null) || row0["picture"].ToString() == "")
                    {
                        btnAddPicture.Visible = true;
                        btnDeletePicture.Visible = false;
                        imgPicture.Visible = false;
                    }
                    else
                    {
                        btnAddPicture.Visible = false;
                        btnDeletePicture.Visible = true;
                        imgPicture.Visible = true;

                        imgPicture.ImageUrl = System.IO.Path.Combine(Server.UrlPathEncode("/MfdImages/ModelImages"), row0["picture"].ToString());
                    }

                    lblUserInsert.Text = row0["UserInsert"].ToString();
                    lblUserUpate.Text = row0["UserUpdate"].ToString();
                    txtLaptopID.Text = laptopID.ToString();
                }
                dsConn.Dispose();
            }
            catch (Exception ex)
            {
                if (dsConn != null)
                    dsConn.Dispose();
                throw ex;
            }
        }

        protected void btnUpdate_Click(object sender, System.EventArgs e)
        {
            saveUpdate();
        }

        private void saveUpdate()
        {
            String sql;
            MupDbConnection dsConn = new MupDbConnection();

            // Validation check invisible
            lblManufError.Visible = false;
            lblProductLineError.Visible = false;
            lblModelError.Visible = false;
            lblMaxTotalError.Visible = false;

            //Check the required fields
            if (txtManufacture.Text.Trim() == "")
            {
                lblManufError.Visible = true;
                return;
            }
            if (txtProductLine.Text.Trim() == "")
            {
                lblProductLineError.Visible = true;
                return;
            }
            if (txtModel.Text.Trim() == "")
            {
                lblModelError.Visible = true;
                return;
            }

            sql = "Update laptopMemory set manufacture = '" + txtManufacture.Text.Trim() + "', ";
            sql += "product_line = '" + txtProductLine.Text.Trim() + "', ";

            sql += "model= '" + txtModel.Text.Trim() + "', ";
            sql += "slot = " + radSlot.SelectedItem.Value + ", ";
            sql += "type = '" + radType.SelectedItem.Value.ToString() + "', ";
            sql += "speed = '" + radMinSpeed.SelectedItem.Value.ToString() + "', ";
            sql += "Max_speed = '" + radMaxSpeed.SelectedItem.Value.ToString() + "', ";

            if (txtMaxTotal.Text == "")
                sql += "0, ";
            else
            {
                int maxTotal = 0;
                if (Regex.IsMatch(txtMaxTotal.Text, "[0-9]+"))
                {
                    if (drpMaxTotal.SelectedValue == "GB")
                    {
                        maxTotal = int.Parse(txtMaxTotal.Text) * 1024;
                    }
                    else if (drpMaxTotal.SelectedValue == "TB")
                        maxTotal = int.Parse(txtMaxTotal.Text) * 1024 * 1024;
                    else
                        maxTotal = int.Parse(txtMaxTotal.Text);
                    sql += "max_total = " + maxTotal + ", ";
                }
                
                else if (!Regex.IsMatch(txtMaxTotal.Text.Trim(), "[0-9]+"))
                {
                    lblMaxTotalError.Visible = true;
                    return;
                }
                else
                    sql += "max_total = " + int.Parse(txtMaxTotal.Text.Trim()) + ", ";
            }

            sql += "standard_size = " + drpStandard.SelectedItem.Value + ", ";
            if (chkRemovable.Checked)
                sql += "std_removable = 1, ";
            else
                sql += "std_removable = 0, ";
            sql += "min_size = " + radMinPerSlot.SelectedItem.Value + ", ";
            sql += "max_size = " + radMaxPerSlot.SelectedItem.Value + ", ";
            sql += "USB = '" + radUsb.SelectedValue + "', ";
            sql += "FireWire = '" + radFireWire.SelectedValue + "', ";
            sql += "ChipSet = '" + txtChipSet.Text.Trim().Replace("'", "''") + "', ";
            sql += "Filter = '" + txtFilter.Text.Trim().Replace("'", "''") + "', ";
            sql += "MB64OP1 = '" + txt64Op1.Text.Trim() + "', ";
            sql += "MB64OP2 = '" + txt64Op2.Text.Trim() + "', ";
            sql += "MB128OP1 = '" + txt128Op1.Text.Trim() + "', ";
            sql += "MB128OP2 = '" + txt128Op2.Text.Trim() + "', ";
            sql += "MB256OP1 = '" + txt256Op1.Text.Trim() + "', ";
            sql += "MB256OP2 = '" + txt256Op2.Text.Trim() + "', ";
            sql += "MB512OP1 = '" + txt512Op1.Text.Trim() + "', ";
            sql += "MB512OP2 = '" + txt512Op2.Text.Trim() + "', ";
            sql += "GB1OP1 = '" + txt1GOp1.Text.Trim() + "', ";
            sql += "GB1OP2 = '" + txt1GOp2.Text.Trim() + "', ";
            sql += "GB2OP1 = '" + txt2GOp1.Text.Trim() + "', ";
            sql += "GB2OP2 = '" + txt2GOp2.Text.Trim() + "', ";
            sql += "GB4OP1 = '" + txt4GOp1.Text.Trim() + "', ";
            sql += "GB4OP2 = '" + txt4GOp2.Text.Trim() + "', ";
            sql += "GB8OP1 = '" + txt8GOp1.Text.Trim() + "', ";
            sql += "GB8OP2 = '" + txt8GOp2.Text.Trim() + "', ";
            sql += "Notes = '" + txtNotes.Text.Trim().Replace("'", "''") + "', ";
            sql += "InstallGuide = '" + txtInstallGuide.Text.Trim().Replace("'", "''") + "', ";
            sql += "Comments = '" + txtComments.Text.Trim().Replace("'", "''") + "', ";
            // dual Channel
            if (chkDualChannel.Checked)
                sql += "Dual_ch = 1, ";
            else
                sql += "Dual_ch = 0, ";
            // Tri Channel
            if (chkTriChannel.Checked)
                sql += "Tri_ch = 1, ";
            else
                sql += "Tri_ch = 0, ";
            // Run in Pair
            if (chkRunInPair.Checked)
                sql += "Run_in_Pair = 1, ";
            else
                sql += "Run_in_Pair = 0, ";
            sql += "UpdateDate = '" + DateTime.Now + "', ";
            if (Session["LoginName"] != null)
                sql += "UserUpdate = '" + Session["LoginName"].ToString() + "' ";
            else
                sql += "UserUpdate = '' ";
            sql += "where laptop_id = " + txtLaptopID.Text.Trim();

            try
            {
                dsConn.ExecSql(sql);
                dsConn.Dispose();
            }
            catch (Exception ex)
            {
                if (dsConn != null)
                    dsConn.Dispose();
                throw ex;
            }

            SystemInformation.Visible = false;
            showSystemInfo.Visible = true;
            showSystemInformation();
        }

        private void ClearFields()
        {
            lblSource.Text = "";
            txtManufacture.Text = "";
            txtProductLine.Text = "";
            txtModel.Text = "";
            lblLastUpdate.Text = "";
            lblViewed.Text = "";
            radType.ClearSelection();
            radMinSpeed.ClearSelection();
            radMaxSpeed.ClearSelection();
            radSlot.ClearSelection();
            drpStandard.ClearSelection();
            chkRemovable.Checked = false;
            txtMaxTotal.Text = "";
            radMinPerSlot.ClearSelection();
            radMaxPerSlot.ClearSelection();
            chkDualChannel.Checked = false;
            chkRunInPair.Checked = false;
            radUsb.ClearSelection();
            radFireWire.ClearSelection();
            txtChipSet.Text = "";
            txtFilter.Text = "";
            txt64Op1.Text = "";
            txt64Op2.Text = "";
            txt128Op1.Text = "";
            txt128Op2.Text = "";
            txt256Op1.Text = "";
            txt256Op2.Text = "";
            txt512Op1.Text = "";
            txt512Op2.Text = "";
            txt1GOp1.Text = "";
            txt1GOp2.Text = "";
            txt2GOp1.Text = "";
            txt2GOp2.Text = "";
            txt4GOp1.Text = "";
            txt4GOp2.Text = "";
            txt8GOp1.Text = "";
            txt8GOp2.Text = "";
            txtNotes.Text = "";
            txtComments.Text = "";
        }

        protected void btnAddModel_Click(object sender, System.EventArgs e)
        {
            selector.Visible = false;
            SystemInformation.Visible = true;
            lblModelExist.Visible = false;

            // Validation check invisible
            lblManufError.Visible = false;
            lblProductLineError.Visible = false;
            lblModelError.Visible = false;
            lblMaxTotalError.Visible = false;

            txtManufacture.Visible = false;
            txtProductLine.Visible = false;
            lstCurrManuf.Visible = true;
            lstCurrLine.Visible = true;
            lblCurrManuf.Visible = true;
            lblCurrLine.Visible = true;
            btnNewManuf.Visible = true;
            btnNewLine.Visible = true;
            btnUpdate.Visible = false;
            btnCancelUpdate.Visible = false;
            btnSaveAs.Visible = false;
            btnDelete.Visible = false;
            btnAddNew.Visible = true;
            btnCancelAdd.Visible = true;
            btnAddPicture.Visible = false;
            btnDeletePicture.Visible = false;
            imgPicture.Visible = false;

            ClearFields();

            if (Page.IsPostBack)
            {
                ListItem li;

                lstCurrManuf.Items.Clear();
                lstCurrLine.Items.Clear();

                li = new ListItem("(Select Manufacturer)", "");
                lstCurrManuf.Items.Add(li);
                lstCurrLine.Items.Add(li);

                fillManufacture(lstCurrManuf);
            }

            showSystemInfo.Visible = false;

        }

        protected void lstCurrManuf_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ListItem li;

            if (lstCurrManuf.SelectedItem.Text == "(Select Manufacturer)")
            {
                lstCurrLine.Items.Clear();
                li = new ListItem("(Select Manufacturer)", "");
                lstCurrLine.Items.Add(li);
            }
            else
            {
                // Manufacture was selected, remove the "(Select a anufacturer") entry
                if (lstCurrManuf.Items[0].Value == "")
                    lstCurrManuf.Items.RemoveAt(0);

                // Now populate the school systems
                fillProductLine(lstCurrManuf.SelectedItem.Value, lstCurrLine);
            }

        }

        protected void btnNewManuf_Click(object sender, System.EventArgs e)
        {
            lstCurrManuf.Visible = false;
            lstCurrLine.Visible = false;
            btnNewManuf.Visible = false;
            btnNewLine.Visible = false;
            lblCurrManuf.Visible = false;
            lblCurrLine.Visible = false;
            txtManufacture.Visible = true;
            txtProductLine.Visible = true;

        }

        protected void btnNewLine_Click(object sender, System.EventArgs e)
        {
            lstCurrLine.Visible = false;
            btnNewLine.Visible = false;
            lblCurrLine.Visible = false;
            txtProductLine.Visible = true;
        }

        protected void btnAddNew_Click(object sender, System.EventArgs e)
        {
            int isRequiredError = 0;
            if (lstCurrManuf.SelectedValue == "(Select Manufacturer)")
            {
                lblManufacture.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblManufacture.Visible = false;

            }

            if (lstCurrLine.SelectedValue == "(Select Manufacturer)")
            {
                lblProductLine.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblProductLine.Visible = false;

            }

            if (string.IsNullOrEmpty(txtModel.Text))
            {
                lblModelError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblModelError.Visible = false;

            }

            if (radType.SelectedIndex <= -1)
            {
                lblTypeError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblTypeError.Visible = false;

            }

            if (radMinSpeed.SelectedIndex <= -1)
            {
                lblSpeedError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblSpeedError.Visible = false;

            }

            if (radMaxSpeed.SelectedIndex <= -1)
            {
                lblMaxSpeedError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblMaxSpeedError.Visible = false;

            }

            if (radSlot.SelectedIndex <= -1)
            {
                lblSlotError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblSlotError.Visible = false;

            }

            /* if (drpStandard.SelectedValue == "0")
            {
                lblStandardSizeError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblStandardSizeError.Visible = false;

            } */

            if (string.IsNullOrEmpty(txtMaxTotal.Text))
            {
                lblMaxTotal.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblMaxTotalError.Visible = false;

            }

            if (drpMaxTotal.SelectedValue == "0")
            {
                lblMaxTotal.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblMaxTotalError.Visible = false;
            }

            if (radMinPerSlot.SelectedIndex <= -1)
            {
                lblMinPerSlotError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblMinPerSlotError.Visible = false;

            }

            if (radMaxPerSlot.SelectedIndex <= -1)
            {
                lblMaxPerSlotError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblMaxPerSlotError.Visible = false;

            }

            if (isRequiredError == 0)
            {
                addNewModel();
                showSystemInformation();
            }
        }

        private void addNewModel()
        {
            String sql;
            System.Data.SqlClient.SqlDataReader dr;
            System.Data.SqlClient.SqlDataReader drExistModel;
            MupDbConnection dsConn = new MupDbConnection();

            // Validation check invisible
            lblManufError.Visible = false;
            lblProductLineError.Visible = false;
            lblModelError.Visible = false;
            lblMaxTotalError.Visible = false;

            //Check the required fields
            if (txtManufacture.Visible == true)
            {
                if (txtManufacture.Text.Trim() == "")
                {
                    lblManufError.Visible = true;
                    return;
                }
            }
            else
            {
                if (lstCurrManuf.SelectedIndex == 0)
                {
                    lblManufError.Visible = true;
                    return;
                }
            }
            if (txtProductLine.Visible == true)
            {
                if (txtProductLine.Text.Trim() == "")
                {
                    lblProductLineError.Visible = true;
                    return;
                }
            }
            else
            {
                if (lstCurrLine.SelectedIndex == 0)
                {
                    lblProductLineError.Visible = true;
                    return;
                }
            }
            if (txtModel.Text.Trim() == "")
            {
                lblModelError.Visible = true;
                return;
            }

            try
            {
                // Check if this model already exist
                if (txtManufacture.Visible == true)
                    sql = "SELECT laptop_id FROM laptopMemory where manufacture = '" + txtManufacture.Text.Trim() + "' ";
                else
                    sql = "SELECT laptop_id FROM laptopMemory where manufacture = '" + lstCurrManuf.SelectedItem.Value + "' ";
                if (txtProductLine.Visible == true)
                    sql += "and product_line = '" + txtProductLine.Text.Trim() + "' ";
                else
                    sql += "and product_line = '" + lstCurrLine.SelectedItem.Value + "' ";
                sql += "and model = '" + txtModel.Text.Trim() + "' ";

                drExistModel = dsConn.GetReader(sql);
                if (drExistModel.Read())
                {
                    // Show message box to tell user this model has exist
                    lblModelExist.Visible = true;
                    dsConn.Dispose();
                    return;
                }
                drExistModel.Close();

                // Insert new model to database
                sql = "Insert into LaptopMemory (laptop_id, manufacture, product_line, model, type, speed, Max_speed, slot, " +
                    "standard_size, std_removable, max_total, min_size, max_size, usb, fireWire, ChipSet, filter, Notes, InstallGuide, Comments,  " +
                    "MB64OP1, MB64OP2, MB128OP1, MB128OP2, MB256OP1, MB256OP2, " +
                    "MB512OP1, MB512OP2, GB1OP1, GB1OP2, GB2OP1, GB2OP2, GB4OP1, GB4OP2, GB8OP1, GB8OP2, " +
                    "Dual_ch, Tri_ch, Run_in_pair, UpdateDate, UserInsert,IdentShortStr,IdentFullStr) values ( ";

                sql += Mup_utilities.GetTopLaptopId() + ", ";
                if (txtManufacture.Visible == true)
                    sql += "'" + txtManufacture.Text.Trim() + "', ";
                else
                    sql += "'" + lstCurrManuf.SelectedItem.Value + "', ";

                if (txtProductLine.Visible == true)
                    sql += "'" + txtProductLine.Text.Trim() + "', ";
                else
                    sql += "'" + lstCurrLine.SelectedItem.Value + "', ";

                sql += "'" + txtModel.Text.Trim() + "', '" + radType.SelectedItem.Value + "', ";
                sql += "'" + radMinSpeed.SelectedItem.Value + "', '" + radMaxSpeed.SelectedItem.Value + "', " + radSlot.SelectedItem.Value + ", ";
                sql += drpStandard.SelectedItem.Value + ", ";

                if (chkRemovable.Checked)
                    sql += 1 + ", ";
                else
                    sql += 0 + ", ";

                if (txtMaxTotal.Text == "")
                    sql += "0, ";
                else
                {
                    int maxTotal = 0;
                    if (Regex.IsMatch(txtMaxTotal.Text, "[0-9]+"))
                    {
                        if (drpMaxTotal.SelectedValue == "GB")
                        {
                            maxTotal = int.Parse(txtMaxTotal.Text) * 1024;
                        }
                        else if (drpMaxTotal.SelectedValue == "TB")
                            maxTotal = int.Parse(txtMaxTotal.Text) * 1024 * 1024;
                        else
                            maxTotal = int.Parse(txtMaxTotal.Text);
                        sql += maxTotal + ", ";
                    }

                    else
                    {
                        lblMaxTotalError.Visible = true;
                        return;
                    }
                }

                sql += radMinPerSlot.SelectedItem.Value + ", ";
                sql += radMaxPerSlot.SelectedItem.Value + ", ";
                if (radUsb.SelectedIndex > 0)
                    sql += "'" + radUsb.SelectedItem.Value + "', ";
                else
                    sql += "'2.0', ";
                if (radFireWire.SelectedIndex > 0)
                    sql += "'" + radFireWire.SelectedItem.Value + "', ";
                else
                    sql += "'None', ";
                sql += "'" + txtChipSet.Text.Replace("'", "''") + "', ";
                sql += "'" + txtFilter.Text.Trim() + "', ";
                sql += "'" + txtNotes.Text.Trim().Replace("'", "''") + "', ";
                sql += "'" + txtInstallGuide.Text.Trim().Replace("'", "''") + "', ";
                sql += "'" + txtComments.Text.Trim().Replace("'", "''") + "', ";
                sql += "'" + txt64Op1.Text.Trim() + "', ";
                sql += "'" + txt64Op2.Text.Trim() + "', '" + txt128Op1.Text.Trim() + "', '" + txt128Op2.Text.Trim() + "', ";
                sql += "'" + txt256Op1.Text.Trim() + "', '" + txt256Op2.Text.Trim() + "', '" + txt512Op1.Text.Trim() + "', ";
                sql += "'" + txt512Op2.Text.Trim() + "', '" + txt1GOp1.Text.Trim() + "', '" + txt1GOp2.Text.Trim() + "', ";
                sql += "'" + txt2GOp1.Text.Trim() + "', '" + txt2GOp2.Text.Trim() + "', ";
                sql += "'" + txt4GOp1.Text.Trim() + "', '" + txt4GOp2.Text.Trim() + "', ";
                sql += "'" + txt8GOp1.Text.Trim() + "', '" + txt8GOp2.Text.Trim() + "', ";
                if (chkDualChannel.Checked)
                    sql += "1, ";
                else
                    sql += "0, ";
                if (chkTriChannel.Checked)
                    sql += "1, ";
                else
                    sql += "0, ";

                // Run in pair
                if (chkRunInPair.Checked)
                    sql += "1, ";
                else
                    sql += "0, ";
                sql += "'" + DateTime.Now + "', ";
                if (Session["LoginName"] != null)
                    sql += "'" + Session["LoginName"].ToString() + "',";
                string identShortStr = string.Empty;
                if (txtProductLine.Visible == true)
                    identShortStr +=txtProductLine.Text.Trim() + " ";
                else
                    identShortStr += lstCurrLine.SelectedItem.Value + " ";

                identShortStr += " " + txtModel.Text.Trim();
                sql += "'" + identShortStr + "',";
                if (txtManufacture.Visible == true)
                    sql += "'" + txtManufacture.Text.Trim() + " " + identShortStr + "')";
                else
                    sql += "'" + lstCurrManuf.SelectedItem.Value + " " + identShortStr + "')";

                // Show system information
                if (dsConn.ExecSql(sql) >= 0)
                {
                    if (txtManufacture.Visible == true)
                        sql = "SELECT laptop_id FROM laptopMemory where manufacture = '" + txtManufacture.Text.Trim() + "' ";
                    else
                        sql = "SELECT laptop_id FROM laptopMemory where manufacture = '" + lstCurrManuf.SelectedItem.Value + "' ";
                    if (txtProductLine.Visible == true)
                        sql += "and product_line = '" + txtProductLine.Text.Trim() + "' ";
                    else
                        sql += "and product_line = '" + lstCurrLine.SelectedItem.Value + "' ";
                    sql += "and model = '" + txtModel.Text.Trim() + "' ";
                    dr = dsConn.GetReader(sql);

                    if (dr == null || !dr.Read())
                    {
                        dsConn.Dispose();
                        MupGenericException ex = new MupGenericException("Registration Error",
                            "Register_Click ()", "CBERegistration.ascx.cs");
                        throw ex;
                    }

                    string laptopID = dr["laptop_id"].ToString();
                    txtLaptopID.Text = laptopID;

                    dr.Close();
                }
                else
                {
                    // If the query failed, throw an exception
                    MupGenericException ex = new MupGenericException("Unable to execute Sql statement",
                        "Register_Click()", "Registration.ascx.cs");
                    throw ex;
                }
                dsConn.Dispose();
            }
            catch (Exception ex)
            {
                if (dsConn != null)
                    dsConn.Dispose();
                throw ex;
            }

            SystemInformation.Visible = false;
            selector.Visible = true;
            showSystemInfo.Visible = true;

            //clear select system panel drop down box
            ListItem li;

            lstManufacture.Items.Clear();
            lstProductLine.Items.Clear();
            lstModel.Items.Clear();

            li = new ListItem("(Select Manufacturer)", "");
            lstManufacture.Items.Add(li);
            lstProductLine.Items.Add(li);
            lstModel.Items.Add(li);

            fillManufacture(lstManufacture);
        }

        private void showSystemInformation()
        {
            System.Data.SqlClient.SqlDataReader dr;
            MupDbConnection dbConn = new MupDbConnection();

            try
            {
                string sql = "SELECT * FROM laptopMemory where laptop_id = " + txtLaptopID.Text;
                dr = dbConn.GetReader(sql);

                if (dr == null || !dr.Read())
                {
                    dbConn.Dispose();
                    MupGenericException ex = new MupGenericException("Registration Error",
                        "Register_Click ()", "CBERegistration.ascx.cs");
                    throw ex;
                }

                lblModelExist.Visible = false;
                lblManufacture.Text = dr["manufacture"].ToString();
                hdnManufacture.Text = dr["manufacture"].ToString();
                lblProductLine.Text = dr["product_line"].ToString();
                hdnProductLine.Text = dr["product_line"].ToString();
                lblModel.Text = dr["model"].ToString();
                hdnModel.Text = dr["model"].ToString();
                lblType.Text = dr["type"].ToString();
                lblSpeed.Text = dr["speed"].ToString();
                lblMaxSpeed.Text = dr["Max_speed"].ToString();
                lblSlot.Text = dr["slot"].ToString();
                lblStandard.Text = dr["standard_size"].ToString();
                if (dr["std_removable"].ToString() == "true")
                    lblRemovable.Text = "Yes";
                else
                    lblRemovable.Text = "No";
                lblMaxTotal.Text = dr["max_total"].ToString();
                lblMinPerSlot.Text = dr["min_size"].ToString();
                lblMaxPerSlot.Text = dr["max_size"].ToString();
                lblFilter.Text = dr["filter"].ToString();
                if (dr["Dual_ch"].ToString() != "")
                {
                    if ((bool)dr["Dual_ch"] == true)
                        lblDualChannel.Text = "Yes";
                    else
                        lblDualChannel.Text = "No";
                }
                if (dr["Tri_ch"].ToString() != "")
                {
                    if ((bool)dr["tri_ch"] == true)
                        lblTriChannel.Text = "Yes";
                    else
                        lblTriChannel.Text = "No";
                }
                if (dr["Run_in_pair"].ToString() != "")
                {
                    if ((bool)dr["Run_in_pair"] == true)
                        lblRunInPair.Text = "Yes";
                    else
                        lblRunInPair.Text = "No";
                }
                lblUSB.Text = dr["USB"].ToString();
                lblChipSet.Text = dr["ChipSet"].ToString();
                lblFireWire.Text = "FireWire " + dr["FireWire"].ToString();
                lblNotes.Text = dr["Notes"].ToString();
                lblInstallGuide.Text = dr["InstallGuide"].ToString();
                lblComments.Text = dr["Comments"].ToString();
                lbl64Op1.Text = dr["MB64OP1"].ToString();
                lbl64Op2.Text = dr["MB64OP2"].ToString();
                lbl128Op1.Text = dr["MB128OP1"].ToString();
                lbl128Op2.Text = dr["MB128OP2"].ToString();
                lbl256Op1.Text = dr["MB256OP1"].ToString();
                lbl256Op2.Text = dr["MB256OP2"].ToString();
                lbl512Op1.Text = dr["MB512OP1"].ToString();
                lbl512Op2.Text = dr["MB512OP2"].ToString();
                lbl1GOp1.Text = dr["GB1OP1"].ToString();
                lbl1GOp2.Text = dr["GB1OP2"].ToString();
                lbl2GOp1.Text = dr["GB2OP1"].ToString();
                lbl2GOp2.Text = dr["GB2OP2"].ToString();
                lbl4GOp1.Text = dr["GB4OP1"].ToString();
                lbl4GOp2.Text = dr["GB4OP2"].ToString();
                lbl8GOp1.Text = dr["GB8OP1"].ToString();
                lbl8GOp2.Text = dr["GB8OP2"].ToString();
                imgShowPicture.ImageUrl = System.IO.Path.Combine(Server.UrlPathEncode("/MfdImages/ModelImages"), dr["picture"].ToString());

                dbConn.Dispose();
            }
            catch (Exception ex)
            {
                if (dbConn != null)
                    dbConn.Dispose();
                throw ex;
            }
        }

        protected void btnCancelUpdate_Click(object sender, System.EventArgs e)
        {
            updateSystemInformation();
        }

        protected void btnCancelAdd_Click(object sender, System.EventArgs e)
        {
            SystemInformation.Visible = false;
            selector.Visible = true;

            // Validation check invisible
            lblManufError.Visible = false;
            lblProductLineError.Visible = false;
            lblModelError.Visible = false;
            lblMaxTotalError.Visible = false;

            //clear select system panel drop down box
            ListItem li;

            lstManufacture.Items.Clear();
            lstProductLine.Items.Clear();
            lstModel.Items.Clear();

            li = new ListItem("(Select Manufacturer)", "");
            lstManufacture.Items.Add(li);
            lstProductLine.Items.Add(li);
            lstModel.Items.Add(li);

            fillManufacture(lstManufacture);
        }

        protected void btnDelete_Click(object sender, System.EventArgs e)
        {
            String sql;

            MupDbConnection dsConn = new MupDbConnection();

            try
            {
                sql = "Delete from laptopMemory where laptop_id = " + txtLaptopID.Text;

                dsConn.ExecSql(sql);
                dsConn.Dispose();
            }
            catch (Exception ex)
            {
                if (dsConn != null)
                    dsConn.Dispose();
                throw ex;
            }

            SystemInformation.Visible = false;
            selector.Visible = true;

            //clear select system panel drop down box
            ListItem li;

            lstManufacture.Items.Clear();
            lstProductLine.Items.Clear();
            lstModel.Items.Clear();

            li = new ListItem("(Select Manufacturer)", "");
            lstManufacture.Items.Add(li);
            lstProductLine.Items.Add(li);
            lstModel.Items.Add(li);

            fillManufacture(lstManufacture);

        }

        private void imgHome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Response.Redirect("default.aspx");
        }

        protected void btnAddPicture_Click(object sender, System.EventArgs e)
        {
            Response.Write("<script language=\"javascript\">window.open('AddPicture.aspx?type=Laptop&system_id=" + txtLaptopID.Text + "','Picture','toolbar=no,center=yes,location=no,titlebar=no,resizable=no,status=no,scrollbars=no,menubar=no,width=500,height=400');</script>");
        }

        protected void btnDeletePicture_Click(object sender, System.EventArgs e)
        {
            String sql;

            try
            {
                MupDbConnection dsConn = new MupDbConnection();

                try
                {
                    sql = "Update laptopMemory set picture = '' where laptop_id = " + txtLaptopID.Text;

                    if (dsConn.ExecSql(sql) >= 0)
                    {
                        btnAddPicture.Visible = true;
                        btnDeletePicture.Visible = false;
                        imgPicture.Visible = false;
                    }
                    dsConn.Dispose();
                }
                catch (Exception ex)
                {
                    if (dsConn != null)
                        dsConn.Dispose();
                    throw ex;
                }
            }
            catch (MupException ex)
            {
                Response.Write(ex.MupMessage);
            }
        }

        protected void btnShowUpdate_Click(object sender, System.EventArgs e)
        {
            showSystemInfo.Visible = false;
            txtManufacture.Visible = true;
            txtProductLine.Visible = true;
            lstCurrManuf.Visible = false;
            lstCurrLine.Visible = false;
            btnNewManuf.Visible = false;
            btnNewLine.Visible = false;
            lblCurrManuf.Visible = false;
            lblCurrLine.Visible = false;
            btnAddNew.Visible = false;
            btnCancelAdd.Visible = false;
            btnUpdate.Visible = true;
            btnCancelUpdate.Visible = true;
            btnDelete.Visible = true;
            updateSystemInformation();
        }

        protected void radType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            changeSpeedType();

            for (int i = 0; i < radMinSpeed.Items.Count; i++)
            {
                if (radMinSpeed.Items[i].Value == txtMinSpeed.Text)
                    radMinSpeed.SelectedValue = txtMinSpeed.Text;
            }

            for (int i = 0; i < radMaxSpeed.Items.Count; i++)
            {
                if (radMaxSpeed.Items[i].Value == txtMaxSpeed.Text)
                    radMaxSpeed.SelectedValue = txtMaxSpeed.Text;
            }
        }

        private void changeSpeedType()
        {
            if (radType.SelectedValue == "DDR2" || radType.SelectedValue == "Micro-Dimm DDR2" ||
                radType.SelectedValue == "DDR2 Long-DIMM" || radType.SelectedValue == "Proprietary DDR2")
            {
                radMinSpeed.Items.Clear();
                radMinSpeed.Items.Add("0");
                radMinSpeed.Items.Add("400");
                radMinSpeed.Items.Add("533");
                radMinSpeed.Items.Add("667");
                radMinSpeed.Items.Add("800");
                radMinSpeed.Items.Add("900");
                radMinSpeed.Items.Add("1066");

                radMaxSpeed.Items.Clear();
                radMaxSpeed.Items.Add("0");
                radMaxSpeed.Items.Add("400");
                radMaxSpeed.Items.Add("533");
                radMaxSpeed.Items.Add("667");
                radMaxSpeed.Items.Add("800");
                radMaxSpeed.Items.Add("900");
                radMaxSpeed.Items.Add("1066");
            }
            else if (radType.SelectedValue == "DDR3" || radType.SelectedValue == "Micro-Dimm DDR3" ||
                radType.SelectedValue == "DDR3 Long-DIMM" || radType.SelectedValue == "Proprietary DDR3")
            {
                radMinSpeed.Items.Clear();
                radMinSpeed.Items.Add("0");
                radMinSpeed.Items.Add("800");
                radMinSpeed.Items.Add("1066");
                radMinSpeed.Items.Add("1333");
                radMinSpeed.Items.Add("1600");

                radMaxSpeed.Items.Clear();
                radMaxSpeed.Items.Add("0");
                radMaxSpeed.Items.Add("800");
                radMaxSpeed.Items.Add("1066");
                radMaxSpeed.Items.Add("1333");
                radMaxSpeed.Items.Add("1600");
            }
            else if (this.radType.SelectedValue == "DDR4" || this.radType.SelectedValue == "Micro-Dimm DDR4" || this.radType.SelectedValue == "DDR4 Long-DIMM")
            {
                radMinSpeed.Items.Clear();
                radMinSpeed.Items.Add("0");
                radMinSpeed.Items.Add("2133");
                radMinSpeed.Items.Add("2400");
                radMinSpeed.Items.Add("2666");
                radMinSpeed.Items.Add("2933");
                radMinSpeed.Items.Add("3200");
                radMinSpeed.Items.Add("3600");
                radMinSpeed.Items.Add("4000");
                radMinSpeed.Items.Add("4400");
                radMaxSpeed.Items.Clear();
                radMaxSpeed.Items.Add("0");
                radMaxSpeed.Items.Add("2133");
                radMaxSpeed.Items.Add("2400");
                radMaxSpeed.Items.Add("2666");
                radMaxSpeed.Items.Add("2933");
                radMaxSpeed.Items.Add("3200");
                radMaxSpeed.Items.Add("3600");
                radMaxSpeed.Items.Add("4000");
                radMaxSpeed.Items.Add("4400");
            }
            else if (this.radType.SelectedValue == "DDR5" || this.radType.SelectedValue == "Micro-Dimm DDR5" || this.radType.SelectedValue == "DDR5 Long-DIMM")
            {
                radMinSpeed.Items.Clear();
                radMinSpeed.Items.Add("0");
                radMinSpeed.Items.Add("2133");
                radMinSpeed.Items.Add("2400");
                radMinSpeed.Items.Add("2666");
                radMinSpeed.Items.Add("2933");
                radMinSpeed.Items.Add("3200");
                radMinSpeed.Items.Add("3600");
                radMinSpeed.Items.Add("4000");
                radMinSpeed.Items.Add("4400");
                radMinSpeed.Items.Add("4800");
                radMinSpeed.Items.Add("5200");
                radMinSpeed.Items.Add("5600");
                radMinSpeed.Items.Add("6000");
                radMinSpeed.Items.Add("6400");
                radMaxSpeed.Items.Clear();
                radMaxSpeed.Items.Add("0");
                radMaxSpeed.Items.Add("2133");
                radMaxSpeed.Items.Add("2400");
                radMaxSpeed.Items.Add("2666");
                radMaxSpeed.Items.Add("2933");
                radMaxSpeed.Items.Add("3200");
                radMaxSpeed.Items.Add("3600");
                radMaxSpeed.Items.Add("4000");
                radMaxSpeed.Items.Add("4400");
                radMaxSpeed.Items.Add("4800");
                radMaxSpeed.Items.Add("5200");
                radMaxSpeed.Items.Add("5600");
                radMaxSpeed.Items.Add("6000");
                radMaxSpeed.Items.Add("6400");
            }
            else
            {
                radMinSpeed.Items.Clear();
                radMinSpeed.Items.Add("0");
                radMinSpeed.Items.Add("100");
                radMinSpeed.Items.Add("133");
                radMinSpeed.Items.Add("266");
                radMinSpeed.Items.Add("333");
                radMinSpeed.Items.Add("400");
                radMinSpeed.Items.Add("433");
                radMinSpeed.Items.Add("466");
                radMinSpeed.Items.Add("500");

                radMaxSpeed.Items.Clear();
                radMaxSpeed.Items.Add("0");
                radMaxSpeed.Items.Add("100");
                radMaxSpeed.Items.Add("133");
                radMaxSpeed.Items.Add("266");
                radMaxSpeed.Items.Add("333");
                radMaxSpeed.Items.Add("400");
                radMaxSpeed.Items.Add("433");
                radMaxSpeed.Items.Add("466");
                radMaxSpeed.Items.Add("500");
            }
        }

        protected void btnSaveAs_Click(object sender, System.EventArgs e)
        {
            string strPicture;
            if (txtReturnCancel.Text == "0")
            {
                strPicture = web.MupUtilities.getLaptopPicture(txtLaptopID.Text);
                addNewModel();
                web.MupUtilities.setLaptopPicture(txtLaptopID.Text, strPicture);
                showSystemInformation();
            }
        }

        protected void btnUpdate2_Click(object sender, EventArgs e)
        {
            saveUpdate();
        }
    }
}
