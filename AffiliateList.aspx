<%@ Page language="c#" Inherits="memoryAdmin.AffiliateList" CodeFile="AffiliateList.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AffiliateList</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0"
				cellPadding="1" width="780" border="0" class="item">
				<tr>
					<td colSpan="3"><A href="default.aspx"><IMG alt="Memory-Up.com" src="/images/memoryselector/bd_fb.png"
								border="0"></A><b>Affiliate Program User List.</b>
					</td>
				</tr>
				<tr>
					<td>
						<P align="right">User Name:
							<asp:textbox id="txtUserName" runat="server"></asp:textbox></P>
					</td>
					<td>
						<P align="right">First Name:
							<asp:textbox id="txtFirstName" runat="server"></asp:textbox></P>
					</td>
					<td>Last Name:
						<asp:textbox id="txtLastName" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td>
						<P align="right">E-Mail;
							<asp:textbox id="txtEmail" runat="server"></asp:textbox></P>
					</td>
					<td>
						<P align="right">Recent Order:
							<asp:textbox id="txtOrderNo" runat="server"></asp:textbox></P>
					</td>
					<td><asp:button id="btnSearch" runat="server" Text="Search" onclick="btnSearch_Click"></asp:button></td>
				</tr>
				<tr>
					<td colSpan="3"><asp:datagrid id="grdUserList" runat="server" CellPadding="3" BackColor="White" BorderWidth="1px"
							BorderStyle="None" BorderColor="#CCCCCC" Width="100%" AutoGenerateColumns="False">
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<ItemStyle Font-Size="10pt" ForeColor="Navy"></ItemStyle>
							<AlternatingItemStyle Font-Size="10pt" ForeColor="Navy" BackColor="LightCyan"></AlternatingItemStyle>
							<HeaderStyle Font-Size="10pt" Font-Bold="True" HorizontalAlign="Center" ForeColor="Navy" BackColor="LightGray"></HeaderStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
								<asp:BoundColumn DataField="User_name" SortExpression="User_name" HeaderText="User Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="First_name" SortExpression="First_name" HeaderText="First Number"></asp:BoundColumn>
								<asp:BoundColumn DataField="Last_name" SortExpression="Last_name" HeaderText="Last Number"></asp:BoundColumn>
								<asp:BoundColumn DataField="Email" SortExpression="Email" HeaderText="Email"></asp:BoundColumn>
								<asp:BoundColumn DataField="Submit_date" SortExpression="Submit_date" HeaderText="Register Date" DataFormatString="{0:d}"></asp:BoundColumn>
								<asp:BoundColumn DataField="Order_no" SortExpression="Order_no" HeaderText="Order Number" ItemStyle-BackColor="AntiqueWhite"></asp:BoundColumn>
								<asp:BoundColumn DataField="Rebate_per" SortExpression="Rebate_per" HeaderText="Rebate %" DataFormatString="{0:P}"
									ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
								<asp:BoundColumn DataField="Rebate_amount" SortExpression="Rebate_amount" HeaderText="Rebate Amount"
									DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
								<asp:BoundColumn DataField="Update_date" SortExpression="Update_date" HeaderText="Update Date"></asp:BoundColumn>
								<asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="Status"></asp:BoundColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td colSpan="3"></td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
