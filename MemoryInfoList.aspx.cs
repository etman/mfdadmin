using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data.SqlClient;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for DeskTopInfo.
	/// </summary>
	public partial class MemoryInfoList : System.Web.UI.Page
	{
		protected System.Data.SqlClient.SqlDataAdapter memoryAdapter;
		protected System.Data.SqlClient.SqlConnection memoryConn;
		protected System.Data.SqlClient.SqlCommand memorySelectCommand;
		protected memoryAdmin.memoryDataSet memoryDataSet1;

		private string strSqlConnectionString = ConfigurationSettings.AppSettings["cnstr"];

		/*
		private const string strSqlConnectionString =
			"Network Library=DBMSSOCN;" +
			"Data Source=WEB_SERVER_1;" +
			"Initial Catalog=MemoryUp;" +
			"User ID=mup;" +
			"Password=sc_mup_sql0103";*/

		/*
		private const string strSqlConnectionString =
			"Data Source=(local);" +
			"User ID=mup;" +
			"pwd=sc_mup_sql0103;" +
			"initial catalog=MemoryUp";*/

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            //if (Session["LoginName"] == null)
            //{
            //    Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
            //}
			
			creatMemoryDataSet();
			if (!IsPostBack)
			{
				Session["sortColumn"] = "Counter";
				Session["Order"] = "DESC";
				grdMemoryList.DataSource = memoryDataSet1.LaptopMemory.DefaultView;
				grdMemoryList.DataBind();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.grdMemoryList.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.grdMemoryList_SortCommand);

		}
		#endregion

		private void creatMemoryDataSet()
		{

			try
			{
				memoryConn = new System.Data.SqlClient.SqlConnection();
				memoryAdapter = new System.Data.SqlClient.SqlDataAdapter();
				memorySelectCommand = new System.Data.SqlClient.SqlCommand();
				memoryDataSet1 = new memoryAdmin.memoryDataSet();

				memoryAdapter.SelectCommand = memorySelectCommand;
				this.memoryAdapter.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																										new System.Data.Common.DataTableMapping("Table", "LaptopMemory", new System.Data.Common.DataColumnMapping[] {
																																																						 new System.Data.Common.DataColumnMapping("Manufacture", "Manufacture"),
																																																						 new System.Data.Common.DataColumnMapping("Product_line", "Product_line"),
																																																						 new System.Data.Common.DataColumnMapping("Model", "Model"),
																																																						 new System.Data.Common.DataColumnMapping("Speed", "Speed"),
																																																						 new System.Data.Common.DataColumnMapping("Chipset", "Chipset"),
																																																						 new System.Data.Common.DataColumnMapping("Counter", "Counter")})});
				memoryConn.ConnectionString = strSqlConnectionString;
				
				memorySelectCommand.CommandText = "Select Manufacture, Product_line, model, Speed, filter, CONVERT(varchar(10), Last_visit, 101) AS date_visit, Counter ";
				memorySelectCommand.CommandText += "From laptopmemory ";
				memorySelectCommand.CommandText += "WHERE (Counter IS NOT NULL) AND (model <> \'(Select Model)\') ";
				memorySelectCommand.CommandText += "And (Last_visit >= '" + DateTime.Today.AddDays(-30) + "' ";
				memorySelectCommand.CommandText += "Or Counter >= 20 ) ";
				memorySelectCommand.CommandText += "ORDER BY Counter DESC, manufacture, Product_line, model";
				memorySelectCommand.Connection = memoryConn;

				memoryAdapter.Fill(memoryDataSet1, "LaptopMemory");
			}
			catch (MupException e)
			{
				Response.Write(e.MupMessage);
			}
		}

		private void grdMemoryList_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			DataView sortView = memoryDataSet1.DefaultViewManager.CreateDataView(memoryDataSet1.Tables["laptopMemory"]);
			if (e.SortExpression == Session["sortColumn"].ToString())
			{
				if (Session["Order"].ToString() == "ASC")
				{
					sortView.Sort = e.SortExpression + " DESC";
					Session["Order"] = "DESC";
				}
				else
				{
					sortView.Sort = e.SortExpression + " ASC";
					Session["Order"] = "ASC";
				}
			}
			else
			{
				sortView.Sort = e.SortExpression + " ASC";
				Session["Order"] = "ASC";
			}
			Session["sortColumn"] = e.SortExpression;
			grdMemoryList.DataSource = sortView;
			grdMemoryList.DataBind();
		}
	}
}
