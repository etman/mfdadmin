<%@ Page language="c#" Inherits="memoryAdmin.CameraUpdate" CodeFile="CameraUpdate.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CameraUpdate</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="780" border="0">
				<TR>
					<TD class="title"><A href="default.aspx"><IMG alt="Memory-Up.com" src="/images/memoryselector/bd_fb.png"
								border="0"></A>Digital Camera Selector</TD>
				</TR>
				<TR>
					<TD><asp:panel id="selector" runat="server">
							<TABLE class="item" id="Table2" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD>Select the digital camera...</TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 6px" align="right">
										<asp:label id="Label1" runat="server">Select the manufacturer...</asp:label></TD>
									<TD style="HEIGHT: 6px">
										<asp:dropdownlist id="lstManufacture" runat="server" AutoPostBack="True" onselectedindexchanged="lstManufacture_SelectedIndexChanged"></asp:dropdownlist>
										<asp:label id="hdnManufacture" runat="server" Visible="False"></asp:label></TD>
									<TD style="HEIGHT: 15px"></TD>
								</TR>
								<TR>
									<TD align="right">
										<asp:label id="Label2" runat="server">Select the product line...</asp:label></TD>
									<TD>
										<asp:dropdownlist id="lstProductLine" runat="server" AutoPostBack="True" onselectedindexchanged="lstProductLine_SelectedIndexChanged"></asp:dropdownlist>
										<asp:label id="hdnProductLine" runat="server" Visible="False"></asp:label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right">
										<asp:label id="Label3" runat="server">Select the model...</asp:label></TD>
									<TD>
										<asp:dropdownlist id="lstModel" runat="server" AutoPostBack="True" onselectedindexchanged="lstModel_SelectedIndexChanged"></asp:dropdownlist>
										<asp:label id="hdnModel" runat="server" Visible="False"></asp:label>Visit:
										<asp:Label id="lblCounter" runat="server"></asp:Label>&nbsp;times by customer</TD>
									<TD>
										<asp:button id="btnAddModel" runat="server" Text="Add New" onclick="btnAddModel_Click"></asp:button></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD><asp:panel id="pnlUpdate" runat="server" Visible="False">
							<TABLE class="item" id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD colSpan="3">
										<HR width="100%" SIZE="1">
										<asp:TextBox id="txtCameraId" runat="server" Visible="False"></asp:TextBox>
										<asp:Label id="lblModelExist" runat="server" ForeColor="Red">This model already exist!!</asp:Label>
										<asp:Label id="lblUpdateError" runat="server" ForeColor="Red">There is an error when updating camera selector!</asp:Label>
										<asp:Label id="lblViewed" runat="server"></asp:Label></TD>
								</TR>
                                <tr>
                                    <td colspan="3">
                                        <asp:Button ID="btnUpdate2" runat="server" OnClick="btnUpdate2_Click" Text="Update" /></td>
                                </tr>
								<TR>
									<TD style="HEIGHT: 22px" bgColor="lightgrey"><STRONG>Manufacturer:</STRONG></TD>
									<TD style="HEIGHT: 22px">
										<asp:TextBox id="txtManufacture" runat="server" MaxLength="50"></asp:TextBox>
										<asp:DropDownList id="lstCurrManuf" runat="server" AutoPostBack="True" onselectedindexchanged="lstCurrManuf_SelectedIndexChanged"></asp:DropDownList>
										<asp:Label id="lblOrAddManuf" runat="server">Or</asp:Label>
										<asp:Button id="btnNewManuf" runat="server" Text="Add New" onclick="btnNewManuf_Click"></asp:Button>
										<asp:Label id="lblManufError" runat="server" Visible="False" ForeColor="Red">Please enter manufacturer!</asp:Label></TD>
									<TD style="HEIGHT: 22px"><FONT color="red">(Requied)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Prodoct Line:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtProductLine" runat="server" MaxLength="50"></asp:TextBox>
										<asp:DropDownList id="lstCurrLine" runat="server"></asp:DropDownList>
										<asp:Label id="lblOrAddLine" runat="server">Or</asp:Label>
										<asp:Button id="btnNewLine" runat="server" Text="Add New" onclick="btnNewLine_Click"></asp:Button>
										<asp:Label id="lblProductLineError" runat="server" Visible="False" ForeColor="Red">Please enter product line!</asp:Label></TD>
									<TD><FONT color="red">(Requied)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Model:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtModel" runat="server" MaxLength="50"></asp:TextBox>
										<asp:Label id="lblModelError" runat="server" Visible="False" ForeColor="Red">Please enter a model!</asp:Label>
										<asp:Label id="lblLastUpdate" runat="server"></asp:Label><br />
                                        Please note: "<span style="font-weight: bold; color: red">First word</span>" in
                                        the model is the most important in order to idendify this model. Please put unique
                                        model number on the first word. For example, M9969LL/A.</TD>
									<TD><FONT color="red">(Requied)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Memory Card Type:</STRONG></TD>
									<TD>
										<asp:checkboxlist id="chkMemoryType" runat="server" CssClass="item" RepeatDirection="Horizontal" RepeatColumns="2"></asp:checkboxlist></TD>
									<TD><FONT color="red">(Requied)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Min. Memory Size:</STRONG></TD>
									<TD>
										<asp:RadioButtonList id="radMinMemorySize" runat="server" CssClass="item" RepeatDirection="Horizontal">
											<asp:ListItem Value="32" Selected="True">32M</asp:ListItem>
											<asp:ListItem Value="64">64M</asp:ListItem>
											<asp:ListItem Value="128">128M</asp:ListItem>
											<asp:ListItem Value="256">256M</asp:ListItem>
											<asp:ListItem Value="512">512M</asp:ListItem>
											<asp:ListItem Value="1024">1G</asp:ListItem>
											<asp:ListItem Value="2048">2G</asp:ListItem>
											<asp:ListItem Value="4096">4G</asp:ListItem>
                                            <asp:ListItem Value="8192">8G</asp:ListItem>
                                            <asp:ListItem Value="16384">16G</asp:ListItem>
                                            <asp:ListItem Value="32768">32G</asp:ListItem>
                                            <asp:ListItem Value="65536">64G</asp:ListItem>
										</asp:RadioButtonList>
										<asp:Label id="lblMinMemoryError" runat="server" ForeColor="Red">Please adjust max. or min. memory size!</asp:Label></TD>
									<TD><FONT color="red">(Requied)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Max. Memory Size:</STRONG></TD>
									<TD>
										<asp:RadioButtonList id="radMaxMemorySize" runat="server" CssClass="item" RepeatDirection="Horizontal">
											<asp:ListItem Value="32">32M</asp:ListItem>
											<asp:ListItem Value="64">64M</asp:ListItem>
											<asp:ListItem Value="128">128M</asp:ListItem>
											<asp:ListItem Value="256">256M</asp:ListItem>
											<asp:ListItem Value="512">512M</asp:ListItem>
											<asp:ListItem Value="1024">1G</asp:ListItem>
											<asp:ListItem Value="2048">2G</asp:ListItem>
											<asp:ListItem Value="4096">4G</asp:ListItem>
                                            <asp:ListItem Value="8192">8G</asp:ListItem>
                                            <asp:ListItem Value="16384">16G</asp:ListItem>
                                            <asp:ListItem Value="32768">32G</asp:ListItem>
                                            <asp:ListItem Value="65536">64G</asp:ListItem>
										</asp:RadioButtonList>
										<asp:Label id="lblMaxMemoryError" runat="server" Visible="False" ForeColor="Red">Please select max. memory size!</asp:Label></TD>
									<TD><FONT color="red">(Requied)</FONT></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 26px" bgColor="lightgrey"><STRONG>Battery:</STRONG></TD>
									<TD style="HEIGHT: 26px">
										<asp:DropDownList id="lstBattery" runat="server">
											<asp:ListItem Value="(Select Battery)">(Select Battery)</asp:ListItem>
										</asp:DropDownList>
										<asp:TextBox id="txtBatteryDesc" runat="server" ReadOnly="True" Width="352px"></asp:TextBox></TD>
									<TD style="HEIGHT: 26px"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 26px" noWrap bgColor="#d3d3d3"><STRONG>Manufacturer Battery Code:</STRONG></TD>
									<TD style="HEIGHT: 26px">
										<asp:TextBox id="txtBatteryCode" runat="server" MaxLength="50"></asp:TextBox></TD>
									<TD style="HEIGHT: 26px"></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Mega Pixels:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtMegaPixels" runat="server" MaxLength="10"></asp:TextBox>Mega 
										Pixels</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="#d3d3d3"><STRONG>Optical Zoom:</STRONG></TD>
									<TD>
										<asp:TextBox id="txtOpticalZoom" runat="server" MaxLength="10"></asp:TextBox>X</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 17px" bgColor="lightgrey"><STRONG>Digital Zoom:</STRONG></TD>
									<TD style="HEIGHT: 17px">
										<asp:TextBox id="txtDigitalZoom" runat="server" MaxLength="10"></asp:TextBox>X</TD>
									<TD style="HEIGHT: 17px"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 17px" bgColor="#d3d3d3"><STRONG>LCD:</STRONG></TD>
									<TD style="HEIGHT: 17px">
										<asp:TextBox id="txtLcd" runat="server" MaxLength="50" Width="420px"></asp:TextBox></TD>
									<TD style="HEIGHT: 17px"></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Build in Flash:</STRONG></TD>
									<TD>
										<asp:CheckBox id="chkFlash" runat="server" Text="Yes" Checked="True"></asp:CheckBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="#d3d3d3"><STRONG>USB Support:</STRONG></TD>
									<TD>
										<asp:CheckBox id="chkUsb" runat="server" Text="Yes"></asp:CheckBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="#d3d3d3"><STRONG>FireWire Support:</STRONG></TD>
									<TD>
										<asp:CheckBox id="chkFireWire" runat="server" Text="Yes"></asp:CheckBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Notes:</STRONG></TD>
									<TD colSpan="2" rowSpan="1">
										<asp:TextBox id="txtNotes" runat="server" TextMode="MultiLine" Rows="6" Columns="65"></asp:TextBox></TD>
								</TR>
								<TR>
									<TD bgColor="#d3d3d3"><STRONG>Internal Comments</STRONG></TD>
									<TD colSpan="2">
										<asp:TextBox id="txtComments" runat="server" TextMode="MultiLine" Rows="6" Columns="65"></asp:TextBox></TD>
								</TR>
								<TR>
									<TD bgColor="#d3d3d3"><STRONG>Picture:</STRONG></TD>
									<TD>
										<asp:Button id="btnAddPicture" runat="server" Text="Add Picture" onclick="btnAddPicture_Click"></asp:Button>
										<asp:Image id="imgPicture" runat="server"></asp:Image>
										<asp:Button id="btnDeletePicture" runat="server" Text="Delete Picture" onclick="btnDeletePicture_Click"></asp:Button></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Accessories:</STRONG></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD>
										<asp:Button id="btnUpdate" runat="server" Visible="False" Text="Update" onclick="btnUpdate_Click"></asp:Button>
										<asp:Button id="btnAdd" runat="server" Text="Add" ForeColor="Navy" onclick="btnAdd_Click"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:Button id="btnCancelUpdate" runat="server" Text="Cancel" onclick="btnCancelUpdate_Click"></asp:Button>
										<asp:Button id="btnCancelAdd" runat="server" Text="Cancel" ForeColor="Navy" onclick="btnCancelAdd_Click"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
									</TD>
									<TD>
										<asp:Button id="btnDelete" runat="server" Text="Delete" onclick="btnDelete_Click"></asp:Button></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD><asp:panel id="pnlInformation" runat="server" Visible="False">
							<TABLE class="item" id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
								<TR>
									<TD colSpan="4">
										<HR width="100%" SIZE="1">
									</TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Manufacturer:</STRONG></TD>
									<TD>
										<asp:Label id="lblManufacture" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Prodoct Line:</STRONG></TD>
									<TD>
										<asp:Label id="lblProductLine" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Model:</STRONG></TD>
									<TD>
										<asp:Label id="lblModel" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Memory Card Type:</STRONG></TD>
									<TD>
										<asp:Label id="lblMemoryType" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Min. Memory Size:</STRONG></TD>
									<TD>
										<asp:Label id="lblMinMemorySize" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Max. Memory Size:</STRONG></TD>
									<TD>
										<asp:Label id="lblMaxMemorySize" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Battery:</STRONG></TD>
									<TD>
										<asp:Label id="lblBattery" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Manufacturer Battery Code:</STRONG></TD>
									<TD>
										<asp:Label id="lblBatteryCode" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Mega Pixels:</STRONG></TD>
									<TD>
										<asp:Label id="lblMegaPixels" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Optical Zoom:</STRONG></TD>
									<TD>
										<asp:Label id="lblOpticalZoom" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Digital Zoom:</STRONG></TD>
									<TD>
										<asp:Label id="lblDigitalZoom" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>LCD:</STRONG></TD>
									<TD>
										<asp:Label id="lblLcd" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Build in Flash:</STRONG></TD>
									<TD>
										<asp:Label id="lblFlash" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>USB Support:</STRONG></TD>
									<TD>
										<asp:Label id="lblUsb" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>FireWire:</STRONG></TD>
									<TD>
										<asp:Label id="lblFireWire" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Notes:</STRONG></TD>
									<TD>
										<asp:Label id="lblNotes" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Internal Comments</STRONG>:</TD>
									<TD>
										<asp:Label id="lblComments" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Picture:</STRONG></TD>
									<TD>
										<asp:Image id="imgShowPicture" runat="server"></asp:Image></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"><STRONG>Accessories:</STRONG></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD align="right"></TD>
									<TD>
										<asp:Button id="btnShowUpdate" runat="server" Text="Update" onclick="btnShowUpdate_Click"></asp:Button></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
