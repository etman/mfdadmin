<%@ Page language="c#" Inherits="memoryAdmin.LaptopUpdate" CodeFile="LaptopUpdate.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>LaptopUpdate</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
		<script>
			function openNewModel()
			{
				var strReturn = new Array('', '', '');
				
				var winSetting = 'status:no;dialogWidth:500px;dialogHeight:220px;dialogHide:true;help:no;scroll:no';
				strReturn = window.showModalDialog('newModel.aspx?type=laptop', null, winSetting);
				if (strReturn != null)
				{
					document.getElementById('txtManufacture').value = strReturn[0].toString();
					document.getElementById('txtProductLine').value = strReturn[1].toString();
					document.getElementById('txtModel').value = strReturn[2].toString();
					document.getElementById('txtReturnCancel').value = '0';
				}
				else
				{
					document.getElementById('txtReturnCancel').value = '1';
				}
			}
		</script>
	</HEAD>
	<body>
		<form method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" cellSpacing="0"
				cellPadding="1" width="800" border="0">
				<TR>
					<TD class="title"><STRONG><asp:imagebutton id="imgHome" style="width:90px;height:40px;" runat="server" ImageUrl="/images/memoryselector/bd_fb.png"></asp:imagebutton>
                        Laptop Memory Administration Page</STRONG></TD>
				</TR>
				<TR>
					<TD><asp:panel id="selector" runat="server">
							<TABLE class="item" id="Table2" cellSpacing="0" cellPadding="3" width="100%" border="0">
								<TR>
									<TD style="HEIGHT: 24px" width="300" colSpan="2">
										<asp:Label id="Label11" runat="server" Width="280px">Select System...</asp:Label></TD>
									<TD style="HEIGHT: 24px"></TD>
								</TR>
								<TR>
									<TD align="right">
										<asp:Label id="Label12" runat="server">Select Laptop Manufacturer: </asp:Label></TD>
									<TD width="300">
										<asp:DropDownList id="lstManufacture" runat="server" AutoPostBack="True" Width="300px"
                                            onselectedindexchanged="lstManufacture_SelectedIndexChanged"></asp:DropDownList>
										<asp:Label id="hdnManufacture" runat="server" Visible="False"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right">
										<asp:Label id="Label8" runat="server">Select Laptop Product Line: </asp:Label></TD>
									<TD width="300">
										<asp:DropDownList id="lstProductLine" runat="server" AutoPostBack="True" Width="300px"
                                            onselectedindexchanged="lstProductLine_SelectedIndexChanged"></asp:DropDownList>
										<asp:Label id="hdnProductLine" runat="server" Visible="False"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right">
										<asp:Label id="Label13" runat="server">Select Laptop Model: </asp:Label></TD>
									<TD width="300">
										<asp:DropDownList id="lstModel" runat="server" AutoPostBack="True" Width="300px"
                                            onselectedindexchanged="lstModel_SelectedIndexChanged"></asp:DropDownList>
										<asp:Label id="hdnModel" runat="server" Visible="False"></asp:Label></TD>
									<TD>
										<asp:Button id="btnAddModel" runat="server" Text="Add New" onclick="btnAddModel_Click"></asp:Button></TD>
								</TR>
							</TABLE>
						</asp:panel>
					</TD>
				</TR>
				<TR>
					<TD><asp:panel id="SystemInformation" runat="server" Visible="False">
							<TABLE class="item" cellSpacing="1" cellPadding="3" width="100%" border="0">
								<TR>
									<TD colSpan="5">
										<HR width="100%" SIZE="1">
										<asp:TextBox id="txtLaptopID" runat="server" Visible="False"></asp:TextBox>
										<asp:Label id="lblModelExist" runat="server" Visible="False" ForeColor="Red">This model already exist!</asp:Label>
										<asp:Label id="lblViewed" runat="server"></asp:Label> Source: 
                                        <asp:Label ID="lblSource" runat="server"></asp:Label>
                                    </TD>
								</TR>
                                <tr>
                                    <td colspan="5">
                                        <asp:Button ID="btnUpdate2" runat="server" OnClick="btnUpdate2_Click" Text="Update" /></td>
                                </tr>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Manufacturer:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:TextBox id="txtManufacture" runat="server"></asp:TextBox>
										<asp:DropDownList id="lstCurrManuf" runat="server" AutoPostBack="True" Visible="False" Width="180px"
                                            onselectedindexchanged="lstCurrManuf_SelectedIndexChanged"></asp:DropDownList>
										<asp:Label id="lblCurrManuf" runat="server" Visible="False">Or</asp:Label>
										<asp:Button id="btnNewManuf" runat="server" Visible="False" Text="Add New" onclick="btnNewManuf_Click"></asp:Button>
										<asp:Label id="lblManufError" runat="server" Visible="False" ForeColor="Red">Please enter manufacturer!</asp:Label><FONT color="red"></FONT></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Product Line:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:TextBox id="txtProductLine" runat="server"></asp:TextBox>
										<asp:DropDownList id="lstCurrLine" runat="server" Visible="False"  Width="180px"></asp:DropDownList>
										<asp:Label id="lblCurrLine" runat="server" Visible="False">Or</asp:Label>
										<asp:Button id="btnNewLine" runat="server" Visible="False" Text="Add New" onclick="btnNewLine_Click"></asp:Button>
										<asp:Label id="lblProductLineError" runat="server" Visible="False" ForeColor="Red">Please enter product line!</asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Model:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:TextBox id="txtModel" runat="server"></asp:TextBox>
										<asp:Label id="lblModelError" runat="server" Visible="False" ForeColor="Red">Please enter a model!</asp:Label>
										<asp:Label id="lblLastUpdate" runat="server"></asp:Label><br />
                                        Please note: "<span style="font-weight: bold; color: red">First word</span>" in the model is the most important in order to idendify
                                        this model. Please put unique model number on the first word. For example, M9969LL/A.</TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Type:</STRONG>&nbsp;
									</TD>
									<TD colSpan="3">
										<asp:RadioButtonList id="radType" runat="server" AutoPostBack="True" RepeatColumns="3" RepeatDirection="Horizontal"
											CssClass="select" onselectedindexchanged="radType_SelectedIndexChanged">
											<asp:ListItem Value="SD">SD</asp:ListItem>
                                            <asp:ListItem>Micro-Dimm SD</asp:ListItem>
											<asp:ListItem Value="SD Long-DIMM">SD Long-DIMM</asp:ListItem>
                                            <asp:ListItem>Proprietary SD</asp:ListItem>
											<asp:ListItem Value="Micro-Dimm">Micro-Dimm</asp:ListItem>
											<asp:ListItem Value="DDR">DDR</asp:ListItem>
											<asp:ListItem Value="DDR Long-DIMM">DDR Long-DIMM</asp:ListItem>
											<asp:ListItem Value="DDR2">DDR2</asp:ListItem>
											<asp:ListItem Value="Micro-Dimm DDR2">Micro-Dimm DDR2</asp:ListItem>
											<asp:ListItem Value="DDR2 Long-DIMM">DDR2 Long-DIMM</asp:ListItem>
                                            <asp:ListItem>Proprietary DDR2</asp:ListItem>
                                            <asp:ListItem>DDR3</asp:ListItem>
                                            <asp:ListItem>Micro-Dimm DDR3</asp:ListItem>
                                            <asp:ListItem>DDR3 Long-DIMM</asp:ListItem>
                                            <asp:ListItem>DDR4</asp:ListItem>
                                            <asp:ListItem>Micro-Dimm DDR4</asp:ListItem>
                                            <asp:ListItem>DDR4 Long-DIMM</asp:ListItem>
                                            <asp:ListItem>DDR5</asp:ListItem>
                                            <asp:ListItem>Micro-Dimm DDR5</asp:ListItem>
                                            <asp:ListItem>DDR5 Long-DIMM</asp:ListItem>
                                            <asp:ListItem>DDR5 CAMM</asp:ListItem>
										</asp:RadioButtonList>
										<asp:Label id="lblTypeError" runat="server" Visible="False" ForeColor="Red">Please select a memory type!</asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Min Speed:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<%--<asp:RadioButtonList id="radSpeed" runat="server" RepeatDirection="Horizontal" CssClass="select">
											<asp:ListItem Value="66">66</asp:ListItem>
											<asp:ListItem Value="100">100</asp:ListItem>
											<asp:ListItem Value="133">133</asp:ListItem>
											<asp:ListItem Value="266">266</asp:ListItem>
											<asp:ListItem Value="333">333</asp:ListItem>
											<asp:ListItem Value="400">400</asp:ListItem>
											<asp:ListItem Value="433">433</asp:ListItem>
											<asp:ListItem Value="466">466</asp:ListItem>
											<asp:ListItem Value="500">500</asp:ListItem>
										</asp:RadioButtonList>--%>
                                        <asp:DropDownList id="radMinSpeed" CssClass="select" Runat="server">
											<asp:ListItem Value="0">0</asp:ListItem>
											<asp:ListItem Value="100">100</asp:ListItem>
											<asp:ListItem Value="133">133</asp:ListItem>
											<asp:ListItem Value="266">266</asp:ListItem>
											<asp:ListItem Value="333">333</asp:ListItem>
											<asp:ListItem Value="400">400</asp:ListItem>
											<asp:ListItem Value="433">433</asp:ListItem>
											<asp:ListItem Value="466">466</asp:ListItem>
											<asp:ListItem Value="500">500</asp:ListItem>
											<asp:ListItem Value="533">533</asp:ListItem>
											<asp:ListItem Value="550">550</asp:ListItem>
											<asp:ListItem Value="566">566</asp:ListItem>
											<asp:ListItem Value="800">800</asp:ListItem>
											<asp:ListItem Value="1066">1066</asp:ListItem>
                                            <asp:ListItem Value="1333">1333</asp:ListItem>
                                            <asp:ListItem Value="1600">1600</asp:ListItem>
                                            <asp:ListItem Value="1866">1866</asp:ListItem>
                                            <asp:ListItem Value="2133">2133</asp:ListItem>
                                            <asp:ListItem Value="2400">2400</asp:ListItem>
                                            <asp:ListItem Value="2666">2666</asp:ListItem>
                                            <asp:ListItem Value="2933">2933</asp:ListItem>
                                            <asp:ListItem Value="3200">3200</asp:ListItem>
                                            <asp:ListItem Value="4800">4800</asp:ListItem>
                                            <asp:ListItem Value="5200">5200</asp:ListItem>
                                            <asp:ListItem Value="5600">5600</asp:ListItem>
                                            <asp:ListItem Value="6000">6000</asp:ListItem>
                                            <asp:ListItem Value="6400">6400</asp:ListItem>
                                            <asp:ListItem Value="6800">6800</asp:ListItem>
                                            <asp:ListItem Value="7200">7200</asp:ListItem>
                                            <asp:ListItem Value="7800">7800</asp:ListItem>
                                            <asp:ListItem Value="8000">8000</asp:ListItem>
										</asp:DropDownList>
										<asp:Label id="lblSpeedError" runat="server" Visible="False" ForeColor="Red">Please select a min memory speed!</asp:Label></TD>
									<TD><FONT color="red">(Required)
											<asp:TextBox id="txtMinSpeed" runat="server" Width="50px" Visible="False" ReadOnly="True"></asp:TextBox></FONT></TD>
								</TR>
								<TR>
									<TD bgColor="#d3d3d3"><STRONG>Max Speed:</STRONG></TD>
									<TD colSpan="3">
                                        <asp:DropDownList id="radMaxSpeed" CssClass="select" Runat="server">
											<asp:ListItem Value="0">0</asp:ListItem>
											<asp:ListItem Value="100">100</asp:ListItem>
											<asp:ListItem Value="133">133</asp:ListItem>
											<asp:ListItem Value="266">266</asp:ListItem>
											<asp:ListItem Value="333">333</asp:ListItem>
											<asp:ListItem Value="400">400</asp:ListItem>
											<asp:ListItem Value="433">433</asp:ListItem>
											<asp:ListItem Value="466">466</asp:ListItem>
											<asp:ListItem Value="500">500</asp:ListItem>
											<asp:ListItem Value="533">533</asp:ListItem>
											<asp:ListItem Value="550">550</asp:ListItem>
											<asp:ListItem Value="566">566</asp:ListItem>
											<asp:ListItem Value="800">800</asp:ListItem>
											<asp:ListItem Value="1066">1066</asp:ListItem>
                                            <asp:ListItem Value="1333">1333</asp:ListItem>
                                            <asp:ListItem Value="1600">1600</asp:ListItem>
                                            <asp:ListItem Value="1866">1866</asp:ListItem>
                                            <asp:ListItem Value="2133">2133</asp:ListItem>
                                            <asp:ListItem Value="2400">2400</asp:ListItem>
                                            <asp:ListItem Value="2666">2666</asp:ListItem>
                                            <asp:ListItem Value="2933">2933</asp:ListItem>
                                            <asp:ListItem Value="3200">3200</asp:ListItem>
                                            <asp:ListItem Value="4800">4800</asp:ListItem>
                                            <asp:ListItem Value="5200">5200</asp:ListItem>
                                            <asp:ListItem Value="5600">5600</asp:ListItem>
                                            <asp:ListItem Value="6000">6000</asp:ListItem>
                                            <asp:ListItem Value="6400">6400</asp:ListItem>
                                            <asp:ListItem Value="6800">6800</asp:ListItem>
                                            <asp:ListItem Value="7200">7200</asp:ListItem>
                                            <asp:ListItem Value="7800">7800</asp:ListItem>
                                            <asp:ListItem Value="8000">8000</asp:ListItem>
										</asp:DropDownList>
										<asp:Label id="lblMaxSpeedError" runat="server" Visible="False" ForeColor="Red">Please select a max memory speed!</asp:Label></TD>
									<TD><FONT color="red">(Required)
											<asp:TextBox id="txtMaxSpeed" runat="server" Width="50px" Visible="False" ReadOnly="True"></asp:TextBox></FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Slot:&nbsp; </STRONG>
									</TD>
									<TD style="HEIGHT: 25px" colSpan="3">
										<asp:RadioButtonList id="radSlot" runat="server" RepeatDirection="Horizontal" CssClass="select">
											<asp:ListItem Value="1">1</asp:ListItem>
											<asp:ListItem Value="2">2</asp:ListItem>
											<asp:ListItem Value="3">3</asp:ListItem>
											<asp:ListItem Value="4">4</asp:ListItem>
										</asp:RadioButtonList>
										<asp:Label id="lblSlotError" runat="server" Visible="False" ForeColor="Red">Please select number of slots!</asp:Label></TD>
									<TD style="HEIGHT: 25px"><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Standard Size:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
                                        <asp:DropDownList ID="drpStandard" runat="server" CssClass="select">
                                            <asp:ListItem Value="0">0</asp:ListItem>
											<asp:ListItem Value="32">32</asp:ListItem>
											<asp:ListItem Value="64">64</asp:ListItem>
											<asp:ListItem Value="128">128</asp:ListItem>
											<asp:ListItem Value="256">256</asp:ListItem>
											<asp:ListItem Value="512">512</asp:ListItem>
											<asp:ListItem Value="1024">1G</asp:ListItem>
											<asp:ListItem Value="2048">2G</asp:ListItem>
											<asp:ListItem Value="3072">3G</asp:ListItem>
											<asp:ListItem Value="4096">4G</asp:ListItem>
											<asp:ListItem Value="5120">5G</asp:ListItem>
											<asp:ListItem Value="6144">6G</asp:ListItem>
                                            <asp:ListItem Value="8192">8G</asp:ListItem>
                                            <asp:ListItem Value="12288">12G</asp:ListItem>
                                            <asp:ListItem Value="16384">16G</asp:ListItem>
                                            <asp:ListItem Value="24576">24G</asp:ListItem>
                                            <asp:ListItem Value="32768">32G</asp:ListItem>
                                        </asp:DropDownList>
										<asp:Label id="lblStandardSizeError" runat="server" Visible="False" ForeColor="Red">Please select onboard standard size!</asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD noWrap bgColor="lightgrey"><STRONG>Standard Removable:</STRONG>&nbsp;
									</TD>
									<TD colSpan="3">
										<asp:CheckBox id="chkRemovable" runat="server" Text="Yes" CssClass="select"></asp:CheckBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Max Total:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:TextBox id="txtMaxTotal" runat="server"></asp:TextBox>
										<asp:Label id="lblMaxTotalError" runat="server" Visible="False" ForeColor="Red">
                                            Please enter valid memory size!</asp:Label>
                                      <asp:DropDownList ID="drpMaxTotal" runat="server" CssClass="select" Width="50px">
                                            <asp:ListItem Value="0"></asp:ListItem>
											<asp:ListItem Value="MB">MB</asp:ListItem>
											<asp:ListItem Value="GB">GB</asp:ListItem>
											<asp:ListItem Value="TB">TB</asp:ListItem>
                                        </asp:DropDownList></TD>
									<TD><FONT color="red">(Required)</FONT>(128, 256, 512, 1024)</TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Min. Per Slot:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:RadioButtonList id="radMinPerSlot" runat="server" RepeatDirection="Horizontal" CssClass="select">
											<asp:ListItem Value="32">32</asp:ListItem>
											<asp:ListItem Value="64">64</asp:ListItem>
											<asp:ListItem Value="128">128</asp:ListItem>
											<asp:ListItem Value="256">256</asp:ListItem>
											<asp:ListItem Value="512">512</asp:ListItem>
											<asp:ListItem Value="1024">1G</asp:ListItem>
											<asp:ListItem Value="2048">2G</asp:ListItem>
											<asp:ListItem Value="4096">4G</asp:ListItem>
											<asp:ListItem Value="8192">8G</asp:ListItem>
                                            <asp:ListItem Value="16384">16G</asp:ListItem>
                                            <asp:ListItem Value="32768">32G</asp:ListItem>
                                            <asp:ListItem Value="65536">64G</asp:ListItem>
                                            <asp:ListItem Value="131072">128G</asp:ListItem>
                                            <asp:ListItem Value="262144">256G</asp:ListItem>
										</asp:RadioButtonList>
										<asp:Label id="lblMinPerSlotError" runat="server" Visible="False" ForeColor="Red">Please select min. memory size per slot!</asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Max. Per Slot:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:RadioButtonList id="radMaxPerSlot" runat="server" RepeatDirection="Horizontal" CssClass="select">
											<asp:ListItem Value="32">32</asp:ListItem>
											<asp:ListItem Value="64">64</asp:ListItem>
											<asp:ListItem Value="128">128</asp:ListItem>
											<asp:ListItem Value="256">256</asp:ListItem>
											<asp:ListItem Value="512">512</asp:ListItem>
											<asp:ListItem Value="1024">1G</asp:ListItem>
											<asp:ListItem Value="2048">2G</asp:ListItem>
											<asp:ListItem Value="4096">4G</asp:ListItem>
											<asp:ListItem Value="8192">8G</asp:ListItem>
                                            <asp:ListItem Value="16384">16G</asp:ListItem>
                                            <asp:ListItem Value="32768">32G</asp:ListItem>
                                            <asp:ListItem Value="65536">64G</asp:ListItem>
                                              <asp:ListItem Value="131072">128G</asp:ListItem>
                                            <asp:ListItem Value="262144">256G</asp:ListItem>
										</asp:RadioButtonList>
										<asp:Label id="lblMaxPerSlotError" runat="server" Visible="False" ForeColor="Red">Please select max. memory size per slot!</asp:Label></TD>
									<TD><FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="#d3d3d3"><STRONG>Dual Channel:</STRONG></TD>
									<TD colSpan="3">
										<asp:CheckBox id="chkDualChannel" runat="server" Text="Yes" CssClass="select"></asp:CheckBox></TD>
									<TD></TD>
								</TR>
                                <tr>
                                    <td bgcolor="#d3d3d3"><STRONG>
                                        Tri. Channel:</STRONG></td>
                                    <td colspan="3">
                                        <asp:CheckBox ID="chkTriChannel" runat="server" Text="Yes" CssClass="select" /></td>
                                    <td>
                                    </td>
                                </tr>
								<TR>
									<TD bgColor="#d3d3d3"><STRONG>Run in Pair:</STRONG></TD>
									<TD colSpan="3">
										<asp:CheckBox id="chkRunInPair" runat="server" Text="Yes" CssClass="select"></asp:CheckBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>USB:</STRONG></TD>
									<TD colSpan="3">
										<asp:RadioButtonList id="radUsb" runat="server" RepeatDirection="Horizontal" CssClass="select">
											<asp:ListItem Value="None">No USB</asp:ListItem>
											<asp:ListItem Value="1.0">USB1.0</asp:ListItem>
											<asp:ListItem Value="2.0" Selected="True">USB2.0</asp:ListItem>
                                            <asp:ListItem Value="3.0">USB3.0</asp:ListItem>
                                            <asp:ListItem Value="TypeC">USB-C</asp:ListItem>
										</asp:RadioButtonList></TD>
									<TD>&nbsp;<FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>SSD type: </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:RadioButtonList id="radFireWire" runat="server" RepeatDirection="Horizontal" CssClass="select">
											<asp:ListItem Value="None" Selected="True">None</asp:ListItem>
											<asp:ListItem Value="SATA">SATA</asp:ListItem>
											<asp:ListItem Value="NVMe">NVMe</asp:ListItem>
										</asp:RadioButtonList></TD>
									<TD>&nbsp;<FONT color="red">(Required)</FONT></TD>
								</TR>
								<TR>
									<TD bgColor="#d3d3d3"><STRONG>Chip Set:</STRONG></TD>
									<TD colSpan="3">
										<asp:TextBox id="txtChipSet" runat="server"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>General Filer:</STRONG></TD>
									<TD colSpan="3">
										<asp:TextBox id="txtFilter" runat="server"></asp:TextBox></TD>
									<TD>&nbsp;</TD>
								</TR>
								<TR style="display:none">
									<TD noWrap bgColor="lightgrey"><STRONG>64 MB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:TextBox id="txt64Op1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="#d3d3d3"><STRONG>Option 2:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:TextBox id="txt64Op2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR style="display:none">
									<TD noWrap bgColor="lightgrey"><STRONG>128 MB Chip Option 1:</STRONG>&nbsp;
									</TD>
									<TD>
										<asp:TextBox id="txt128Op1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="#d3d3d3"><STRONG>Option 2:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:TextBox id="txt128Op2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR style="display:none">
									<TD noWrap bgColor="lightgrey"><STRONG>256 MB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:TextBox id="txt256Op1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="#d3d3d3"><STRONG>Option 2:</STRONG>&nbsp;
									</TD>
									<TD>
										<asp:TextBox id="txt256Op2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR  style="display:none">
									<TD noWrap bgColor="lightgrey"><STRONG>512 MB Chip Option 1:</STRONG>&nbsp;
									</TD>
									<TD>
										<asp:TextBox id="txt512Op1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="#d3d3d3"><STRONG>Option 2:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:TextBox id="txt512Op2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR  style="display:none">
									<TD noWrap bgColor="lightgrey"><STRONG>1 GB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:TextBox id="txt1GOp1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="#d3d3d3"><STRONG>Option 2:</STRONG></TD>
									<TD>
										<asp:TextBox id="txt1GOp2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR  style="display:none">
									<TD noWrap bgColor="lightgrey"><STRONG>2&nbsp;GB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:TextBox id="txt2GOp1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="#d3d3d3"><STRONG>Option 2:&nbsp;</STRONG></TD>
									<TD>
										<asp:TextBox id="txt2GOp2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR  style="display:none">
									<TD noWrap bgColor="#d3d3d3"><STRONG>4 GB Chip Option 1:</STRONG></TD>
									<TD>
										<asp:TextBox id="txt4GOp1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="#d3d3d3"><STRONG>Option 2:</STRONG></TD>
									<TD>
										<asp:TextBox id="txt4GOp2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR  style="display:none">
									<TD noWrap bgColor="#d3d3d3"><STRONG>8 GB Chip Option 1:</STRONG></TD>
									<TD>
										<asp:TextBox id="txt8GOp1" runat="server"></asp:TextBox></TD>
									<TD noWrap bgColor="#d3d3d3"><STRONG>Option 2:</STRONG></TD>
									<TD>
										<asp:TextBox id="txt8GOp2" runat="server"></asp:TextBox></TD>
									<TD>(16x8, 32x8, 64x8, 16x16)</TD>
								</TR>
								<TR>
									<TD bgColor="lightgrey"><STRONG>Notes:</STRONG></TD>
									<TD colSpan="4">
										<asp:TextBox id="txtNotes" runat="server" Columns="70" Rows="6" TextMode="MultiLine"></asp:TextBox></TD>
									<TD></TD>
								</TR>
								<caption>
                                    &lt;&gt;
                                    <tr>
                                        <td bgcolor="#d3d3d3" style="width: 148px"><strong>Installation Guide:</strong></td>
                                        <td colspan="4" style="width: 488px">
                                            <asp:TextBox ID="txtInstallGuide" runat="server" Columns="70" Rows="6" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="lightgrey"><strong>Internal Comment: </strong></td>
                                        <td colspan="4">
                                            <asp:TextBox ID="txtComments" runat="server" Columns="70" Rows="6" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="lightgrey"><strong>Picture:</strong>&nbsp; </td>
                                        <td colspan="3">&nbsp;
                                            <asp:Button ID="btnAddPicture" runat="server" onclick="btnAddPicture_Click" Text="Add Picture" Visible="False" />
                                            <asp:Image ID="imgPicture" runat="server" />
                                            <asp:Button ID="btnDeletePicture" runat="server" onclick="btnDeletePicture_Click" Text="Delete Picture" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="3">
                                            <asp:Button ID="btnUpdate" runat="server" onclick="btnUpdate_Click" Text="Update" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="btnAddNew" runat="server" ForeColor="Navy" onclick="btnAddNew_Click" Text="Add" Visible="False" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="btnCancelUpdate" runat="server" onclick="btnCancelUpdate_Click" Text="Cancel" />
                                            <asp:Button ID="btnCancelAdd" runat="server" ForeColor="Navy" onclick="btnCancelAdd_Click" Text="Cancel" Visible="False" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="btnSaveAs" runat="server" onclick="btnSaveAs_Click" Text="Save As" />
                                            <div style="DISPLAY: none">
                                                <asp:TextBox ID="txtReturnCancel" runat="server"></asp:TextBox>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" Text="Delete" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="3">Data Inserted by:
                                            <asp:Label ID="lblUserInsert" runat="server"></asp:Label>
                                            <br />
                                            Data Updated by:
                                            <asp:Label ID="lblUserUpate" runat="server"></asp:Label>
                                        </td>
                                        <td></td>
                                    </tr>
                                </caption>
                                
							</TABLE>
						</asp:panel></TD>
				</TR>
				<tr>
					<td><asp:panel id="showSystemInfo" runat="server" Visible="False">
							<TABLE class="item" cellSpacing="1" cellPadding="3" width="100%" border="0">
								<TR>
									<TD colSpan="5">
										<HR width="100%" SIZE="1">
									</TD>
								</TR>
								<TR>
									<TD align="right" width="400"><STRONG>Manufacture:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblManufacture" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Product Line:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblProductLine" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Model:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblModel" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Type:</STRONG></TD>
									<TD colSpan="3">
										<asp:Label id="lblType" runat="server"></asp:Label>-RAM</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Min Speed:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblSpeed" runat="server"></asp:Label>&nbsp;MHz</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Max Speed:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblMaxSpeed" runat="server"></asp:Label>&nbsp;MHz</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Slot:&nbsp;</STRONG></TD>
									<TD colSpan="3">
										<asp:Label id="lblSlot" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Standard Size:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblStandard" runat="server"></asp:Label>&nbsp;MB</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Standard Removable:</STRONG>&nbsp;
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblRemovable" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Max Total:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblMaxTotal" runat="server"></asp:Label>&nbsp;MB</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Min. Per Slot:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblMinPerSlot" runat="server"></asp:Label>&nbsp;MB</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Max. Per Slot:&nbsp; </STRONG>
									</TD>
									<TD colSpan="3">
										<asp:Label id="lblMaxPerSlot" runat="server"></asp:Label>&nbsp;MB</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Dual Channel:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblDualChannel" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
                                <tr>
                                    <td align="right"><STRONG>
                                        Tri Channel:</STRONG>&nbsp;
                                    </td>
                                    <td colspan="3">
                                        <asp:Label ID="lblTriChannel" runat="server"></asp:Label></td>
                                    <td>
                                    </td>
                                </tr>
								<TR>
									<TD align="right"><STRONG>Run in Pair:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblRunInPair" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>USB:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblUSB" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>FireWire (IEEE 1394):</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblFireWire" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Chip Set:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblChipSet" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>General Filter:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblFilter" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD noWrap align="right"><STRONG>64 MB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:Label id="lbl64Op1" runat="server"></asp:Label></TD>
									<TD><STRONG>Option 2:</STRONG></TD>
									<TD>
										<asp:Label id="lbl64Op2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD noWrap align="right"><STRONG>128 MB Chip Option 1:</STRONG>&nbsp;
									</TD>
									<TD>
										<asp:Label id="lbl128Op1" runat="server"></asp:Label></TD>
									<TD><STRONG>Option 2</STRONG>:</TD>
									<TD>
										<asp:Label id="lbl128Op2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD noWrap align="right"><STRONG>256 MB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:Label id="lbl256Op1" runat="server"></asp:Label></TD>
									<TD><STRONG>Option 2:</STRONG></TD>
									<TD>
										<asp:Label id="lbl256Op2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD noWrap align="right"><STRONG>512 MB Chip Option 1:</STRONG>&nbsp;
									</TD>
									<TD>
										<asp:Label id="lbl512Op1" runat="server"></asp:Label></TD>
									<TD><STRONG>Option 2:</STRONG></TD>
									<TD>
										<asp:Label id="lbl512Op2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD noWrap align="right"><STRONG>1 GB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:Label id="lbl1GOp1" runat="server"></asp:Label></TD>
									<TD><STRONG>Option 2:</STRONG></TD>
									<TD>
										<asp:Label id="lbl1GOp2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD noWrap align="right"><STRONG>2&nbsp;GB Chip Option 1:&nbsp; </STRONG>
									</TD>
									<TD>
										<asp:Label id="lbl2GOp1" runat="server"></asp:Label></TD>
									<TD><STRONG>Option 2:</STRONG></TD>
									<TD>
										<asp:Label id="lbl2GOp2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD noWrap align="right"><STRONG>4 GB Chip Option 1:&nbsp;</STRONG></TD>
									<TD>
										<asp:Label id="lbl4GOp1" runat="server"></asp:Label></TD>
									<TD><STRONG>Option 2:</STRONG></TD>
									<TD>
										<asp:Label id="lbl4GOp2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD noWrap align="right"><STRONG>8 GB Chip Option 1:</STRONG>&nbsp;</TD>
									<TD>
										<asp:Label id="lbl8GOp1" runat="server"></asp:Label></TD>
									<TD><STRONG>Option 2:</STRONG></TD>
									<TD>
										<asp:Label id="lbl8GOp2" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Notes:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Label id="lblNotes" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
                                <tr>
                                    <td align="right">
                                        <strong>Installation Guide:</strong></td>
                                    <td colspan="3">
                                        <asp:Label id="lblInstallGuide" runat="server"></asp:Label></td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
								<TR>
									<TD align="right"><STRONG>Internal Comments:&nbsp;</STRONG></TD>
									<TD colSpan="3">
										<asp:Label id="lblComments" runat="server"></asp:Label></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"><STRONG>Picture:</STRONG>&nbsp;</TD>
									<TD colSpan="3">
										<asp:Image id="imgShowPicture" runat="server"></asp:Image></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD align="right"></TD>
									<TD colSpan="3">
										<asp:Button id="btnShowUpdate" runat="server" Text="Update" onclick="btnShowUpdate_Click"></asp:Button></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</asp:panel></td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
