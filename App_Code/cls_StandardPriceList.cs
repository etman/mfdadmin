using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for cls_StandardPriceList
/// </summary>
public class cls_StandardPriceList
{
    private string strSku;
    private string strProductId;
    private string strVariantId;
    private string strName;
    private decimal decPrice;
    private int intSpeed;
    private int intSize;
    private string strModuleId;
    private string strBrandName;
    private string strModuleName;
    private string strSpeedName;
    private string strSpeed;
    private string strSizeName;
    private string strMemoryCode;
    private string strTypeCode;
    private string strTableName;
    private string strManufacture;
    private string strProdLine;
    private string strModel;
    private string strMemoryTypeOrg;

	public cls_StandardPriceList()
	{
		//
		// TODO: Add constructor logic here
		//
        strSku = "";
        strProductId = "";
        strVariantId = "";
        strName = "";
        decPrice = 0;
        intSpeed = 0;
        intSize = 0;
        strModuleId = "";
        strBrandName = "";
        strModuleName = "";
        strSpeedName = "";
        strSpeed = "";
        strSizeName = "";
        strMemoryCode = "";
        strTypeCode = "";
        strTableName = "";
        strManufacture = "";
        strProdLine = "";
        strModel = "";
        strMemoryTypeOrg = "";
	}  

    public string SKU
    {
        get
        {
            return strSku;
        }
        set
        {
            strSku = value;
        }
    }

    public string ProductId
    {
        get
        {
            return strProductId;
        }
        set
        {
            strProductId = value;
        }
    }

    public string VariantId
    {
        get
        {
            return strVariantId;
        }
        set
        {
            strVariantId = value;
        }
    }

    public string Name
    {
        get
        {
            return strName;
        }
        set
        {
            strName = value;
        }
    }

    public decimal Price
    {
        get
        {
            return decPrice;
        }
        set
        {
            decPrice = value;
        }
    }

    public int Speed
    {
        get
        {
            return intSpeed;
        }
        set
        {
            intSpeed = value;
        }
    }

    public int Size
    {
        get
        {
            return intSize;
        }
        set
        {
            intSize = value;
        }
    }

    public string ModuleId
    {
        get
        {
            return strModuleId;
        }
        set
        {
            strModuleId = value;
        }
    }

    public string BrandName
    {
        get { return strBrandName; }
        set { strBrandName = value; }
    }

    public string ModuleName
    {
        get { return strModuleName; }
        set { strModuleName = value; }
    }

    public string SpeedName
    {
        get { return strSpeedName; }
        set { strSpeedName = value; }
    }

    public string SpeedString
    {
        get { return strSpeed; }
        set { strSpeed = value; }
    }

    public string SizeName
    {
        get { return strSizeName; }
        set { strSizeName = value; }
    }

    public string MemoryCode
    {
        get { return strMemoryCode; }
        set { strMemoryCode = value; }
    }

    public string MemoryTypeCode
    {
        get { return strTypeCode; }
        set { strTypeCode = value; }
    }

    public string MemoryTypeOrg
    {
        get { return strMemoryTypeOrg; }
        set { strMemoryTypeOrg = value; }
    }

    public string TableName
    {
        get { return strTableName; }
        set { strTableName = value; }
    }

    public string Manufacturer
    {
        get { return strManufacture; }
        set { strManufacture = value; }
    }

    public string ProductLine
    {
        get { return strProdLine; }
        set { strProdLine = value; }
    }
    public string Model
    {
        get { return strModel; }
        set { strModel = value; }
    }
}
