using System;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for SubmitModelSearchEventArgs.
	/// </summary>
	public class SubmitModelSearchEventArgs
	{
		private string strModelId;
		private string strManufacturer;
		private string strProdLine;
		private string strModel;

		public SubmitModelSearchEventArgs()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string ModelId
		{
			get
			{
				return strModelId;
			}
			set
			{
				strModelId = value;
			}
		}

		public string Manufacturer
		{
			get
			{
				return strManufacturer;
			}
			set
			{
				strManufacturer = value;
			}
		}

		public string ProductLine
		{
			get
			{
				return strProdLine;
			}
			set
			{
				strProdLine = value;
			}
		}

		public string Model
		{
			get
			{
				return strModel;
			}
			set
			{
				strModel = value;
			}
		}
	}
}
