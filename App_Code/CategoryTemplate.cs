using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Data;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for CategoryTemplate.
	/// </summary>
	public class CategoryNameTemplate : ITemplate
	{
		ListItemType templateType;
		string strColumnName;
        string strColumnSizeName;
		string strColumnDescr;
		int intFontSize;

		public CategoryNameTemplate(ListItemType type, string colName, string colSize, string colDescr, int fontSize)
		{
			//
			// TODO: Add constructor logic here
			//
			templateType = type;
			strColumnName = colName;
            strColumnSizeName = colSize;
			strColumnDescr = colDescr;
			intFontSize = fontSize;
		}

		public void InstantiateIn(System.Web.UI.Control container)
		{
			Literal litName = new Literal();
			switch (templateType)
			{
				case ListItemType.Header:
					litName.Text = "<strong>Memory Module</strong>";
					container.Controls.Add(litName);
					break;
				case ListItemType.Item:
					litName.DataBinding += new EventHandler(BindLiteralColumn);
					container.Controls.Add(litName);
					break;
				case ListItemType.Footer:
					container.Controls.Add(litName);
					break;
			}
		}

		public void BindLiteralColumn(object sender, EventArgs e)
		{
			Literal litName = (Literal)sender;
			DataGridItem container = (DataGridItem)litName.NamingContainer;

			litName.Text = "<font style=\"font-family: tahoma; font-size: " + intFontSize.ToString() + "pt; font-weight:bold; \">";
            if (strColumnSizeName != "")
                litName.Text += Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strColumnSizeName)) + " ";
			litName.Text += Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strColumnName));
			litName.Text += "</font><br>";
			litName.Text += "<font style=\"font-family: tahoma; font-size: 8pt; font-weight:regular; \">";
			litName.Text += Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strColumnDescr));
			litName.Text += "</font>";
		}
	}

	public class CategorySizeTemplate : ITemplate
	{
		ListItemType templateType;
		string strColumnSize;
		string strColumnSizeName;
		string strColumnCode;

		public CategorySizeTemplate(ListItemType type, string colSize, string colSizeName, string colCode)
		{
			//
			// TODO: Add constructor logic here
			//
			templateType = type;
			strColumnSize = colSize;
			strColumnSizeName = colSizeName;
			strColumnCode = colCode;
		}

		public void InstantiateIn(System.Web.UI.Control container)
		{
			Literal litItem = new Literal();
			switch (templateType)
			{
				case ListItemType.Header:
					litItem.Text = "<strong>Module Size</strong>";
					container.Controls.Add(litItem);
					break;
				case ListItemType.Item:
					HyperLink lnkMemorySize = new HyperLink();
					lnkMemorySize.DataBinding += new EventHandler(BindHyperLinkColumn);
					container.Controls.Add(lnkMemorySize);
					break;
				case ListItemType.Footer:
					container.Controls.Add(litItem);
					break;
			}
		}

		public void BindHyperLinkColumn(object sender, EventArgs e)
		{
			HyperLink lnkMemorySize = (HyperLink)sender;
			DataGridItem container = (DataGridItem)lnkMemorySize.NamingContainer;
			string strVals;

			strVals = "http://www.memory-up.com/merchant2/merchant.mv?screen=SRCH&search=" + 
				Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strColumnCode)) +
				Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strColumnSize));
			lnkMemorySize.NavigateUrl = strVals;
			lnkMemorySize.Text = Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strColumnSizeName));
			lnkMemorySize.Style.Add("font-family", "tahoma");
			lnkMemorySize.Style.Add("font-size", "13pt");
			lnkMemorySize.Style.Add("font-weight", "bold");
		}
	}

    public class CategoryItemDescrTemplate : ITemplate
    {
        ListItemType templateType;
        string strColumnTitle;
        string strColumnName;
        bool isBold;
        int intFontSize;

        public CategoryItemDescrTemplate(ListItemType type, string colTitle, string colName, bool colBold, int fontSize)
        {
            //
            // TODO: Add constructor logic here
            //
            templateType = type;
            strColumnTitle = colTitle;
            strColumnName = colName;
            isBold = colBold;
            intFontSize = fontSize;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            Literal litName = new Literal();
            switch (templateType)
            {
                case ListItemType.Header:
                    litName.Text = "<strong>" + strColumnTitle + "</strong>";
                    container.Controls.Add(litName);
                    break;
                case ListItemType.Item:
                    litName.DataBinding += new EventHandler(BindLiteralColumn);
                    container.Controls.Add(litName);
                    break;
                case ListItemType.Footer:
                    container.Controls.Add(litName);
                    break;
            }
        }

        public void BindLiteralColumn(object sender, EventArgs e)
        {
            Literal litName = (Literal)sender;
            DataGridItem container = (DataGridItem)litName.NamingContainer;

            litName.Text += "<font style=\"font-family: tahoma; font-size: " + intFontSize.ToString() + "pt; ";
            if (isBold)
                litName.Text += "font-weight:bold; \">";
            else
                litName.Text += "font-weight:regular; \">";
            if (strColumnName.ToUpper() == "PRICE")
            {
                litName.Text += "$";
                litName.Text += ((decimal)DataBinder.Eval(((DataGridItem)container).DataItem, strColumnName)).ToString("0.00");
            }
            else
                litName.Text += Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strColumnName));
            litName.Text += "</font>";
        }
    }

    public class CategoryAddToCartTemplate : ITemplate
    {
        ListItemType templateType;
        string strFormId;
        string strProductId;
        string strVariantId;

        public CategoryAddToCartTemplate(ListItemType type, string colFormId, string colProductId, string colVariantId)
        {
            //
            // TODO: Add constructor logic here
            //
            templateType = type;
            strFormId = colFormId;
            strProductId = colProductId;
            strVariantId = colVariantId;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            Literal litName = new Literal();
            switch (templateType)
            {
                case ListItemType.Header:
                    litName.Text = "<strong>Order</strong>";
                    container.Controls.Add(litName);
                    break;
                case ListItemType.Item:
                    litName.DataBinding += new EventHandler(BindLiteralColumn);
                    container.Controls.Add(litName);
                    break;
                case ListItemType.Footer:
                    container.Controls.Add(litName);
                    break;
            }
        }

        public void BindLiteralColumn(object sender, EventArgs e)
        {
            Literal litName = (Literal)sender;
            DataGridItem container = (DataGridItem)litName.NamingContainer;

            litName.Text += "\n";
            litName.Text += "<form style=\"margin-top: 0px; margin-bottom: 0px;\" method=\"POST\" ";
            litName.Text += "id=\"AddToCartForm_";
            litName.Text += Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strProductId)) + "\" ";
            litName.Text += "name=\"AddToCartForm_";
            litName.Text += Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strProductId)) + "\" ";
            litName.Text += "onsubmit='return AddToCartForm_Validator(this)' ";
            litName.Text += "action=\"/checkoutmodel.aspx?returnurl=priceCategory.aspx\">\n";
            litName.Text += "<input type=\"hidden\" name=\"ProductID\" id=\"ProductID\" value=\"";
            litName.Text += Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strProductId)) + "\" />\n";
            litName.Text += "<input type=\"hidden\" name=\"VariantID\" id=\"VariantID\" value=\"";
            litName.Text += Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strVariantId)) + "\" />\n";
            litName.Text += "Quantity: <input type=\"text\" value=\"1\" name=\"Quantity\" id=\"Quantity\" size=\"3\" maxlength=\"4\">\n";
            litName.Text += "<input name=\"Quantity_vldt\" type=\"hidden\" value=\"[req][number][blankalert=Please enter a quantity][invalidalert=Please enter a number for the quantity]\">\n";
            litName.Text += "<input type=\"Image\" align=\"bottom\" src=\"/memory_finders/images/MemorySelector/btnBuyNow.gif\" style=\"cursor: hand; font-weight:bold\" onclick=\"if(AddToCartForm_Validator(document.AddToCartForm_";
            litName.Text += Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strProductId)) + ")) {document.AddToCartForm_";
            litName.Text += Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strProductId)) + ".submit();}\" value=\"Buy\" name=\"Submit\">\n";
            litName.Text += "</form>\n";
        }
    }

	public class CategoryPriceTemplate : ITemplate
	{
		ListItemType templateType;
		string strColumnName;
		string strColumnPrice;
		string strColumnCode;
		string strColumnSize;

		public CategoryPriceTemplate(ListItemType type, string colName, string colPrice, string colCode, string colSize)
		{
			//
			// TODO: Add constructor logic here
			//
			templateType = type;
			strColumnName = colName;
			strColumnPrice = colPrice;
			strColumnCode = colCode;
			strColumnSize = colSize;
		}

		public void InstantiateIn(System.Web.UI.Control container)
		{
			Literal litItem = new Literal();
			switch (templateType)
			{
				case ListItemType.Header:
					litItem.Text = "<strong>Show List</strong>";
					container.Controls.Add(litItem);
					break;
				case ListItemType.Item:
					litItem.DataBinding += new EventHandler(BindHyperLinkColumn);
					container.Controls.Add(litItem);
					break;
				case ListItemType.Footer:
					container.Controls.Add(litItem);
					break;
			}
		}

		public void BindHyperLinkColumn(object sender, EventArgs e)
		{
			Literal litName = (Literal)sender;
			DataGridItem container = (DataGridItem)litName.NamingContainer;

			litName.Text = "<font style=\"font-family: tahoma; font-size: 9pt; \">";
			litName.Text += "<a href='http://www.memory-up.com/merchant2/merchant.mv?screen=SRCH&search=";
			litName.Text += Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strColumnCode));
			litName.Text += Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strColumnSize)) + "'>";
			litName.Text += "<img alt='" + Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strColumnName));
			litName.Text += "' src='http://www.memory-up.com/images/memoryselector/mark.gif' border='0' width='13' height='13'>";
			litName.Text += Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strColumnName));
			litName.Text += " memory start from $ <strong>" + Convert.ToString(DataBinder.Eval(((DataGridItem)container).DataItem, strColumnPrice)) + "</strong></a>";
			litName.Text += "</font>";
		}
	}
}
