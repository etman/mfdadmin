using System;


namespace memoryAdmin
{
	/* CBE Exception Base Class */
	/*
	 * This is the base class for all MupException types.
	 * This class must be inherited from.
	 * */
	public abstract class MupException : Exception
	{
		// Whenever we add a new specific exception type,
		// we must create a constant here
		public const int MUPEX_CODE_GENERIC		= 101;
		public const int MUPEX_CODE_NULLPOINTER	= 102;
		public const int MUPEX_CODE_DB			= 103;

		// Base members to belong to all CBE*Exception types
		protected string	m_strError;
		protected int		m_intErrorCode;
		protected string	m_strFix;
		protected string	m_strMethod;
		protected string	m_strFile;
		protected Exception m_pException;

		// Sets or receives the exception message
		public string MupMessage
		{	
			get{return m_strError;}
			set{m_strError = value;}
		}

		// Sets or receives the exception error code
		// These error codes correspond to the defined
		// codes above.
		public int MupErrorCode
		{
			get{return m_intErrorCode;}
			set{m_intErrorCode = value;}
		}

		// Sets or received the Fix string. This string
		// should provide a solution for the exception
		// if known.
		public string MupFix
		{
			get{return m_strFix;}
			set{m_strFix = value;}
		}

		/*
		 * Constructor 1
		 * Arguments:
		 *		Exception object
		 *		Name of source method
		 *		Name of source file
		 * Purpose:
		 *		To prepare a basic exception message and MupException object.
		 *		This detailed message is taylored based on CBE requirements
		 * */
		public MupException (Exception e, string strMethod, string strFile)
		{
			if (e != null)
			{
				Exception exInner = e.InnerException;

				while (exInner != null)
				{
					m_strError = exInner.Message + "\n" + m_strError;
					exInner = exInner.InnerException;
				}

				m_strError += "\n\n" + e.Message;
#if DEBUG			
				m_strError += "\n\n" + e.StackTrace;
#endif
				m_pException = e;
			}	
			m_strFile = strFile;
			m_strMethod = strMethod;
			m_intErrorCode = MUPEX_CODE_GENERIC;
		}

		/*
		 * Constructor 2
		 * Arguments:
		 *		Exception string
		 *		Name of source method
		 *		Name of source file
		 * Purpose:
		 *		To prepare a basic exception message and MupException object.
		 * */
		public MupException (string strException, string strMethod, string strFile)
		{
			m_strFile = strFile;
			m_strMethod = strMethod;
			m_strError += "\n\n" + strException;
			m_pException = null;
			m_intErrorCode = MUPEX_CODE_GENERIC;
		}
	}

	/*
	 * Class: MupGenericException
	 * Inherits: MupException
	 * Purpose:
	 *		This class handles all un-identified CBE errors.
	 *		This exception type contains "unsure" information about
	 *		the error unless a detailed Exception or string is passed.
	 * */
	public class MupGenericException : memoryAdmin.MupException
	{
		public MupGenericException (string strError, string strMethod, string strFile) :  
			base (strError, strMethod, strFile)
		{

			m_intErrorCode = MUPEX_CODE_DB;	
			m_strFix = "CBE is unable to determine source of error.";
		}
		public MupGenericException (Exception e, string strMethod, string strFile) :  
			base (e, strMethod, strFile)
		{

			m_intErrorCode = MUPEX_CODE_DB;	
			m_strFix = "MUP is unable to determine source of error.";
		}
	}


	/*
	 * Class: MUPDbException
	 * Inherits: MupException
	 * Purpose:
	 *		This class handles all database-related MUP errors.
	 *		This exception should contained detailed error
	 *		information if it was created via a SqlException object.
	 * */
	public class MupDbException : memoryAdmin.MupException
	{
		public MupDbException (string strError, string strMethod, string strFile) :  
			base (strError, strMethod, strFile)
		{

			m_intErrorCode = MUPEX_CODE_DB;	
			m_strFix = "Check the previous database operation.";
		}
		public MupDbException (Exception e, string strMethod, string strFile) :  
			base (e, strMethod, strFile)
		{

			m_intErrorCode = MUPEX_CODE_DB;	
			m_strFix = "Check the previous database operation.";
		}
	}
}
