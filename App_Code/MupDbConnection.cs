using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

	/*
	 * Class MupDbConnection
	 * Purpose:
	 *		This class handles the opening, closing, and error
	 *		handling for Sql database connections in Memory-Up.
	 * */
namespace memoryAdmin
{
	public class MupDbConnection
	{
	
		private SqlConnection	m_dbConnection	= null;
		private SqlDataAdapter	m_dbAdapter		= null;
		private SqlCommand		m_dbCommand		= null;
		private SqlTransaction	m_dbTransaction	= null;

		/**
		 * Create the Connection String for Sql Server
		 * */

		private string strSqlConnectionString = ConfigurationSettings.AppSettings["cnstr"];

		/*
		private const string strSqlConnectionString =
			"Network Library=DBMSSOCN;" +
			"Data Source=WEB_SERVER_1;" +
			"Initial Catalog=MemoryUp;" +
			"User ID=mup;" +
			"Password=sc_mup_sql0103";*/

		/*
		private const string strSqlConnectionString =
			"Data Source=(local);" +
			"User ID=mup;" +
			"pwd=sc_mup_sql0103;" +
			"initial catalog=MemoryUp";*/


		/* Method: StartTransaction
		 * Purpose:dsf
		 *		If we need to monitor a database transaction ,
		 *		we can retrieve a SqlTransaction object via
		 *		this public method */
		public SqlTransaction StartTransaction ()
		{
			if (m_dbConnection != null)
			{
				m_dbTransaction = m_dbConnection.BeginTransaction ();
				return m_dbTransaction;
			}
			else
				return null;
		}

		/*
		 * Constructor
		 * Purpose:
		 *		Create the database connection with error handling
		 * */
		public MupDbConnection()
		{
			// Intialize the database
			try
			{
				// Create the SqlConnection object
				m_dbConnection = new SqlConnection (strSqlConnectionString);

				// Attempt to open the object
				m_dbConnection.Open ();
				
			} 
			catch (SqlException se)
			{
				// Clean up the database object
				m_dbConnection = null;

				// Throw the database exception
				MupDbException ce = 
					new MupDbException (se, "MupDbConnection()", "MupDbConnection.cs");
				throw ce;
			}
			catch (Exception e)
			{
				// Clean up the database object
				m_dbConnection = null;

				// Throw a generic exception, since a SqlException wasn't caught
				MupGenericException ce = 
					new MupGenericException (e, "MupDbConnection()", "MupDbConnection.cs");
				throw ce;
			}
		}

		/*
		 * Destructor
		 * Purpose:
		 *		Close the database with error handling
		 * */
		~MupDbConnection ()
		{
			Dispose ();
		}

		/*
		 * Method: Dispose
		 * Purpose:
		 *		Creates a new dataset object and returns it to the caller.
		 * */
		public void Dispose ()
		{
			if (m_dbConnection != null)
			{
				if (m_dbConnection.State == ConnectionState.Open)
					m_dbConnection.Close ();

				m_dbConnection.Dispose ();
			}
		}

		/*
		 * Method: CreateDataSet
		 * Purpose:
		 *		Creates a new dataset object and returns it to the caller.
		 * */
		public DataSet CreateDataSet ()
		{
			return new DataSet ();
		}


		/*
		 * Method: FillDataSet
		 * Arguments: 
		 *		Query String
		 *		Table Name
		 *		DataSet pointer
		 * Purpose:
		 *		Queries the database using the supplied string, and stores
		 *		the results in the dataset under the table name provided.
		 * */
		public bool FillDataSet (string strQuery, string strTable, ref DataSet ds)
		{
			// If the passed dataset is null, we exit
			if (ds == null)
			{
				MupGenericException ce =
					new MupGenericException ("Supplied dataset is invalid", "FillDataSet()", "MupDbConnection.cs");
			}
			
			try
			{
				// Create the command object
				m_dbCommand = new SqlCommand (strQuery, m_dbConnection);
				// This command is a "text" query
				m_dbCommand.CommandType = CommandType.Text;
				// Create the sql adapter
				m_dbAdapter = new SqlDataAdapter (m_dbCommand);

				// Fill the dataset
				m_dbAdapter.Fill (ds, strTable);

				// Delete the allocated memory
				m_dbAdapter.Dispose ();
				m_dbCommand.Dispose ();
			} 
			catch (SqlException se)
			{
				// Throw a generic exception, since a SqlException wasn't caught
				MupDbException ce = 
					new MupDbException (se, "FillDataSet()", "MupDbConnection.cs");
				throw ce;
			}
			catch (Exception e)
			{
				// Throw a generic exception, since a SqlException wasn't caught
				MupGenericException ce = 
					new MupGenericException (e, "FillDataSet()", "MupDbConnection.cs");
				throw ce;				
			}

			return true;
		}

		public int ExecSql (string strQuery)
		{
			int nResult;

			try
			{
				m_dbCommand = new SqlCommand (strQuery, m_dbConnection);
				m_dbCommand.CommandType = CommandType.Text;

				nResult = m_dbCommand.ExecuteNonQuery ();

				m_dbCommand.Dispose ();
				m_dbCommand = null;

				return nResult;

			}
			catch (SqlException se)
			{
				// Throw a generic exception, since a SqlException wasn't caught
				MupDbException ce = 
					new MupDbException (se, "ExecSql()", "MupDbConnection.cs");
				throw ce;
			}
			catch (Exception e)
			{
				// Throw a generic exception, since a SqlException wasn't caught
				MupGenericException ce = 
					new MupGenericException (e, "ExecSql()", "MupDbConnection.cs");
				throw ce;	
			}
		}

		/*
		 * Method: GetReader
		 * Arguments:
		 *		Query string
		 * Purpose:
		 *		Queries the database using the supplied query and stores
		 *		the results in a new SqlDataReader (returned to the caller)
		 * */
		public SqlDataReader GetReader (string strQuery)
		{
			SqlDataReader dr;
			try
			{
				// Create the command object
				m_dbCommand = new SqlCommand (strQuery, m_dbConnection);
				// Execute the query and return a datareader
				dr = m_dbCommand.ExecuteReader ();

				// Delete the allocated memory
				m_dbCommand.Dispose ();

				// Return the new datareader
				return dr;
			}
			catch (SqlException se)
			{
				// Throw a generic exception, since a SqlException wasn't caught
				MupDbException ce = 
					new MupDbException (se, "GetReader()", "MupDbConnection.cs");
				throw ce;
			}
			catch (Exception e)
			{
				// Throw a generic exception, since a SqlException wasn't caught
				MupGenericException ce = 
					new MupGenericException (e, "GetReader()", "MupDbConnection.cs");
				throw ce;	
			}
		}
	}
}