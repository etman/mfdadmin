using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

namespace memoryAdmin
{
    /// <summary>
    /// Summary description for Mup_utilities.
    /// </summary>
    public class Mup_utilities
    {
        private static string strSqlConnString = ConfigurationSettings.AppSettings["cnstr"];
        private static string strSqlConnStringProd = ConfigurationSettings.AppSettings["prodStr"];

        public static void fillManufacture(DropDownList manufactureList, string strTableName)
        {
            SqlDataReader drManufacture;

            try
            {
                string strSql = "Select distinct manufacture from " + strTableName + " order by manufacture";
                SqlConnection drConn = new SqlConnection(strSqlConnString);
                drConn.Open();
                SqlCommand drComm = new SqlCommand(strSql, drConn);
                drManufacture = drComm.ExecuteReader();

                manufactureList.DataSource = drManufacture;
                manufactureList.DataTextField = "Manufacture";
                manufactureList.DataValueField = "Manufacture";
                manufactureList.DataBind();

                drManufacture.Close();
                drConn.Dispose();
            }
            catch (MupException e)
            {
                throw e;
            }
        }

        public static int GetTopLaptopId()
        {
            int laptopId = 0;
            try
            {
                string strSql = "SELECT laptop_Id FROM LaptopMemory ORDER BY laptop_id DESC";
                SqlConnection drConn = new SqlConnection(strSqlConnString);
                drConn.Open();
                SqlCommand drComm = new SqlCommand(strSql, drConn);
                laptopId = Convert.ToInt32(drComm.ExecuteScalar());
                drComm.Dispose();
                drConn.Close();
            }
            catch(MupException ex)
            {
                throw ex;
            }

            return laptopId + 1;
        }

        public static int GetMemoryScannerId()
        {
            int memoryScannerId = 0;
            try
            {
                string strSql = "SELECT Id FROM MemorySearchToken ORDER BY Id DESC";
                SqlConnection drConn = new SqlConnection(strSqlConnString);
                drConn.Open();
                SqlCommand drComm = new SqlCommand(strSql, drConn);
                memoryScannerId = Convert.ToInt32(drComm.ExecuteScalar());
                drComm.Dispose();
                drConn.Close();
            }
            catch (MupException ex)
            {
                throw ex;
            }

            return memoryScannerId + 1;
        }

        public static int GetTopDesktopId()
        {
            int desktopId = 0;
            try
            {
                string strSql = "SELECT desktop_id FROM DesktopMemory ORDER BY desktop_id DESC";
                SqlConnection drConn = new SqlConnection(strSqlConnString);
                drConn.Open();
                SqlCommand drComm = new SqlCommand(strSql, drConn);
                desktopId = Convert.ToInt32(drComm.ExecuteScalar());
                drComm.Dispose();
                drConn.Close();
            }
            catch (MupException ex)
            {
                throw ex;
            }

            return desktopId + 1;
        }
        public static void fillProductLine(string strManufacture, DropDownList productLineList, string strTableName)
        {
            // SQL command for query manufacture list
            SqlDataReader drProductLine;

            try
            {
                string strSql = "Select distinct product_line from " + strTableName + " ";
                strSql += "Where Manufacture = '" + strManufacture + "' ";
                strSql += "Or Manufacture = '(Select Manufacturer)' order by product_line";
                SqlConnection drConn = new SqlConnection(strSqlConnString);
                drConn.Open();
                SqlCommand drComm = new SqlCommand(strSql, drConn);
                drProductLine = drComm.ExecuteReader();

                productLineList.DataSource = drProductLine;
                productLineList.DataTextField = "Product_line";
                productLineList.DataValueField = "Product_line";
                productLineList.DataBind();

                drProductLine.Close();
                drConn.Dispose();
            }
            catch (MupException e)
            {
                throw e;
            }
        }

        public static void fillModel(string strManufacture, string strProductLine, DropDownList lstModel, string strTableName)
        {
            // SQL command for query manufacture list
            SqlDataReader drModel;

            try
            {
                string strSql = "Select model from " + strTableName + " ";
                strSql += "Where Manufacture = '" + strManufacture + "' ";
                strSql += "And Product_Line = '" + strProductLine + "' ";
                strSql += "Or Manufacture = '(Select Manufacturer)' order by Model";
                SqlConnection drConn = new SqlConnection(strSqlConnString);
                drConn.Open();
                SqlCommand drComm = new SqlCommand(strSql, drConn);
                drModel = drComm.ExecuteReader();

                lstModel.DataSource = drModel;
                lstModel.DataTextField = "model";
                lstModel.DataValueField = "model";
                lstModel.DataBind();

                drModel.Close();
                drConn.Dispose();
            }
            catch (MupException e)
            {
                throw e;
            }
        }

        public static void fillFlashType(CheckBoxList chkMemoryType)
        {
            SqlDataReader drMemory;

            try
            {
                string strSql = "Select code, Description from Memory_Card ";
                strSql += "Where Active = 1 ";
                strSql += "Order by Description ";
                SqlConnection drConn = new SqlConnection(strSqlConnString);
                drConn.Open();
                SqlCommand drComm = new SqlCommand(strSql, drConn);
                drMemory = drComm.ExecuteReader();

                chkMemoryType.DataSource = drMemory;
                chkMemoryType.DataTextField = "Description";
                chkMemoryType.DataValueField = "Code";
                chkMemoryType.DataBind();

                drMemory.Close();
                drConn.Dispose();
            }
            catch (MupException e)
            {
                throw e;
            }
        }

        public static void ShowBatteryItems(DropDownList lstBattery)
        {
            try
            {
                string strSql;
                int tempIndex;
                string tempCode;
                int i = 1;

                strSql = "Select * from Product inner join ProductCategory ON pk_product = fk_product ";
                strSql += "Inner Join Category On pk_category = fk_category ";
                strSql += "Where Category.Name = 'Digital Camera Batteries' ";

                SqlConnection drConn = new SqlConnection(strSqlConnStringProd);
                drConn.Open();
                SqlCommand drComm = new SqlCommand(strSql, drConn);
                SqlDataReader drModel = drComm.ExecuteReader();

                lstBattery.Items.Clear();
                ListItem tempListItem = new ListItem("(Select Battery)", "");
                lstBattery.Items.Add(tempListItem);

                while (drModel.Read())
                {
                    tempIndex = drModel["Link"].ToString().IndexOf("Product_Code");
                    tempCode = drModel["Link"].ToString().Substring(tempIndex + 13).Trim();
                    lstBattery.Items.Add(tempCode);
                    lstBattery.Items[i].Value = drModel["Name"].ToString().Trim();
                    i++;
                }

                drModel.Close();
                drConn.Dispose();
            }
            catch (MupException ex)
            {
                throw ex;
            }
        }

        public static DateTime GetLatestUpdate(string strTableName)
        {
            string strSql;
            DateTime datLatest = new DateTime();

            try
            {
                strSql = "Select top 1 UpdateDate from " + strTableName + " ";
                strSql += "Order by UpdateDate desc";

                SqlConnection drConn = new SqlConnection(strSqlConnString);
                drConn.Open();
                SqlCommand drComm = new SqlCommand(strSql, drConn);
                SqlDataReader drModel = drComm.ExecuteReader();

                if (drModel.Read())
                {
                    if (drModel["UpdateDate"] != DBNull.Value)
                        datLatest = (DateTime)drModel["UpdateDate"];
                }

                drModel.Close();
                drConn.Dispose();
            }
            catch (MupException ex)
            {
                throw ex;
            }

            return datLatest;
        }

        public static void saveMemoryCard(string strType, string strModelId, CheckBoxList chkMemoryType)
        {
            int intData;

            try
            {
                string strSql = "Delete from " + strType + "_memory where " + strType + "_id = " + strModelId + " ";

                SqlConnection dsConn = new SqlConnection(strSqlConnString);
                SqlCommand dsComm = new SqlCommand();
                dsConn.Open();
                dsComm.Connection = dsConn;
                dsComm.CommandText = strSql;

                if (dsComm.ExecuteNonQuery() >= 0)
                {
                    for (int i = 0; i < chkMemoryType.Items.Count; i++)
                    {
                        if (chkMemoryType.Items[i].Selected)
                        {
                            strSql = "Insert into " + strType + "_memory (" + strType + "_id, Memory_code) values (";
                            strSql += strModelId + ", '" + chkMemoryType.Items[i].Value + "') ";

                            dsComm.CommandText = strSql;
                            intData = dsComm.ExecuteNonQuery();
                        }
                    }
                }

                dsConn.Dispose();
            }
            catch (MupException ex)
            {
                throw ex;
            }
        }

        public static void DeleteMemroyCard(string strTable, string strType, string strModelId)
        {
            try
            {
                string strSql = "Delete from " + strTable + " where " + strType + "_id = " + strModelId + " ";

                SqlConnection dsConn = new SqlConnection(strSqlConnString);
                SqlCommand dsComm = new SqlCommand();
                dsConn.Open();
                dsComm.Connection = dsConn;
                dsComm.CommandText = strSql;

                dsComm.ExecuteNonQuery();
                dsConn.Dispose();
            }
            catch (MupException ex)
            {
                throw ex;
            }
        }

        public static ArrayList ListFlashCode()
        {
            string strSql;
            ArrayList aryCode = new ArrayList();

            try
            {
                strSql = "Select distinct code_2 from Memory_card ";

                SqlConnection drConn = new SqlConnection(strSqlConnString);
                drConn.Open();
                SqlCommand drComm = new SqlCommand(strSql, drConn);
                SqlDataReader drModel = drComm.ExecuteReader();

                while (drModel.Read())
                {
                    aryCode.Add(drModel["code_2"].ToString());
                }
            }
            catch (MupException ex)
            {
                throw ex;
            }
            return aryCode;
        }

        public static string LoginCheck(string strUserName, string strPassword)
        {
            string strFullName = "";
            string strSql = "";
            SqlConnection drConn = new SqlConnection(strSqlConnString);

            try
            {
                strSql = "SELECT * FROM MfdUser ";
                strSql += "WHERE LoginName = @LoginName ";
                strSql += "AND LoginPass = @LoginPass ";

                SqlCommand drComm = new SqlCommand(strSql, drConn);
                drComm.Parameters.AddWithValue("@LoginName", strUserName);
                drComm.Parameters.AddWithValue("@LoginPass", strPassword);
                drConn.Open();
                SqlDataReader drMemory = drComm.ExecuteReader();

                if (drMemory.Read())
                {
                    strFullName = drMemory["FullName"].ToString();
                }

                drConn.Close();
                drConn.Dispose();
            }
            catch (Exception ex)
            {
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
            }

            return strFullName;
        }

        public static bool ChangePassword(string strUserName, string strNewPassword)
        {
            string strSql = "";
            bool booSuccess = false;
            SqlConnection drConn = new SqlConnection(strSqlConnString);

            try
            {
                strSql = "UPDATE MfdUser ";
                strSql += "SET LoginPass = @LoginPass ";
                strSql += "WHERE LoginName = @LoginName ";

                SqlCommand drComm = new SqlCommand(strSql, drConn);
                drComm.Parameters.AddWithValue("@LoginName", strUserName);
                drComm.Parameters.AddWithValue("@LoginPass", strNewPassword);
                drConn.Open();
                if (drComm.ExecuteNonQuery() > 0)
                    booSuccess = true;

                drConn.Close();
                drConn.Dispose();
            }
            catch (Exception ex)
            {
                if (drConn != null)
                {
                    drConn.Close();
                    drConn.Dispose();
                }
            }

            return booSuccess;
        }

        public static ArrayList GetStandardPriceList(string strCode, bool isStandard, string strSpecific)
        {
            string strSql;
            string strSqlConnString = ConfigurationSettings.AppSettings["cnstr"];
            ArrayList ListPrice = new ArrayList();
            SqlDataReader drComm;
            decimal decPrice = 0;
            SqlConnection dsConn = new SqlConnection(strSqlConnString);

            try
            {
                if (!isStandard)
                {
                    strSql = "SELECT P.SKU, P.ProductId, P.name, V.VariantId, V.Price, V.SalePrice, 0 AS speed, 0 AS size, '' AS module_id ";
                    strSql += "From AspDotNetStoreFront.dbo.Product P ";
                    strSql += "Inner Join AspDotNetStoreFront.dbo.ProductVariant V ON P.ProductId = V.ProductId ";
                    strSql += "Where P.SKU like @strCode + '%' + @strSpecific + '%' ";
                    strSql += "And P.Deleted <> 1 ";
                    strSql += "ORDER BY V.Price, P.ProductId ";
                }
                else
                {
                    strSql = "Select distinct P.SKU, P.ProductId, P.name, V.VariantId, V.Price, V.SalePrice, S.speed, S.size, S.module_id ";
                    strSql += "From AspDotNetStoreFront.dbo.Product P ";
                    strSql += "Inner Join AspDotNetStoreFront.dbo.ProductVariant V ON P.ProductId = V.ProductId ";
                    strSql += "Inner Join (Select 'M-' + cast(M.speed AS varchar(8)) + substring(M.Type, 2, 3) + '-' + cast(M.size AS varchar(8)) + 'BD' + C.module_id AS std_SKU, ";
                    strSql += "M.speed, M.size, C.module_id ";
                    strSql += "From MemoryUp.dbo.MemoryStandard M ";
                    strSql += "Inner Join MemoryUp.dbo.MemoryConfig C ON C.config_1 = M.config_1 and C.config_2 = M.config_2) AS S ON S.std_SKU = P.SKU ";
                    strSql += "Where P.SKU like @strCode + '%' ";
                    strSql += "And V.IsDefault = 1 ";
                    strSql += "And P.Deleted <> 1 ";
                    strSql += "Order by S.speed, S.size ";
                }


                SqlCommand dsComm = new SqlCommand(strSql, dsConn);
                dsComm.Parameters.AddWithValue("@strCode", strCode);
                if (!isStandard)
                {
                    dsComm.Parameters.AddWithValue("@strSpecific", strSpecific);
                }
                dsConn.Open();
                drComm = dsComm.ExecuteReader();

                while (drComm.Read())
                {
                    cls_StandardPriceList prodList = new cls_StandardPriceList();

                    if (drComm["SalePrice"] != DBNull.Value)
                        decPrice = (decimal)drComm["SalePrice"];
                    else if (drComm["Price"] != DBNull.Value)
                        decPrice = (decimal)drComm["Price"];
                    else
                        decPrice = 0.0M;
                    prodList.Price = decPrice;
                    prodList.SKU = drComm["SKU"].ToString();
                    prodList.ProductId = drComm["ProductId"].ToString();
                    prodList.VariantId = drComm["VariantId"].ToString();
                    prodList.Name = drComm["name"].ToString();
                    prodList.ModuleId = drComm["module_id"].ToString();
                    prodList.Speed = (int)drComm["speed"];
                    prodList.Size = (int)drComm["size"];
                    if (decPrice != 0)
                        ListPrice.Add(prodList);
                }

                dsConn.Close();
                dsConn.Dispose();
            }
            catch (MupException ex)
            {
                if (dsConn != null)
                {
                    dsConn.Close();
                    dsConn.Dispose();
                }
                throw ex;
            }

            return ListPrice;
        }
    }
}
