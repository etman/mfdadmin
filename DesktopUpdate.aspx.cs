using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace memoryAdmin
{
    /// <summary>
    /// Summary description for DesktopUpdate.
    /// </summary>
    public partial class DesktopUpdate : System.Web.UI.Page
	{
		private SqlConnection	m_dbConnection	= null;
		private SqlDataAdapter	m_dbAdapter		= null;
		private SqlCommand		m_dbCommand		= null;

		private int DesktopID;
		private DataSet dsSystem;
		protected System.Web.UI.WebControls.CheckBoxList CheckBoxList1;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            if (Session["LoginName"] == null)
            {
                Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
            }

			if (!Page.IsPostBack)
			{
				btnSaveAs.Attributes.Add("onclick", "openNewModel();");

				ListItem li;

				// The other's shouldn't contain data yet
				li = new ListItem("(Select Manufacturer)","");
				lstManufacture.Items.Add (li);
				lstProductLine.Items.Add (li);
				lstModel.Items.Add (li);

				fillManufacture(lstManufacture);

                drpMaxTotal.SelectedValue = "GB";
                if (Request.Params["modelid"] != null)
                {
                    txtDesktopID.Text = Request.Params["modelid"].ToString();
                    updateSystemInformation();
                }
            }

            lblUser.Text = Session["LoginName"].ToString();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.imgHome.Click += new System.Web.UI.ImageClickEventHandler(this.imgHome_Click);

		}
		#endregion

		protected void lstManufacture_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem li;

			if (lstManufacture.SelectedItem.Text == "(Select Manufacturer)")
			{
				lstProductLine.Items.Clear ();
				lstModel.Items.Clear ();
				li = new ListItem ("(Select Manufacturer)", "");
				lstProductLine.Items.Add(li);
				lstModel.Items.Add (li);
			}
			else
			{
				hdnManufacture.Text = lstManufacture.SelectedItem.ToString();
				// Manufacture was selected, remove the "(Select a State") entry
				if (lstManufacture.Items[0].Value == "")
					lstManufacture.Items.RemoveAt(0);

				// Set the school name and class level entries to "Select a School System"
				lstModel.Items.Clear();
				li = new ListItem ("(Select Product Line)", "");
				lstModel.Items.Add(li);

				// Now populate the school systems
				fillProductLine (lstManufacture.SelectedItem.Value, lstProductLine);
			}
		}

		private void fillManufacture(System.Web.UI.WebControls.DropDownList manufactureList)
		{
			System.Data.SqlClient.SqlDataReader drManufacture;
            MupDbConnection drConn = new MupDbConnection();

			try
			{
				drManufacture = drConn.GetReader("Select distinct manufacture from DesktopMemory order by manufacture");
			
				manufactureList.DataSource = drManufacture;
				manufactureList.DataTextField = "Manufacture";
				manufactureList.DataValueField = "Manufacture";
				manufactureList.DataBind();

				drConn.Dispose();
			}
			catch (MupException e)
			{
                if (drConn != null)
                {
                    drConn.Dispose();
                }
				Response.Write(e.MupMessage);
			}
		}

		protected void lstProductLine_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem li;

			if (lstManufacture.SelectedItem.Text == "(Select Product Line)")
			{
				lstModel.Items.Clear ();
				li = new ListItem ("(Select Product Line)", "");
				lstProductLine.Items.Add(li);
			}
			else
			{
				hdnProductLine.Text = lstProductLine.SelectedItem.ToString();
				// Manufacture was selected, remove the "(Select a State") entry
				if (lstProductLine.Items[0].Value == "")
					lstProductLine.Items.RemoveAt(0);

				// Set the school name and class level entries to "Select a School System"
				li = new ListItem ("(Select Model)", "");

				// Now populate the school systems
				fillModel (lstManufacture.SelectedItem.Value, lstProductLine.SelectedItem.Value);
			}
		}

		private void fillProductLine(string strManufacture, System.Web.UI.WebControls.DropDownList productLineList)
		{
			// SQL command for query manufacture list
			System.Data.SqlClient.SqlDataReader drProductLine;
            MupDbConnection drConn = new MupDbConnection();

			try
			{
				drProductLine = drConn.GetReader("Select distinct Product_line from DesktopMemory " + 
					"where Manufacture = '" + strManufacture + 
					"' or Manufacture = '(Select Manufacturer)' order by Product_line");
			
				productLineList.DataSource = drProductLine;
				productLineList.DataTextField = "Product_line";
				productLineList.DataValueField = "Product_line";
				productLineList.DataBind();

				drConn.Dispose();
			}
			catch (MupException e)
			{
                if (drConn != null)
                {
                    drConn.Dispose();
                }
				Response.Write(e.MupMessage);
			}
		}

		protected void lstModel_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem li;

			showSystemInfo.Visible = false;
			btnUpdate.Visible = true;
			btnAddNew.Visible = false;
			btnCancelUpdate.Visible = true;
			btnCancelAdd.Visible = false;
			btnSaveAs.Visible = true;
			btnDelete.Visible = true;
			lstCurrManuf.Visible = false;
			lstCurrLine.Visible = false;
			lblCurrManuf.Visible = false;
			lblCurrLine.Visible = false;
			btnNewManuf.Visible = false;
			btnNewLine.Visible = false;
			txtManufacture.Visible = true;
			txtProductLine.Visible = true;
            txtDesktopID.Text = "";

			if (lstManufacture.SelectedItem.Text == "(Select Model)")
			{
				SystemInformation.Visible = false;
				li = new ListItem ("(Select Model)", "");
			}
			else
			{
				hdnModel.Text = lstModel.SelectedItem.ToString();
				updateSystemInformation();
			}
		}

		private void fillModel(string strManufacture, string strProductLine)
		{
			// SQL command for query manufacture list
			System.Data.SqlClient.SqlDataReader drModel;
            MupDbConnection drConn = new MupDbConnection();

			try
			{
				drModel = drConn.GetReader("Select model from DesktopMemory " +
					"where (Manufacture = '" + strManufacture +
					"' and product_line = '" + strProductLine + "') " +
					"or Manufacture = '(Select Manufacturer)' order by model");
			
				lstModel.DataSource = drModel;
				lstModel.DataTextField = "model";
				lstModel.DataValueField = "model";
				lstModel.DataBind();

                drConn.Dispose();
			}
			catch(MupException e)
			{
                if (drConn != null)
                {
                    drConn.Dispose();
                }
				Response.Write(e.MupMessage);
			}
		}

		private void updateSystemInformation()
		{
			string strManufacture = hdnManufacture.Text;
			string strProductLine = hdnProductLine.Text;
			string strModel = hdnModel.Text;

			// Validation check invisible
			lblManufError.Visible = false;
			lblProductLineError.Visible = false;
			lblModelError.Visible = false;
			lblMaxTotalError.Visible = false;
            string maxTotalSize = drpMaxTotal.SelectedValue.ToString();

            SystemInformation.Visible = true;
				
			String sql = "Select * from DesktopMemory ";
            if (txtDesktopID.Text != "")
                sql += String.Format("where desktop_id = '{0}'", txtDesktopID.Text);
            else
			    sql += String.Format("where Manufacture = '{0}' and product_line = '{1}' and model = '{2}'",
                    strManufacture, strProductLine, strModel);

			MupDbConnection dsConn = new MupDbConnection();

            try
            {
                dsSystem = dsConn.CreateDataSet();
                dsConn.FillDataSet(sql, "Desktop", ref dsSystem);
                DataTable desktopTable = dsSystem.Tables["Desktop"];

                if (desktopTable != null && desktopTable.Rows.Count > 0)
                {
                    ClearFields();
                    DataRow row0 = desktopTable.Rows[0];

                    txtManufacture.Text = row0["Manufacture"].ToString();
                    txtProductLine.Text = row0["product_line"].ToString();
                    txtModel.Text = row0["model"].ToString();
                    DesktopID = (int)row0["Desktop_id"];
                    if (row0["counter"] != null && row0["counter"].ToString() != "")
                        lblViewed.Text = "Viewed by customers: " + (int)row0["counter"];
                    else
                        lblViewed.Text = "";
                    if (row0["UpdateDate"] != DBNull.Value)
                        lblLastUpdate.Text = "Last Update: " + ((DateTime)row0["UpdateDate"]).ToShortDateString();
                    else
                        lblLastUpdate.Text = "";

                    if (row0["type"].ToString() != "")
                    {
                        for (int i = 0; i < radType.Items.Count; i++)
                        {
                            if (radType.Items[i].Value == row0["type"].ToString())
                                radType.Items[i].Selected = true;
                        }
                        changeSpeedType();
                    }
                    else
                        radType.SelectedIndex = -1;

                    //changeSpeedType();
                    txtMinSpeed.Text = row0["speed_l"].ToString();
                    txtMaxSpeed.Text = row0["speed_h"].ToString();

                    if (row0["speed_l"].ToString() != "")
                    {
                        for (int i = 0; i < radMinSpeed.Items.Count; i++)
                        {
                            if (radMinSpeed.Items[i].Value == row0["speed_l"].ToString())
                                radMinSpeed.Items[i].Selected = true;
                        }
                    }
                    else
                        radMinSpeed.SelectedIndex = -1;

                    if (row0["speed_h"].ToString() != "")
                    {
                        for (int i = 0; i < radMaxSpeed.Items.Count; i++)
                        {
                            if (radMaxSpeed.Items[i].Value == row0["speed_h"].ToString())
                                radMaxSpeed.Items[i].Selected = true;
                        }
                    }
                    else
                        radMaxSpeed.SelectedIndex = -1;

                    if (row0["slot"].ToString() != "")
                    {
                        for (int i = 0; i < radSlot.Items.Count; i++)
                        {
                            if (radSlot.Items[i].Value == row0["slot"].ToString())
                                radSlot.Items[i].Selected = true;
                        }
                    }
                    else
                        radSlot.SelectedIndex = -1;

                    if (row0["standard_size"].ToString() != "")
                    {
                        for (int i = 0; i < drpStandard.Items.Count; i++)
                        {
                            if (drpStandard.Items[i].Value == row0["standard_size"].ToString())
                                drpStandard.Items[i].Selected = true;
                        }
                    }
                    else
                        drpStandard.SelectedIndex = -1;

                    txtMaxTotal.Text = row0["max_total"].ToString();
                    if (maxTotalSize == "TB")
                        txtMaxTotal.Text = (int.Parse(txtMaxTotal.Text) / 1024 / 1024).ToString();
                    else if (maxTotalSize == "GB")
                        txtMaxTotal.Text = (int.Parse(txtMaxTotal.Text) / 1024).ToString();

                    if (row0["min_size"].ToString() != "")
                        {
                            for (int i = 0; i < radMinPerSlot.Items.Count; i++)
                            {
                                if (radMinPerSlot.Items[i].Value == row0["min_size"].ToString())
                                    radMinPerSlot.Items[i].Selected = true;
                            }
                        }
                    else
                        radMinPerSlot.SelectedIndex = -1;

                    if (row0["max_size"].ToString() != "")
                    {
                        for (int i = 0; i < radMaxPerSlot.Items.Count; i++)
                        {
                            if (radMaxPerSlot.Items[i].Value == row0["max_size"].ToString())
                                radMaxPerSlot.Items[i].Selected = true;
                        }
                    }
                    else
                        radMaxPerSlot.SelectedIndex = -1;

                    // Dual Channel
                    if (row0["Dual_ch"].ToString() != "")
                    {
                        if ((bool)row0["Dual_ch"] == true)
                            chkDualChannel.Checked = true;
                        else
                            chkDualChannel.Checked = false;
                    }
                    else
                        chkDualChannel.Checked = false;

                    // Tri Channel
                    if (row0["Tri_ch"].ToString() != "")
                    {
                        if ((bool)row0["Tri_ch"] == true)
                            chkTriChannel.Checked = true;
                        else
                            chkTriChannel.Checked = false;
                    }
                    else
                        chkTriChannel.Checked = false;

                    // Run in Pair
                    if (row0["Run_in_pair"].ToString() != "")
                    {
                        if ((bool)row0["Run_in_pair"] == true)
                            chkRunInPair.Checked = true;
                        else
                            chkRunInPair.Checked = false;
                    }
                    else
                        chkRunInPair.Checked = false;

                    // Server
                    if (row0["Server"].ToString() != "")
                    {
                        if ((bool)row0["Server"] == true)
                            chkServer.Checked = true;
                        else
                            chkServer.Checked = false;
                    }
                    else
                        chkServer.Checked = false;

                    // Non, ECC, Register
                    if (row0["NonECCReg"] != DBNull.Value)
                    {
                        if ((bool)row0["NonECCReg"] == true)
                            chkECC.Items[0].Selected = true;
                        else
                            chkECC.Items[0].Selected = false;
                    }
                    else
                        chkECC.Items[0].Selected = false;

                    if (row0["ECC"] != DBNull.Value)
                    {
                        if ((bool)row0["ECC"] == true)
                            chkECC.Items[1].Selected = true;
                        else
                            chkECC.Items[1].Selected = false;
                    }
                    else
                        chkECC.Items[1].Selected = false;

                    if (row0["Register"] != DBNull.Value)
                    {
                        if ((bool)row0["Register"] == true)
                            chkECC.Items[2].Selected = true;
                        else
                            chkECC.Items[2].Selected = false;
                    }
                    else
                        chkECC.Items[2].Selected = false;

                    if (row0["FullyBuffered"] != DBNull.Value)
                    {
                        if ((bool)row0["FullyBuffered"] == true)
                            chkECC.Items[3].Selected = true;
                        else
                            chkECC.Items[3].Selected = false;
                    }
                    else
                        chkECC.Items[3].Selected = false;

                    if (row0["AppleFB"] != DBNull.Value)
                    {
                        if ((bool)row0["AppleFB"] == true)
                            chkECC.Items[4].Selected = true;
                        else
                            chkECC.Items[4].Selected = false;
                    }
                    else
                        chkECC.Items[4].Selected = false;

                    // If none of ECC check is selected
                    if (chkECC.SelectedIndex == -1)
                        chkECC.SelectedIndex = 0;

                    // USB, SerialATA, FireWire
                    if (row0["USB"].ToString() != "")
                    {
                        for (int i = 0; i < radUsb.Items.Count; i++)
                        {
                            if (radUsb.Items[i].Value == row0["USB"].ToString())
                                radUsb.Items[i].Selected = true;
                        }
                    }
                    if (radUsb.SelectedIndex == -1)
                        radUsb.SelectedIndex = 0;

                    if (row0["serialAta"].ToString() != "")
                    {
                        for (int i = 0; i < radSerialATA.Items.Count; i++)
                        {
                            if (radSerialATA.Items[i].Value == row0["serialAta"].ToString())
                                radSerialATA.Items[i].Selected = true;
                        }
                    }
                    if (radSerialATA.SelectedIndex == -1)
                        radSerialATA.SelectedIndex = 0;

                    if (row0["Fire_wire"].ToString() != "")
                    {
                        for (int i = 0; i < radFireWire.Items.Count; i++)
                        {
                            if (radFireWire.Items[i].Value == row0["Fire_wire"].ToString())
                                radFireWire.Items[i].Selected = true;
                        }
                    }
                    if (radFireWire.SelectedIndex == -1)
                        radFireWire.SelectedIndex = 0;

                    if (!row0["ChipSet"].Equals(null))
                        txtChipSet.Text = row0["ChipSet"].ToString();
                    if (!row0["Filter"].Equals(null))
                        txtFilter.Text = row0["Filter"].ToString();
                    if (!row0["notes"].Equals(null))
                        txtNotes.Text = row0["notes"].ToString();
                    if (!row0["comments"].Equals(null))
                        txtComments.Text = row0["comments"].ToString();
                    if (row0["picture"].ToString().Equals(null) || row0["picture"].ToString() == "")
                    {
                        btnAddPicture.Visible = true;
                        btnDeletePicture.Visible = false;
                        imgPicture.Visible = false;
                    }
                    else
                    {
                        btnAddPicture.Visible = false;
                        btnDeletePicture.Visible = true;
                        imgPicture.Visible = true;

                        imgPicture.ImageUrl = System.IO.Path.Combine(Server.UrlPathEncode("/MfdImages/ModelImages"), row0["picture"].ToString());
                    }

                    lblUserInsert.Text = row0["UserInsert"].ToString();
                    lblUserUpate.Text = row0["UserUpdate"].ToString();

                    DataSet dsOption = new DataSet();
                    sql = "Select * from chipOption where Desktop_id = " + DesktopID + " order by Memory_size";
                    dsOption = dsConn.CreateDataSet();
                    dsConn.FillDataSet(sql, "Option", ref dsOption);
                    foreach (DataRow pRow in dsOption.Tables["Option"].Rows)
                    {
                        if ((int)pRow["Memory_size"] == 64)
                        {
                            txt64Op1.Text = pRow["option1"].ToString();
                            txt64Op2.Text = pRow["option2"].ToString();
                        }
                        else if ((int)pRow["Memory_size"] == 128)
                        {
                            txt128Op1.Text = pRow["option1"].ToString();
                            txt128Op2.Text = pRow["option2"].ToString();
                        }
                        else if ((int)pRow["Memory_size"] == 256)
                        {
                            txt256Op1.Text = pRow["option1"].ToString();
                            txt256Op2.Text = pRow["option2"].ToString();
                        }
                        else if ((int)pRow["Memory_size"] == 512)
                        {
                            txt512Op1.Text = pRow["option1"].ToString();
                            txt512Op2.Text = pRow["option2"].ToString();
                        }
                        else if ((int)pRow["Memory_size"] == 1024)
                        {
                            txt1GOp1.Text = pRow["option1"].ToString();
                            txt1GOp2.Text = pRow["option2"].ToString();
                        }
                        else if ((int)pRow["Memory_size"] == 2048)
                        {
                            txt2GOp1.Text = pRow["option1"].ToString();
                            txt2GOp2.Text = pRow["option2"].ToString();
                        }
                        else if ((int)pRow["Memory_size"] == 4096)
                        {
                            txt4GOp1.Text = pRow["option1"].ToString();
                            txt4GOp2.Text = pRow["option2"].ToString();
                        }
                        else if ((int)pRow["Memory_size"] == 8192)
                        {
                            txt8GOp1.Text = pRow["option1"].ToString();
                            txt8GOp2.Text = pRow["option2"].ToString();
                        }
                        else if ((int)pRow["Memory_size"] == 16384)
                        {
                            txt16GOp1.Text = pRow["option1"].ToString();
                            txt16GOp2.Text = pRow["option2"].ToString();
                        }
                        else if ((int)pRow["Memory_size"] == 32768)
                        {
                            txt32GOp1.Text = pRow["option1"].ToString();
                            txt32GOp2.Text = pRow["option2"].ToString();
                        }
                        else if ((int)pRow["Memory_size"] == 65536)
                        {
                            txt64GOp1.Text = pRow["option1"].ToString();
                            txt64GOp2.Text = pRow["option2"].ToString();
                        }
                    }

                    txtDesktopID.Text = DesktopID.ToString();
                }
                dsConn.Dispose();
            }
            catch (Exception ex)
            {
                if (dsConn != null)
                    dsConn.Dispose();
                
                throw ex;
            }
		}

		protected void btnUpdate_Click(object sender, System.EventArgs e)
		{
			saveUpdate();
		}

		private void saveUpdate()
		{
			String sql;
			MupDbConnection dsConn = new MupDbConnection();

			// Validation check invisible
			lblManufError.Visible = false;
			lblProductLineError.Visible = false;
			lblModelError.Visible = false;
			lblMaxTotalError.Visible = false;

			//Check the required fields
			if (txtManufacture.Text.Trim() == "")
			{
				lblManufError.Visible = true;
				return;
			}
			if (txtProductLine.Text.Trim() == "")
			{
				lblProductLineError.Visible = true;
				return;
			}
			if (txtModel.Text.Trim() == "")
			{
				lblModelError.Visible = true;
				return;
			}

			sql = "Update DesktopMemory set manufacture = '" + txtManufacture.Text.Trim() + "', ";
			sql += "product_line = '" + txtProductLine.Text.Trim() + "', ";

			sql += "model= '" + txtModel.Text.Trim() + "', ";
			sql += "slot = " + radSlot.SelectedItem.Value + ", ";
			sql += "type = '" + radType.SelectedItem.Value.ToString() + "', ";
			sql += "speed_l = '" + radMinSpeed.SelectedItem.Value.ToString() + "', ";
			sql += "speed_h = '" + radMaxSpeed.SelectedItem.Value.ToString() + "', ";

			if (txtMaxTotal.Text == "")
				sql += "0, ";
			else
			{
                int maxTotal = 0;
                if (Regex.IsMatch(txtMaxTotal.Text, "[0-9]+"))
                {
                    if (drpMaxTotal.SelectedValue == "GB")
                    {
                        maxTotal = int.Parse(txtMaxTotal.Text) * 1024;
                    }
                    else if (drpMaxTotal.SelectedValue == "TB")
                        maxTotal = int.Parse(txtMaxTotal.Text) * 1024 * 1024;
                    else
                        maxTotal = int.Parse(txtMaxTotal.Text);
                    sql += "max_total = " + maxTotal + ", ";
                }
                else if (!Regex.IsMatch(txtMaxTotal.Text, "[0-9]+"))
				{
					lblMaxTotalError.Visible = true;
					return;
				}
				else
					sql += "max_total = " + int.Parse(txtMaxTotal.Text) + ", ";
			}

            sql += "standard_size = " + drpStandard.SelectedItem.Value + ", ";
			sql += "min_size = " + radMinPerSlot.SelectedItem.Value + ", ";
			sql += "max_size = " + radMaxPerSlot.SelectedItem.Value + ", ";

			// dual Channel
			if (chkDualChannel.Checked)
				sql += "Dual_ch = 1, ";
			else
				sql += "Dual_ch = 0, ";
            // Tri Channel
            if (chkTriChannel.Checked)
                sql += "Tri_ch = 1, ";
            else
                sql += "Tri_ch = 0, ";

			// Run in Pair
			if (chkRunInPair.Checked)
				sql += "Run_in_Pair = 1, ";
			else
				sql += "Run_in_Pair = 0, ";

            // Server
            if (chkServer.Checked)
                sql += "Server = 1, ";
            else
                sql += "Server = 0, ";

			// Non ECC Register,
			if (chkECC.Items[0].Selected)
				sql += "NonECCReg = 1, ";
			else
				sql += "NonECCReg = 0, ";
			if (chkECC.Items[1].Selected)
				sql += "ECC = 1, ";
			else
				sql += "ECC = 0, ";
			if (chkECC.Items[2].Selected)
				sql += "Register = 1, ";
			else
				sql += "Register = 0, ";
			if (chkECC.Items[3].Selected)
				sql += "FullyBuffered = 1, ";
			else
				sql += "FullyBuffered = 0, ";
            if (chkECC.Items[4].Selected)
                sql += "AppleFB = 1, ";
            else
                sql += "AppleFB = 0, ";

			sql += "USB = '" + radUsb.SelectedValue + "', ";
			sql += "Fire_wire = '" + radFireWire.SelectedValue + "', ";
			sql += "SerialATA = '" + radSerialATA.SelectedValue + "', ";
			sql += "Chipset = '" + txtChipSet.Text.Replace("'", "''") + "', ";
			sql += "Filter = '" + txtFilter.Text.Replace("'", "''") + "', ";
			sql += "Notes = '" + txtNotes.Text.Replace("'", "''") + "', ";
            sql += "InstallGuide = '" + txtInstallGuide.Text.Trim().Replace("'", "''") + "', ";
			sql += "Comments = '" + txtComments.Text.Replace("'", "''") + "', ";
            if (Session["LoginName"] != null)
                sql += "UserUpdate = '" + Session["LoginName"].ToString() + "', ";
            else
                sql += "UserUpdate = '', ";
			sql += "UpdateDate = '" + DateTime.Now + "' ";
			sql += "where Desktop_id = " + txtDesktopID.Text;

            try
            {
                dsConn.ExecSql(sql);
            }
            catch (Exception ex)
            {
                throw ex;
            }

			sql = "Delete from ChipOption where Desktop_id = " + txtDesktopID.Text;
			if (dsConn.ExecSql(sql) >= 0)
			{
				if (txt64Op1.Text != "" || txt64Op2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 64, ";
					sql += "'" + txt64Op1.Text + "', '" + txt64Op2.Text + "') ";
					dsConn.ExecSql(sql);
				}
				if (txt128Op1.Text != "" || txt128Op2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 128, ";
					sql += "'" + txt128Op1.Text + "', '" + txt128Op2.Text + "') ";
					dsConn.ExecSql(sql);
				}
				if (txt256Op1.Text != "" || txt256Op2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 256, ";
					sql += "'" + txt256Op1.Text + "', '" + txt256Op2.Text + "') ";
					dsConn.ExecSql(sql);
				}
				if (txt512Op1.Text != "" || txt512Op2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 512, ";
					sql += "'" + txt512Op1.Text + "', '" + txt512Op2.Text + "') ";
					dsConn.ExecSql(sql);
				}
				if (txt1GOp1.Text != "" || txt1GOp2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 1024, ";
					sql += "'" + txt1GOp1.Text + "', '" + txt1GOp2.Text + "') ";
					dsConn.ExecSql(sql);
				}
				if (txt2GOp1.Text != "" || txt2GOp2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 2048, ";
					sql += "'" + txt2GOp1.Text + "', '" + txt2GOp2.Text + "') ";
					dsConn.ExecSql(sql);
				}
				if (txt4GOp1.Text != "" || txt4GOp2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 4096, ";
					sql += "'" + txt4GOp1.Text + "', '" + txt4GOp2.Text + "') ";
					dsConn.ExecSql(sql);
				}
				if (txt8GOp1.Text != "" || txt8GOp2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 8192, ";
					sql += "'" + txt8GOp1.Text + "', '" + txt8GOp2.Text + "') ";
					dsConn.ExecSql(sql);
				}
                if (txt16GOp1.Text != "" || txt16GOp2.Text != "")
                {
                    sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
                    sql += txtDesktopID.Text + ", 16384, ";
                    sql += "'" + txt16GOp1.Text + "', '" + txt16GOp2.Text + "') ";
                    dsConn.ExecSql(sql);
                }
                if (txt32GOp1.Text != "" || txt32GOp2.Text != "")
                {
                    sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
                    sql += txtDesktopID.Text + ", 32768, ";
                    sql += "'" + txt32GOp1.Text + "', '" + txt32GOp2.Text + "') ";
                    dsConn.ExecSql(sql);
                }
                if (txt64GOp1.Text != "" || txt64GOp2.Text != "")
                {
                    sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
                    sql += txtDesktopID.Text + ", 65536, ";
                    sql += "'" + txt64GOp1.Text + "', '" + txt64GOp2.Text + "') ";
                    dsConn.ExecSql(sql);
                }
			}

			dsConn.Dispose();

			SystemInformation.Visible = false;
			showSystemInfo.Visible = true;
			showSystemInformation();
		}

        private void ClearFields()
        {
            lblSource.Text = "";
            txtManufacture.Text = "";
            txtProductLine.Text = "";
            txtModel.Text = "";
            lblLastUpdate.Text = "";
            lblViewed.Text = "";
            radType.ClearSelection();
            radMinSpeed.ClearSelection();
            radMaxSpeed.ClearSelection();
            radSlot.ClearSelection();
            drpStandard.ClearSelection();
            txtMaxTotal.Text = "";
            radMinPerSlot.ClearSelection();
            radMaxPerSlot.ClearSelection();
            chkDualChannel.Checked = false;
            chkRunInPair.Checked = false;
            chkECC.ClearSelection();
            radUsb.ClearSelection();
            radFireWire.ClearSelection();
            radSerialATA.ClearSelection();
            txtChipSet.Text = "";
            txtFilter.Text = "";
            txt64Op1.Text = "";
            txt64Op2.Text = "";
            txt128Op1.Text = "";
            txt128Op2.Text = "";
            txt256Op1.Text = "";
            txt256Op2.Text = "";
            txt512Op1.Text = "";
            txt512Op2.Text = "";
            txt1GOp1.Text = "";
            txt1GOp2.Text = "";
            txt2GOp1.Text = "";
            txt2GOp2.Text = "";
            txt4GOp1.Text = "";
            txt4GOp2.Text = "";
            txt8GOp1.Text = "";
            txt8GOp2.Text = "";
            txt16GOp1.Text = "";
            txt16GOp2.Text = "";
            txt32GOp1.Text = "";
            txt32GOp2.Text = "";
            txt64GOp1.Text = "";
            txt64GOp2.Text = "";
            txtNotes.Text = "";
            txtComments.Text = "";
        }

		protected void btnAddModel_Click(object sender, System.EventArgs e)
		{
			selector.Visible = false;
			SystemInformation.Visible = true;
			lblModelExist.Visible = false;

			// Validation check invisible
			lblManufError.Visible = false;
			lblProductLineError.Visible = false;
			lblModelError.Visible = false;
			lblMaxTotalError.Visible = false;

			txtManufacture.Visible = false;
			txtProductLine.Visible = false;
			lstCurrManuf.Visible = true;
			lstCurrLine.Visible = true;
			lblCurrManuf.Visible = true;
			lblCurrLine.Visible = true;
			btnNewManuf.Visible = true;
			btnNewLine.Visible = true;
			btnUpdate.Visible = false;
			btnCancelUpdate.Visible = false;
			btnSaveAs.Visible = false;
			btnDelete.Visible = false;
			btnAddNew.Visible = true;
			btnCancelAdd.Visible = true;
			btnAddPicture.Visible = false;
			btnDeletePicture.Visible = false;
			imgPicture.Visible = false;

            ClearFields();

			if (Page.IsPostBack)
			{
				ListItem li;

				lstCurrManuf.Items.Clear();
				lstCurrLine.Items.Clear();

				li = new ListItem("(Select Manufacturer)","");
				lstCurrManuf.Items.Add (li);
				lstCurrLine.Items.Add (li);

				fillManufacture(lstCurrManuf);
			}

			showSystemInfo.Visible = false;
		}

		protected void lstCurrManuf_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem li;

			if (lstCurrManuf.SelectedItem.Text == "(Select Manufacturer)")
			{
				lstCurrLine.Items.Clear ();
				li = new ListItem ("(Select Manufacturer)", "");
				lstCurrLine.Items.Add(li);
			}
			else
			{
				// Manufacture was selected, remove the "(Select a anufacturer") entry
				if (lstCurrManuf.Items[0].Value == "")
					lstCurrManuf.Items.RemoveAt(0);

				// Now populate the school systems
				fillProductLine (lstCurrManuf.SelectedItem.Value, lstCurrLine);
			}
		}

		protected void btnNewManuf_Click(object sender, System.EventArgs e)
		{
			lstCurrManuf.Visible = false;
			lstCurrLine.Visible = false;
			btnNewManuf.Visible = false;
			btnNewLine.Visible = false;
			lblCurrManuf.Visible = false;
			lblCurrLine.Visible = false;
			txtManufacture.Visible = true;
			txtProductLine.Visible = true;
		}

		protected void btnNewLine_Click(object sender, System.EventArgs e)
		{
			lstCurrLine.Visible = false;
			btnNewLine.Visible = false;
			lblCurrLine.Visible = false;
			txtProductLine.Visible = true;
		}

		protected void btnAddNew_Click(object sender, System.EventArgs e)
		{
            int isRequiredError = 0;
            if (lstCurrManuf.SelectedValue == "(Select Manufacturer)")
            {
                lblManufacture.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblManufacture.Visible = false;
            }

            if (lstCurrLine.SelectedValue == "(Select Manufacturer)")
            {
                lblProductLine.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblProductLine.Visible = false;
            }

            if (string.IsNullOrEmpty(txtModel.Text))
            {
                lblModelError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblModelError.Visible = false;
            }

            if (radType.SelectedIndex <= -1)
            {
                lblTypeError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblTypeError.Visible = false;
            }

            if (radMinSpeed.SelectedValue == "0")
            {
                lblMinSpeedError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblMinSpeedError.Visible = false;
            }

            if (radMaxSpeed.SelectedValue == "0")
            {
                lblMaxSpeedError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblMaxSpeedError.Visible = false;
            }

            if (radSlot.SelectedIndex <= -1)
            {
                lblSlotError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblSlotError.Visible = false;
            }

            if (string.IsNullOrEmpty(txtMaxTotal.Text))
            {
                lblMaxTotal.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblMaxTotalError.Visible = false;
            }

            if (radMinPerSlot.SelectedIndex <= -1)
            {
                lblMinPerSlotError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblMinPerSlotError.Visible = false;
            }

            if (radMaxPerSlot.SelectedIndex <= -1)
            {
                lblMaxPerSlotError.Visible = true;
                isRequiredError++;
            }
            else
            {
                lblMaxPerSlotError.Visible = false;
            }

            if (isRequiredError == 0)
            {
                addNewModel();
                showSystemInformation();
            }
        }

		private void addNewModel()
		{
			String sql;
			System.Data.SqlClient.SqlDataReader dr;
			System.Data.SqlClient.SqlDataReader drExistModel;
			MupDbConnection dsConn = new MupDbConnection();
			MupDbConnection dsConnOption = new MupDbConnection();

			// Validation check invisible
			lblManufError.Visible = false;
			lblProductLineError.Visible = false;
			lblModelError.Visible = false;
			lblMaxTotalError.Visible = false;

			//Check the required fields
			if (txtManufacture.Visible == true)
			{
				if (txtManufacture.Text.Trim() == "")
				{
					lblManufError.Visible = true;
					return;
				}
			}
			else
			{
				if (lstCurrManuf.SelectedIndex == 0)
				{
					lblManufError.Visible = true;
					return;
				}
			}
			if (txtProductLine.Visible == true)
			{
				if (txtProductLine.Text.Trim() == "")
				{
					lblProductLineError.Visible = true;
					return;
				}
			}
			else
			{
				if (lstCurrLine.SelectedIndex == 0)
				{
					lblProductLineError.Visible = true;
					return;
				}
			}
			if (txtModel.Text.Trim() == "")
			{
				lblModelError.Visible = true;
				return;
			}

			// Check if this model already exist
			if (txtManufacture.Visible == true)
				sql = "SELECT Desktop_id FROM DesktopMemory where manufacture = '" + txtManufacture.Text.Trim() + "' ";
			else
				sql = "SELECT Desktop_id FROM DesktopMemory where manufacture = '" + lstCurrManuf.SelectedItem.Value + "' ";
			if (txtProductLine.Visible == true)
				sql += "and product_line = '" + txtProductLine.Text.Trim() + "' ";
			else
				sql += "and product_line = '" + lstCurrLine.SelectedItem.Value + "' ";
			sql += "and model = '" + txtModel.Text.Trim() + "' ";

			drExistModel = dsConn.GetReader(sql);
			if (drExistModel.Read())
			{
				// Show message box to tell user this model has exist
				lblModelExist.Visible = true;
				dsConn.Dispose();
				return;
			}
			drExistModel.Close();

			// Insert new model to database
			sql = "Insert into DesktopMemory (desktop_id,manufacture, product_line, model, type, speed_l, speed_h, slot, " + 
				"standard_size, max_total, min_size, max_size, Dual_ch, Tri_ch, Run_in_pair, Server, NonECCReg, ECC, Register, FullyBuffered, AppleFB, Chipset, USB, Fire_Wire, serialATA, filter, Notes, InstallGuide, Comments, " +
                "UpdateDate, UserInsert ) values ( ";
            sql += Mup_utilities.GetTopDesktopId() + ", ";
			if (txtManufacture.Visible == true)
				sql += "'" + txtManufacture.Text.Trim() + "', ";
			else
				sql += "'" + lstCurrManuf.SelectedItem.Value + "', ";

			if (txtProductLine.Visible == true)
				sql += "'" + txtProductLine.Text.Trim() + "', ";
			else
				sql += "'" + lstCurrLine.SelectedItem.Value + "', ";

			sql += "'" + txtModel.Text.Trim() + "', '" + radType.SelectedItem.Value + "', ";
			sql += "'" + radMinSpeed.SelectedItem.Value + "', '" + radMaxSpeed.SelectedItem.Value + "', " + radSlot.SelectedItem.Value + ", ";
            sql += drpStandard.SelectedItem.Value + ", ";

			if (txtMaxTotal.Text == "")
				sql += "0, ";
			else
			{
                int maxTotal = 0;
                if (Regex.IsMatch(txtMaxTotal.Text, "[0-9]+"))
                {
                    if (drpMaxTotal.SelectedValue == "GB")
                    {
                        maxTotal = int.Parse(txtMaxTotal.Text) * 1024;
                    }
                    else if (drpMaxTotal.SelectedValue == "TB")
                        maxTotal = int.Parse(txtMaxTotal.Text) * 1024 * 1024;
                    else
                        maxTotal = int.Parse(txtMaxTotal.Text);
                    sql += maxTotal + ", ";
                }
                else
				{
					lblMaxTotalError.Visible = true;
					return;
				}
			}

			sql += radMinPerSlot.SelectedItem.Value + ", ";
			sql += radMaxPerSlot.SelectedItem.Value + ", ";

			if (chkDualChannel.Checked)
				sql += "1, ";
			else
				sql += "0, ";

            // Tri channel
            if (chkTriChannel.Checked)
                sql += "1, ";
            else
                sql += "0, ";

			// Run in pair
			if (chkRunInPair.Checked)
				sql += "1, ";
			else
				sql += "0, ";

            // Server
            if (chkServer.Checked)
                sql += "1, ";
            else
                sql += "0, ";

			// Non, ECC, Register
			if (chkECC.Items[0].Selected)
				sql += "1, ";
			else
				sql += "0, ";
			if (chkECC.Items[1].Selected)
				sql += "1, ";
			else
				sql += "0, ";
			if (chkECC.Items[2].Selected)
				sql += "1, ";
			else
				sql += "0, ";
			if (chkECC.Items[3].Selected)
				sql += "1, ";
			else
				sql += "0, ";
            if (chkECC.Items[4].Selected)
                sql += "1, ";
            else
                sql += "0, ";

			sql += "'" + txtChipSet.Text.Replace("'", "''") + "', ";
			sql += "'" + radUsb.SelectedItem.Value + "', ";
			sql += "'" + radFireWire.SelectedItem.Value + "', ";
			sql += "'" + radSerialATA.SelectedItem.Value + "', ";
			sql += "'" + txtFilter.Text.Replace("'", "''") + "', ";
			sql += "'" + txtNotes.Text.Replace("'", "''") + "', ";
            sql += "'" + txtInstallGuide.Text.Trim().Replace("'", "''") + "', ";
			sql += "'" + txtComments.Text.Replace("'", "''") + "', ";
			sql += "'" + DateTime.Now + "', ";
            sql += "'" + Session["LoginName"].ToString() + "') ";

			// Show system information
			if (dsConn.ExecSql(sql) >= 0)
			{
				if (txtManufacture.Visible == true)
					sql = "SELECT Desktop_id FROM DesktopMemory where manufacture = '" + txtManufacture.Text.Trim() + "' ";
				else
					sql = "SELECT Desktop_id FROM DesktopMemory where manufacture = '" + lstCurrManuf.SelectedItem.Value + "' ";
				if (txtProductLine.Visible == true)
					sql += "and product_line = '" + txtProductLine.Text.Trim() + "' ";
				else
					sql += "and product_line = '" + lstCurrLine.SelectedItem.Value + "' ";
				sql += "and model = '" + txtModel.Text.Trim() + "' ";
				dr = dsConn.GetReader(sql);

				if (dr == null || !dr.Read ())
				{
					dsConn.Dispose ();
					MupGenericException ex = new MupGenericException ("Registration Error",
						"Register_Click ()", "CBERegistration.ascx.cs");
					throw ex;
				}

				string DesktopID = dr["Desktop_id"].ToString ();
				txtDesktopID.Text = DesktopID;

				// insert chipOption SQL
				if (txt64Op1.Text != "" || txt64Op2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += DesktopID + ", 64, ";
					sql += "'" + txt64Op1.Text + "', '" + txt64Op2.Text + "') ";
					dsConnOption.ExecSql(sql);
				}
				if (txt128Op1.Text != "" || txt128Op2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 128, ";
					sql += "'" + txt128Op1.Text + "', '" + txt128Op2.Text + "') ";
					dsConnOption.ExecSql(sql);
				}
				if (txt256Op1.Text != "" || txt256Op2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 256, ";
					sql += "'" + txt256Op1.Text + "', '" + txt256Op2.Text + "') ";
					dsConnOption.ExecSql(sql);
				}
				if (txt512Op1.Text != "" || txt512Op2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 512, ";
					sql += "'" + txt512Op1.Text + "', '" + txt512Op2.Text + "') ";
					dsConnOption.ExecSql(sql);
				}
				if (txt1GOp1.Text != "" || txt1GOp2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 1024, ";
					sql += "'" + txt1GOp1.Text + "', '" + txt1GOp2.Text + "') ";
					dsConnOption.ExecSql(sql);
				}
				if (txt2GOp1.Text != "" || txt2GOp2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 2048, ";
					sql += "'" + txt2GOp1.Text + "', '" + txt2GOp2.Text + "') ";
					dsConnOption.ExecSql(sql);
				}
				if (txt4GOp1.Text != "" || txt4GOp2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 4096, ";
					sql += "'" + txt4GOp1.Text + "', '" + txt4GOp2.Text + "') ";
					dsConnOption.ExecSql(sql);
				}
				if (txt8GOp1.Text != "" || txt8GOp2.Text != "")
				{
					sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
					sql += txtDesktopID.Text + ", 8192, ";
					sql += "'" + txt8GOp1.Text + "', '" + txt8GOp2.Text + "') ";
					dsConnOption.ExecSql(sql);
				}
                if (txt16GOp1.Text != "" || txt16GOp2.Text != "")
                {
                    sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
                    sql += txtDesktopID.Text + ", 16384, ";
                    sql += "'" + txt16GOp1.Text + "', '" + txt16GOp2.Text + "') ";
                    dsConnOption.ExecSql(sql);
                }
                if (txt32GOp1.Text != "" || txt32GOp2.Text != "")
                {
                    sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
                    sql += txtDesktopID.Text + ", 32768, ";
                    sql += "'" + txt32GOp1.Text + "', '" + txt32GOp2.Text + "') ";
                    dsConnOption.ExecSql(sql);
                }
                if (txt64GOp1.Text != "" || txt64GOp2.Text != "")
                {
                    sql = "Insert into ChipOption (Desktop_id, Memory_size, option1, option2) values ( ";
                    sql += txtDesktopID.Text + ", 65536, ";
                    sql += "'" + txt64GOp1.Text + "', '" + txt64GOp2.Text + "') ";
                    dsConnOption.ExecSql(sql);
                }

				dr.Close();
			}
			else
			{
				// If the query failed, throw an exception
				MupGenericException ex = new MupGenericException ("Unable to execute Sql statement", 
					"Register_Click()", "Registration.ascx.cs");
				throw ex;
			}
			dsConn.Dispose();
			dsConnOption.Dispose(); 

			SystemInformation.Visible = false;
			selector.Visible = true;
			showSystemInfo.Visible = true;

			//clear select system panel drop down box
			ListItem li;

			lstManufacture.Items.Clear();
			lstProductLine.Items.Clear();
			lstModel.Items.Clear();
				
			li = new ListItem("(Select Manufacturer)","");
			lstManufacture.Items.Add (li);
			lstProductLine.Items.Add (li);
			lstModel.Items.Add (li);

			fillManufacture(lstManufacture);
		}

		private void showSystemInformation()
		{
			System.Data.SqlClient.SqlDataReader dr;
			System.Data.SqlClient.SqlDataReader drOption;
			MupDbConnection dbConn = new MupDbConnection();
			MupDbConnection dbConnOption = new MupDbConnection();

			string sql = "SELECT * FROM DesktopMemory where Desktop_id = " + txtDesktopID.Text;
			dr = dbConn.GetReader(sql);

			if (dr == null || !dr.Read ())
			{
				dbConn.Dispose ();
				MupGenericException ex = new MupGenericException ("Registration Error",
					"Register_Click ()", "CBERegistration.ascx.cs");
				throw ex;
			}

            lblSource.Text = dr["Source"].ToString();
			lblModelExist.Visible = false;
			lblManufacture.Text = dr["manufacture"].ToString();
			hdnManufacture.Text = dr["manufacture"].ToString();
			lblProductLine.Text = dr["product_line"].ToString();
			hdnProductLine.Text = dr["product_line"].ToString();
			lblModel.Text = dr["model"].ToString();
			hdnModel.Text = dr["model"].ToString();
			lblType.Text = dr["type"].ToString();
			lblMinSpeed.Text = dr["speed_l"].ToString();
			lblMaxSpeed.Text = dr["speed_h"].ToString();
			lblSlot.Text = dr["slot"].ToString();
			lblMaxTotal.Text = dr["max_total"].ToString();
			lblMinPerSlot.Text = dr["min_size"].ToString();
			lblMaxPerSlot.Text = dr["max_size"].ToString();
            if (dr["Dual_ch"].ToString() != "")
            {
                if ((bool)dr["Dual_ch"] == true)
                    lblDualChannel.Text = "Yes";
                else
                    lblDualChannel.Text = "No";
            }
            else
                lblDualChannel.Text = "No";
			if (dr["Tri_ch"].ToString() != "")
			{
				if ((bool)dr["Tri_ch"] == true)
					lblTriChannel.Text = "Yes";
				else
                    lblTriChannel.Text = "No";
			}
            else
                lblTriChannel.Text = "No";
			if (dr["Run_in_pair"].ToString() != "")
			{
				if ((bool)dr["Run_in_pair"] == true)
					lblRunInPair.Text = "Yes";
				else
					lblRunInPair.Text = "No";
			}
            else
                lblRunInPair.Text = "No";
            if (dr["Server"].ToString() != "")
            {
                if ((bool)dr["Server"] == true)
                    lblServer.Text = "Yes";
                else
                    lblServer.Text = "No";
            }
            else
                lblServer.Text = "No";
			// Non ECC Registerd
			lblECC.Text = "";
			if ((bool)dr["NonECCReg"] == true)
				lblECC.Text = "Non-ECC Non-Registered, ";
			if ((bool)dr["ECC"] == true)
				lblECC.Text = lblECC.Text + "ECC Non-Registered, ";
			if ((bool)dr["Register"] == true)
				lblECC.Text = lblECC.Text + "ECC Registered, ";
			if ((bool)dr["FullyBuffered"] == true)
				lblECC.Text = lblECC.Text + "Fully Buffered ";
			lblChipSet.Text = dr["Chipset"].ToString();
			lblFilter.Text = dr["filter"].ToString();
			lblUSB.Text = dr["USB"].ToString();
			lblFireWire.Text = "FireWire " + dr["Fire_wire"].ToString();
			if (dr["SerialATA"].ToString() == "1.0")
				lblSerialATA.Text = "Serial ATA 1.0";
			else if (dr["SerialATA"].ToString() == "2.0")
				lblSerialATA.Text = "Serial ATA II";
			else
				lblSerialATA.Text = "None";
			lblNotes.Text = dr["Notes"].ToString();
            lblInstallGuide.Text = dr["InstallGuide"].ToString();
			lblComments.Text = dr["Comments"].ToString();

			int temp;
			sql = "Select * from ChipOption where Desktop_id = " + txtDesktopID.Text + " order by Memory_size";
			drOption = dbConnOption.GetReader(sql);

            lbl64Op1.Text = "";
            lbl64Op2.Text = "";
            lbl128Op1.Text = "";
            lbl128Op2.Text = "";
            lbl256Op1.Text = "";
            lbl256Op2.Text = "";
            lbl512Op1.Text = "";
            lbl512Op2.Text = "";
            lbl1GOp1.Text = "";
            lbl1GOp2.Text = "";
            lbl2GOp1.Text = "";
            lbl2GOp2.Text = "";
            lbl4GOp1.Text = "";
            lbl4GOp2.Text = "";
            lbl8GOp1.Text = "";
            lbl8GOp2.Text = "";
            lbl16GOp1.Text = "";
            lbl16GOp2.Text = "";
            lbl32GOp1.Text = "";
            lbl32GOp2.Text = "";
            lbl64GOp1.Text = "";
            lbl64GOp2.Text = "";

			while (drOption.Read())
			{
				temp = (int)drOption["Memory_size"];
				if ((int)drOption["Memory_size"] == 64)
				{
					lbl64Op1.Text = drOption["option1"].ToString();
					lbl64Op2.Text = drOption["option2"].ToString();
				}
				else if ((int)drOption["Memory_size"] == 128)
				{
					lbl128Op1.Text = drOption["option1"].ToString();
					lbl128Op2.Text = drOption["option2"].ToString();
				}
				else if ((int)drOption["Memory_size"] == 256)
				{
					lbl256Op1.Text = drOption["option1"].ToString();
					lbl256Op2.Text = drOption["option2"].ToString();
				}
				else if ((int)drOption["Memory_size"] == 512)
				{
					lbl512Op1.Text = drOption["option1"].ToString();
					lbl512Op2.Text = drOption["option2"].ToString();
				}
				else if ((int)drOption["Memory_size"] == 1024)
				{
					lbl1GOp1.Text = drOption["option1"].ToString();
					lbl1GOp2.Text = drOption["option2"].ToString();
				}
				else if ((int)drOption["Memory_size"] == 2048)
				{
					lbl2GOp1.Text = drOption["option1"].ToString();
					lbl2GOp2.Text = drOption["option2"].ToString();
				}
				else if ((int)drOption["Memory_size"] == 4096)
				{
					lbl4GOp1.Text = drOption["option1"].ToString();
					lbl4GOp2.Text = drOption["option2"].ToString();
				}
				else if ((int)drOption["Memory_size"] == 8192)
				{
					lbl8GOp1.Text = drOption["option1"].ToString();
					lbl8GOp2.Text = drOption["option2"].ToString();
				}
                else if ((int)drOption["Memory_size"] == 16384)
                {
                    lbl16GOp1.Text = drOption["option1"].ToString();
                    lbl16GOp2.Text = drOption["option2"].ToString();
                }
                else if ((int)drOption["Memory_size"] == 32768)
                {
                    lbl32GOp1.Text = drOption["option1"].ToString();
                    lbl32GOp2.Text = drOption["option2"].ToString();
                }
                else if ((int)drOption["Memory_size"] == 65536)
                {
                    lbl64GOp1.Text = drOption["option1"].ToString();
                    lbl64GOp2.Text = drOption["option2"].ToString();
                }
			} 
			
			imgShowPicture.ImageUrl = System.IO.Path.Combine(Server.UrlPathEncode("/MfdImages/ModelImages"), dr["picture"].ToString());

			dr.Close();
			drOption.Close();
			dbConn.Dispose();
			dbConnOption.Dispose();
		}

		protected void btnCancelUpdate_Click(object sender, System.EventArgs e)
		{
			updateSystemInformation();
		}

		protected void btnCancelAdd_Click(object sender, System.EventArgs e)
		{
			SystemInformation.Visible = false;
			selector.Visible = true;

			// Validation check invisible
			lblManufError.Visible = false;
			lblProductLineError.Visible = false;
			lblModelError.Visible = false;
			lblMaxTotalError.Visible = false;

			//clear select system panel drop down box
			ListItem li;

			lstManufacture.Items.Clear();
			lstProductLine.Items.Clear();
			lstModel.Items.Clear();
				
			li = new ListItem("(Select Manufacturer)","");
			lstManufacture.Items.Add (li);
			lstProductLine.Items.Add (li);
			lstModel.Items.Add (li);

			fillManufacture(lstManufacture);
		}

		protected void btnDelete_Click(object sender, System.EventArgs e)
		{
			String sql;

			MupDbConnection dsConn = new MupDbConnection();

            try
            {
                sql = "Delete from DesktopMemory where Desktop_id = " + txtDesktopID.Text;
                dsConn.ExecSql(sql);
                sql = "Delete from chipOption where Desktop_id = " + txtDesktopID.Text;
                dsConn.ExecSql(sql);
                dsConn.Dispose();
                dsConn = null;
            }
            catch (Exception ex)
            {
                if (dsConn != null)
                    dsConn.Dispose();
                throw ex;
            }

			SystemInformation.Visible = false;
			selector.Visible = true;

			//clear select system panel drop down box
			ListItem li;

			lstManufacture.Items.Clear();
			lstProductLine.Items.Clear();
			lstModel.Items.Clear();
				
			li = new ListItem("(Select Manufacturer)","");
			lstManufacture.Items.Add (li);
			lstProductLine.Items.Add (li);
			lstModel.Items.Add (li);

            //dsConn.Dispose();

			fillManufacture(lstManufacture);
		}

		private void imgHome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("default.aspx");
		}

		protected void btnAddPicture_Click(object sender, System.EventArgs e)
		{
			Response.Write("<script language=\"javascript\">window.open('AddPicture.aspx?type=Desktop&system_id=" + txtDesktopID.Text + "','Picture','toolbar=no,center=yes,location=no,titlebar=no,resizable=no,status=no,scrollbars=no,menubar=no,width=500,height=400');</script>");
		}

		protected void btnDeletePicture_Click(object sender, System.EventArgs e)
		{
			String sql;

			try
			{
				MupDbConnection dsConn = new MupDbConnection();

				sql = "Update DesktopMemory set picture = '' where Desktop_id = " + txtDesktopID.Text;

				if (dsConn.ExecSql(sql) >= 0)
				{
					btnAddPicture.Visible = true;
					btnDeletePicture.Visible = false;
					imgPicture.Visible = false;
				}
				dsConn.Dispose();
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}

		protected void btnShowUpdate_Click(object sender, System.EventArgs e)
		{
			showSystemInfo.Visible = false;
			txtManufacture.Visible = true;
			txtProductLine.Visible = true;
			lstCurrManuf.Visible = false;
			lstCurrLine.Visible = false;
			btnNewManuf.Visible = false;
			btnNewLine.Visible = false;
			lblCurrManuf.Visible = false;
			lblCurrLine.Visible = false;
			btnAddNew.Visible = false;
			btnCancelAdd.Visible = false;
			btnUpdate.Visible = true;
			btnCancelUpdate.Visible = true;
			btnSaveAs.Visible = true;
			btnDelete.Visible = true;
			updateSystemInformation();
		}

		protected void radType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			changeSpeedType();

			for (int i = 0; i < radMinSpeed.Items.Count; i++)
			{
				if (radMinSpeed.Items[i].Value == txtMinSpeed.Text)
					radMinSpeed.SelectedValue = txtMinSpeed.Text;
			}

			for (int i = 0; i < radMaxSpeed.Items.Count; i++)
			{
				if (radMaxSpeed.Items[i].Value == txtMaxSpeed.Text)
					radMaxSpeed.SelectedValue = txtMaxSpeed.Text;
			}
		}

		private void changeSpeedType()
		{
			if (radType.SelectedValue == "DDR2" || radType.SelectedValue == "DDR2 SODIMM")
			{
				radMinSpeed.Items.Clear();
				radMinSpeed.Items.Add("0");
				radMinSpeed.Items.Add("400");
				radMinSpeed.Items.Add("533");
				radMinSpeed.Items.Add("667");
				radMinSpeed.Items.Add("800");				
				radMinSpeed.Items.Add("1066");

				radMaxSpeed.Items.Clear();
				radMaxSpeed.Items.Add("0");
				radMaxSpeed.Items.Add("400");
				radMaxSpeed.Items.Add("533");
				radMaxSpeed.Items.Add("667");
				radMaxSpeed.Items.Add("800");
				radMaxSpeed.Items.Add("1066");
			}
            else if (radType.SelectedValue == "DDR3" || radType.SelectedValue == "DDR3 SODIMM" || radType.SelectedValue == "DDR3 ECC SODIMMs")
            {
                radMinSpeed.Items.Clear();
                radMinSpeed.Items.Add("0");
                radMinSpeed.Items.Add("800");
                radMinSpeed.Items.Add("1066");
                radMinSpeed.Items.Add("1333");
                radMinSpeed.Items.Add("1600");
                radMinSpeed.Items.Add("1866");

                radMaxSpeed.Items.Clear();
                radMaxSpeed.Items.Add("0");
                radMaxSpeed.Items.Add("800");
                radMaxSpeed.Items.Add("1066");
                radMaxSpeed.Items.Add("1333");
                radMaxSpeed.Items.Add("1600");
                radMaxSpeed.Items.Add("1866");
            }
            else if (this.radType.SelectedValue == "DDR4" || this.radType.SelectedValue == "DDR4 SODIMM" || this.radType.SelectedValue == "DDR4 ECC SODIMMs")
            {
                radMinSpeed.Items.Clear();
                radMinSpeed.Items.Add("0");
                radMinSpeed.Items.Add("2133");
                radMinSpeed.Items.Add("2400");
                radMinSpeed.Items.Add("2666");
                radMinSpeed.Items.Add("2933");
                radMinSpeed.Items.Add("3200");
                radMinSpeed.Items.Add("3600");
                radMinSpeed.Items.Add("4000");
                radMinSpeed.Items.Add("4400");
                radMaxSpeed.Items.Clear();
                radMaxSpeed.Items.Add("0");
                radMaxSpeed.Items.Add("2133");
                radMaxSpeed.Items.Add("2400");
                radMaxSpeed.Items.Add("2666");
                radMaxSpeed.Items.Add("2933");
                radMaxSpeed.Items.Add("3200");
                radMaxSpeed.Items.Add("3600");
                radMaxSpeed.Items.Add("4000");
                radMaxSpeed.Items.Add("4400");
            }
            else if (this.radType.SelectedValue == "DDR5" || this.radType.SelectedValue == "DDR5 SODIMM" || this.radType.SelectedValue == "DDR5 ECC SODIMMs" || this.radType.SelectedValue == "DDR5 CAMM")
            {
                radMinSpeed.Items.Clear();
                radMinSpeed.Items.Add("0");
                radMinSpeed.Items.Add("2133");
                radMinSpeed.Items.Add("2400");
                radMinSpeed.Items.Add("2666");
                radMinSpeed.Items.Add("2933");
                radMinSpeed.Items.Add("3200");
                radMinSpeed.Items.Add("3600");
                radMinSpeed.Items.Add("4000");
                radMinSpeed.Items.Add("4400");
                radMinSpeed.Items.Add("4800");
                radMinSpeed.Items.Add("5200");
                radMinSpeed.Items.Add("5600");
                radMinSpeed.Items.Add("6000");
                radMinSpeed.Items.Add("6400");
                radMinSpeed.Items.Add("6800");
                radMinSpeed.Items.Add("7200");
                radMinSpeed.Items.Add("7600");
                radMinSpeed.Items.Add("8000");

                radMaxSpeed.Items.Clear();
                radMaxSpeed.Items.Add("0");
                radMaxSpeed.Items.Add("2133");
                radMaxSpeed.Items.Add("2400");
                radMaxSpeed.Items.Add("2666");
                radMaxSpeed.Items.Add("2933");
                radMaxSpeed.Items.Add("3200");
                radMaxSpeed.Items.Add("3600");
                radMaxSpeed.Items.Add("4000");
                radMaxSpeed.Items.Add("4400");
                radMaxSpeed.Items.Add("4800");
                radMaxSpeed.Items.Add("5200");
                radMaxSpeed.Items.Add("5600");
                radMaxSpeed.Items.Add("6000");
                radMaxSpeed.Items.Add("6400");
                radMaxSpeed.Items.Add("6800");
                radMaxSpeed.Items.Add("7200");
                radMaxSpeed.Items.Add("7600");
                radMaxSpeed.Items.Add("8000");
            }
            else
            {
                radMinSpeed.Items.Clear();
                radMinSpeed.Items.Add("0");
                radMinSpeed.Items.Add("100");
                radMinSpeed.Items.Add("133");
                radMinSpeed.Items.Add("266");
                radMinSpeed.Items.Add("333");
                radMinSpeed.Items.Add("400");
                radMinSpeed.Items.Add("433");
                radMinSpeed.Items.Add("466");
                radMinSpeed.Items.Add("500");
                radMinSpeed.Items.Add("533");
                radMinSpeed.Items.Add("550");
                radMinSpeed.Items.Add("566");
                radMinSpeed.Items.Add("800");
                radMinSpeed.Items.Add("1066");

                radMaxSpeed.Items.Clear();
                radMaxSpeed.Items.Add("0");
                radMaxSpeed.Items.Add("100");
                radMaxSpeed.Items.Add("133");
                radMaxSpeed.Items.Add("266");
                radMaxSpeed.Items.Add("333");
                radMaxSpeed.Items.Add("400");
                radMaxSpeed.Items.Add("433");
                radMaxSpeed.Items.Add("466");
                radMaxSpeed.Items.Add("500");
                radMaxSpeed.Items.Add("533");
                radMaxSpeed.Items.Add("550");
                radMaxSpeed.Items.Add("566");
                radMaxSpeed.Items.Add("800");
                radMaxSpeed.Items.Add("1066");
            }
		}

		protected void btnSaveAs_Click(object sender, System.EventArgs e)
		{
			string strPicture;
			if (txtReturnCancel.Text == "0")
			{
				strPicture = web.MupUtilities.getDesktopPicture(txtDesktopID.Text);
				addNewModel();
				web.MupUtilities.setDesktopPicture(txtDesktopID.Text, strPicture);
				showSystemInformation();
			}
		}

        protected void btnUpdate2_Click(object sender, EventArgs e)
        {
            saveUpdate();
        }

        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }
}
}
