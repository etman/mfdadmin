<%@ Page language="c#" CodeFile="Laptopdata.aspx.cs" Inherits="memoryAdmin.Laptopdata" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Laptopdata</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:DataGrid id=DataGrid1 style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 60px" runat="server" Height="252px" Width="673px" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="3" DataSource="<%# dataSet1 %>" AllowPaging="True" PageSize="20">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
				<ItemStyle ForeColor="#000066"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:ButtonColumn Text="Delete" ButtonType="PushButton" CommandName="Delete"></asp:ButtonColumn>
					<asp:EditCommandColumn ButtonType="PushButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Center" ForeColor="#000066" BackColor="White" PageButtonCount="20" Mode="NumericPages"></PagerStyle>
			</asp:DataGrid>
			<asp:Label id="ErrorMessage" style="Z-INDEX: 102; LEFT: 16px; POSITION: absolute; TOP: 15px" runat="server" Height="40px" Width="760px">Label</asp:Label>
		</form>
	</body>
</HTML>
