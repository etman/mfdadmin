namespace memoryAdmin
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	public delegate void ucModelSelectorEventHandler(object sender, SubmitModelSearchEventArgs e);
	/// <summary>
	///		Summary description for ucModelSelector.
	/// </summary>
	public partial class ucModelSelector : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.Label lblCounter;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (!Page.IsPostBack)
			{
				InitialSelector();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		public event ucModelSelectorEventHandler SubmitModel;

		public void PreLoad(string strTableName)
		{
			txtTableName.Text = strTableName;
			if (txtTableName.Text == "Phone_model")
				lblGuide.Text = "Please select the cellular phone...";
			else if (txtTableName.Text == "PDA_Model")
				lblGuide.Text = "Please select the PDA...";
			else if (txtTableName.Text == "Digital_Camera")
				lblGuide.Text = "Please select the digital camera...";
		}

		public void InitialSelector()
		{
			lstManufacture.Items.Clear();
			lstProductLine.Items.Clear();
			lstModel.Items.Clear();

			ListItem tempListItem = new ListItem("(Select Manufacturer)", "");
			lstManufacture.Items.Add(tempListItem);
			lstProductLine.Items.Add(tempListItem);
			lstModel.Items.Add(tempListItem);

			Mup_utilities.fillManufacture(lstManufacture, txtTableName.Text);
		}

		protected void lstManufacture_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem tempListItem;

			if (lstManufacture.SelectedItem.Text == "(Select Manufacturer)")
			{
				lstProductLine.Items.Clear ();
				lstModel.Items.Clear ();
				tempListItem = new ListItem("(Select Manufacturer)", "");
				lstProductLine.Items.Add(tempListItem);
				lstModel.Items.Add (tempListItem);
			}
			else
			{
				hdnManufacture.Text = lstManufacture.SelectedItem.ToString();
				// Manufacturer was selected, remove the "(Select a Manufacturer") entry
				if (lstManufacture.Items[0].Value == "")
					lstManufacture.Items.RemoveAt(0);

				// Set the product line and model entries to "Select Product Line"
				lstModel.Items.Clear();
				tempListItem = new ListItem ("(Select Product Line)", "");
				lstModel.Items.Add(tempListItem);

				// Now populate the product line
				Mup_utilities.fillProductLine (lstManufacture.SelectedItem.Value, lstProductLine, txtTableName.Text);
			}
		}

		protected void lstProductLine_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem tempListItem;

			if (lstProductLine.SelectedItem.Text == "(Select Product Line)")
			{
				lstModel.Items.Clear ();
				tempListItem = new ListItem ("(Select Product Line)", "");
				lstModel.Items.Add(tempListItem);
			}
			else
			{
				hdnProductLine.Text = lstProductLine.SelectedItem.ToString();
				// Manufacture was selected, remove the "(Select a State") entry
				if (lstProductLine.Items[0].Value == "")
					lstProductLine.Items.RemoveAt(0);

				// Set the school name and class level entries to "Select a School System"
				tempListItem = new ListItem ("(Select Model)", "");

				// Now populate the school systems
				Mup_utilities.fillModel (lstManufacture.SelectedItem.Value, lstProductLine.SelectedItem.Value, lstModel, txtTableName.Text);
			}
		}

		protected void lstModel_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem tempListItem;

			if (lstModel.SelectedItem.Text == "(Select Model)")
			{
				tempListItem = new ListItem ("(Select Model)", "");
			}
			else
			{
				hdnModel.Text = lstModel.SelectedItem.ToString();
				SubmitModelSearchEventArgs args = new SubmitModelSearchEventArgs();
				args.Manufacturer = hdnManufacture.Text;
				args.ProductLine = hdnProductLine.Text;
				args.Model = hdnModel.Text;
				OnSubmitModelSearch(args);
			}
		}

		protected void btnAddModel_Click(object sender, System.EventArgs e)
		{
			SubmitModelSearchEventArgs args = new SubmitModelSearchEventArgs();
			
			hdnManufacture.Text = "";
			hdnProductLine.Text = "";
			hdnModel.Text = "";

			OnSubmitModelSearch(args);
		}

		protected virtual void OnSubmitModelSearch(SubmitModelSearchEventArgs args)
		{
			if (SubmitModel != null)
			{
				SubmitModel(this, args);
			}
		}

		public SubmitModelSearchEventArgs GetUpdateModel()
		{
			SubmitModelSearchEventArgs modelUpdate = new SubmitModelSearchEventArgs();
			modelUpdate.Manufacturer = hdnManufacture.Text;
			modelUpdate.ProductLine = hdnProductLine.Text;
			modelUpdate.Model = hdnModel.Text;

			return modelUpdate;
		}

		public void SetNewModel(SubmitModelSearchEventArgs newModel, string strTableName)
		{
			hdnManufacture.Text = newModel.Manufacturer;
			hdnProductLine.Text = newModel.ProductLine;
			hdnModel.Text = newModel.Model;

			Mup_utilities.fillManufacture(lstManufacture, strTableName);
			lstManufacture.SelectedValue = newModel.Manufacturer;
			Mup_utilities.fillProductLine(lstManufacture.SelectedValue, lstProductLine, strTableName);
			lstProductLine.SelectedValue = newModel.ProductLine;
			Mup_utilities.fillModel(lstManufacture.SelectedValue, lstProductLine.SelectedValue, lstModel, strTableName);
			lstModel.SelectedValue = newModel.Model;
		}
	}
}
