using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for CustomerSupportAdmin.
	/// </summary>
	public partial class CustomerSupportAdmin : System.Web.UI.Page
	{
		private DataSet dsCase;

		protected System.Web.UI.WebControls.Button btnUpdate;
		protected System.Web.UI.WebControls.Button btnReset;
		protected System.Web.UI.WebControls.TextBox TextBox1;
		protected System.Web.UI.WebControls.TextBox TextBox2;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

            //if (Session["LoginName"] == null)
            //{
            //    Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
            //}

			if (!Page.IsPostBack)
			{
				String sql;
				MupDbConnection dsConn = new MupDbConnection();

				sql = "Select * from CaseRequest LEFT OUTER JOIN RMA ON CaseRequest.Case_number = RMA.Case_number WHERE (CaseRequest.Status < 'h') Order by CaseRequest.Case_number";
				dsCase = dsConn.CreateDataSet();
				dsConn.FillDataSet(sql, "caseList", ref dsCase);

				supportStatus.DataSource = dsCase;
				supportStatus.DataBind();

				sql = "Select * from CaseRequest LEFT OUTER JOIN RMA ON CaseRequest.Case_number = RMA.Case_number WHERE (Last_visit > update_date) and (CaseRequest.Status < 'h') Order by Last_visit";
				dsCase = dsConn.CreateDataSet();
				dsConn.FillDataSet(sql, "caseList", ref dsCase);

				newList.DataSource = dsCase;
				newList.DataBind();

				dsConn.Dispose();
				dsConn = null;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.imgHome.Click += new System.Web.UI.ImageClickEventHandler(this.imgHome_Click);

		}
		#endregion

		private void imgHome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("default.aspx");
		}

		protected void btnShowComplete_Click(object sender, System.EventArgs e)
		{
			completeSupport.Visible = true;
			btnShowRuningProblem.Visible = true;
			runingSupport.Visible = false;
			btnShowComplete.Visible = false;

			String sql;
			MupDbConnection dsConn = new MupDbConnection();

			sql = "Select * from CaseRequest LEFT OUTER JOIN RMA ON CaseRequest.Case_number = RMA.Case_number WHERE ((CaseRequest.Status = 'h') or (CaseRequest.Status = 'h: complete')) and (DATEDIFF(d, Complete_date, { fn NOW() }) < 30) Order by CaseRequest.Case_number";
			dsCase = dsConn.CreateDataSet();
			dsConn.FillDataSet(sql, "caseList", ref dsCase);

			completeList.DataSource = dsCase;
			completeList.DataBind();

			dsConn.Dispose();
			dsConn = null;
		}

		protected void btnShowRuningProblem_Click(object sender, System.EventArgs e)
		{
			runingSupport.Visible = true;
			btnShowComplete.Visible = true;
			completeSupport.Visible = false;
			btnShowRuningProblem.Visible = false;

			String sql;
			MupDbConnection dsConn = new MupDbConnection();

			sql = "Select * from CaseRequest LEFT OUTER JOIN RMA ON CaseRequest.Case_number = RMA.Case_number WHERE (CaseRequest.Status < 'h') Order by CaseRequest.Case_number";
			dsCase = dsConn.CreateDataSet();
			dsConn.FillDataSet(sql, "caseList", ref dsCase);

			supportStatus.DataSource = dsCase;
			supportStatus.DataBind();

			dsConn.Dispose();
			dsConn = null;
		}

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			runingSupport.Visible = true;
			btnShowComplete.Visible = true;
			completeSupport.Visible = false;
			btnShowRuningProblem.Visible = true;

			String sql;
			MupDbConnection dsConn = new MupDbConnection();

			sql = "Select * from CaseRequest LEFT OUTER JOIN RMA ON CaseRequest.Case_number = RMA.Case_number WHERE ";
			if (txtCaseNo.Text.Trim() != "")
				sql += "CaseRequest.case_number = " + txtCaseNo.Text.Trim() + " and ";
			if (txtRmaNo.Text.Trim() != "")
				sql += "RMA.RMA_number = " + txtRmaNo.Text.Trim() + " and ";
			if (txtOrderNo.Text.Trim() != "")
				sql += "CaseRequest.order_number = '" + txtOrderNo.Text.Trim() + "' and ";
			if (txtLastName.Text.Trim() != "")
				sql += "CaseRequest.Last_name like '%" + txtLastName.Text.Trim() + "%' and ";
			if (txtFirstName.Text.Trim() != "")
				sql += "CaseRequest.First_name like '%" + txtFirstName.Text.Trim() + "%' and ";
			if (txtCaseNo.Text.Trim() == "" && txtRmaNo.Text.Trim() == "" && txtOrderNo.Text.Trim() == "" && txtLastName.Text.Trim() == "" && txtFirstName.Text.Trim() == "")
				sql += "DATEDIFF(d, Receive_date, { fn NOW() }) < 90 and ";
			sql += "CaseRequest.status >= 'a' Order by CaseRequest.Case_number";
			dsCase = dsConn.CreateDataSet();
			dsConn.FillDataSet(sql, "caseList", ref dsCase);

			supportStatus.DataSource = dsCase;
			supportStatus.DataBind();

			dsConn.Dispose();
			dsConn = null;
		}
	}
}
