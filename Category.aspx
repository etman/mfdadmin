<%@ Page language="c#" Inherits="memoryAdmin.Category" CodeFile="Category.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Category</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="780" border="0" cellspacing="0" cellpadding="3">
				<TR>
					<TD class="title"><STRONG><asp:imagebutton id="imgHome" runat="server" ImageUrl="http://www.memory-up.com/merchant2/images/logo_big.gif"></asp:imagebutton>
							Laptop Accessary Category </STRONG>
					</TD>
				</TR>
				<tr>
					<td>&nbsp;
						<asp:DataGrid id="DataGrid1" runat="server">
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
				<tr>
					<td>&nbsp;
						<asp:LinkButton id="LinkButton1" runat="server" Width="123px" BorderStyle="Outset" BackColor="#E0E0E0"
							Height="29px">Add New</asp:LinkButton>
					</td>
				</tr>
				<tr>
					<td>&nbsp;
						<asp:Panel id="Panel1" runat="server"></asp:Panel>
					</td>
				</tr>
				<tr>
					<td>&nbsp;
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
