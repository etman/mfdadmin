using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Drawing;

public partial class MemoryScannerHistory : System.Web.UI.Page
{
    private string strSqlConnString = ConfigurationSettings.AppSettings["cnstr"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtDateFrom.Text = DateTime.Today.AddDays(-3).ToShortDateString();
            txtDateTo.Text = DateTime.Today.ToShortDateString();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ShowTopModel();
        ShowScannerHistory();
    }
    private void ShowScannerHistory()
    {
        SqlConnection conMemoryUp = new SqlConnection(strSqlConnString);
        string strSql = "";

        strSql = "SELECT *, 'https://blackdiamondmemory.com/AnalysisComplete?nosave=True&' + querystring AS HyperLink ";
        strSql += "FROM ProfilerUsage ";
        strSql += "WHERE TimeStamp >= @DateFrom AND DATEADD(day, -1, TimeStamp) <= @DateTo ";
        if (chkEmailOnly.Checked == true)
            strSql += "AND UserEmail IS NOT NULL AND UserEmail <> '' ";
        strSql += "ORDER BY TimeStamp ";

        try
        {
            SqlCommand comMemoryUp = new SqlCommand(strSql, conMemoryUp);
            comMemoryUp.Parameters.AddWithValue("@DateFrom", txtDateFrom.Text);
            comMemoryUp.Parameters.AddWithValue("@DateTo", txtDateTo.Text);
            conMemoryUp.Open();
            SqlDataReader readMemoryUp = comMemoryUp.ExecuteReader();

            grdScannerHistory.DataSource = readMemoryUp;
            grdScannerHistory.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            conMemoryUp.Close();
        }
    }
    private void ShowTopModel()
    {
        SqlConnection conMemoryUp = new SqlConnection(strSqlConnString);
        string strSql = "";

        strSql = "SELECT TOP 20 COUNT(Motherboard) AS ModelCount, Manufacturer, Model, Motherboard, Chipset FROM ProfilerUsage ";
        strSql += "WHERE TimeStamp >= @DateFrom AND DATEADD(day, -1, TimeStamp) <= @DateTo ";
        strSql += "GROUP BY Motherboard, Model, Manufacturer, Chipset ";
        strSql += "ORDER BY ModelCount DESC ";

        try
        {
            SqlCommand comMemoryUp = new SqlCommand(strSql, conMemoryUp);
            comMemoryUp.Parameters.AddWithValue("@DateFrom", txtDateFrom.Text);
            comMemoryUp.Parameters.AddWithValue("@DateTo", txtDateTo.Text);
            conMemoryUp.Open();
            SqlDataReader readMemoryUp = comMemoryUp.ExecuteReader();

            grdTopModel.DataSource = readMemoryUp;
            grdTopModel.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            conMemoryUp.Close();
        }
    }
    protected void txtDateFrom_TextChanged(object sender, EventArgs e)
    {
        if (txtDateFrom.Text.Trim() != "" && txtDateTo.Text.Trim() == "")
        {
            txtDateTo.Text = txtDateFrom.Text.Trim();
        }
        
    }
    protected void txtDateTo_TextChanged(object sender, EventArgs e)
    {
        if (txtDateTo.Text.Trim() != "" && txtDateFrom.Text.Trim() == "")
        {
            txtDateFrom.Text = txtDateTo.Text.Trim();
        }
    }
    protected void grdScannerHistory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string strScanRedult = "";

        if (e.Row.RowIndex >= 0)
        {
            strScanRedult = ((Label)e.Row.FindControl("lblScanResult")).Text.Replace("\r\r", "<br />");
            ((Label)e.Row.FindControl("lblScanResult")).Text = strScanRedult;
            if (strScanRedult.Trim() == "")
                e.Row.BackColor = Color.LightPink;

            ((Label)e.Row.FindControl("lblChipSet")).Text = ((Label)e.Row.FindControl("lblChipSet")).Text.Replace("&lt;br /&gt;", "<br />");
        }
    }
}
