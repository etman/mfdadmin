using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for commentsAdmin.
	/// </summary>
	public partial class commentsAdmin : System.Web.UI.Page
	{
		private DataSet dsCase;

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

            //if (Session["LoginName"] == null)
            //{
            //    Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
            //}

			string sql;
			MupDbConnection dsConn = new MupDbConnection();

			if (!IsPostBack)
			{
				sql = "Select * from comments where deleted is null order by comment_id";
				dsCase = dsConn.CreateDataSet();
				dsConn.FillDataSet(sql, "caseList", ref dsCase);

			
				comments.DataSource = dsCase;
				comments.DataBind();
			}

			dsConn.Dispose();
			dsConn = null;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.imgHome.Click += new System.Web.UI.ImageClickEventHandler(this.imgHome_Click);
			this.comments.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.comments_DeleteCommand);

		}
		#endregion

		private void comments_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			string sql;
			try
			{
				MupDbConnection dsConnDel = new MupDbConnection();
				MupDbConnection dsConn = new MupDbConnection();

				sql = "update comments set deleted = 1 where comment_id = " + e.Item.Cells[0].Text;
				dsConnDel.ExecSql(sql);
				dsConnDel.Dispose();
				dsConnDel = null;

				sql = "Select * from comments where deleted is null order by comment_id";
				dsCase = dsConn.CreateDataSet();
				dsConn.FillDataSet(sql, "caseList", ref dsCase);

				comments.DataSource = dsCase;
				comments.DataBind();

				dsConn.Dispose();
				dsConn = null;
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}

		private void imgHome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("default.aspx");
		}

	}
}
