using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;

namespace web
{
	/// <summary>
	/// Summary description for LoginForm.
	/// </summary>
	public partial class LoginForm : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void LoginButtonClick(object sender, System.EventArgs e)
		{
            string strFullName = memoryAdmin.Mup_utilities.LoginCheck(UserName.Text.Trim(), Password.Text.Trim());
            if (strFullName != "")
			{
                string strReturnUrl;

                if (Session["LoginName"] != null)
                    Session["LoginName"] = strFullName;
                else
                    Session.Add("LoginName", strFullName);

                Message.Text = "";


                bool persist = chkRemenberPass.Checked;
                FormsAuthentication.SetAuthCookie(UserName.Text.Trim().ToUpper(), persist);
                FormsAuthenticationTicket ticket1 = new FormsAuthenticationTicket(
                   UserName.Text.Trim(),
                   persist,
                   525600);

                Response.Cookies.Clear();
                HttpCookie cookie1 = new HttpCookie(
                  FormsAuthentication.FormsCookieName,
                  FormsAuthentication.Encrypt(ticket1));
                Response.Cookies.Add(cookie1);

                FormsAuthentication.RedirectFromLoginPage(UserName.Text.ToUpper(), true);

                if (Request.Params["ref"] == null)
                    strReturnUrl = "default.aspx";
                else
                    strReturnUrl = Request.Params["ref"];
                this.Response.Redirect(strReturnUrl);
			}
			else
			{
                Session.Remove("LoginName");

				Message.Text = "Invalide username or password";
			}
		}
        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            pnlLogin.Visible = false;
            pnlChangePass.Visible = true;
        }
        protected void btnPassChangeConfirm_Click(object sender, EventArgs e)
        {
            string strFullName = memoryAdmin.Mup_utilities.LoginCheck(txtPassChangeUserName.Text.Trim(), 
                txtPassChangeOldPass.Text.Trim());
            if (strFullName != "")
            {
                
                lblPassChangeError.Text = "";
                if (memoryAdmin.Mup_utilities.ChangePassword(txtPassChangeUserName.Text.Trim(), txtPassChangeNewPass.Text.Trim()))
                {
                    Response.Cookies.Clear();
                    pnlChangePass.Visible = false;
                    pnlLogin.Visible = true;
                }
                else
                {

                }
            }
            else
            {
                lblPassChangeError.Text = "Login Failure!";
            }
        }
        protected void btnPassChangeCancel_Click(object sender, EventArgs e)
        {
            pnlLogin.Visible = true;
            pnlChangePass.Visible = false;
        }
}
}
