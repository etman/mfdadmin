using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for AffiliateList.
	/// </summary>
	public partial class AffiliateList : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.ImageButton imgHome;
		private string strSqlConnString = ConfigurationSettings.AppSettings["cnstr"];
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            //if (Session["LoginName"] == null)
            //{
            //    Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
            //}

			if (!Page.IsPostBack)
				showUserList("", "");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.grdUserList.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdUserList_EditCommand);

		}
		#endregion

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			if (txtUserName.Text.Trim() != "")
				showUserList("User_name", txtUserName.Text.Trim());
			else if (txtFirstName.Text.Trim() != "")
				showUserList("First_name", txtFirstName.Text.Trim());
			else if (txtLastName.Text.Trim() != "")
				showUserList("Last_name", txtLastName.Text.Trim());
			else if (txtEmail.Text.Trim() != "")
				showUserList("Email", txtEmail.Text.Trim());
			else if (txtOrderNo.Text.Trim() != "")
				showUserList("Order_no", txtOrderNo.Text.Trim());
			else
				showUserList("", "");
		}

		private void showUserList(string searchType, string searchPara)
		{
			try
			{
				SqlDataAdapter affAdapter = new SqlDataAdapter();
				SqlConnection affConn = new SqlConnection(strSqlConnString);
				SqlCommand affComm = new SqlCommand();
				dsAffiliateList affDataSet = new dsAffiliateList();

				affAdapter.SelectCommand = affComm;
				affAdapter.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																								new System.Data.Common.DataTableMapping("Table", "Affilate_order", new System.Data.Common.DataColumnMapping[] {
																																																				  new System.Data.Common.DataColumnMapping("User_name", "User_name"),
																																																				  new System.Data.Common.DataColumnMapping("First_name", "First_name"),
																																																				  new System.Data.Common.DataColumnMapping("Last_name", "Last_name"),
																																																				  new System.Data.Common.DataColumnMapping("Email", "Email"),
																																																				  new System.Data.Common.DataColumnMapping("A_Order_date", "A_Order_date"),
																																																				  new System.Data.Common.DataColumnMapping("Rebate_per", "Rebate_per"),
																																																				  new System.Data.Common.DataColumnMapping("Rebate_amount", "Rebate_amount"),
																																																				  new System.Data.Common.DataColumnMapping("Submit_date", "Submit_date"),
																																																				  new System.Data.Common.DataColumnMapping("Update_date", "Update_date"),
																																																				  new System.Data.Common.DataColumnMapping("Description", "Description"),
																																																				  new System.Data.Common.DataColumnMapping("Order_no", "Order_no")})});

				
				affComm.Connection = affConn;

				if (searchType == "Order_no")
				{
					affComm.CommandText = "Select User_name, First_name, Last_name, Email, Convert(varchar(10), Order_date, 101) As A_Order_date, Order_no, Rebate_per, Rebate_amount, Affiliate_order.Update_date, Description, Submit_date ";
					affComm.CommandText += "From Affiliate_order ";
					affComm.CommandText += "INNER JOIN Affiliate_status on pk_id = status ";
					affComm.CommandText += "LEFT OUTER JOIN Affiliate ON Affiliate_order.fk_User_name = Affiliate.User_name ";
					affComm.CommandText += "Where order_no = '" + searchPara + "' ";
				}
				else if (searchType != "" && searchPara != "")
				{
					affComm.CommandText = "Select User_name, First_name, Last_name, Email, Convert(varchar(10), Order_date, 101) As A_Order_date, Order_no, Rebate_per, Rebate_amount, Affiliate_order.Update_date, Description, Submit_date ";
					affComm.CommandText += "From Affiliate ";
					affComm.CommandText += "LEFT OUTER JOIN Affiliate_order ON Affiliate_order.fk_User_name = Affiliate.User_name ";
					affComm.CommandText += "LEFT OUTER JOIN Affiliate_status on pk_id = status ";
					affComm.CommandText += "Where " + searchType + " like '%" + searchPara + "%' ";
				}
				else
				{
					affComm.CommandText = "Select User_name, First_name, Last_name, Email, Convert(varchar(10), Order_date, 101) As A_Order_date, Order_no, Rebate_per, Rebate_amount, Affiliate_order.Update_date, Description, Submit_date ";
					affComm.CommandText += "From Affiliate ";	
					affComm.CommandText += "LEFT OUTER JOIN Affiliate_order ON Affiliate_order.fk_User_name = Affiliate.User_name ";
					affComm.CommandText += "LEFT OUTER JOIN Affiliate_status on pk_id = status ";
					affComm.CommandText += "Where submit_date >= '" + DateTime.Today.AddMonths(-1) + "' ";
					affComm.CommandText += "Or (order_date >= '" + DateTime.Today.AddMonths(-2) + "' ";
					affComm.CommandText += "And status <= 30) ";
					affComm.CommandText += "Order by Order_no ";
				}

				affComm.Connection = affConn;
				affAdapter.Fill(affDataSet, "Affiliate");
				grdUserList.DataSource = affDataSet.Affiliate.DefaultView;
				grdUserList.DataBind();
			}
			catch (MupException ex)
			{
				Response.Write (ex.MupMessage);
			}
		}

		private void grdUserList_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			string strUserName = e.Item.Cells[1].Text;

			Response.Redirect("AffiliateEdit.aspx?user_name=" + strUserName);
		}

		private void checkNewOrders()
		{

		}
	}
}
