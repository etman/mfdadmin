using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using web;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for newModel.
	/// </summary>
	public partial class newModel : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (!Page.IsPostBack)
			{
				txtMemoryType.Text = Request.Params["type"];

				btnSubmit.Attributes.Add("onclick", "submitModel();");
				btnCancel.Attributes.Add("onclick", "window.close();");

				ListItem li;

				// The other's shouldn't contain data yet
				li = new ListItem("(Select Manufacturer)","");
				drpManufacturer.Items.Add (li);
				drpProdLine.Items.Add (li);

                if (txtMemoryType.Text == "laptop")
                    MupUtilities.fillLaptopManufacture(drpManufacturer);
                else if (txtMemoryType.Text == "desktop")
                    MupUtilities.fillDesktopManufacture(drpManufacturer);
                else if (txtMemoryType.Text == "camera")
                    Mup_utilities.fillManufacture(drpManufacturer, "");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnNewManufacturer_Click(object sender, System.EventArgs e)
		{
			drpManufacturer.Visible = false;
			drpProdLine.Visible = false;
			pnlNewManuf.Visible = false;
			pnlNewProdLine.Visible = false;

			txtManufacturer.Visible = true;
			txtProdLine.Visible = true;
		}

		protected void btnNewProdLine_Click(object sender, System.EventArgs e)
		{
			drpProdLine.Visible = false;
			pnlNewProdLine.Visible = false;

			txtProdLine.Visible = true;
		}

		protected void drpManufacturer_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ListItem li;

			if (drpManufacturer.SelectedValue == "(Select Manufacturer)")
			{
				drpProdLine.Items.Clear ();
				li = new ListItem ("(Select Manufacturer)", "");
				drpProdLine.Items.Add(li);
			}
			else
			{
				// Manufacture was selected, remove the "(Select a State") entry
				if (drpManufacturer.Items[0].Value == "")
					drpManufacturer.Items.RemoveAt(0);

				// Set the school name and class level entries to "Select a School System"
				li = new ListItem ("(Select Product Line)", "");

				if (txtMemoryType.Text == "laptop")
					MupUtilities.fillLaptopProductLine(drpManufacturer.SelectedValue, drpProdLine);
				else if (txtMemoryType.Text == "desktop")
					MupUtilities.fillDesktopProductLine(drpManufacturer.SelectedValue, drpProdLine);

			}
		}

		protected void btnSubmit_Click(object sender, System.EventArgs e)
		{
			Response.Write("<script>window.close();</script>");
		}
	}
}
