using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;

namespace memoryAdmin
{
	/// <summary>
	/// Summary description for EMailRMA.
	/// </summary>
	public partial class EMailRMA : System.Web.UI.Page
	{
		string caseNumber;

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

            //if (Session["LoginName"] == null)
            //{
            //    Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
            //}

			System.Data.SqlClient.SqlDataReader drCase;
			if (!Page.IsPostBack)
			{
				caseNumber = Request.Params["case_number"];
				tblAfterEmail.Visible = false;
				//string status;

				string sql;
				MupDbConnection dsConn = new MupDbConnection();

				sql = "Select * from CaseRequest where Case_number = "+ caseNumber;
				drCase = dsConn.GetReader(sql);

				if (drCase == null || !drCase.Read ())
				{
					dsConn.Dispose ();
					MupGenericException ex = new MupGenericException ("Case Display Error",
						"Register_Click ()", "MupRegistration.ascx.cs");
					throw ex;
				}

				txtTo.Text = drCase["Email"].ToString();
				txtBCC.Text = "technical@memory-up.com";

				dsConn.Dispose();
				dsConn = null;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnSend_Click(object sender, System.EventArgs e)
		{
			// insert e-mail notification here
			try
			{
				MailMessage mailRMA = new MailMessage();
				mailRMA.To = txtTo.Text;
				mailRMA.Bcc = txtBCC.Text;
				mailRMA.Subject = txtSubject.Text;
				mailRMA.Body = txtBody.Text;
				mailRMA.From = "technical@memory-up.com";
				SmtpMail.SmtpServer = "weibow.com";
				SmtpMail.Send(mailRMA);
			}
			catch
			{
			}

			tblEmail.Visible = false;
			tblAfterEmail.Visible = true;
		}
	}
}
