<%@ Register TagPrefix="uc1" TagName="ucAdminHeader" Src="ucAdminHeader.ascx" %>
<%@ Page language="c#" Inherits="memoryAdmin.MemoryPrice" CodeFile="MemoryPrice.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>MemoryPrice</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
		<script language="javascript" type="text/javascript">
		function checkAllPartCode()
		{
			var intGrdCount;
			
			intGrdCount = parseInt(document.all("txtGrdCount").value);
			
			if (document.all("chkSelectAll").checked == true)
			{
				for (var i = 2; i < intGrdCount + 2; i++)
				{
					document.all("grdMemoryPrice__ctl" + i + "_chkUpdate").checked = true;
				}
			}
			else
			{
				for (var i = 2; i < intGrdCount + 2; i++)
				{
					document.all("grdMemoryPrice__ctl" + i + "_chkUpdate").checked = false;
				}
			}
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="600" border="0">
				<TR>
					<TD colSpan="2"><uc1:ucadminheader id="ucHeader" runat="server"></uc1:ucadminheader></TD>
				</TR>
				<TR>
					<TD width="100"></TD>
					<TD>
						<TABLE id="Table2" cellSpacing="0" cellPadding="3" width="100%" border="0">
							<TR class="item">
								<TD>Min.&nbsp;Memory Size:</TD>
								<TD><asp:textbox id="txtLowSize" runat="server" Columns="6"></asp:textbox></TD>
								<TD>Max.&nbsp;Memory Size:</TD>
								<TD><asp:textbox id="txtTopSize" runat="server" Columns="6"></asp:textbox></TD>
							</TR>
							<TR class="item">
								<TD>Lowest Speed:</TD>
								<TD><asp:textbox id="txtLowSpeed" runat="server" Columns="6"></asp:textbox></TD>
								<TD>Highest Speed:</TD>
								<TD><asp:textbox id="txtTopSpeed" runat="server" Columns="6"></asp:textbox></TD>
							</TR>
							<TR class="item">
								<TD>&nbsp;</TD>
								<TD>&nbsp;</TD>
								<TD>&nbsp;</TD>
								<TD>&nbsp;</TD>
							</TR>
							<TR class="item">
								<TD>Flash Min. Size:</TD>
								<TD>
									<asp:TextBox id="txtFlashMin" runat="server" Columns="6"></asp:TextBox></TD>
								<TD>Flash Max. Size:</TD>
								<TD>
									<asp:TextBox id="txtFlashMax" runat="server" Columns="6"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="item" colSpan="4"><asp:checkbox id="chkSelectAll" Runat="server" Text="Check All for Update"></asp:checkbox>
									<div id="divCount" style="DISPLAY: none"><asp:textbox id="txtGrdCount" runat="server"></asp:textbox></div>
									<asp:datagrid id="grdMemoryPrice" runat="server" Width="100%" CellPadding="3" BackColor="White"
										BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC" AutoGenerateColumns="False" CssClass="item">
										<HeaderStyle Font-Bold="True" BackColor="LightGray"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="Checked Update" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
												<ItemTemplate>
													<asp:CheckBox ID="chkUpdate" Runat="server"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Part Code" ItemStyle-Wrap="False">
												<ItemTemplate>
													<asp:Label ID="lblPartCode" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Part_code") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Lowest Price">
												<ItemTemplate>
													<asp:TextBox ID="txtLowPrice" Runat="server" Columns="6" Text='<%# DataBinder.Eval(Container.DataItem, "Lowest_price") %>'>
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Comments">
												<ItemTemplate>
													<asp:TextBox ID="txtComments" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Comments") %>'>
													</asp:TextBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
												<ItemTemplate>
													<asp:CheckBox ID="chkDelete" Runat="server"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD><asp:button id="btnSave" runat="server" Text="Save" onclick="btnSave_Click"></asp:button></TD>
								<TD><asp:button id="btnLatest" runat="server" Text="Get Latest" onclick="btnLatest_Click"></asp:button></TD>
								<TD></TD>
								<TD><asp:button id="btnDelete" runat="server" Text="Delete Selected" onclick="btnDelete_Click"></asp:button></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD width="100"></TD>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
