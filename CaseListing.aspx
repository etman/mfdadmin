<%@ Page language="c#" Inherits="memoryAdmin.CaseListing" CodeFile="CaseListing.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CaseListing</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE WIDTH="780" BORDER="0" CELLSPACING="0" CELLPADDING="1">
				<TR>
					<TD></TD>
				</TR>
				<TR>
					<TD>
						<asp:DataGrid id="DataGrid1" runat="server" AutoGenerateColumns="False" BorderColor="#CCCCCC"
							BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="3" AllowSorting="True">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle ForeColor="#000066"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:HyperLinkColumn DataNavigateUrlField="case_number" DataNavigateUrlFormatString="TechnicalAdmin.aspx"
									DataTextField="case_number" HeaderText="Case Number"></asp:HyperLinkColumn>
								<asp:BoundColumn DataField="Invoice_number" HeaderText="Invoice Number"></asp:BoundColumn>
								<asp:BoundColumn DataField="Order_number" HeaderText="Order Number"></asp:BoundColumn>
								<asp:BoundColumn DataField="Last_name" HeaderText="Last_Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="first_name" HeaderText="First_Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="status" HeaderText="Status"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:DataGrid></TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
