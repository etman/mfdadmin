namespace memoryAdmin
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for ucAdminHeader.
	/// </summary>
	public partial class ucAdminHeader : System.Web.UI.UserControl
	{

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		public void PreLoad()
		{
            //if (Session["LoginName"] == null || Request.Params["user_name"] == "")
            //{
            //    Response.Redirect("LoginForm.aspx?ref=" + Request.RawUrl);
            //}
		}

		public void SetPageTitle(string strHeader)
		{
			lblHeader.Text = strHeader;
		}

		private void ImageButton1_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("");
		}
	}
}
