<%@ Page language="c#" Inherits="memoryAdmin.DeskTopInfo"  CodeFile="DesktopInfo.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DeskTopInfo</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/memoryUp.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="3" width="780" border="0">
				<TR>
					<TD class="title"><a href="default.aspx"><IMG alt="Memory-Up.com" src="/images/memoryselector/bd_fb.png"
								border="0"></a>Desktop Hit Count</TD>
				</TR>
				<TR>
					<TD><asp:datagrid id="grdMemoryList" runat="server" AutoGenerateColumns="False" CellPadding="3" BackColor="White"
							BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC" CssClass="item" AllowSorting="True">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle ForeColor="#000066"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:BoundColumn DataField="Manufacture" SortExpression="Manufacture" HeaderText="Manufacture"></asp:BoundColumn>
								<asp:BoundColumn DataField="Product_line" SortExpression="Product_line" HeaderText="Product Line"></asp:BoundColumn>
								<asp:BoundColumn DataField="Model" SortExpression="Model" HeaderText="Model"></asp:BoundColumn>
								<asp:BoundColumn DataField="speed_h" SortExpression="Speed_h" HeaderText="Speed"></asp:BoundColumn>
								<asp:BoundColumn DataField="Chipset" SortExpression="Chipset" HeaderText="Chipset"></asp:BoundColumn>
								<asp:BoundColumn DataField="date_visit" SortExpression="date_visit" HeaderText="Last Visit"></asp:BoundColumn>
								<asp:BoundColumn DataField="Counter" SortExpression="Counter" HeaderText="Counter"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
